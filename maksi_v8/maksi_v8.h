/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_v8.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:43 2011
 *
 * @brief   V8 engine
 *
 */

#ifndef MAKSI_V8_H
#define MAKSI_V8_H

#include <QObject>
#include <QUrl>

#include <v8.h>

#include <maksi/engine.h>
#include <maksi/script.h>


class Maksi_V8_EnginePlugin: public QObject, maksi::ScriptEnginePlugin
{
    Q_OBJECT
    Q_INTERFACES(maksi::ScriptEnginePlugin);
public:
    ~Maksi_V8_EnginePlugin();

    virtual QString description() const;
    virtual maksi::ScriptEngine *newEngine(maksi::Maksi *maksi, const QString &name) const;
};


namespace maksi
{

class V8_Engine : public ScriptEngine
{
    Q_OBJECT
    Q_DISABLE_COPY(V8_Engine);

public:
    V8_Engine(Maksi *maksi, const QString &name);
    ~V8_Engine();

    virtual Script *newScript(const QString &scriptName);

protected:
    virtual bool evalCode(const QString &code, const QUrl &url, int line);

private:
    v8::Isolate *m_iso;
    v8::Persistent<v8::Context> m_ctx;

    friend class V8_Script;
    friend class V8_Context;
};


class V8_Script : public Script
{
    Q_OBJECT
    Q_DISABLE_COPY(V8_Script);
public:

    struct Arg {
        Arg(const QString &name, const QVariant &val) : m_name(name), m_val(val) {}

        QString m_name;
        QVariant m_val;
    };

    struct Code  {
        Code(v8::Persistent<v8::Script> script) : m_script(script) {}

        v8::Persistent<v8::Script> m_script;
    };

    V8_Script(Maksi *maksi, const QString &name, V8_Engine *engine);
    ~V8_Script();

    virtual void addArg(const QString &name, const QVariant &val, const Element *conf);
    virtual void addCode(const QString &code, const Element *conf);

    virtual Context *newContext();

private:
    V8_Engine *m_engine;
    QList<Arg> m_args;
    QList<Code*> m_code;

    friend class V8_Context;
};


class V8_Context : public Context
{
    Q_OBJECT
    Q_DISABLE_COPY(V8_Context);
public:

    V8_Context(V8_Script *script);
    ~V8_Context();

    virtual QVariant arg(const QString &name);
    virtual void setArg(const QString &name, const QVariant &val);
    virtual QVariant eval();

private:
    V8_Script *m_script;
    v8::Persistent<v8::Object> m_global;
};

};

#endif

/* End of file */
