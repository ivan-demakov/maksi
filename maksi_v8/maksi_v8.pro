include(../maksi.pri)

TEMPLATE = lib
CONFIG += plugin

QT += xmlpatterns
QT -= gui

HEADERS = maksi_v8.h
SOURCES = maksi_v8.cpp

unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksiV8)
  DESTDIR = ../maksi
  LIBS += -lv8
  CONFIG += debug
}
win32 {
  TARGET = maksiV8
  DESTDIR = ../win32
  QMAKE_LIBDIR += ../win32
  LIBS += -lmaksi
  CONFIG += release
}

INCLUDEPATH += ..
