/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_v8.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:50 2011
 *
 * @brief   XQuery engine
 *
 */

#include "maksi_v8.h"

#include <maksi/maksi.h>
#include <maksi/xmlelem.h>
#include <maksi/loader.h>

#include <QtPlugin>
#include <QTextStream>


Q_EXPORT_PLUGIN2(maksiXQ, Maksi_V8_EnginePlugin);

Maksi_V8_EnginePlugin::~Maksi_V8_EnginePlugin()
{
}

QString Maksi_V8_EnginePlugin::description() const
{
    return "JavaScript";
}

maksi::ScriptEngine *Maksi_V8_EnginePlugin::newEngine(maksi::Maksi *maksi, const QString &name) const
{
    return new maksi::V8_Engine(maksi, name);
}


namespace maksi
{

static v8::Handle<v8::Value> printCallback(maksi::message_t type, const v8::Arguments &args)
{
    if (args.Length() >= 1) {
        v8::HandleScope scope;

        Maksi *maksi = reinterpret_cast<Maksi *>(v8::External::Cast(*args.Data())->Value());
        if (maksi) {
            for (int i = 0; i < args.Length(); ++i) {
                v8::Handle<v8::Value> arg = args[i];
                v8::String::Value utf(arg);
                QString s = QString::fromUtf16(*utf, utf.length());
                maksi->sendMessage(type, s);
            }
        }
    }
    return v8::Undefined();
}

static v8::Handle<v8::Value> warnCallback(const v8::Arguments &args)
{
    return printCallback(MESSAGE_WARN, args);
}

static v8::Handle<v8::Value> noteCallback(const v8::Arguments &args)
{
    return printCallback(MESSAGE_NOTE, args);
}

static v8::Handle<v8::Value> infoCallback(const v8::Arguments &args)
{
    return printCallback(MESSAGE_INFO, args);
}

static v8::Handle<v8::Value> debugCallback(const v8::Arguments &args)
{
    return printCallback(MESSAGE_DEBUG, args);
}


V8_Engine::V8_Engine(Maksi *maksi, const QString &name) : ScriptEngine(maksi, name)
{
    m_iso = v8::Isolate::New();

    v8::Locker locker(m_iso);
    v8::Isolate::Scope isolate_scope(m_iso);

    v8::HandleScope handle_scope;

    v8::Handle<v8::ObjectTemplate> global = v8::ObjectTemplate::New();
    v8::Handle<v8::External> maksi_ex = v8::External::New(reinterpret_cast<void *>(maksi));

    global->Set(v8::String::New("print"), v8::FunctionTemplate::New(noteCallback, maksi_ex));
    global->Set(v8::String::New("warn"), v8::FunctionTemplate::New(warnCallback, maksi_ex));
    global->Set(v8::String::New("note"), v8::FunctionTemplate::New(noteCallback, maksi_ex));
    global->Set(v8::String::New("info"), v8::FunctionTemplate::New(infoCallback, maksi_ex));
    global->Set(v8::String::New("debug"), v8::FunctionTemplate::New(debugCallback, maksi_ex));

    m_ctx = v8::Context::New(NULL, global);
}

V8_Engine::~V8_Engine()
{
    {
        v8::Locker locker(m_iso);
        v8::Isolate::Scope isolate_scope(m_iso);
        m_ctx.Dispose();
    }
    m_iso->Dispose();
}

bool V8_Engine::evalCode(const QString &code, const QUrl &src, int line)
{
    v8::Locker locker(m_iso);
    v8::Isolate::Scope isolate_scope(m_iso);

    v8::HandleScope handle_scope;
    v8::Context::Scope context_scope(m_ctx);

    v8::Local<v8::String> scriptCode = v8::String::New(code.utf16());
    v8::Local<v8::String> scriptSrc = v8::String::New(src.toString().utf16());
    v8::ScriptOrigin orig(scriptSrc, v8::Integer::New(line));

    v8::TryCatch try_catch;

    v8::Local<v8::Script> script = v8::Script::Compile(scriptCode, &orig);
    if (script.IsEmpty()) {
        v8::String::Value err(try_catch.Exception());
        maksi()->sendMessage(MESSAGE_ERROR, QString::fromUtf16(*err, err.length()));
        return false;
    }

    v8::Handle<v8::Value> val = script->Run();
    if (val.IsEmpty()) {
        v8::String::Value err(try_catch.Exception());
        maksi()->sendMessage(MESSAGE_ERROR, QString::fromUtf16(*err, err.length()));
        return false;
    }

    return true;
}

Script *V8_Engine::newScript(const QString &scriptName)
{
    return new V8_Script(maksi(), scriptName, this);
}


V8_Script::V8_Script(Maksi *maksi, const QString &name, V8_Engine *engine) :
    Script(maksi, name),
    m_engine(engine)
{
}

V8_Script::~V8_Script()
{
    v8::Locker locker(m_engine->m_iso);
    v8::Isolate::Scope isolate_scope(m_engine->m_iso);

    while (!m_code.isEmpty()) {
        Code *code = m_code.takeFirst();
        code->m_script.Dispose();
        delete code;
    }
}

void V8_Script::addArg(const QString &name, const QVariant &val, const Element *)
{
    m_args.append(Arg(name, val));
}

void V8_Script::addCode(const QString &code, const Element *conf)
{
    v8::Locker locker(m_engine->m_iso);
    v8::Isolate::Scope isolate_scope(m_engine->m_iso);

    v8::HandleScope handle_scope;
    v8::Context::Scope context_scope(m_engine->m_ctx);

    v8::Local<v8::String> scriptUrl;
    v8::Local<v8::Integer> scriptLine;
    if (conf) {
        scriptUrl = v8::String::New(conf->elementSourceUrl().toString().utf16());
        scriptLine = v8::Integer::New(conf->elementSourceLine());
    }

    v8::ScriptOrigin scriptOrigin(scriptUrl, scriptLine);
    v8::Local<v8::String> scriptCode = v8::String::New(code.utf16());

    v8::TryCatch try_catch;

    v8::Local<v8::Script> script = v8::Script::New(scriptCode, &scriptOrigin);
    if (script.IsEmpty()) {
        v8::String::Value msg(try_catch.Exception());
        maksi()->sendMessage(MESSAGE_ERROR, QString::fromUtf16(*msg, msg.length()));
    } else {
        Code *code = new Code(v8::Persistent<v8::Script>::New(script));
        m_code.append(code);
    }
}

Context *V8_Script::newContext()
{
    return new V8_Context(this);
}


V8_Context::V8_Context(V8_Script *script) :
  m_script(script)
{
    v8::Locker locker(m_script->m_engine->m_iso);
    v8::Isolate::Scope isolate_scope(m_script->m_engine->m_iso);

    v8::HandleScope handle_scope;
    v8::Context::Scope context_scope(m_script->m_engine->m_ctx);

    m_global = v8::Persistent<v8::Object>::New(v8::Object::New());
    for (int i = 0; i < m_script->m_args.size(); ++i) {
        qDebug("set %s=%s", qPrintable(m_script->m_args.at(i).m_name), qPrintable(m_script->m_args.at(i).m_val.toString()));
        v8::Local<v8::String> n = v8::String::New(m_script->m_args.at(i).m_name.utf16());
        v8::Local<v8::String> v = v8::String::New(m_script->m_args.at(i).m_val.toString().utf16());
        m_global->Set(n, v);
    }
}

V8_Context::~V8_Context()
{
//    v8::Locker locker(m_script->m_engine->m_iso);
//    v8::Isolate::Scope isolate_scope(m_script->m_engine->m_iso);
}

QVariant V8_Context::arg(const QString &name)
{
    v8::Locker locker(m_script->m_engine->m_iso);
    v8::Isolate::Scope isolate_scope(m_script->m_engine->m_iso);

    v8::HandleScope handle_scope;
    v8::Context::Scope context_scope(m_script->m_engine->m_ctx);

    v8::Local<v8::String> n = v8::String::New(name.utf16());
    v8::Local<v8::Value> v = m_global->Get(n);

    return QVariant();
}

void V8_Context::setArg(const QString &name, const QVariant &val)
{
    v8::Locker locker(m_script->m_engine->m_iso);
    v8::Isolate::Scope isolate_scope(m_script->m_engine->m_iso);

    v8::HandleScope handle_scope;
    v8::Context::Scope context_scope(m_script->m_engine->m_ctx);

    v8::Local<v8::String> n = v8::String::New(name.utf16());
    v8::Local<v8::String> v = v8::String::New(val.toString().utf16());

    m_global->Set(n, v);
}

QVariant V8_Context::eval()
{
    v8::Locker locker(m_script->m_engine->m_iso);
    v8::Isolate::Scope isolate_scope(m_script->m_engine->m_iso);

    v8::HandleScope handle_scope;
    v8::Context::Scope context_scope(m_script->m_engine->m_ctx);

    m_script->m_engine->m_ctx->DetachGlobal();

    v8::Local<v8::Value> globalProto = m_global->GetPrototype();
    v8::Local<v8::Object> contextProto = m_script->m_engine->m_ctx->Global();

    m_global->SetPrototype(contextProto);
    m_script->m_engine->m_ctx->ReattachGlobal(m_global);

    v8::TryCatch try_catch;

    for (int i = 0; i < m_script->m_code.size(); ++i) {
        v8::Handle<v8::Value> val = m_script->m_code.at(i)->m_script->Run();
        if (val.IsEmpty()) {
            v8::String::Value err(try_catch.Exception());
            m_script->maksi()->sendMessage(MESSAGE_ERROR, QString::fromUtf16(*err, err.length()));
            return QVariant();
        }
    }

    m_script->m_engine->m_ctx->DetachGlobal();
    m_global->SetPrototype(globalProto);
    m_script->m_engine->m_ctx->ReattachGlobal(contextProto);

    return QVariant();
}


};

 /* End of code */
