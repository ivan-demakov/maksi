/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_js.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:43 2011
 *
 * @brief   javascript engine
 *
 */

#ifndef MAKSI_JS_H
#define MAKSI_JS_H

#include <QObject>
#include <QQueue>
#include <QScriptValue>
#include <QScriptClass>
#include <QScriptClassPropertyIterator>
#include <QScriptProgram>
#include <QScriptable>
#include <QMutex>

#include <maksi/engine.h>
#include <maksi/script.h>
#include <maksi/elem.h>

#include "script_log.h"


#if defined(_MSC_VER)
#  pragma comment(lib, "maksi")
#endif


namespace maksi
{

class Js_EnginePlugin: public QObject, EnginePlugin
{
    Q_OBJECT;
    Q_INTERFACES(maksi::EnginePlugin);
    Q_PLUGIN_METADATA(IID MaksiEnginePlugin_IID FILE "maksi-js.json");

public:
    ~Js_EnginePlugin();

    virtual QString description() const override;
    virtual Engine *newEngine(Maksi *maksi, const QString &name) const override;
};


class ContextClass_Js;


class Js_Engine : public Engine
{
    Q_OBJECT

public:
    Js_Engine(Maksi *maksi, const QString &name);
    ~Js_Engine();

    bool evalCode(const QString &code, const QUrl &url, int line, QVariant *val) override;
    Script *newScript(const QString &scriptName) override;

    QScriptValue glob() { return m_glob; }
    void setGlob(QScriptValue x) { m_glob = x; }

    static QString toString(const MaksiElementPtr &val);

    ScriptLog *log() { return m_log; }
    QScriptEngine *engine() { return m_engine; }
    QScriptContext *pushContext() { m_mutex.lock(); return m_engine->pushContext(); }
    void popContext() { m_mutex.unlock(); m_engine->popContext(); }

private:
    QMutex m_mutex;
    QScriptValue m_glob;
    QScriptEngine *m_engine;
    ScriptLog *m_log;
};


struct Js_Arg
{
    Js_Arg(const QString &name, const QVariant &val) : m_name(name), m_val(val) {}

    QString m_name;
    QVariant m_val;
};


class Js_Script : public Script
{
public:
    Js_Script(Maksi *maksi, const QString &name, Js_Engine *jse);
    ~Js_Script();

    virtual bool addArg(const QString &name, const QVariant &val, const MaksiElement *conf) override;
    virtual void addCode(const QString &code, const MaksiElement *conf) override;
    virtual Context *newContext() override;

    const QList<QScriptProgram> progs() const { return m_progs; }

    static QList<QScriptProgram> parseProgs(const MaksiElement *conf);

private:
    Js_Engine *m_jse;
    QList<Js_Arg> m_args;
    QList<QScriptProgram> m_progs;
};


class Js_Context : public Context
{
public:
    Js_Context(Js_Engine *jse, QList<Js_Arg> args, QList<QScriptProgram> p, ScriptLog *log, Maksi *m);
    ~Js_Context();

    virtual QVariant arg(const QString &name) override;
    virtual void setArg(const QString &name, const QVariant &val) override;
    virtual QVariant eval() override;

private:
    QList<QScriptProgram> m_prog;
    Js_Engine *m_jse;
    QScriptValue m_vars;
    ScriptLog *m_log;
    Maksi *m_maksi;
};


class ContextClass_Js : public QObject, public QScriptClass
{
    Q_OBJECT;
    Q_DISABLE_COPY(ContextClass_Js);
public:
    ContextClass_Js(QScriptEngine *engine, Maksi *maksi);
    ~ContextClass_Js();

    QScriptValue constructor();

    virtual QString name() const;
    virtual QScriptValue prototype() const;

    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

private:
    QScriptValue m_proto;
    QScriptValue m_ctor;
    Maksi *m_maksi;
    QHash<QString, Context*> m_sxs;
};


class ContextPrototype_Js : public QObject, public QScriptable
{
    Q_OBJECT;
public:
    ContextPrototype_Js(QObject *parent = 0);
    ~ContextPrototype_Js();

public slots:
    QString toString() const;
    QVariant arg(const QString &name) const;
    void setArg(const QString &name, QVariant val);
    QVariant eval();
};


class TypeClass_Js : public QObject, public QScriptClass
{
    Q_OBJECT;
    Q_DISABLE_COPY(TypeClass_Js);
public:
    TypeClass_Js(QScriptEngine *engine, Maksi *maksi);
    ~TypeClass_Js();

    QScriptValue constructor();

    virtual QString name() const;
    virtual QScriptValue prototype() const;

    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

private:
    QScriptValue m_proto;
    QScriptValue m_ctor;
    Maksi *m_maksi;
};


class TypePrototype_Js : public QObject, public QScriptable
{
    Q_OBJECT;
public:
    TypePrototype_Js(QObject *parent = 0);
    ~TypePrototype_Js();

public slots:
    QString toString() const;
    MaksiElementPtr create(const QString &name);
    MaksiElementPtr create(const QString &name, const QString &ns);
};

};


Q_DECLARE_METATYPE(maksi::ContextClass_Js*)
Q_DECLARE_METATYPE(maksi::TypeClass_Js*)


#endif

/* End of file */
