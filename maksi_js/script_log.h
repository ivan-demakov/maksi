/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2009, 2010, 2011, 2012, 2013, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    script_log.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Mon Nov  9 15:57:45 2009
 *
 * @brief   log innterface for javascript
 *
 */

#ifndef MAKSI_SCRIPT_LOG_H
#define MAKSI_SCRIPT_LOG_H

#include <maksi/maksi.h>

#include <QScriptEngine>


namespace maksi
{

class ScriptLog : public QObject
{
    Q_OBJECT;
    Q_DISABLE_COPY(ScriptLog);

public:
    ScriptLog(QObject *o, Maksi *maksi) : QObject(o), m_maksi(maksi) {}
    virtual ~ScriptLog();

    static QScriptValue warnPrint(QScriptContext *context, QScriptEngine *engine);
    static QScriptValue notePrint(QScriptContext *context, QScriptEngine *engine);
    static QScriptValue infoPrint(QScriptContext *context, QScriptEngine *engine);
    static QScriptValue debugPrint(QScriptContext *context, QScriptEngine *engine);

    void sendMessage(message_t level, const QString &msg);

private:
    static QScriptValue print(message_t level, QScriptContext *context, QScriptEngine *engine);

    Maksi *m_maksi;
};

};

#endif

/* End of file */
