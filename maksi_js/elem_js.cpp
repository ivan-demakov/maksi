/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2013, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    elem_js.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Jun  2 02:57:19 2011
 *
 * @brief   arrays
 *
 */

#include "elem_js.h"
#include "maksi_js.h"

#include <maksi/xml.h>

#include <QScriptEngine>
#include <QScriptValueIterator>
#include <QScriptClassPropertyIterator>


Q_DECLARE_METATYPE(maksi::ElementClass_Js*)


namespace maksi
{

static QString elementClassName("MaksiElement");


class ElementClassPropertyIterator_Js : public QScriptClassPropertyIterator
{
public:
    ElementClassPropertyIterator_Js(const QScriptValue &object);
    ~ElementClassPropertyIterator_Js();

    bool hasNext() const;
    void next();
    bool hasPrevious() const;
    void previous();
    void toFront();
    void toBack();
    QScriptString name() const;
    uint id() const;

private:
    int m_index;
    int m_last;
};

ElementClassPropertyIterator_Js::ElementClassPropertyIterator_Js(const QScriptValue &object)
    : QScriptClassPropertyIterator(object)
{
    toFront();
}

ElementClassPropertyIterator_Js::~ElementClassPropertyIterator_Js()
{
}

bool ElementClassPropertyIterator_Js::hasNext() const
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(object()));
    return m_index < elem->elementCount();
}

void ElementClassPropertyIterator_Js::next()
{
    m_last = m_index;
    ++m_index;
}

bool ElementClassPropertyIterator_Js::hasPrevious() const
{
    return (m_index > 0);
}

void ElementClassPropertyIterator_Js::previous()
{
    --m_index;
    m_last = m_index;
}

void ElementClassPropertyIterator_Js::toFront()
{
    m_index = 0;
    m_last = -1;
}

void ElementClassPropertyIterator_Js::toBack()
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(object()));
    m_index = elem->elementCount();
    m_last = -1;
}

QScriptString ElementClassPropertyIterator_Js::name() const
{
    return object().engine()->toStringHandle(QString::number(m_last));
}

uint ElementClassPropertyIterator_Js::id() const
{
    return m_last;
}


ElementClass_Js::ElementClass_Js(QScriptEngine *engine) :
    QObject(engine),
    QScriptClass(engine)
{
    qScriptRegisterMetaType<MaksiElementPtr>(engine, toScriptValue, fromScriptValue);

    s_length = engine->toStringHandle(QLatin1String("length"));

    m_proto = engine->newQObject(new ElementPrototype_Js(this),
                                 QScriptEngine::QtOwnership,
                                 QScriptEngine::SkipMethodsInEnumeration
                                 | QScriptEngine::ExcludeSuperClassMethods
                                 | QScriptEngine::ExcludeSuperClassProperties);

    QScriptValue global = engine->globalObject();
    m_proto.setPrototype(global.property("Object").property("prototype"));

    m_ctor = engine->newFunction(construct, m_proto);
    m_ctor.setData(qScriptValueFromValue(engine, this));
}

ElementClass_Js::~ElementClass_Js()
{
}

MaksiElementPtr ElementClass_Js::toElement(QScriptValue val)
{
    //MaksiElementPtr elem = qvariant_cast<MaksiElementPtr>(val.data().toVariant());
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(val));
    if (elem) {
        return elem;
    }

    if (val.isArray()) {
        QScriptValue len = val.property("length");
        if (len.isValid()) {
            int cnt = len.toInt32();
            MaksiElement_List *ls = new MaksiElement_List();
            for (int i = 0; i < cnt; ++i) {
                MaksiElementPtr e(toElement(val.property(i)));
                ls->elementAppend(e);
            }
            return MaksiElementPtr(ls);
        }
    } else if (val.isBool()) {
        return MaksiElement::newBool(val.toBool());
    } else if (val.isDate()) {
        return MaksiElement::newDate(val.toDateTime());
    } else if (val.isNumber()) {
        return MaksiElement::newDouble(val.toNumber());
    } else if (val.isString()) {
        return MaksiElement::newString(val.toString());
    } else if (val.isVariant()) {
        return MaksiElement::newVariant(val.toVariant());
    }

    return MaksiElementPtr();
}

QScriptValue ElementClass_Js::fromElement(QScriptEngine *engine, const MaksiElementPtr &val)
{
    if (!val)
        return engine->undefinedValue();

    if (!val->isElementList() && val->isElementSimple()) {
        QVariant vv = val->elementToVariant();
        if (vv.isValid()) {
            switch (static_cast<QMetaType::Type>(vv.type())) {
            case QMetaType::Bool:
                return QScriptValue(vv.toBool());

            case QMetaType::Int:
            case QMetaType::Short:
            case QMetaType::SChar:
            case QMetaType::Char:
                return QScriptValue(vv.toInt());

            case QMetaType::UInt:
            case QMetaType::UShort:
            case QMetaType::UChar:
                return QScriptValue(vv.toUInt());

            case QMetaType::Long:
            case QMetaType::LongLong:
            case QMetaType::ULong:
            case QMetaType::ULongLong:
            case QMetaType::Float:
            case QMetaType::Double:
                return QScriptValue(vv.toReal());

            case QMetaType::QChar:
                return QScriptValue(vv.toChar());

            case QMetaType::QString:
                return QScriptValue(vv.toString());

            case QMetaType::QDate:
            case QMetaType::QTime:
            case QMetaType::QDateTime:
                return engine->newDate(vv.toDateTime());

            default:
                break;
            }
        }
    }

    QScriptValue ctor = engine->globalObject().property(elementClassName);
    ElementClass_Js *cls = qscriptvalue_cast<ElementClass_Js*>(ctor.data());
    //qDebug("ElementClass_Js::toScriptValue: elem=%s", !elem ? "[null]" : qPrintable(elem->elementText()));
    if (cls) {
        return cls->newInstance(val);
    }
    return engine->newVariant(QVariant::fromValue(val));
}

bool ElementClass_Js::hasAttribute(const MaksiElement *elem, const QScriptString &name)
{
    name_t xmlName = elem->attributeName(name);
    if (!xmlName)
        return false;
    return elem->hasAttribute(xmlName);
}

bool ElementClass_Js::hasMethod(const MaksiElement *elem, const QScriptString &name)
{
    QList<QString> as(elem->elementMethods());
    for (QList<QString>::const_iterator i = as.constBegin(); i != as.constEnd(); ++i) {
        if (name == engine()->toStringHandle(*i))
            return true;
    }
    return false;
}

QScriptClass::QueryFlags ElementClass_Js::queryProperty(const QScriptValue &object, const QScriptString &name, QueryFlags flags, uint *id)
{
    //qDebug("ElementClass_Js::queryProperty(%s)", qPrintable(name));

    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(object));
    if (!elem)
        return 0;

    if (name == s_length) {
        //qDebug("has length");
        *id = -1;
        return flags;
    }

    if (hasAttribute(elem.data(), name)) {
        //qDebug("has attribute");
        *id = -2;
        return flags;
    }

    if (hasMethod(elem.data(), name)) {
        //qDebug("has method");
        *id = -3;
        return flags;
    }

    if (elem->isElementList()) {
        bool isArrayIndex;
        qint32 pos = name.toArrayIndex(&isArrayIndex);
        if (isArrayIndex) {
            *id = pos;
            return flags;
        }
    }
    return 0;
}

QScriptValue ElementClass_Js::property(const QScriptValue &object, const QScriptString &name, uint id)
{
    //qDebug("ElementClass_Js::property(%s, %d)", qPrintable(name), id);

    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(object));
    if (!elem)
        return 0;

    if (name == s_length) {
        return elem->elementCount();
    }
    if (hasAttribute(elem.data(), name)) {
        return fromElement(engine(), elem->attribute(name));
    }
    if (hasMethod(elem.data(), name)) {
        QScriptValue fun = engine()->newFunction(methodCaller);
        fun.setData(name.toString());
        return fun;
    }

    qint32 pos = id;
    if ((pos < 0) || (pos >= elem->elementCount()))
        return QScriptValue();

    MaksiElementPtr val = elem->element(pos);
    return fromElement(engine(), val);
}

void ElementClass_Js::setProperty(QScriptValue &object, const QScriptString &name, uint /*id*/, const QScriptValue &value)
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(object));
    if (!elem)
        return;

    if (name == s_length) {
        return;
    }
    if (hasAttribute(elem.data(), name)) {
        if (!elem->attributeFixed(name)) {
            elem->setAttribute(name, toElement(value));
        }
        return;
    }
    if (hasMethod(elem.data(), name)) {
        return;
    }
}

QScriptValue::PropertyFlags ElementClass_Js::propertyFlags(const QScriptValue &object, const QScriptString &name, uint id)
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(object));
    if (!elem)
        return 0;

    if (name == s_length) {
        return QScriptValue::ReadOnly | QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
    }
    if (hasAttribute(elem.data(), name)) {
        if (elem->attributeFixed(name))
            return QScriptValue::ReadOnly | QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
        else
            return QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
    }
    if (hasMethod(elem.data(), name)) {
        return QScriptValue::ReadOnly | QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
    }

    qint32 pos = id;
    if ((pos < 0) || (pos >= elem->elementCount()))
        return QScriptValue::ReadOnly | QScriptValue::Undeletable;

    return 0;
}

QScriptClassPropertyIterator *ElementClass_Js::newIterator(const QScriptValue &object)
{
    return new ElementClassPropertyIterator_Js(object);
}

QString ElementClass_Js::name() const
{
    return elementClassName;
}

QScriptValue ElementClass_Js::prototype() const
{
    return m_proto;
}

QScriptValue ElementClass_Js::constructor()
{
    return m_ctor;
}

QScriptValue ElementClass_Js::newInstance()
{
    //qDebug("ElementClass_Js::newInstance()");

    MaksiElementPtr elem;
    return newInstance(elem);
}

QScriptValue ElementClass_Js::newInstance(const MaksiElementPtr &elem)
{
    //qDebug("ElementClass_Js::newInstance(%s)", !elem ? "[null]" : qPrintable(elem->elementText()));

    QScriptValue data = engine()->newVariant(QVariant::fromValue(elem));
    return engine()->newObject(this, data);
}

QScriptValue ElementClass_Js::construct(QScriptContext *ctx, QScriptEngine *)
{
    //qDebug("ElementClass_Js::construct");

    ElementClass_Js *cls = qscriptvalue_cast<ElementClass_Js*>(ctx->callee().data());
    if (!cls)
        return QScriptValue();

    QScriptValue arg = ctx->argument(0);
    if (arg.instanceOf(ctx->callee()))
        return cls->newInstance(qvariant_cast<MaksiElementPtr>(arg.data().toVariant()));

    return cls->newInstance();
}

QScriptValue ElementClass_Js::toScriptValue(QScriptEngine *eng, const MaksiElementPtr &elem)
{
    return fromElement(eng, elem);
}

void ElementClass_Js::fromScriptValue(const QScriptValue &obj, MaksiElementPtr &elem)
{
    elem = qvariant_cast<MaksiElementPtr>(obj.data().toVariant());
    if (!elem)
        elem = toElement(obj);
    //qDebug("ElementClass_Js::fromScriptValue: elem=%s", !elem ? "[null]" : qPrintable(elem->elementText()));
}

QScriptValue ElementClass_Js::methodCaller(QScriptContext *ctx, QScriptEngine *eng)
{
    //qDebug("ElementClass_Js::methodCaller");

    QScriptValue val = ctx->thisObject();
    MaksiElementPtr elem;
    fromScriptValue(val, elem);
    if (!elem)
        return QScriptValue();

    QScriptValue fun = ctx->callee();
    QString name = fun.data().toString();

    QList<QVariant> args;
    for (int i = 0; i < ctx->argumentCount(); ++i) {
        QScriptValue a(ctx->argument(i));
        args << a.toVariant();
    }

    QVariant res = elem->callMethod(name, args);
    return eng->newVariant(res);
}

ElementPrototype_Js::ElementPrototype_Js(QObject *parent) :
    QObject(parent)
{
}

ElementPrototype_Js::~ElementPrototype_Js()
{
}

QString ElementPrototype_Js::nameOf() const
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(thisObject()));
    if (!elem)
        return QString();

    name_t name = elem->elementName();
    if (!name)
        return QString();

    return name->toString();
}

QVariant ElementPrototype_Js::valueOf() const
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(thisObject()));
    if (!elem)
        return QVariant();

    return elem->elementToVariant();
}

QString ElementPrototype_Js::toString() const
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(thisObject()));
    return Js_Engine::toString(elem);
}

bool ElementPrototype_Js::isValid() const
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(thisObject()));
    if (!elem)
        return false;
    return elem->isElementValid();
}

bool ElementPrototype_Js::isFixed(const QString &name)
{
    QScriptClass *cls = thisObject().scriptClass();
    if (cls) {
        QScriptValue::PropertyFlags flags = thisObject().propertyFlags(name);
        if ((flags & QScriptValue::ReadOnly) != 0)
            return true;
    }
    return false;
}

bool ElementPrototype_Js::isExists(const QString &name)
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(thisObject()));
    if (!elem)
        return false;
    return elem->hasAttribute(name);
}

bool ElementPrototype_Js::append(MaksiElementPtr e)
{
    MaksiElementPtr elem(qscriptvalue_cast<MaksiElementPtr>(thisObject()));
    if (!elem)
        return false;
    return elem->elementAppend(e);
}

};

/* End of code */
