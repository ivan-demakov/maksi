/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    array_js.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Jun  2 02:57:19 2011
 *
 * @brief   arrays
 *
 */

#include "array_js.h"
#include "maksi_js.h"

#include <QScriptEngine>
#include <QScriptValueIterator>
#include <QScriptClassPropertyIterator>


Q_DECLARE_METATYPE(maksi::ArrayClass_Js*)
Q_DECLARE_METATYPE(maksi::Array*)


namespace maksi
{

QString ArrayClass_Js::type_name = QLatin1String("MaksiArray");


class ArrayClassPropertyIterator_Js : public QScriptClassPropertyIterator
{
public:
    ArrayClassPropertyIterator_Js(const QScriptValue &object);
    ~ArrayClassPropertyIterator_Js();

    bool hasNext() const;
    void next();
    bool hasPrevious() const;
    void previous();
    void toFront();
    void toBack();
    QScriptString name() const;
    uint id() const;

private:
    int m_index;
    int m_last;
};


ArrayClass_Js::ArrayClass_Js(QScriptEngine *engine) :
    QObject(engine),
    QScriptClass(engine)
{
    qScriptRegisterMetaType<Array>(engine, toScriptValue, fromScriptValue);

    s_length = engine->toStringHandle(QLatin1String("length"));

    m_proto = engine->newQObject(new ArrayPrototype_Js(this),
                                 QScriptEngine::QtOwnership,
                                 QScriptEngine::SkipMethodsInEnumeration
                                 | QScriptEngine::ExcludeSuperClassMethods
                                 | QScriptEngine::ExcludeSuperClassProperties);

    QScriptValue global = engine->globalObject();
    m_proto.setPrototype(global.property("Object").property("prototype"));

    m_ctor = engine->newFunction(construct, m_proto);
    m_ctor.setData(qScriptValueFromValue(engine, this));
}

ArrayClass_Js::~ArrayClass_Js()
{
}

QScriptClass::QueryFlags ArrayClass_Js::queryProperty(const QScriptValue &object, const QScriptString &name, QueryFlags flags, uint *id)
{
    Array *ba = qscriptvalue_cast<Array*>(object.data());
    if (!ba)
        return 0;

    if (name == s_length) {
        return flags;
    } else {
        bool isArrayIndex;
        qint32 pos = name.toArrayIndex(&isArrayIndex);
        if (!isArrayIndex)
            return 0;

        *id = pos;
        if ((flags & HandlesReadAccess) && (pos >= ba->size()))
            flags &= ~HandlesReadAccess;

        return flags;
    }
}

QScriptValue ArrayClass_Js::property(const QScriptValue &object, const QScriptString &name, uint id)
{
    Array *ba = qscriptvalue_cast<Array*>(object.data());
    if (!ba)
        return 0;

    if (name == s_length) {
        return ba->length();
    } else {
        qint32 pos = id;
        if ((pos < 0) || (pos >= ba->size()))
            return QScriptValue();
        return engine()->toScriptValue(ba->at(pos));
    }
    return QScriptValue();
}

void ArrayClass_Js::setProperty(QScriptValue &object, const QScriptString &name, uint id, const QScriptValue &value)
{
    Array *ba = qscriptvalue_cast<Array*>(object.data());
    if (!ba)
        return;

    if (name == s_length) {
        //resize(*ba, value.toInt32());
    } else {
        qint32 pos = id;
        if (value.isValid())
            ba->replace(pos, value.toVariant());
        else
            ba->removeAt(pos);
    }
}

QScriptValue::PropertyFlags ArrayClass_Js::propertyFlags(const QScriptValue &/*object*/, const QScriptString &name, uint /*id*/)
{
    if (name == s_length) {
        return QScriptValue::ReadOnly | QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
    }
    return 0;
}

QScriptClassPropertyIterator *ArrayClass_Js::newIterator(const QScriptValue &object)
{
    return new ArrayClassPropertyIterator_Js(object);
}

QString ArrayClass_Js::name() const
{
    return type_name;
}

QScriptValue ArrayClass_Js::prototype() const
{
    return m_proto;
}

QScriptValue ArrayClass_Js::constructor()
{
    return m_ctor;
}

QScriptValue ArrayClass_Js::newInstance()
{
    Array arr;
    return newInstance(arr);
}

QScriptValue ArrayClass_Js::newInstance(const Array &ba)
{
    QScriptValue data = engine()->newVariant(QVariant::fromValue(ba));
    return engine()->newObject(this, data);
}

QScriptValue ArrayClass_Js::construct(QScriptContext *ctx, QScriptEngine *)
{
    ArrayClass_Js *cls = qscriptvalue_cast<ArrayClass_Js*>(ctx->callee().data());
    if (!cls)
        return QScriptValue();

    QScriptValue arg = ctx->argument(0);
    if (arg.instanceOf(ctx->callee()))
        return cls->newInstance(qscriptvalue_cast<Array>(arg));

    Array ba;
    for (int i = 0; i < ctx->argumentCount(); i++) {
        arg = ctx->argument(i);
        ba.append(arg.toVariant());
    }
    return cls->newInstance(ba);
}

QScriptValue ArrayClass_Js::toScriptValue(QScriptEngine *eng, const Array &ba)
{
    QScriptValue ctor = eng->globalObject().property(type_name);
    ArrayClass_Js *cls = qscriptvalue_cast<ArrayClass_Js*>(ctor.data());
    if (!cls)
        return eng->newVariant(QVariant::fromValue(ba));
    return cls->newInstance(ba);
}

void ArrayClass_Js::fromScriptValue(const QScriptValue &obj, Array &ba)
{
    ba = qvariant_cast<Array>(obj.data().toVariant());
}


ArrayClassPropertyIterator_Js::ArrayClassPropertyIterator_Js(const QScriptValue &object)
    : QScriptClassPropertyIterator(object)
{
    toFront();
}

ArrayClassPropertyIterator_Js::~ArrayClassPropertyIterator_Js()
{
}

bool ArrayClassPropertyIterator_Js::hasNext() const
{
    Array ba = qscriptvalue_cast<Array>(object().data());
    return m_index < ba.size();
}

void ArrayClassPropertyIterator_Js::next()
{
    m_last = m_index;
    ++m_index;
}

bool ArrayClassPropertyIterator_Js::hasPrevious() const
{
    return (m_index > 0);
}

void ArrayClassPropertyIterator_Js::previous()
{
    --m_index;
    m_last = m_index;
}

void ArrayClassPropertyIterator_Js::toFront()
{
    m_index = 0;
    m_last = -1;
}

void ArrayClassPropertyIterator_Js::toBack()
{
    Array ba = qscriptvalue_cast<Array>(object().data());
    m_index = ba.size();
    m_last = -1;
}

QScriptString ArrayClassPropertyIterator_Js::name() const
{
    return object().engine()->toStringHandle(QString::number(m_last));
}

uint ArrayClassPropertyIterator_Js::id() const
{
    return m_last;
}


ArrayPrototype_Js::ArrayPrototype_Js(QObject *parent) :
    QObject(parent)
{
}

ArrayPrototype_Js::~ArrayPrototype_Js()
{
}

Array *ArrayPrototype_Js::thisArray() const
{
    return qscriptvalue_cast<Array*>(thisObject().data());
}

bool ArrayPrototype_Js::equals(const Array &other)
{
    return *thisArray() == other;
}

QScriptValue ArrayPrototype_Js::clear()
{
    Array *ba = thisArray();
    ba->clear();
    return thisObject();
}

QScriptValue ArrayPrototype_Js::remove(int pos)
{
    thisArray()->removeAt(pos);
    return thisObject();
}

QScriptValue ArrayPrototype_Js::valueOf() const
{
    return thisObject().data();
}

QScriptValue ArrayPrototype_Js::toString() const
{
    Array *ba = thisArray();
    return Js_Engine::toString(QVariant::fromValue(*ba));
}

};

/* End of code */
