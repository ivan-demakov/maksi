include(../maksi.pri)

TEMPLATE = lib
CONFIG += plugin

QT -= gui
QT += script

unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksi_js)
  CONFIG += debug
}
win32 {
  TARGET = maksi_js
  LIBS += -lmaksi
  CONFIG += release
}

INCLUDEPATH += ..

HEADERS = maksi_js.h script_log.h elem_js.h
SOURCES = maksi_js.cpp script_log.cpp elem_js.cpp

OTHER_FILES  += maksi-js.json
