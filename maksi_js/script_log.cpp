/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2009, 2010, 2012, 2013, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    script_log.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Mon Nov  9 15:58:18 2009
 *
 * @brief   log interface for javascript
 *
 */

#include "script_log.h"


namespace maksi
{

ScriptLog::~ScriptLog()
{
}

void ScriptLog::sendMessage(message_t level, const QString &msg)
{
    if (m_maksi) {
        m_maksi->sendMessage(level, msg);
    }
}

QScriptValue ScriptLog::print(message_t level, QScriptContext *context, QScriptEngine *engine)
{
    QString msg;
    for (int i = 0; i < context->argumentCount(); ++i) {
        if (i > 0)
            msg.append(" ");
        msg.append(context->argument(i).toString());
    }

    QScriptValue calleeData = context->callee().data();
    ScriptLog *p = qobject_cast<maksi::ScriptLog*>(calleeData.toQObject());
    if (p) {
        p->sendMessage(level, msg);
    }
    return engine->undefinedValue();
}

QScriptValue ScriptLog::warnPrint(QScriptContext *context, QScriptEngine *engine)
{
    return print(MESSAGE_WARN, context, engine);
}

QScriptValue ScriptLog::notePrint(QScriptContext *context, QScriptEngine *engine)
{
    return print(MESSAGE_NOTE, context, engine);
}

QScriptValue ScriptLog::infoPrint(QScriptContext *context, QScriptEngine *engine)
{
    return print(MESSAGE_INFO, context, engine);
}

QScriptValue ScriptLog::debugPrint(QScriptContext *context, QScriptEngine *engine)
{
    return print(MESSAGE_DEBUG, context, engine);
}

};


Q_DECLARE_METATYPE(maksi::ScriptLog*);


/* End of code */
