/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    elem_js.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Jun  2 02:49:13 2011
 *
 * @brief   maksi elements for javascript engine
 *
 */

#ifndef MAKSI_ELEM_JS_H
#define MAKSI_ELEM_JS_H

#include <maksi/elem.h>

#include <QObject>
#include <QScriptClass>
#include <QScriptString>
#include <QScriptContext>
#include <QScriptable>

namespace maksi
{

class ElementClass_Js : public QObject, public QScriptClass
{
    Q_OBJECT;
    Q_DISABLE_COPY(ElementClass_Js);
public:
    ElementClass_Js(QScriptEngine *engine);
    ~ElementClass_Js();

    QScriptValue constructor();
    QScriptValue newInstance();
    QScriptValue newInstance(const MaksiElementPtr &elem);

    virtual QString name() const;
    virtual QScriptValue prototype() const;
    virtual QScriptClass::QueryFlags queryProperty(const QScriptValue &obj, const QScriptString &name, QScriptClass::QueryFlags flags, uint *id);
    virtual QScriptValue::PropertyFlags propertyFlags(const QScriptValue &obj, const QScriptString &name, uint id);
    virtual QScriptValue property(const QScriptValue &obj, const QScriptString &name, uint id);
    virtual void setProperty(QScriptValue &obj, const QScriptString &name, uint id, const QScriptValue &value);
    virtual QScriptClassPropertyIterator *newIterator(const QScriptValue &obj);

    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);
    static QScriptValue toScriptValue(QScriptEngine *eng, const MaksiElementPtr &ba);
    static void fromScriptValue(const QScriptValue &obj, MaksiElementPtr &ba);

    static QScriptValue methodCaller(QScriptContext *ctx, QScriptEngine *eng);

    static MaksiElementPtr toElement(QScriptValue val);
    static QScriptValue fromElement(QScriptEngine *engine, const MaksiElementPtr &val);

private:
    bool hasAttribute(const MaksiElement *elem, const QScriptString &name);
    bool hasMethod(const MaksiElement *elem, const QScriptString &name);

    QScriptString s_length;
    QScriptValue m_proto;
    QScriptValue m_ctor;
};


class ElementPrototype_Js : public QObject, public QScriptable
{
    Q_OBJECT;
public:
    ElementPrototype_Js(QObject *parent = 0);
    ~ElementPrototype_Js();

    bool isValid() const;

public slots:
    QString nameOf() const;
    QVariant valueOf() const;
    QString toString() const;
    bool isFixed(const QString &name);
    bool isExists(const QString &name);
    bool append(MaksiElementPtr e);
};

};

#endif

/* End of file */
