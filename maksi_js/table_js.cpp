/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    table_js.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Jun  2 02:57:19 2011
 *
 * @brief   tables
 *
 */

#include "table_js.h"
#include "maksi_js.h"

#include <QScriptEngine>
#include <QScriptValueIterator>
#include <QScriptClassPropertyIterator>


Q_DECLARE_METATYPE(maksi::TableClass_Js*)
Q_DECLARE_METATYPE(maksi::TableRowClass_Js*)


namespace maksi
{

class TableClassPropertyIterator_Js : public QScriptClassPropertyIterator
{
public:
    TableClassPropertyIterator_Js(const QScriptValue &object);
    ~TableClassPropertyIterator_Js();

    bool hasNext() const;
    void next();
    bool hasPrevious() const;
    void previous();
    void toFront();
    void toBack();
    QScriptString name() const;
    uint id() const;

private:
    int m_index;
    int m_last;
};

class TableRowClassPropertyIterator_Js : public QScriptClassPropertyIterator
{
public:
    TableRowClassPropertyIterator_Js(const QScriptValue &object);
    ~TableRowClassPropertyIterator_Js();

    bool hasNext() const;
    void next();
    bool hasPrevious() const;
    void previous();
    void toFront();
    void toBack();
    QScriptString name() const;
    uint id() const;

private:
    int m_index;
    int m_last;
};


TableClass_Js::TableClass_Js(QScriptEngine *engine) :
    QObject(engine),
    QScriptClass(engine)
{
    qScriptRegisterMetaType<Table>(engine, toScriptValue, fromScriptValue);

    s_length = engine->toStringHandle(QLatin1String("length"));

    m_proto = engine->newQObject(new TablePrototype_Js(this),
                                 QScriptEngine::QtOwnership,
                                 QScriptEngine::SkipMethodsInEnumeration
                                 | QScriptEngine::ExcludeSuperClassMethods
                                 | QScriptEngine::ExcludeSuperClassProperties);

    QScriptValue global = engine->globalObject();
    m_proto.setPrototype(global.property("Object").property("prototype"));

    m_ctor = engine->newFunction(construct, m_proto);
    m_ctor.setData(qScriptValueFromValue(engine, this));
}

TableClass_Js::~TableClass_Js()
{
}

QScriptClass::QueryFlags TableClass_Js::queryProperty(const QScriptValue &/*obj*/, const QScriptString &name, QueryFlags flags, uint *id)
{
    if (name == s_length) {
        return flags;
    } else {
        bool isIndex;
        qint32 pos = name.toArrayIndex(&isIndex);
        if (!isIndex)
            return 0;

        *id = pos;
        return flags;
    }
}

QScriptValue TableClass_Js::property(const QScriptValue &obj, const QScriptString &name, uint id)
{
    Table ba(qscriptvalue_cast<Table>(obj));
    if (name == s_length) {
        return ba.length();
    } else {
        qint32 pos = id;
        if ((pos < 0) || (pos >= ba.size()))
            return QScriptValue();

        return engine()->toScriptValue(ba.row(pos));
    }
    return QScriptValue();
}

void TableClass_Js::setProperty(QScriptValue &obj, const QScriptString &name, uint id, const QScriptValue &value)
{
    Table ba(qscriptvalue_cast<Table>(obj));
    if (name == s_length) {
        //resize(ba, value.toInt32());
    } else {
        qint32 pos = id;
        if (value.isValid()) {
            //ba.replace(pos, value.toVariant());
        } else {
            ba.removeRow(pos);
        }
    }
}

QScriptValue::PropertyFlags TableClass_Js::propertyFlags(const QScriptValue &/*obj*/, const QScriptString &name, uint /*id*/)
{
    if (name == s_length) {
        return QScriptValue::ReadOnly | QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
    }
    return QScriptValue::ReadOnly;
}

QScriptClassPropertyIterator *TableClass_Js::newIterator(const QScriptValue &obj)
{
    return new TableClassPropertyIterator_Js(obj);
}

QString TableClass_Js::name() const
{
    static QString n(QString::fromLatin1("Table"));
    return n;
}

QScriptValue TableClass_Js::prototype() const
{
    return m_proto;
}

QScriptValue TableClass_Js::constructor()
{
    return m_ctor;
}

QScriptValue TableClass_Js::newInstance()
{
    Table ba;
    return newInstance(ba);
}

QScriptValue TableClass_Js::newInstance(const Table &ba)
{
    QScriptValue data = engine()->newVariant(QVariant::fromValue(ba));
    return engine()->newObject(this, data);
}

QScriptValue TableClass_Js::construct(QScriptContext *ctx, QScriptEngine *)
{
    TableClass_Js *cls = qscriptvalue_cast<TableClass_Js*>(ctx->callee().data());
    if (!cls)
        return QScriptValue();

    QScriptValue arg = ctx->argument(0);
    if (arg.instanceOf(ctx->callee()))
        return cls->newInstance(qscriptvalue_cast<Table>(arg));

    QList<QString> names;
    for (int i = 0; i < ctx->argumentCount(); i++) {
        arg = ctx->argument(i);
        names.append(arg.toString());
    }
    Table ba(names);
    return cls->newInstance(ba);
}

QScriptValue TableClass_Js::toScriptValue(QScriptEngine *eng, const Table &ba)
{
    QScriptValue ctor = eng->globalObject().property(QString::fromLatin1("Table"));
    TableClass_Js *cls = qscriptvalue_cast<TableClass_Js*>(ctor.data());
    if (cls)
        return cls->newInstance(ba);

    return eng->newVariant(QVariant::fromValue(ba));
}

void TableClass_Js::fromScriptValue(const QScriptValue &obj, Table &ba)
{
    ba = qvariant_cast<Table>(obj.data().toVariant());
}


TableClassPropertyIterator_Js::TableClassPropertyIterator_Js(const QScriptValue &obj)
    : QScriptClassPropertyIterator(obj)
{
    toFront();
}

TableClassPropertyIterator_Js::~TableClassPropertyIterator_Js()
{
}

bool TableClassPropertyIterator_Js::hasNext() const
{
    Table ba(qscriptvalue_cast<Table>(object()));
    return m_index < ba.size();
}

void TableClassPropertyIterator_Js::next()
{
    m_last = m_index;
    ++m_index;
}

bool TableClassPropertyIterator_Js::hasPrevious() const
{
    return (m_index > 0);
}

void TableClassPropertyIterator_Js::previous()
{
    --m_index;
    m_last = m_index;
}

void TableClassPropertyIterator_Js::toFront()
{
    m_index = 0;
    m_last = -1;
}

void TableClassPropertyIterator_Js::toBack()
{
    Table ba(qscriptvalue_cast<Table>(object()));
    m_index = ba.size();
    m_last = -1;
}

QScriptString TableClassPropertyIterator_Js::name() const
{
    return object().engine()->toStringHandle(QString::number(m_last));
}

uint TableClassPropertyIterator_Js::id() const
{
    return m_last;
}


TablePrototype_Js::TablePrototype_Js(QObject *parent) :
    QObject(parent)
{
}

TablePrototype_Js::~TablePrototype_Js()
{
}

QVariant TablePrototype_Js::valueOf() const
{
    return thisObject().data().toVariant();
}

QString TablePrototype_Js::toString() const
{
    return Js_Engine::toString(thisObject().data().toVariant());
}

bool TablePrototype_Js::equals(const Table &other)
{
    Table ba(qscriptvalue_cast<Table>(thisObject()));
    return ba == other;
}

void TablePrototype_Js::clear()
{
    Table ba(qscriptvalue_cast<Table>(thisObject()));
    ba.clear();
}

void TablePrototype_Js::remove(int pos)
{
    Table ba(qscriptvalue_cast<Table>(thisObject()));
    ba.removeRow(pos);
}

QString TablePrototype_Js::nameOf(int i) const
{
    Table ba(qscriptvalue_cast<Table>(thisObject()));
    return ba.colName(i);
}


TableRowClass_Js::TableRowClass_Js(QScriptEngine *engine) :
    QObject(engine),
    QScriptClass(engine)
{
    qScriptRegisterMetaType<Table::Row>(engine, toScriptValue, fromScriptValue);

    s_length = engine->toStringHandle(QLatin1String("length"));

    m_proto = engine->newQObject(new TableRowPrototype_Js(this),
                                 QScriptEngine::QtOwnership,
                                 QScriptEngine::SkipMethodsInEnumeration
                                 | QScriptEngine::ExcludeSuperClassMethods
                                 | QScriptEngine::ExcludeSuperClassProperties);

    QScriptValue global = engine->globalObject();
    m_proto.setPrototype(global.property("Object").property("prototype"));

    m_ctor = engine->newFunction(construct, m_proto);
    m_ctor.setData(qScriptValueFromValue(engine, this));
}

TableRowClass_Js::~TableRowClass_Js()
{
}

QScriptClass::QueryFlags TableRowClass_Js::queryProperty(const QScriptValue &obj, const QScriptString &name, QueryFlags flags, uint *id)
{
    if (name == s_length) {
        return flags;
    } else {
        bool isIndex;
        qint32 pos = name.toArrayIndex(&isIndex);
        if (isIndex) {
            *id = pos;
            return flags;
        }

        Table::Row tr(qscriptvalue_cast<Table::Row>(obj));
        int index = tr.indexOf(name);
        if (index >= 0) {
            *id = index;
            return flags;
        }
    }
    return 0;
}

QScriptValue TableRowClass_Js::property(const QScriptValue &obj, const QScriptString &name, uint id)
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(obj));
    if (name == s_length) {
        return tr.length();
    } else {
        qint32 pos = id;
        if (0 <= pos && pos < tr.size())
            return engine()->toScriptValue(tr.at(pos));
    }
    return QScriptValue();
}

void TableRowClass_Js::setProperty(QScriptValue &obj, const QScriptString &, uint id, const QScriptValue &value)
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(obj));
    qint32 pos = id;
    if (value.isValid()) {
        tr.replace(pos, value.toVariant());
    }
}

QScriptValue::PropertyFlags TableRowClass_Js::propertyFlags(const QScriptValue &/*obj*/, const QScriptString &name, uint /*id*/)
{
    if (name == s_length) {
        return QScriptValue::ReadOnly | QScriptValue::Undeletable | QScriptValue::SkipInEnumeration;
    }
    return QScriptValue::Undeletable;
}

QScriptClassPropertyIterator *TableRowClass_Js::newIterator(const QScriptValue &object)
{
    return new TableRowClassPropertyIterator_Js(object);
}

QString TableRowClass_Js::name() const
{
    return QString::fromLatin1("TableRow");
}

QScriptValue TableRowClass_Js::prototype() const
{
    return m_proto;
}

QScriptValue TableRowClass_Js::constructor()
{
    return m_ctor;
}

QScriptValue TableRowClass_Js::newInstance()
{
    Table::Row tr;
    return newInstance(tr);
}

QScriptValue TableRowClass_Js::newInstance(const Table::Row &tr)
{
    QScriptValue data = engine()->newVariant(QVariant::fromValue(tr));
    return engine()->newObject(this, data);
}

QScriptValue TableRowClass_Js::construct(QScriptContext *ctx, QScriptEngine *)
{
    TableRowClass_Js *cls = qscriptvalue_cast<TableRowClass_Js*>(ctx->callee().data());
    if (!cls)
        return QScriptValue();

    QScriptValue arg = ctx->argument(0);
    if (arg.instanceOf(ctx->callee()))
        return cls->newInstance(qscriptvalue_cast<Table::Row>(arg));

    return cls->newInstance();
}

QScriptValue TableRowClass_Js::toScriptValue(QScriptEngine *eng, const Table::Row &tr)
{
    QScriptValue ctor = eng->globalObject().property(QString::fromLatin1("TableRow"));
    TableRowClass_Js *cls = qscriptvalue_cast<TableRowClass_Js*>(ctor.data());
    if (cls)
        return cls->newInstance(tr);

    return eng->newVariant(QVariant::fromValue(tr));
}

void TableRowClass_Js::fromScriptValue(const QScriptValue &obj, Table::Row &tr)
{
    tr = qvariant_cast<Table::Row>(obj.data().toVariant());
}


TableRowClassPropertyIterator_Js::TableRowClassPropertyIterator_Js(const QScriptValue &object)
    : QScriptClassPropertyIterator(object)
{
    toFront();
}

TableRowClassPropertyIterator_Js::~TableRowClassPropertyIterator_Js()
{
}

bool TableRowClassPropertyIterator_Js::hasNext() const
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(object()));
    return m_index < tr.size();
}

void TableRowClassPropertyIterator_Js::next()
{
    m_last = m_index;
    ++m_index;
}

bool TableRowClassPropertyIterator_Js::hasPrevious() const
{
    return (m_index > 0);
}

void TableRowClassPropertyIterator_Js::previous()
{
    --m_index;
    m_last = m_index;
}

void TableRowClassPropertyIterator_Js::toFront()
{
    m_index = 0;
    m_last = -1;
}

void TableRowClassPropertyIterator_Js::toBack()
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(object()));
    m_index = tr.size();
    m_last = -1;
}

QScriptString TableRowClassPropertyIterator_Js::name() const
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(object()));
    return object().engine()->toStringHandle(tr.nameOf(m_last));
}

uint TableRowClassPropertyIterator_Js::id() const
{
    return m_last;
}


TableRowPrototype_Js::TableRowPrototype_Js(QObject *parent) :
    QObject(parent)
{
}

TableRowPrototype_Js::~TableRowPrototype_Js()
{
}

QVariant TableRowPrototype_Js::valueOf() const
{
    return thisObject().data().toVariant();
}

QString TableRowPrototype_Js::toString() const
{
    return Js_Engine::toString(thisObject().data().toVariant());
}

QString TableRowPrototype_Js::nameOf(int i) const
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(thisObject()));
    return tr.nameOf(i);
}

Table::Row TableRowPrototype_Js::prev() const
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(thisObject()));
    return tr.prev();
}

Table::Row TableRowPrototype_Js::next() const
{
    Table::Row tr(qscriptvalue_cast<Table::Row>(thisObject()));
    return tr.next();
}

};

/* End of code */
