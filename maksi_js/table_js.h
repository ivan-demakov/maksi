/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    array_js.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Jun  2 02:49:13 2011
 *
 * @brief   maksi tables for javascript engine
 *
 */

#ifndef MAKSI_TABLE_JS_H
#define MAKSI_TABLE_JS_H

#include <maksi/defs.h>

#include <QObject>
#include <QScriptClass>
#include <QScriptString>
#include <QScriptContext>
#include <QScriptable>

namespace maksi
{

class TableClass_Js : public QObject, public QScriptClass
{
    Q_OBJECT;
    Q_DISABLE_COPY(TableClass_Js);
public:
    TableClass_Js(QScriptEngine *engine);
    ~TableClass_Js();

    virtual QString name() const;
    virtual QScriptValue prototype() const;
    virtual QScriptClass::QueryFlags queryProperty(const QScriptValue &obj, const QScriptString &name, QScriptClass::QueryFlags flags, uint *id);
    virtual QScriptValue::PropertyFlags propertyFlags(const QScriptValue &obj, const QScriptString &name, uint id);
    virtual QScriptValue property(const QScriptValue &obj, const QScriptString &name, uint id);
    virtual void setProperty(QScriptValue &obj, const QScriptString &name, uint id, const QScriptValue &value);
    virtual QScriptClassPropertyIterator *newIterator(const QScriptValue &obj);

    QScriptValue constructor();
    QScriptValue newInstance();
    QScriptValue newInstance(const Table &ba);

    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);
    static QScriptValue toScriptValue(QScriptEngine *eng, const Table &ba);
    static void fromScriptValue(const QScriptValue &obj, Table &ba);

private:
    QScriptString s_length;
    QScriptValue m_proto;
    QScriptValue m_ctor;
};


class TablePrototype_Js : public QObject, public QScriptable
{
    Q_OBJECT;
public:
    TablePrototype_Js(QObject *parent = 0);
    ~TablePrototype_Js();

public slots:
    QVariant valueOf() const;
    QString toString() const;
    bool equals(const Table &other);
    void clear();
    void remove(int pos);
    QString nameOf(int i) const;
};


class TableRowClass_Js : public QObject, public QScriptClass
{
    Q_OBJECT;
    Q_DISABLE_COPY(TableRowClass_Js);
public:
    TableRowClass_Js(QScriptEngine *engine);
    ~TableRowClass_Js();

    virtual QString name() const;
    virtual QScriptValue prototype() const;
    virtual QScriptClass::QueryFlags queryProperty(const QScriptValue &obj, const QScriptString &name, QScriptClass::QueryFlags flags, uint *id);
    virtual QScriptValue::PropertyFlags propertyFlags(const QScriptValue &obj, const QScriptString &name, uint id);
    virtual QScriptValue property(const QScriptValue &obj, const QScriptString &name, uint id);
    virtual void setProperty(QScriptValue &obj, const QScriptString &name, uint id, const QScriptValue &value);
    virtual QScriptClassPropertyIterator *newIterator(const QScriptValue &obj);

    QScriptValue constructor();
    QScriptValue newInstance();
    QScriptValue newInstance(const Table::Row &tr);

    static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);
    static QScriptValue toScriptValue(QScriptEngine *eng, const Table::Row &ba);
    static void fromScriptValue(const QScriptValue &obj, Table::Row &ba);

private:
    QScriptString s_length;
    QScriptValue m_proto;
    QScriptValue m_ctor;
};


class TableRowPrototype_Js : public QObject, public QScriptable
{
    Q_OBJECT;
public:
    TableRowPrototype_Js(QObject *parent = 0);
    ~TableRowPrototype_Js();

public slots:
    QVariant valueOf() const;
    QString toString() const;
    QString nameOf(int i) const;
    Table::Row prev() const;
    Table::Row next() const;
};


};

#endif

/* End of file */
