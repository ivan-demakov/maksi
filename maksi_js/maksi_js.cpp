/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_js.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:50 2011
 *
 * @brief   javascript engine
 *
 */

#include "maksi_js.h"
#include "elem_js.h"

#include <maksi/xmlelem.h>
#include <maksi/maksi.h>

#include <QtPlugin>
#include <QTextStream>
#include <QStringList>
#include <QDateTime>



namespace maksi
{



Js_EnginePlugin::~Js_EnginePlugin()
{
}

QString Js_EnginePlugin::description() const
{
    return "JavaScript";
}

Engine *Js_EnginePlugin::newEngine(Maksi *maksi, const QString &name) const
{
    return new Js_Engine(maksi, name);
}



Js_Engine::Js_Engine(Maksi *maksi, const QString &name) :
    Engine(maksi, name),
    m_mutex(QMutex::Recursive),
    m_engine(new QScriptEngine()),
    m_log(0)
{
    m_log = new ScriptLog(m_engine, maksi);

    QScriptValue glob(m_engine->globalObject());

    QScriptValue fun = m_engine->newFunction(ScriptLog::notePrint);
    fun.setData(m_engine->newQObject(m_log));
    glob.setProperty("print", fun);

    fun = m_engine->newFunction(ScriptLog::warnPrint);
    fun.setData(m_engine->newQObject(m_log));
    glob.setProperty("warn", fun);

    fun = m_engine->newFunction(ScriptLog::notePrint);
    fun.setData(m_engine->newQObject(m_log));
    glob.setProperty("note", fun);

    fun = m_engine->newFunction(ScriptLog::infoPrint);
    fun.setData(m_engine->newQObject(m_log));
    glob.setProperty("info", fun);

    fun = m_engine->newFunction(ScriptLog::debugPrint);
    fun.setData(m_engine->newQObject(m_log));
    glob.setProperty("debug", fun);

    ElementClass_Js *ce = new ElementClass_Js(m_engine);
    glob.setProperty(ce->name(), ce->constructor());

    ContextClass_Js *cc = new ContextClass_Js(m_engine, maksi);
    glob.setProperty(cc->name(), cc->constructor());

    TypeClass_Js *ct = new TypeClass_Js(m_engine, maksi);
    glob.setProperty(ct->name(), ct->constructor());
}

Js_Engine::~Js_Engine()
{
    delete m_engine;
}

bool Js_Engine::evalCode(const QString &code, const QUrl &url, int line, QVariant *rval)
{
    //qDebug("Js_Engine::evalCode(%s)", qPrintable(code));

    QScriptValue val = m_engine->evaluate(code, url.toString(), line);
    if (m_engine->hasUncaughtException()) {
        QStringList ls = m_engine->uncaughtExceptionBacktrace();
        QScriptValue err = m_engine->uncaughtException();
        m_log->sendMessage(MESSAGE_ERROR, QString("%1\n  %2").arg(err.toString()).arg(ls.join("\n  ")));
        m_engine->clearExceptions();
        return false;
    }
    if (rval) {
        *rval = val.toVariant();
    }
    return true;
}

Script *Js_Engine::newScript(const QString &scriptName)
{
    return new Js_Script(maksi(), scriptName, this);
}

QString Js_Engine::toString(const MaksiElementPtr &val)
{
    //qDebug("Js_Engine::toString");

    QString result;
    if (val->elementToString(&result))
        return result;

    result = QString::fromLatin1("Element(");
    if (val) {
        result += val->elementXmlText();
    } else {
        result += QString::fromLatin1("null");
    }
    result += QString::fromLatin1(")");
    return result;
}


Js_Script::Js_Script(Maksi *maksi, const QString &name, Js_Engine *jse) :
    Script(maksi, name),
    m_jse(jse)
{
}

Js_Script::~Js_Script()
{
}

bool Js_Script::addArg(const QString &name, const QVariant &val, const MaksiElement *)
{
    //qDebug("arg %s", qPrintable(name));
    m_args.append(Js_Arg(name, val));
    return true;
}

void Js_Script::addCode(const QString &code, const MaksiElement *conf)
{
    //qDebug("code %s", qPrintable(code));
    m_progs.append(QScriptProgram(code, conf->elementSourceUrl().toString(), conf->elementSourceLine()));
}

Context *Js_Script::newContext()
{
    return new Js_Context(m_jse, m_args, m_progs, m_jse->log(), maksi());
}


Js_Context::Js_Context(Js_Engine *jse, QList<Js_Arg> args, QList<QScriptProgram> p, ScriptLog *l, Maksi *m) :
    m_prog(p),
    m_jse(jse),
    m_log(l),
    m_maksi(m)
{
    m_vars = m_jse->engine()->newObject();
    for (int i = 0; i < args.size(); ++i) {
        const Js_Arg &arg(args.at(i));
        m_vars.setProperty(arg.m_name, m_jse->engine()->toScriptValue(arg.m_val));
    }
}

Js_Context::~Js_Context()
{
}

QVariant Js_Context::arg(const QString &name)
{
    QScriptValue val(m_vars.property(name));
    return val.toVariant();
}

void Js_Context::setArg(const QString &name, const QVariant &val)
{
    //qDebug("Js_Context::setArg(%s, %s)", qPrintable(name), !val ? "[null]" : qPrintable(val->elementText()));
    m_vars.setProperty(name, m_jse->engine()->newVariant(val));
}

QVariant Js_Context::eval()
{
    QScriptValue rval;

    QScriptValue oldPrototype = m_vars.prototype();
    QScriptContext *ctx = m_jse->pushContext();
    m_vars.setPrototype(ctx->activationObject().prototype());
    ctx->setActivationObject(m_vars);

    for (int i = 0; i < m_prog.size(); ++i) {
        rval = m_jse->engine()->evaluate(m_prog.at(i));
        if (m_jse->engine()->hasUncaughtException()) {
            QScriptValue err = m_jse->engine()->uncaughtException();
            //QStringList ls = m_jse->engine()->uncaughtExceptionBacktrace();
            //m_log->sendMessage(MAKSI_MESSAGE_ERROR, QString("%1\n  %2").arg(err.toString()).arg(ls.join("\n  ")));
            m_log->sendMessage(MESSAGE_ERROR, QString("%1").arg(err.toString()));
            m_jse->engine()->clearExceptions();
            break;
        }
    }
    m_jse->popContext();
    m_vars.setPrototype(oldPrototype);

    if (rval.isValid())
        return rval.toVariant();
    return QVariant();
}


ContextClass_Js::ContextClass_Js(QScriptEngine *engine, Maksi *maksi) :
    QObject(engine),
    QScriptClass(engine),
    m_maksi(maksi)
{
    m_proto = engine->newQObject(new ContextPrototype_Js(this),
                                 QScriptEngine::QtOwnership,
                                 QScriptEngine::SkipMethodsInEnumeration
                                 | QScriptEngine::ExcludeSuperClassMethods
                                 | QScriptEngine::ExcludeSuperClassProperties);

    QScriptValue global = engine->globalObject();
    m_proto.setPrototype(global.property("Object").property("prototype"));

    m_ctor = engine->newFunction(construct, m_proto);
    m_ctor.setData(qScriptValueFromValue(engine, this));
}

ContextClass_Js::~ContextClass_Js()
{
    for (QHash<QString, Context*>::iterator i = m_sxs.begin(); i != m_sxs.end(); ++i) {
        delete i.value();
    }
}

QString ContextClass_Js::name() const
{
    return QString::fromLatin1("MaksiContext");
}

QScriptValue ContextClass_Js::prototype() const
{
    return m_proto;
}

QScriptValue ContextClass_Js::constructor()
{
    return m_ctor;
}

QScriptValue ContextClass_Js::construct(QScriptContext *ctx, QScriptEngine *engine)
{
    ContextClass_Js *cls = qscriptvalue_cast<ContextClass_Js*>(ctx->callee().data());
    if (!cls) {
        return QScriptValue();
    }
    if (ctx->argumentCount() < 1) {
        return QScriptValue();
    }


    QString name = ctx->argument(0).toString();
    Context *sx = cls->m_sxs.value(name);
    if (!sx) {
	sx = maksi::scriptContext(cls->m_maksi, name);
        if (sx) {
            cls->m_sxs.insert(name, sx);
            //qDebug("insert context class %s", qPrintable(name));
        }
    }

    if (sx) {
        QScriptValue val = engine->newObject(cls, engine->newVariant(QVariant::fromValue(sx)));
        //val.setPrototype(cls->prototype());
        return val;
    }
    return engine->undefinedValue();
}



ContextPrototype_Js::ContextPrototype_Js(QObject *parent) :
    QObject(parent)
{
}

ContextPrototype_Js::~ContextPrototype_Js()
{
}

QString ContextPrototype_Js::toString() const
{
    //QScriptValue val(thisObject().data());
    return "[object MaksiContext]";
}

QVariant ContextPrototype_Js::arg(const QString &name) const
{
    //qDebug("arg %s", qPrintable(name));
    Context *ctx(qscriptvalue_cast<Context*>(thisObject().data()));
    if (ctx)
        return ctx->arg(name);
    return QVariant();
}

void ContextPrototype_Js::setArg(const QString &name, QVariant val)
{
    //qDebug("ContextPrototype_Js::setArg: name=%s val=%s", qPrintable(name), !val ? "[null]" : qPrintable(val->elementText()));
    Context *ctx(qscriptvalue_cast<Context*>(thisObject().data()));
    if (ctx)
        ctx->setArg(name, val);
}

QVariant ContextPrototype_Js::eval()
{
    Context *ctx(qscriptvalue_cast<Context*>(thisObject().data()));
    if (ctx) {
        QVariant val(ctx->eval());
        //qDebug("val %s", val ? qPrintable(val->elementText()) : "[null]");
        return val;
    }
    return QVariant();
}


TypeClass_Js::TypeClass_Js(QScriptEngine *engine, Maksi *maksi) :
    QObject(engine),
    QScriptClass(engine),
    m_maksi(maksi)
{
    m_proto = engine->newQObject(new TypePrototype_Js(this),
                                 QScriptEngine::QtOwnership,
                                 QScriptEngine::SkipMethodsInEnumeration
                                 | QScriptEngine::ExcludeSuperClassMethods
                                 | QScriptEngine::ExcludeSuperClassProperties);

    QScriptValue global = engine->globalObject();
    m_proto.setPrototype(global.property("Object").property("prototype"));

    m_ctor = engine->newFunction(construct, m_proto);
    m_ctor.setData(qScriptValueFromValue(engine, this));
}

TypeClass_Js::~TypeClass_Js()
{
}

QString TypeClass_Js::name() const
{
    return QString::fromLatin1("MaksiType");
}

QScriptValue TypeClass_Js::prototype() const
{
    return m_proto;
}

QScriptValue TypeClass_Js::constructor()
{
    return m_ctor;
}

QScriptValue TypeClass_Js::construct(QScriptContext *ctx, QScriptEngine *engine)
{
    TypeClass_Js *cls = qscriptvalue_cast<TypeClass_Js*>(ctx->callee().data());
    if (!cls) {
        return QScriptValue();
    }
    if (ctx->argumentCount() < 1) {
        return QScriptValue();
    }

    name_t name;
    if (ctx->argumentCount() < 2) {
        name = xml::qname(ctx->argument(0).toString());
    } else {
        name = xml::qname(ctx->argument(0).toString(), ctx->argument(1).toString());
    }
    //qDebug("type: %s", qPrintable(xml::toString(name)));

    Type type = cls->m_maksi->type(name);
    if (type.isValid()) {
        QScriptValue val = engine->newObject(cls, engine->newVariant(QVariant::fromValue(type)));
        //val.setPrototype(cls->prototype());
        //qDebug("type=%s", qPrintable(val.toString()));
        return val;
    }
    return engine->undefinedValue();
}



TypePrototype_Js::TypePrototype_Js(QObject *parent) :
    QObject(parent)
{
}

TypePrototype_Js::~TypePrototype_Js()
{
}

QString TypePrototype_Js::toString() const
{
    return "[object MaksiType]";
}

MaksiElementPtr TypePrototype_Js::create(const QString &name, const QString &ns)
{
    Type type(qscriptvalue_cast<Type>(thisObject().data()));
    if (type.isValid()) {
        name_t xmlName;
        if (ns.isEmpty()) {
            xmlName = xml::qname(name);
        } else {
            xmlName = xml::qname(name, ns);
        }

        MaksiElementPtr val = type.createNew(0);
        val->setElementName(xmlName);
        return val;
    }
    return MaksiElementPtr();
}


MaksiElementPtr TypePrototype_Js::create(const QString &name)
{
    return create(name, QString());
}

}

 /* End of code */
