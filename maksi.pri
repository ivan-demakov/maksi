MAKSI_VERSION=3.0

DEFINES += MAKSI_VERSION=\\\"$${MAKSI_VERSION}\\\"

unix {
  VERSION = $${MAKSI_VERSION}
  DESTDIR = ../bin
  QMAKE_LIBDIR += ../bin
}
win32 {
  DESTDIR = ../win32
  QMAKE_LIBDIR += ../win32
}
win64 {
  DESTDIR = ../win64
  QMAKE_LIBDIR += ../win64
}
