/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_sql.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jun 10 01:01:03 2011
 *
 * @brief   sql engine
 *
 */

#ifndef MAKSI_SQL_H
#define MAKSI_SQL_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QQueue>

#include <maksi/engine.h>
#include <maksi/script.h>
#include <maksi/elem.h>


#if defined(_MSC_VER)
#  pragma comment(lib, "maksi")
#endif

namespace maksi
{

class Sql_EnginePlugin: public QObject, EnginePlugin
{
    Q_OBJECT;
    Q_INTERFACES(maksi::EnginePlugin);
    Q_PLUGIN_METADATA(IID MaksiEnginePlugin_IID FILE "maksi-sql.json");

public:
    ~Sql_EnginePlugin();

    virtual QString description() const override;
    virtual Engine *newEngine(Maksi *maksi, const QString &name) const override;
};


class Sql_Engine : public Engine
{
    Q_OBJECT

public:
    Sql_Engine(Maksi *maksi, const QString &name);
    ~Sql_Engine();

    virtual Script *newScript(const QString &scriptName) override;
    virtual bool preinitEngine(const MaksiElement *conf) override;
    virtual bool evalCode(const QString &code, const QUrl &url, int line, QVariant *val) override;

protected:
    bool evalQuery(const QString &conn, const QString &text);

    QString m_connection;
};


struct Sql_Arg {
    explicit Sql_Arg(const QString &name, QSql::ParamType type) : m_name(name), m_type(type) {}
    explicit Sql_Arg(const QString &name, QSql::ParamType type, const QVariant &val) : m_name(name), m_val(val), m_type(type) {}

    QString m_name;
    QVariant m_val;
    QSql::ParamType m_type;
};

class Sql_Script : public Script
{
public:
    Sql_Script(Maksi *maksi, const QString &name, Sql_Engine *engine);
    ~Sql_Script();

    virtual Context *newContext() override;
    virtual bool addArg(const QString &name, const QVariant &val, const MaksiElement *conf) override;
    virtual void addCode(const QString &code, const MaksiElement *conf) override;

private:
    Sql_Engine *m_engine;
    QList<Sql_Arg> m_args;
    QString m_connection;
    QString m_query;

    friend class Sql_Context;
};


class Sql_Context : public Context
{
public:
    Sql_Context(Maksi *maksi, Sql_Script *script);
    ~Sql_Context();

    virtual QVariant arg(const QString &name);
    virtual void setArg(const QString &name, const QVariant &val);
    virtual QVariant eval();

    void setArg(const QString &name, const QVariant &val, QSql::ParamType type);
    void sendMessage(message_t type, const QString &msg);

private:
    Sql_Script *m_script;
    Maksi *m_maksi;
    QSqlDatabase m_sql;
    QSqlQuery m_query;
};


class Sql_QueryElement : public MaksiElement_List
{
    Q_OBJECT

public:
    class TypeFactory : public ListTypeFactory {
    public:
        TypeFactory();
        virtual MaksiElementPtr createNew(Type type, const MaksiElement *from) const;
    };

    static Type schemaType();

    Sql_QueryElement(name_t name, const QSqlQuery &q, Type type=schemaType()) : MaksiElement_List(name, type), m_query(q) {}
    virtual ~Sql_QueryElement();

    virtual bool isElementValid() const;
    virtual bool isElementSimple() const;
    virtual bool isElementEmpty() const;

    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;

    virtual int elementCount() const;
    virtual MaksiElementPtr element(int index) const;
    virtual bool appendElement(const MaksiElementPtr &value);

public slots:
    QString nameOf(int pos);
    int indexOf(QString field);
    bool first();
    bool last();
    bool next();
    bool previous();
    bool seek(int pos);

protected:
    QSqlQuery m_query;
};

};

#endif

/* End of file */
