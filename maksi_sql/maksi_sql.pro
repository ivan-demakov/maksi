include(../maksi.pri)

TEMPLATE = lib
CONFIG += plugin

QT += sql network
QT -= gui

unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksi_sql)
  CONFIG += debug
}
win32 {
  TARGET = maksi_sql
  LIBS += -lmaksi
  CONFIG += release
}

INCLUDEPATH += ..

HEADERS = maksi_sql.h
SOURCES = maksi_sql.cpp

OTHER_FILES  += maksi-sql.json
