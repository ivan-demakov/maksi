/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_sql.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jun 10 01:02:59 2011
 *
 * @brief   sql engine
 *
 */

#include "maksi_sql.h"

#include <maksi/elem.h>
#include <maksi/maksi.h>
#include <maksi/xml.h>
#include <maksi/package.h>

#include <QtPlugin>
#include <QFileInfo>
#include <QUrl>
#include <QSqlRecord>
#include <QTextStream>


namespace maksi
{

Sql_EnginePlugin::~Sql_EnginePlugin()
{
}

QString Sql_EnginePlugin::description() const
{
    return "SQL";
}

Engine *Sql_EnginePlugin::newEngine(Maksi *maksi, const QString &name) const
{
    return new Sql_Engine(maksi, name);
}


Sql_Engine::Sql_Engine(Maksi *maksi, const QString &name) :
    Engine(maksi, name)
{
}

Sql_Engine::~Sql_Engine()
{
}

bool Sql_Engine::evalQuery(const QString &conn, const QString &text)
{
    QSqlDatabase sql(QSqlDatabase::database(conn, true));
    if (sql.isValid()) {
        QSqlQuery query(sql);
        query.setForwardOnly(true);
        if (query.exec(text)) {
            return true;
        }

        QSqlError err = query.lastError();
        if (err.isValid())
            maksi()->sendMessage(MESSAGE_ERROR, QString("sql error in connection '%1': %2").arg(conn).arg(err.text()));
        else
            maksi()->sendMessage(MESSAGE_ERROR, QString("sql error in connection '%1'").arg(conn));
    } else {
        maksi()->sendMessage(MESSAGE_ERROR, QString("sql connection '%1' is not valid").arg(conn));
        QSqlError err = sql.lastError();
        if (err.isValid())
            maksi()->sendMessage(MESSAGE_ERROR, QString("last sql error: %1").arg(err.text()));
    }
    return false;
}

bool Sql_Engine::preinitEngine(const MaksiElement *conf)
{
    static name_t maksi_connection = xml::qname("connection");
    static name_t maksi_name = xml::qname("name");
    static name_t maksi_type = xml::qname("type");
    static name_t maksi_db = xml::qname("db");
    static name_t maksi_host = xml::qname("host");
    static name_t maksi_port = xml::qname("port");
    static name_t maksi_user = xml::qname("user");
    static name_t maksi_pass = xml::qname("password");
    static name_t maksi_options = xml::qname("options");
    static name_t maksi_load = xml::qname("load");
    static name_t maksi_href = xml::qname("href");
    static name_t maksi_eval = xml::qname("eval");

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (node && node->elementName() == maksi_connection) {
            QString name;
            if (node->hasAttribute(maksi_name) && node->attribute(maksi_name)->elementToString(&name)) {
                /* nothing */
            }

            QString type;
            if (node->hasAttribute(maksi_type) && node->attribute(maksi_type)->elementToString(&type)) {
                type = type.toUpper();
            } else {
                maksi()->sendMessage(MESSAGE_ERROR, node.data(), tr("connection has not required attribute type"));
                continue;
            }
            if (type == "POSTGRE" || type == "POSTGRES" || type == "POSTGRESQL")
                type = "QPSQL";
            else if (type == "ORACLE")
                type = "QOCI";
            else if (type == "SYBASE")
                type = "QTDS";
            else if (type == "INTERBASE")
                type = "QIBASE";
            else if (type.at(0) != 'Q')
                type.prepend('Q');

            QString db = node->attribute(maksi_db)->elementText();
            if (db.isEmpty()) {
                maksi()->sendMessage(MESSAGE_ERROR, node.data(), tr("connection has not required attribute db"));
                continue;
            }
            if (type == "QSQLITE") {
                QFileInfo fileInfo(db);
                if (fileInfo.isRelative()) {
                    QUrl url(node->elementSourceUrl());
                    if (url.isValid() && url.scheme() == "file") {
                        QFileInfo confInfo(url.path());
                        db = confInfo.absolutePath() + "/" + db;
                    }
                }
            }

            QSqlDatabase sql = QSqlDatabase::addDatabase(type, name);
            sql.setDatabaseName(db);

            QString host;
            if (node->hasAttribute(maksi_host) && node->attribute(maksi_host)->elementToString(&host)) {
                sql.setHostName(host);
            }

            int32_t port;
            if (node->hasAttribute(maksi_port) && node->attribute(maksi_port)->elementToInt(&port) && port > 0) {
                sql.setPort(port);
            }

            QString user;
            if (node->hasAttribute(maksi_user) && node->attribute(maksi_user)->elementToString(&user)) {
                sql.setUserName(user);
            }
            QString pass;
            if (node->hasAttribute(maksi_pass) && node->attribute(maksi_pass)->elementToString(&pass)) {
                sql.setPassword(pass);
                for (int i = 0; i < pass.size(); ++i)
                    pass[i] = QChar::fromLatin1('X');
            }
            QString options;
            if (node->hasAttribute(maksi_options) && node->attribute(maksi_options)->elementToString(&options)) {
                sql.setConnectOptions(options);
            }

            QString save_connection(m_connection);
            for (int k = 0; k < node->elementCount(); ++k) {
                MaksiElementPtr elem = node->element(i);
                if (elem) {
                    m_connection = name;

                    if (elem->elementName() == maksi_load) {
                        if (elem->hasAttribute(maksi_href)) {
                            QString href = elem->attribute(maksi_href)->elementText();
                            QUrl url(href, QUrl::StrictMode);
                            QByteArray data;
                            if (maksi()->loadPackageData(url, &data)) {
                                QTextStream stream(&data, QIODevice::ReadOnly);
                                stream.setAutoDetectUnicode(true);
                                QString code = stream.readAll();

                                evalCode(code, url, 0, 0);
                            } else {
                                maksi()->sendMessage(MESSAGE_WARN, tr("cannot find url: %1").arg(url.toString()));
                            }
                        }
                    } else if (elem->elementName() == maksi_eval) {
                        QString text = elem->elementText();
                        evalQuery(name, text);
                    }
                }
            }
            m_connection = save_connection;
        }
    }
    return true;
}

bool Sql_Engine::evalCode(const QString &code, const QUrl &, int, QVariant *)
{
    return evalQuery(m_connection, code);
}

Script *Sql_Engine::newScript(const QString &scriptName)
{
    return new Sql_Script(maksi(), scriptName, this);
}


Sql_Script::Sql_Script(Maksi *maksi, const QString &name, Sql_Engine *engine) :
    Script(maksi, name),
    m_engine(engine)
{
}

Sql_Script::~Sql_Script()
{
}

bool Sql_Script::addArg(const QString &name, const QVariant &val, const MaksiElement *conf)
{
    static name_t maksi_dir = xml::qname("direction");

    QSql::ParamType sql_type = QSql::In;
    if (conf->hasAttribute(maksi_dir)) {
        QString dir;
        if (conf->attribute(maksi_dir)->elementToString(&dir)) {
            if (dir.compare("Out", Qt::CaseInsensitive) == 0)
                sql_type = QSql::Out;
            else if (dir.compare("InOut", Qt::CaseInsensitive) == 0)
                sql_type = QSql::InOut;
        }
    }

    m_args.append(Sql_Arg(name, sql_type, val));
    return true;
}

void Sql_Script::addCode(const QString &code, const MaksiElement *conf)
{
    static name_t maksi_connection = xml::qname("connection");

    if (m_query.isEmpty()) {
        m_query = code;
        if (conf->hasAttribute(maksi_connection)) {
            conf->attribute(maksi_connection)->elementToString(&m_connection);
        }
    } else {
        maksi()->sendMessage(MESSAGE_ERROR, conf, tr("sql engine do not support multiple eval sections"));
    }
}

Context *Sql_Script::newContext()
{
    return new Sql_Context(maksi(), this);
}


Sql_Context::Sql_Context(Maksi *maksi, Sql_Script *script) :
    m_script(script),
    m_maksi(maksi),
    m_sql(QSqlDatabase::database(m_script->m_connection, true)),
    m_query(m_sql)
{
    if (m_sql.isValid()) {
        m_query.setForwardOnly(true);
        if (m_query.prepare(m_script->m_query)) {
            for (int i = 0; i < m_script->m_args.size(); ++i) {
                setArg(m_script->m_args.at(i).m_name, m_script->m_args.at(i).m_val, m_script->m_args.at(i).m_type);
            }
        } else {
            QSqlError err = m_query.lastError();
            if (err.isValid())
                sendMessage(MESSAGE_ERROR, QString("sql error in connection '%1': %2").arg(script->m_connection).arg(err.text()));
            else
                sendMessage(MESSAGE_ERROR, QString("sql error in connection '%1'").arg(script->m_connection));
        }
    } else {
        sendMessage(MESSAGE_ERROR, QString("sql connection '%1' is not valid").arg(script->m_connection));
        QSqlError err = m_sql.lastError();
        if (err.isValid())
            sendMessage(MESSAGE_ERROR, QString("last sql error: %1").arg(err.text()));
    }
}

Sql_Context::~Sql_Context()
{
}

void Sql_Context::sendMessage(message_t type, const QString &msg)
{
    if (m_maksi)
        m_maksi->sendMessage(type, msg);
}

QVariant Sql_Context::arg(const QString &name)
{
    QString internal_name(":");
    internal_name += name;
    return m_query.boundValue(internal_name);
}

void Sql_Context::setArg(const QString &name, const QVariant &val)
{
    for (int i = 0; i < m_script->m_args.size(); ++i) {
        if (m_script->m_args.at(i).m_name == name) {
            setArg(name, val, m_script->m_args.at(i).m_type);
            return;
        }
    }
    setArg(name, val, QSql::In);
}

void Sql_Context::setArg(const QString &name, const QVariant &val, QSql::ParamType type)
{
    QString internal_name(":");
    internal_name += name;
    m_query.bindValue(internal_name, val, type);
}

QVariant Sql_Context::eval()
{
    static name_t xmlName = xml::qname("SqlQuery");

    if (m_query.exec()) {
        if (m_query.isSelect()) {
            MaksiElementPtr e(new Sql_QueryElement(xmlName, m_query));
            //qDebug("Sql_Context::eval(): isSelect");
            return QVariant::fromValue(e);
        } else {
            //qDebug("Sql_Context::eval(): isNotSelect");
            return m_query.numRowsAffected();
        }
    }

    QSqlError err = m_query.lastError();
    if (err.isValid())
        sendMessage(MESSAGE_ERROR, QString("last sql error: %1").arg(err.text()));
    else
        sendMessage(MESSAGE_ERROR, QString("last sql error: unknown error"));
    return QVariant();
}


Sql_QueryElement::TypeFactory::TypeFactory() :
    ListTypeFactory(Type::anyType())
{
}

MaksiElementPtr Sql_QueryElement::TypeFactory::createNew(Type , const MaksiElement *) const
{
    // This is internal object, so it cannot be created
    return MaksiElementPtr();
}

Type Sql_QueryElement::schemaType()
{
    static Type type;
    if (!type.isValid()) {
        static QSharedPointer<TypeFactory> factory(new TypeFactory());
        type.setFactory(factory);
    }
    return type;
}

Sql_QueryElement::~Sql_QueryElement()
{
}

bool Sql_QueryElement::isElementValid() const
{
    return m_query.isValid();
}

bool Sql_QueryElement::isElementSimple() const
{
    return false;
}

bool Sql_QueryElement::isElementEmpty() const
{
    return false;
}

bool Sql_QueryElement::elementToString(QString *str) const
{
    if (str)
        *str = QString::fromLatin1("[Sql Query]");
    return true;
}

QString Sql_QueryElement::elementText() const
{
    return QString::fromLatin1("[Sql Query]");
}

int Sql_QueryElement::elementCount() const
{
    QSqlRecord rec = m_query.record();
    return rec.count();
}

MaksiElementPtr Sql_QueryElement::element(int index) const
{
    QVariant val = m_query.value(index);
    //qDebug("val=%s type=%s", qPrintable(val.toString()), qPrintable(val.typeName()));
    return MaksiElement::newVariant(val);
}

bool Sql_QueryElement::appendElement(const MaksiElementPtr &)
{
    return false;
}

QString Sql_QueryElement::nameOf(int pos)
{
    QSqlRecord rec = m_query.record();
    return rec.fieldName(pos);
}

int Sql_QueryElement::indexOf(QString fld)
{
    QSqlRecord rec = m_query.record();
    return rec.indexOf(fld);
}

bool Sql_QueryElement::first()
{
    return m_query.first();
}

bool Sql_QueryElement::last()
{
    return m_query.last();
}

bool Sql_QueryElement::next()
{
    return m_query.next();
}

bool Sql_QueryElement::previous()
{
    return m_query.previous();
}

bool Sql_QueryElement::seek(int pos)
{
    return m_query.seek(pos);
}


};


/* End of code */
