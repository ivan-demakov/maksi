include(../maksi.pri)

TEMPLATE = lib
TARGET = $$qtLibraryTarget(maksiXQ)
VERSION = $${MAKSI_VERSION}

CONFIG += plugin
QT += xmlpatterns
QT -= gui

INCLUDEPATH += ..
DESTDIR = ../maksi

HEADERS = maksi_xq.h script_xq.h
SOURCES = maksi_xq.cpp script_xq.cpp
