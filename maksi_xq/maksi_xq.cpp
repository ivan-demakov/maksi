/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_xq.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:50 2011
 *
 * @brief   XQuery engine
 *
 */

#include "maksi_xq.h"
#include "script_xq.h"

#include <maksi/xml.h>

#include <QtPlugin>


Q_EXPORT_PLUGIN2(maksiXQ, Maksi_Xq_EnginePlugin);

Maksi_Xq_EnginePlugin::~Maksi_Xq_EnginePlugin()
{
}

QString Maksi_Xq_EnginePlugin::description() const
{
    return "XQuery";
}

maksi::ScriptEngine *Maksi_Xq_EnginePlugin::newEngine(maksi::Maksi *maksi, const QString &name) const
{
    return new maksi::Xq_Engine(maksi, name);
}


namespace maksi
{

Xq_Engine::Xq_Engine(Maksi *maksi, const QString &name) :
    ScriptEngine(maksi, name)
{
}

Xq_Engine::~Xq_Engine()
{
}

void Xq_Engine::initEngine(Worker *worker, const Element *conf)
{
    static QXmlName maksi_load = xml::qname("load");
    static QXmlName maksi_href = xml::qname("href");
    static QXmlName maksi_eval = xml::qname("eval");

    for (int i = 0; i < conf->countElements(); i++) {
        QVariant elem = conf->element(i);
        if (elem.canConvert<element_t>()) {
            element_t node = elem.value<element_t>();

            if (node->elementName() == maksi_load) {
                if (node->hasAttribute(maksi_href)) {
                    QString href = node->attribute(maksi_href).toString();
                    QUrl url(href, QUrl::StrictMode);
                    if (url.isValid()) {
                        if (url.isRelative()) {
                            QUrl base(node->elementSourceUrl());
                            if (base.isValid())
                                url = base.resolved(url);
                        }

                        Xq_ScriptContext ctx(m_maksi, worker, url);
                        ctx.eval();
                    } else {
                        worker->sendMessage(MESSAGE_ERROR, node, tr("invalid url '%1'").arg(href));
                    }
                }
            } else if (node->elementName() == maksi_eval) {
                QString text = node->elementText();
                Xq_ScriptContext ctx(m_maksi, worker, text, node->elementSourceUrl());
                ctx.eval();
            }
        }
    }

    engineInited();
}

Script *Xq_Engine::newScript(const QString &scriptName)
{
    return new Xq_Script(m_maksi, scriptName, this);
}

};

 /* End of code */
