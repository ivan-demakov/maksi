/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    script_xq.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Sun Apr  3 12:44:20 2011
 *
 * @brief   XQuery
 *
 */

#include "script_xq.h"

#include <maksi/xml.h>
#include <maksi/maksi.h>

#include <QBuffer>
#include <QXmlQuery>


namespace maksi
{


Xq_Script::Xq_Script(Maksi *maksi, const QString &name, Xq_Engine *engine) :
    Script(maksi, name),
    m_engine(engine)
{
}

Xq_Script::~Xq_Script()
{
}

void Xq_Script::initScript(Worker *worker, const Element *conf)
{
    static QXmlName maksi_arg = xml::qname("arg");
    static QXmlName maksi_name = xml::qname("name");
    static QXmlName maksi_eval = xml::qname("eval");

    for (int i = 0; i < conf->countElements(); ++i) {
        QVariant val = conf->element(i);
        if (val.canConvert<maksi::element_t>()) {
            element_t e = val.value<element_t>();
            if (e->elementName() == maksi_arg) {
                if (e->hasAttribute(maksi_name)) {
                    QString name = e->attribute(maksi_name).toString();
                    result_t res = getValue(worker, e, &val);
                    if (RESULT_OK == res) {
                        m_args.append(Arg(name, val));
                    } else {
                        m_args.append(Arg(name));
                    }
                } else {
                    worker->sendMessage(MESSAGE_ERROR, e, tr("arg has not reqiued attribute 'name'"));
                }
            } else if (e->elementName() == maksi_eval) {
                m_prog = e->elementText();
                m_src = e->elementSourceUrl();
            }
        }
    }
}

ScriptContext *Xq_Script::newContext(Worker *worker)
{
    Xq_ScriptContext *q = new Xq_ScriptContext(m_maksi, worker);
    for (int i = 0; i < m_args.size(); ++i) {
        q->setArg(m_args.at(i).m_name, m_args.at(i).m_val);
    }
    q->setQuery(m_prog, m_src);
    return q;
}


Xq_ScriptContext::Xq_ScriptContext(Maksi *maksi, Worker *worker) :
    m_query(QXmlQuery::XQuery10, xml::namePool),
    m_worker(worker),
    m_maksi(maksi)
{
    m_query.setMessageHandler(this);
    if (m_worker)
        m_query.setNetworkAccessManager(m_worker->network());
}

Xq_ScriptContext::Xq_ScriptContext(Maksi *maksi, Worker *worker, const QUrl &src) :
    m_query(QXmlQuery::XQuery10, xml::namePool),
    m_worker(worker),
    m_maksi(maksi)
{
    m_query.setMessageHandler(this);
    if (m_worker)
        m_query.setNetworkAccessManager(m_worker->network());
    m_query.setQuery(src, src);
}

Xq_ScriptContext::Xq_ScriptContext(Maksi *maksi, Worker *worker, const QString &query, const QUrl &src) :
    m_query(QXmlQuery::XQuery10, xml::namePool),
    m_worker(worker),
    m_maksi(maksi)
{
    m_query.setMessageHandler(this);
    if (m_worker)
        m_query.setNetworkAccessManager(m_worker->network());
    m_query.setQuery(query, src);
}

Xq_ScriptContext::~Xq_ScriptContext()
{
}

QVariant Xq_ScriptContext::arg(const QString &name)
{
    return QVariant();
}

void Xq_ScriptContext::setArg(const QString &name, const QVariant &val)
{
    m_query.bindVariable(name, val);
}

QVariant Xq_ScriptContext::eval()
{
    QString res;
    m_query.evaluateTo(&res);
    return res;
}

void Xq_ScriptContext::handleMessage(QtMsgType type, const QString &description, const QUrl &/*identifier*/, const QSourceLocation &source)
{
    if (m_worker) {
        switch (type) {
        case QtDebugMsg:
            m_worker->sendMessage(MESSAGE_DEBUG, QString("%1:%2: %3").arg(source.uri().toString()).arg(source.line()).arg(description));
            break;

        case QtWarningMsg:
            m_worker->sendMessage(MESSAGE_WARN, QString("%1:%2: %3").arg(source.uri().toString()).arg(source.line()).arg(description));
            break;

        default:
            m_worker->sendMessage(MESSAGE_ERROR, QString("%1:%2: %3").arg(source.uri().toString()).arg(source.line()).arg(description));
            break;
        }
    } else if (m_maksi) {
        switch (type) {
        case QtDebugMsg:
            m_maksi->sendMessage(MESSAGE_DEBUG, QString("%1:%2: %3").arg(source.uri().toString()).arg(source.line()).arg(description));
            break;

        case QtWarningMsg:
            m_maksi->sendMessage(MESSAGE_WARN, QString("%1:%2: %3").arg(source.uri().toString()).arg(source.line()).arg(description));
            break;

        default:
            m_maksi->sendMessage(MESSAGE_ERROR, QString("%1:%2: %3").arg(source.uri().toString()).arg(source.line()).arg(description));
            break;
        }
    }
}

};


/* End of code */
