/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_xq.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:43 2011
 *
 * @brief   XQuery engine
 *
 */

#ifndef MAKSI_XQ_H
#define MAKSI_XQ_H

#include <QObject>

#include <maksi/engine.h>


class Maksi_Xq_EnginePlugin: public QObject, maksi::ScriptEnginePlugin
{
    Q_OBJECT
    Q_INTERFACES(maksi::ScriptEnginePlugin);
public:
    ~Maksi_Xq_EnginePlugin();

    virtual QString description() const;
    virtual maksi::ScriptEngine *newEngine(maksi::Maksi *maksi, const QString &name) const;
};


namespace maksi
{

class Xq_Engine : public ScriptEngine
{
    Q_OBJECT
    Q_DISABLE_COPY(Xq_Engine);

public:
    Xq_Engine(Maksi *maksi, const QString &name);
    ~Xq_Engine();

    virtual void initEngine(Worker *worker, const Element *conf);
    virtual Script *newScript(const QString &scriptName);
};

};

#endif

/* End of file */
