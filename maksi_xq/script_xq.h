/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    script_xq.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri May 21 23:51:16 2010
 *
 * @brief   XQuery
 *
 */

#ifndef SCRIPT_XQ_H
#define SCRIPT_XQ_H


#include "maksi_xq.h"

#include <maksi/script.h>

#include <QAbstractMessageHandler>
#include <QXmlQuery>


namespace maksi
{

class Xq_Script : public Script
{
public:
    Xq_Script(Maksi *maksi, const QString &name, Xq_Engine *engine);
    ~Xq_Script();

    struct Arg {
        explicit Arg(const QString &name) : m_name(name) {}
        explicit Arg(const QString &name, const QVariant &val) : m_name(name), m_val(val) {}

        QString m_name;
        QVariant m_val;
    };

    virtual void initScript(Worker *worker, const Element *conf);
    virtual ScriptContext *newContext(Worker *worker);

private:
    Xq_Engine *m_engine;
    QList<Arg> m_args;
    QString m_prog;
    QUrl m_src;
};


class Xq_ScriptContext : public ScriptContext, public QAbstractMessageHandler
{
public:
    Xq_ScriptContext(Maksi *maksi, Worker *worker);
    Xq_ScriptContext(Maksi *maksi, Worker *worker, const QUrl &url);
    Xq_ScriptContext(Maksi *maksi, Worker *worker, const QString &query, const QUrl &src);
    ~Xq_ScriptContext();

    void setQuery(const QString &query, const QUrl &src) { m_query.setQuery(query, src); }

    virtual QVariant arg(const QString &name);
    virtual void setArg(const QString &name, const QVariant &val);
    virtual QVariant eval();

    virtual void handleMessage(QtMsgType type, const QString &description, const QUrl &identifier, const QSourceLocation &sourceLocation);

private:
    QXmlQuery m_query;
    Worker *m_worker;
    Maksi *m_maksi;
};

};

#endif

/* End of file */
