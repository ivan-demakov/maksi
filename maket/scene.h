/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    scene.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Sun Oct 17 09:16:54 2010
 *
 * @brief   scene
 *
 */

#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QSet>
#include <QUuid>

#include <maksi/elem.h>
#include <maksi/schema.h>

#include "defs.h"


//! maket namespace
namespace maket
{

class Maket;
class View;
class LayerElement;
class Graphics;


//! The Scene class provides interface to the maket scene
class MAKET_EXPORT Scene : public QGraphicsScene, public maksi::Element
{
    Q_OBJECT
    Q_DISABLE_COPY(Scene);
public:

    class TypeFactory : public maksi::StructTypeFactory
    {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    Scene(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~Scene();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);
    virtual QVariant appendElement(const QVariant &value);

    /** Get list of layers
     *
     * @return list of layers
     */
    QList<LayerElement*> layers();

    void loadGraphics(const QString &url);
    void clearGraphics();

    //Graphics *addShape(const QString &script_id);

    Maket *maket() { return m_maket; }

signals:
    void startGraphicsLoading();
    void endGraphicsLoading();
    void graphicsLoading(QVariant val);

private slots:
    void loadGraphics(QVariant val);

private:
    enum select_t {
        SELECT_IN,
        SELECT_OUT
    };

    void emitStartGraphicsLoading() { emit startGraphicsLoading(); }
    void emitEndGraphicsLoading() { emit endGraphicsLoading(); }
    void emitGraphicsLoading(QVariant val) { emit graphicsLoading(val); }

    // helpers for Graphics
    LayerElement *getLayerByName(QString name, QList<LayerElement*> ls);
    LayerElement *getGraphicsLayer(Graphics *pg, QList<LayerElement*> ls);
    bool setGraphicsLayer(Graphics *pg, LayerElement *layer);
    bool setGraphicsState(Graphics *item, const QString &state);

    // helpers for View
    void moveSelected(qreal dx, qreal dy);
    bool hasSelected() const;
    void selectItems(const View *view, const QRect &rect, Qt::ItemSelectionMode mode, select_t type);
    QVariantList itemsAt(const View *view, bool selected, const QRect &rect, Qt::ItemSelectionMode mode);

    Maket *m_maket;
    QSet<Graphics*> m_selected;
    maksi::schema_type_t m_graphicsType;
    QMap<QUuid, maksi::element_t> m_graphics;

    friend class View;
    friend class Graphics;
    friend class LoadGraphicsJob;
};


};

Q_DECLARE_METATYPE(maket::Scene*)

#endif

/* End of file */
