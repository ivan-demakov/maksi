/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    brush.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Jun 29 00:40:42 2011
 *
 * @brief   brushes
 *
 */

#include "brush.h"
#include "maket.h"

#include <maksi/conf.h>
#include <maksi/loadjob.h>

#include <QBitmap>


namespace maket
{

BrushElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t BrushElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<BrushElement> elem(new BrushElement(m_maket, conf.name(), type));
        *value = maksi::fromElement(elem);
    }
    return maksi::RESULT_OK;
}

BrushElement::BrushElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    maksi::Element(name, type),
    m_maket(maket)
{
}

BrushElement::~BrushElement()
{
}

QColor BrushElement::getColor()
{
    if (attributeExists("color"))
        return m_maket->color(attribute("color").toString());

    for (int i = 0; i < countElements(); ++i) {
        QVariant val = element(i);
        if (val.canConvert<maksi::element_t>()) {
            maksi::element_t elem = val.value<maksi::element_t>();
            val = elem->elementValue();
        }
        if (val.canConvert<QColor>()) {
            return val.value<QColor>();
        }
    }
    return QColor();
}

QBrush BrushElement::getBrush()
{
    if (attributeExists("ref"))
        return m_maket->brush(attribute("ref").toString());

    if (attributeExists("style")) {
        QString style(attribute("style").toString());

        if (style == "no-brush") {
            return QBrush(Qt::NoBrush);
        } else if (style == "solid") {
            return QBrush(getColor(), Qt::SolidPattern);
        } else if (style == "hlines") {
            return QBrush(getColor(), Qt::HorPattern);
        } else if (style == "vlines") {
            return QBrush(getColor(), Qt::VerPattern);
        } else if (style == "cross") {
            return QBrush(getColor(), Qt::CrossPattern);
        } else if (style == "bdiag") {
            return QBrush(getColor(), Qt::BDiagPattern);
        } else if (style == "fdiag") {
            return QBrush(getColor(), Qt::FDiagPattern);
        } else if (style == "diagcross") {
            return QBrush(getColor(), Qt::DiagCrossPattern);
        }
    } else {
        for (int i = 0; i < countElements(); ++i) {
            QVariant val = element(i);
            if (val.canConvert<maksi::element_t>()) {
                maksi::element_t elem = val.value<maksi::element_t>();
                val = elem->elementValue();
            }

            if (val.canConvert<QColor>()) {
                return QBrush(val.value<QColor>(), Qt::SolidPattern);
            } else if (val.canConvert<QLinearGradient>()) {
                return QBrush(val.value<QLinearGradient>());
            } else if (val.canConvert<QRadialGradient>()) {
                return QBrush(val.value<QRadialGradient>());
            } else if (val.canConvert<TextureElement::Value>()) {
                TextureElement::Value data(val.value<TextureElement::Value>());
                if (data.href.isValid()) {
                    maksi::LoadJob *job = new maksi::LoadJob(data.href, false);
                    m_maket->maksi()->startJob(job);
                    job->wait();

                    maksi::result_t res = job->result();
                    QByteArray imageData(job->value().toByteArray());
                    delete job;

                    if (res == maksi::RESULT_OK) {
                        QImage image;
                        if (image.loadFromData(imageData)) {
                            if (image.depth() == 1 && data.color.isValid()) {
                                return QBrush(data.color, QBitmap::fromImage(image));
                            } else {
                                return QBrush(image);
                            }
                        }
                    }
                }
            }
        }

        if (attributeExists("color"))
            return QBrush(m_maket->color(attribute("color").toString()), Qt::SolidPattern);
    }
    return QBrush();
}

QVariant BrushElement::elementValue()
{
    return QVariant::fromValue(getBrush());
}


GradientStopElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GradientStopElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        maksi::element_t elem(new GradientStopElement(m_maket, conf.name(), type));
        *value = QVariant::fromValue(elem);
    }
    return maksi::RESULT_OK;
}

GradientStopElement::GradientStopElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    maksi::Element(name, type),
    m_maket(maket)
{
}

GradientStopElement::~GradientStopElement()
{
}

QVariant GradientStopElement::elementValue()
{
    Value data;
    data.offset = attribute("offset").toReal();

    if (attributeExists("color")) {
        data.color = m_maket->color(attribute("color").toString());
    } else {
        for (int i = 0; i < countElements(); ++i) {
            QVariant val = element(i);
            if (val.canConvert<maksi::element_t>()) {
                maksi::element_t elem = val.value<maksi::element_t>();
                val = elem->elementValue();
            }
            if (val.canConvert<QColor>()) {
                data.color = val.value<QColor>();
                break;
            }
        }
    }

    return QVariant::fromValue(data);
}


LinearGradientElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t LinearGradientElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        maksi::element_t elem(new LinearGradientElement(m_maket, conf.name(), type));
        *value = QVariant::fromValue(elem);
    }
    return maksi::RESULT_OK;
}

LinearGradientElement::LinearGradientElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    maksi::Element(name, type),
    m_maket(maket)
{
}

LinearGradientElement::~LinearGradientElement()
{
}

QVariant LinearGradientElement::elementValue()
{
    return QVariant::fromValue(getGradient());
}

QLinearGradient LinearGradientElement::getGradient()
{
    QLinearGradient g(attribute("x1").toReal(), attribute("y1").toReal(),
                      attribute("x2").toReal(), attribute("y2").toReal());

    QString spread = attribute("spread").toString();
    if (spread == "pad")
        g.setSpread(QGradient::PadSpread);
    else if (spread == "repeat")
        g.setSpread(QGradient::RepeatSpread);
    else if (spread == "reflect")
        g.setSpread(QGradient::ReflectSpread);

    for (int n = 0; n < countElements(); ++n) {
        QVariant val = element(n);
        if (val.canConvert<maksi::element_t>()) {
            maksi::element_t elem = val.value<maksi::element_t>();
            val = elem->elementValue();
        }
        if (val.canConvert<GradientStopElement::Value>()) {
            GradientStopElement::Value v(val.value<GradientStopElement::Value>());
            g.setColorAt(v.offset, v.color);
        }
    }
    return g;
}


RadialGradientElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t RadialGradientElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        maksi::element_t elem(new RadialGradientElement(m_maket, conf.name(), type));
        *value = QVariant::fromValue(elem);
    }
    return maksi::RESULT_OK;
}

RadialGradientElement::RadialGradientElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    maksi::Element(name, type),
    m_maket(maket)
{
}

RadialGradientElement::~RadialGradientElement()
{
}

QVariant RadialGradientElement::elementValue()
{
    return QVariant::fromValue(getGradient());
}

QRadialGradient RadialGradientElement::getGradient()
{
    QRadialGradient g(attribute("cx").toReal(), attribute("cy").toReal(),
                      attribute("r").toReal(),
                      attribute("fx").toReal(), attribute("fy").toReal());

    QString spread = attribute("spread").toString();
    if (spread == "pad")
        g.setSpread(QGradient::PadSpread);
    else if (spread == "repeat")
        g.setSpread(QGradient::RepeatSpread);
    else if (spread == "reflect")
        g.setSpread(QGradient::ReflectSpread);

    for (int n = 0; n < countElements(); ++n) {
        QVariant val = element(n);
        if (val.canConvert<maksi::element_t>()) {
            maksi::element_t elem = val.value<maksi::element_t>();
            val = elem->elementValue();
        }
        if (val.canConvert<GradientStopElement::Value>()) {
            GradientStopElement::Value v(val.value<GradientStopElement::Value>());
            g.setColorAt(v.offset, v.color);
        }
    }
    return g;
}


TextureElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t TextureElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        maksi::element_t elem(new TextureElement(m_maket, conf.name(), type, conf.source()));
        *value = QVariant::fromValue(elem);
    }
    return maksi::RESULT_OK;
}

TextureElement::TextureElement(Maket *maket, QXmlName name, maksi::schema_type_t type, QUrl base) :
    maksi::Element(name, type),
    m_maket(maket),
    m_base(base)
{
}

TextureElement::~TextureElement()
{
}

QVariant TextureElement::elementValue()
{
    Value data;

    QUrl url(attribute("href").toUrl());
    if (url.isRelative()) {
        url = m_base.resolved(url);
    }
    data.href = url;

    if (attributeExists("color")) {
        data.color = m_maket->color(attribute("color").toString());
    } else {
        for (int i = 0; i < countElements(); ++i) {
            QVariant val = element(i);
            if (val.canConvert<maksi::element_t>()) {
                maksi::element_t elem = val.value<maksi::element_t>();
                val = elem->elementValue();
            }
            if (val.canConvert<QColor>()) {
                data.color = val.value<QColor>();
                break;
            }
        }
    }

    return QVariant::fromValue(data);
}


};

/* End of code */
