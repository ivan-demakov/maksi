/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    brush.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Jun 29 00:35:27 2011
 *
 * @brief   brush element
 *
 */

#ifndef MAKET_BRUSH_H
#define MAKET_BRUSH_H

#include <QBrush>
#include <QUrl>

#include <maksi/elem.h>
#include <maksi/schema.h>

#include "defs.h"


//! maket namespace
namespace maket
{

class Maket;


class MAKET_EXPORT BrushElement : public maksi::Element
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    BrushElement(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~BrushElement();

    virtual QVariant elementValue();

private:
    QColor getColor();
    QBrush getBrush();

    Maket *m_maket;
};


class MAKET_EXPORT GradientStopElement : public maksi::Element
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    struct Value {
        qreal offset;
        QColor color;
    };

    GradientStopElement(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~GradientStopElement();

    virtual QVariant elementValue();

private:
    Maket *m_maket;
};


class MAKET_EXPORT LinearGradientElement : public maksi::Element
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };


    LinearGradientElement(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~LinearGradientElement();

    virtual QVariant elementValue();

private:
    QLinearGradient getGradient();

    Maket *m_maket;
};


class MAKET_EXPORT RadialGradientElement : public maksi::Element
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    RadialGradientElement(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~RadialGradientElement();

    virtual QVariant elementValue();

private:
    QRadialGradient getGradient();

    Maket *m_maket;
};


class MAKET_EXPORT TextureElement : public maksi::Element
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    struct Value {
        QUrl href;
        QColor color;
    };

    TextureElement(Maket *maket, QXmlName name, maksi::schema_type_t type, QUrl base);
    virtual ~TextureElement();

    virtual QVariant elementValue();

private:
    Maket *m_maket;
    QUrl m_base;
};

};

Q_DECLARE_METATYPE(QLinearGradient)
Q_DECLARE_METATYPE(QRadialGradient)
Q_DECLARE_METATYPE(maket::GradientStopElement::Value)
Q_DECLARE_METATYPE(maket::TextureElement::Value)


#endif

/* End of file */
