/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    pen.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jul  1 03:20:39 2011
 *
 * @brief   pen stylees
 *
 */

#include "pen.h"
#include "maket.h"

#include <maksi/conf.h>


namespace maket
{

PenElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory.isNull() ? maksi::schema_type_t() : oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t PenElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        maksi::element_t elem(new PenElement(m_maket, conf.name(), type));
        *value = QVariant::fromValue(elem);
    }
    return maksi::RESULT_OK;
}

PenElement::PenElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_maket(maket)
{
}

PenElement::~PenElement()
{
}

QVariant PenElement::elementValue()
{
    if (attributeExists("ref"))
        return m_maket->pen(attribute("ref").toString());

    QString style;
    if (attributeExists("style")) {
        style = attribute("style").toString();
    } else {
        if (attributeExists("dashes"))
            style = "dash";
        else
            style = "solid";
    }

    if (style == "no-pen")
        return QPen(Qt::NoPen);

    QPen pen;
    if (style == "solid") {
        pen = QPen(Qt::SolidLine);
    } else if (style == "dash") {
        QVariant dashes(attribute("dashes"));
        QVector<qreal> ds;
        if (dashes.canConvert<maksi::Array>()) {
            const maksi::Array ar(dashes.value<maksi::Array>());
            for (maksi::Array::const_iterator i = ar.begin(); i != ar.end(); ++i) {
                ds << i->toReal();
            }
            if (ds.size() % 2 == 1)
                ds << 0.0;
        }

        if (ds.size() == 0) {
            pen = QPen(Qt::DashLine);
        } else {
            pen = QPen(Qt::CustomDashLine);
            pen.setDashPattern(ds);
        }
    } else if (style == "dot") {
        pen = QPen(Qt::DotLine);
    } else if (style == "dashdot") {
        pen = QPen(Qt::DashDotLine);
    }

    if (attributeExists("width")) {
        qreal w = attribute("width").toReal();
        if (w > 0.0) {
            pen.setWidthF(w);

            QString cap = attribute("cap").toString();
            if (cap == "square")
                pen.setCapStyle(Qt::SquareCap);
            else if (cap == "round")
                pen.setCapStyle(Qt::RoundCap);
            else if (cap == "flat")
                pen.setCapStyle(Qt::FlatCap);

            QString join = attribute("join").toString();
            if (join == "mitter")
                pen.setJoinStyle(Qt::MiterJoin);
            else if (join == "round")
                pen.setJoinStyle(Qt::RoundJoin);
            else if (join == "bevel")
                pen.setJoinStyle(Qt::BevelJoin);
        }
    }

    if (attributeExists("color")) {
        pen.setColor(m_maket->color(attribute("color").toString()));
    } else if (attributeExists("brush")) {
        pen.setBrush(m_maket->brush(attribute("brush").toString()));
    } else {
        for (int i = 0; i < countElements(); ++i) {
            QVariant val = element(i);
            if (val.canConvert<maksi::element_t>()) {
                maksi::element_t elem = val.value<maksi::element_t>();
                val = elem->elementValue();
            }

            if (val.canConvert<QColor>()) {
                pen.setColor(val.value<QColor>());
                break;
            } else if (val.canConvert<QBrush>()) {
                pen.setBrush(val.value<QBrush>());
                break;
            }
        }
    }

    return pen;
}

};

/* End of code */
