include(../maksi.pri)

TEMPLATE = lib
TARGET = $$qtLibraryTarget(maket)
VERSION = $${MAKSI_VERSION}

QT += xmlpatterns

HEADERS += defs.h color.h brush.h pen.h font.h layer.h scene.h event.h view.h graphics.h maket.h

SOURCES += defs.cpp color.cpp brush.cpp pen.cpp font.cpp layer.cpp scene.cpp event.cpp view.cpp graphics.cpp maket.cpp


win32 {
  RC_FILE = maket.rc
  DEFINES += MAKET_LIBRARY=1
  release {
    LIBS += ..\maksi\release\maksi1.lib
  } else {
    LIBS += ..\maksi\debug\maksi1.lib
  }
}

unix {
  LIBS += -L../maksi -lmaksi
}

INCLUDEPATH += ..
#LIBS +=
DESTDIR = ../maksi
