/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    graphics.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 21 06:18:22 2010
 *
 * @brief   graphics
 *
 */

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <QGraphicsObject>
#include <QPen>
#include <QFont>
#include <QUuid>

#include <maksi/elem.h>
#include <maksi/schema.h>

#include "defs.h"


namespace maket
{

class Maket;
class Scene;


class MAKET_EXPORT Graphics : public QGraphicsObject, public maksi::Element
{
    Q_OBJECT
    Q_DISABLE_COPY(Graphics);
public:

    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    Graphics(const QUuid &uuid, QXmlName name, maksi::schema_type_t type);
    ~Graphics();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);
    virtual QVariant appendElement(const QVariant &value);
    virtual QList<QString> methodNames();
    virtual QVariant callMethod(const QString &name, QVariantList args);

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    const QUuid &uuid() const { return m_uuid; }

private:
    QUuid m_uuid;
};


class GraphicsItem : public QGraphicsItem, public maksi::Element
{
public:
    GraphicsItem(QXmlName name, maksi::schema_type_t type);

    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);

    void setPen(QPen val);
    void setBrush(QBrush val);
    virtual void updateStyles();

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    QPainterPath penShape(QPainterPath path) const;

    mutable QRectF m_boundingRect;
    QBrush m_brush;
    QPen m_pen;
};

class GraphicsRect : public GraphicsItem
{
    Q_DISABLE_COPY(GraphicsRect);
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    GraphicsRect(QXmlName name, maksi::schema_type_t type);
    ~GraphicsRect();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    qreal w() const { return m_w; }
    qreal h() const { return m_h; }

protected:
    qreal m_w, m_h;
};


class GraphicsOval : public GraphicsItem
{
    Q_DISABLE_COPY(GraphicsOval);
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    enum style_t {
        STYLE_ARC,
        STYLE_PIE,
        STYLE_CHORD,
        STYLE_ARCHORD
    };

    GraphicsOval(QXmlName name, maksi::schema_type_t type);
    ~GraphicsOval();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    qreal w, h, start, span;
    style_t style;
};


class GraphicsText : public GraphicsItem
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    GraphicsText(QXmlName name, maksi::schema_type_t type);
    ~GraphicsText();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setFont(QFont val);
    virtual void updateStyles();

protected:
    QString m_text;
    QFont m_font;
};


class GraphicsPoint : public maksi::Element {
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    GraphicsPoint(QXmlName name, maksi::schema_type_t type);
    ~GraphicsPoint();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);

    qreal x();
    qreal y();
    void setX(qreal x);
    void setY(qreal y);

    void setPoly(QPolygonF *poly, int index);
    int index() const { return m_index; }

private:
    QPolygonF *m_poly;
    int m_index;
};

class GraphicsPolyItem : public GraphicsItem
{
public:
    GraphicsPolyItem(QXmlName name, maksi::schema_type_t type);

    virtual int countElements();
    virtual QVariant element(int index);
    virtual QVariant appendElement(const QVariant &value);

    QRectF boundingRect() const;

protected:
    QPolygonF m_poly;
    QWeakPointer<GraphicsPoint> m_curPoint;
};


class GraphicsPolygon : public GraphicsPolyItem
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    GraphicsPolygon(QXmlName name, maksi::schema_type_t type);
    ~GraphicsPolygon();

    virtual QVariant elementValue();

    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};


class GraphicsPolyline : public GraphicsPolyItem
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    GraphicsPolyline(QXmlName name, maksi::schema_type_t type);
    ~GraphicsPolyline();

    virtual QVariant elementValue();

    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};


class GraphicsMultiline : public GraphicsPolyItem
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    GraphicsMultiline(QXmlName name, maksi::schema_type_t type);
    ~GraphicsMultiline();

    virtual QVariant elementValue();

    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};


};

Q_DECLARE_METATYPE(maket::Graphics*)
Q_DECLARE_METATYPE(maket::GraphicsRect*)
Q_DECLARE_METATYPE(maket::GraphicsOval*)
Q_DECLARE_METATYPE(maket::GraphicsText*)
Q_DECLARE_METATYPE(maket::GraphicsPoint*)
Q_DECLARE_METATYPE(maket::GraphicsPolygon*)
Q_DECLARE_METATYPE(maket::GraphicsPolyline*)
Q_DECLARE_METATYPE(maket::GraphicsMultiline*)

#endif

/* End of file */
