/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    scene.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Sun Oct 17 09:50:36 2010
 *
 * @brief   scene definition
 *
 */

#include "scene.h"
#include "layer.h"
#include "maket.h"
#include "view.h"
#include "graphics.h"

#include <maksi/conf.h>
#include <maksi/maksi_jobs.h>

#include <QApplication>


namespace maket
{

Scene::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t Scene::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    static QXmlName maksi_graphics = maksi::xml::qname("graphics");

    maksi::schema_type_t graphicsType = m_maket->maksi()->type(maksi_graphics);
    if (!graphicsType.isValid()) {
        m_maket->emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_graphics)));
        return maksi::RESULT_ERROR;
    }

    if (value) {
        QSharedPointer<Scene> elem(new Scene(m_maket, conf.name(), type));
        *value = maksi::fromElement(elem);
    }
    return maksi::RESULT_OK;
}

Scene::Scene(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_maket(maket)
{
    static QXmlName maksi_graphics = maksi::xml::qname("graphics");

    m_graphicsType = m_maket->maksi()->type(maksi_graphics);
}

Scene::~Scene()
{
    qDebug("delete scene %s", qPrintable(attribute("name").toString()));
}

QVariant Scene::elementValue()
{
    return QVariant::fromValue(this);
}

bool Scene::attributeExists(const QString &name)
{
    return Element::attributeExists(name);
}

QVariant Scene::attribute(const QString &name)
{
    return Element::attribute(name);
}

void Scene::setAttribute(const QString &name, const QVariant &value)
{
    Element::setAttribute(name, value);
    if (name == "width" || name =="height") {
        qreal w = attribute("width").toReal();
        qreal h = attribute("height").toReal();
        if (w > 0.0 && h > 0.0)
            setSceneRect(0.0, 0.0, w, h);
    } else if (name == "background") {
        setBackgroundBrush(m_maket->brush(value.toString()));
    }
}

QVariant Scene::appendElement(const QVariant &value)
{
    QVariant val(Element::appendElement(value));
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem(val.value<maksi::element_t>());
        val = elem->elementValue();
    }
    if (val.canConvert<LayerElement*>()) {
        LayerElement *layer = val.value<LayerElement*>();
        addItem(layer);
    }
    return val;
}


QList<LayerElement*> Scene::layers()
{
    QList<LayerElement*> ls;
    for (int i = 0; i < countElements(); ++i) {
        QVariant val(element(i));
        if (val.canConvert<maksi::element_t>()) {
            maksi::element_t elem(val.value<maksi::element_t>());
            val = elem->elementValue();
        }
        if (val.canConvert<LayerElement*>()) {
            LayerElement* layer = val.value<LayerElement*>();
            ls.append(layer);
        }
    }
    return ls;
}

bool Scene::hasSelected() const
{
    return !m_selected.isEmpty();
}

QVariantList Scene::itemsAt(const View *view, bool selected, const QRect &rect, Qt::ItemSelectionMode mode)
{
    QPolygonF poly = view->mapToScene(rect);
    QList<QGraphicsItem *> ls = QGraphicsScene::items(poly, mode, Qt::DescendingOrder, view->viewportTransform());

    QSet<Graphics *> found;
    for (int i = 0; i < ls.size(); ++i) {
        for (QGraphicsItem *item = ls.at(i); item; item = item->parentItem()) {
            Graphics *graphics = qobject_cast<Graphics *>(item->toGraphicsObject());
            if (graphics) {
                if (!selected || m_selected.contains(graphics)) {
                    found.insert(graphics);
                    if (mode == Qt::IntersectsItemShape)
                        goto next;
                }
                break;
            }
        }
    }
next:
    QVariantList items;
    for (QSet<Graphics *>::const_iterator it = found.constBegin(); it != found.constEnd(); ++it) {
        QWeakPointer<Graphics> g(*it);
        items << maksi::fromElement(g.toStrongRef());
    }
    return items;
}

void Scene::selectItems(const View *view, const QRect &rect, Qt::ItemSelectionMode mode, select_t type)
{
    QPolygonF poly = view->mapToScene(rect);
    QList<QGraphicsItem *> ls = QGraphicsScene::items(poly, mode, Qt::DescendingOrder, view->viewportTransform());

    for (int i = 0; i < ls.size(); i++) {
        for (QGraphicsItem *item = ls.at(i); item; item = item->parentItem()) {
            Graphics *graphics = qobject_cast<Graphics *>(item->toGraphicsObject());
            if (graphics) {
                if (type == SELECT_OUT) {
                    if (m_selected.contains(graphics)) {
                        setGraphicsState(graphics, "normal");
                        m_selected.remove(graphics);
                    }
                } else {
                    if (!m_selected.contains(graphics)) {
                        setGraphicsState(graphics, "selected");
                        m_selected.insert(graphics);
                    }
                }
                if (mode == Qt::IntersectsItemShape) {
                    goto next;
                }
                break;
            }
        }
    }
next:;
}

void Scene::moveSelected(qreal dx, qreal dy)
{
    for (QSet<Graphics *>::const_iterator it = m_selected.constBegin(); it != m_selected.constEnd(); ++it) {
        Graphics *graphics = *it;
        graphics->moveBy(dx, dy);
    }
}

LayerElement *Scene::getLayerByName(QString name, QList<LayerElement*> ls)
{
    for (int i = 0; i < ls.size(); ++i) {
        LayerElement *layer = ls.at(i);
        if (layer->attribute("name").toString() == name)
            return layer;

        QList<LayerElement*> ss(layer->layers());
        if (!ss.isEmpty()) {
            layer = getLayerByName(name, ss);
            if (layer)
                return layer;
        }
    }
    return 0;
}

bool Scene::setGraphicsLayer(Graphics *pg, LayerElement *layer)
{
    if (layer) {
        pg->setGroup(layer);
        return true;
    } else {
        pg->setGroup(0);
    }
    return false;
}

LayerElement *Scene::getGraphicsLayer(Graphics *pg, QList<LayerElement*> ls)
{
    QGraphicsItemGroup *group = pg->group();
    if (group) {
        LayerElement *layer = static_cast<LayerElement *>(group);

        for (int i = 0; i < ls.size(); ++i) {
            if (ls.at(i) == layer)
                return layer;

            QList<LayerElement*> ss(ls.at(i)->layers());
            if (!ss.isEmpty()) {
                LayerElement *le = getGraphicsLayer(pg, ss);
                if (le)
                    return le;
            }
        }
    }
    return 0;
}


#if 0
Graphics *Scene::addShape(const QString &id)
{
    Graphics *pg = new Graphics(id);
    if (pg) {
        addItem(pg->m_group);
    }
    return pg;
}

#endif

bool Scene::setGraphicsState(Graphics *item, const QString &state)
{
    bool found = false;
    Q_UNUSED(item);
    Q_UNUSED(state);
#if 0
    if (item) {
        const GraphicsData &data = m_graphics.value(item->script_id());
        for (int i = 0; i < data.features.size(); i++) {
            if (data.features.at(i).state == state) {
                if (item->setFeature(data.features.at(i).id))
                    found = true;
            }
        }
    }
#endif
    return found;
}


class LoadGraphicsJob : public maksi::LoadXmlJob
{
public:
    LoadGraphicsJob(const QString &url, Scene *scene) : LoadXmlJob(url, true), m_scene(scene) {}

protected:
    void startDocument();
    void endDocument();
    bool processNode(int level, maksi::Conf *node);

    Scene *m_scene;
};

void LoadGraphicsJob::startDocument()
{
    m_scene->emitStartGraphicsLoading();
    qDebug("start loading");
}

void LoadGraphicsJob::endDocument()
{
    m_scene->emitEndGraphicsLoading();
    qDebug("end loading");
}

bool LoadGraphicsJob::processNode(int level, maksi::Conf *node)
{
    static QXmlName maksi_graphics = maksi::xml::qname("graphics");

    if (level == 1) {
        QVariant val;
        maksi::result_t res = m_scene->m_graphicsType.getValue(*node, maksi_graphics, &val, m_scene->maket()->maksi(), true);
        if (maksi::RESULT_OK == res) {
            if (val.canConvert<maksi::element_t>()) {
                maksi::element_t elem = val.value<maksi::element_t>();
                QVariant v = elem->elementValue();
                if (v.canConvert<Graphics*>()) {
                    Graphics *gr = v.value<Graphics*>();
                    gr->moveToThread(QApplication::instance()->thread());
                }
            }
            m_scene->emitGraphicsLoading(val);
        }

        return true;
    }
    return false;
}

void Scene::loadGraphics(const QString &url)
{
    LoadGraphicsJob *job = new LoadGraphicsJob(url, this);
    connect(this, SIGNAL(graphicsLoading(QVariant)), this, SLOT(loadGraphics(QVariant)), Qt::QueuedConnection);

    m_maket->maksi()->startJob(job);
}

void Scene::loadGraphics(QVariant val)
{
    maksi::element_t elem;
    if (val.canConvert<maksi::element_t>()) {
        elem = val.value<maksi::element_t>();
        val = elem->elementValue();
    }
    if (val.canConvert<Graphics*>()) {
        Graphics *gr = val.value<Graphics*>();
        if (gr->attributeExists("layer")) {
            LayerElement *layer = getLayerByName(gr->attribute("layer").toString(), layers());
            if (layer) {
                setGraphicsLayer(gr, layer);
                m_graphics.insert(gr->uuid(), elem);
            }
        }
    }
}

void Scene::clearGraphics()
{
#if 0
    for (QMap<QUuid, maksi::element_t>::iterator it = m_graphics.begin(); it != m_graphics.end(); ++it) {
        QVariant val = it.value()->elementValue();
        Graphics *gr = val.value<Graphics*>();
        if (gr) {
            gr->setGroup(0);
        }
    }
#else
    m_graphics.clear();
#endif
}


}

 /* End of code */
