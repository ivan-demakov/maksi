/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    defs.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 14 21:40:30 2010
 *
 * @brief   defs
 *
 */

#ifndef MAKET_DEFS_H
#define MAKET_DEFS_H

#if defined(MAKET_LIBRARY)
#  define MAKET_EXPORT Q_DECL_EXPORT
#else
#  define MAKET_EXPORT Q_DECL_IMPORT
#endif

#include <maksi/defs.h>


//! maket namespace
namespace maket
{
};


#endif

/* End of file */
