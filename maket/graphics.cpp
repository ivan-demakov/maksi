/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    graphics.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 21 08:57:18 2010
 *
 * @brief   graphics defs
 *
 */

#include "graphics.h"
#include "scene.h"
#include "layer.h"
#include "maket.h"

#include <maksi/conf.h>

#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include <math.h>


namespace maket
{

Graphics::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t Graphics::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    static QXmlName maksi_uuid = maksi::xml::qname("uuid");

    if (value) {
        QUuid uuid;
        if (conf.hasAttribute(maksi_uuid)) {
            uuid = QUuid(conf.attribute(maksi_uuid));
            if (uuid.isNull())
                return maksi::RESULT_INVALID_VALUE;
        } else {
            uuid = QUuid::createUuid();
        }

        QSharedPointer<Graphics> graphics(new Graphics(uuid, conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

Graphics::Graphics(const QUuid &uuid, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_uuid(uuid)
{
    setFlag(QGraphicsItem::ItemHasNoContents, true);
}

Graphics::~Graphics()
{
    qDebug("Graphics::~Graphics()");
}

QVariant Graphics::elementValue()
{
    return QVariant::fromValue(this);
}

bool Graphics::attributeExists(const QString &name)
{
    if (name == "x" || name == "y" || name == "scale" || name == "rotation" || name == "opacity" || name == "visible" || name == "uuid") {
        return true;
    }
    return Element::attributeExists(name);
}

QVariant Graphics::attribute(const QString &name)
{
    if (name == "x") {
        return x();
    } else if (name == "y") {
        return y();
    } else if (name == "scale") {
        return scale();
    } else if (name == "rotation") {
        return -rotation();
    } else if (name == "opacity") {
        return opacity();
    } else if (name == "visible") {
        return isVisible();
    } else if (name == "uuid") {
        return m_uuid.toString();
    }
    return Element::attribute(name);
}

void Graphics::setAttribute(const QString &name, const QVariant &val)
{
    if (name == "x") {
        setX(val.toReal());
    } else if (name == "y") {
        setY(val.toReal());
    } else if (name == "scale") {
        setScale(val.toReal());
    } else if (name == "rotation") {
        setRotation(-fmod(val.toReal(), 360.0));
    } else if (name == "opacity") {
        setOpacity(val.toReal());
    } else if (name == "visible") {
        setVisible(val.toBool());
    } else if (name == "uuid") {
        QUuid uuid(val.toString());
        if (!uuid.isNull())
            m_uuid = uuid;
    } else {
        Element::setAttribute(name, val);
        if (name == "layer") {
            Scene *ps = static_cast<Scene *>(scene());
            if (ps) {
                LayerElement *layer = ps->getLayerByName(val.toString(), ps->layers());
                if (layer) {
                    ps->setGraphicsLayer(this, layer);
                }
            }
        }
    }
}

QVariant Graphics::appendElement(const QVariant &value)
{
    QVariant val(Element::appendElement(value));
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem(val.value<maksi::element_t>());
        val = elem->elementValue();
    }

    if (val.canConvert<GraphicsRect*>()) {
        GraphicsRect *p(val.value<GraphicsRect*>());
        p->setParentItem(this);
    } else if (val.canConvert<GraphicsOval*>()) {
        GraphicsOval *p(val.value<GraphicsOval*>());
        p->setParentItem(this);
    } else if (val.canConvert<GraphicsText*>()) {
        GraphicsText *p(val.value<GraphicsText*>());
        p->setParentItem(this);
    } else if (val.canConvert<GraphicsPolygon*>()) {
        GraphicsPolygon *p(val.value<GraphicsPolygon*>());
        p->setParentItem(this);
    }
    return val;
}

QList<QString> Graphics::methodNames()
{
    static QList<QString> ms;
    if (ms.isEmpty()) {
        ms << "move";
        ms << "rotate";
    }
    return ms;
}

QVariant Graphics::callMethod(const QString &id, QVariantList args)
{
    if (id == "move") {
        if (args.size() >= 2) {
            moveBy(args.at(0).toReal(), args.at(1).toReal());
            return true;
        }
    } else if (id == "rotate") {
        if (args.size() >= 1) {
            setRotation(fmod(-rotation() + args.at(0).toReal(), 360.0));
            return true;
        }
    }
    return false;
}

QRectF Graphics::boundingRect() const
{
    return childrenBoundingRect();
}

void Graphics::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}


GraphicsItem::GraphicsItem(QXmlName name, maksi::schema_type_t type) :
    Element(name, type)
{
}

QVariant GraphicsItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemSceneHasChanged) {
        updateStyles();
    }
    return value;
}

QVariant GraphicsItem::attribute(const QString &name)
{
    if (name == "x") {
        return x();
    } else if (name == "y") {
        return y();
    } else if (name == "visible") {
        return isVisible();
    } else if (name == "rotation") {
        return -rotation();
    } else {
        return maksi::Element::attribute(name);
    }
}

void GraphicsItem::setAttribute(const QString &name, const QVariant &val)
{
    if (name == "x") {
        setX(val.toReal());
    } else if (name == "y") {
        setY(val.toReal());
    } else if (name == "visible") {
        setVisible(val.toBool());
    } else if (name == "rotation") {
        setRotation(-fmod(val.toReal(), 360.0));
    } else if (name == "pen") {
        if (val.type() == QVariant::Pen) {
            setPen(val.value<QPen>());
        } else {
            maksi::Element::setAttribute(name, val);
            Scene *ps = static_cast<Scene *>(scene());
            if (ps) {
                setPen(ps->maket()->pen(val.toString()));
            }
        }
    } else if (name == "brush") {
        if (val.type() == QVariant::Brush) {
            setBrush(val.value<QBrush>());
        } else {
            maksi::Element::setAttribute(name, val);
            Scene *ps = static_cast<Scene *>(scene());
            if (ps) {
                setBrush(ps->maket()->brush(val.toString()));
            }
        }
    } else {
        maksi::Element::setAttribute(name, val);
    }
}

void GraphicsItem::setPen(QPen val)
{
    prepareGeometryChange();
    m_pen = val;
    m_boundingRect.setRect(0, 0, 0, 0);
    update();

    qDebug("setPen: %s", qPrintable(val.color().name()));
}

void GraphicsItem::setBrush(QBrush val)
{
    prepareGeometryChange();
    m_brush = val;
    m_boundingRect.setRect(0, 0, 0, 0);
    update();
    qDebug("setBrush: %s", qPrintable(val.color().name()));
}

void GraphicsItem::updateStyles()
{
    qDebug("GraphicsItem::updateStyles(): %s", qPrintable(maksi::xml::toString(elementName())));
    Scene *ps = static_cast<Scene *>(scene());
    if (ps) {
        if (attributeExists("brush")) {
            QVariant val(attribute("brush"));
            setBrush(ps->maket()->brush(val.toString()));
        }
        if (attributeExists("pen")) {
            QVariant val(attribute("pen"));
            setPen(ps->maket()->pen(val.toString()));
        }
    }
}

QPainterPath GraphicsItem::penShape(QPainterPath path) const
{
    QPainterPath res;
    if (m_pen.style() != Qt::NoPen) {
        QPainterPathStroker ps;
        ps.setWidth(qMax(0.0000001, m_pen.widthF()));
        ps.setCapStyle(m_pen.capStyle());
        ps.setJoinStyle(m_pen.joinStyle());
        ps.setMiterLimit(m_pen.miterLimit());

        QPainterPath p = ps.createStroke(path);
        res.addPath(p);
    }
    return res;
}


GraphicsRect::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsRect::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsRect> graphics(new GraphicsRect(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

GraphicsRect::GraphicsRect(QXmlName name, maksi::schema_type_t type) :
    GraphicsItem(name, type),
    m_w(0.0), m_h(0.0)
{
}

GraphicsRect::~GraphicsRect()
{
    qDebug("GraphicsRect::~GraphicsRect()");
}

QVariant GraphicsRect::elementValue()
{
    return QVariant::fromValue(this);
}

bool GraphicsRect::attributeExists(const QString &name)
{
    if (name == "w" || name == "h") {
        return true;
    }
    return GraphicsItem::attributeExists(name);
}

QVariant GraphicsRect::attribute(const QString &name)
{
    if (name == "w") {
        return w();
    } else if (name == "h") {
        return h();
    }
    return GraphicsItem::attribute(name);
}

void GraphicsRect::setAttribute(const QString &name, const QVariant &val)
{
    if (name == "w") {
        prepareGeometryChange();
        m_w = val.toReal();
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (name == "h") {
        prepareGeometryChange();
        m_h = val.toReal();
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else {
        GraphicsItem::setAttribute(name, val);
    }
}

QRectF GraphicsRect::boundingRect() const
{
    if (m_boundingRect.isNull()) {
        qreal pw = m_pen.widthF(), hw = pw / 2;
        m_boundingRect.setRect(-hw, -hw, m_w+pw, m_h+pw);
    }
    return m_boundingRect;
}

QPainterPath GraphicsRect::shape() const
{
    QRectF rect(0.0, 0.0, m_w, m_h);
    QPainterPath path;
    path.addRect(rect);

    QPainterPath res = penShape(path);
    if (m_brush.style() != Qt::NoBrush) {
        res.addRect(rect);
    }
    return res;
}

void GraphicsRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QRectF rect(0.0, 0.0, m_w, m_h);
    painter->setPen(m_pen);
    painter->setBrush(m_brush);
    painter->drawRect(rect);
}


GraphicsOval::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsOval::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsOval> graphics(new GraphicsOval(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

GraphicsOval::GraphicsOval(QXmlName name, maksi::schema_type_t type) :
    GraphicsItem(name, type),
    w(0.0), h(0.0), start(0.0), span(360.0),
    style(STYLE_ARC)
{
}

GraphicsOval::~GraphicsOval()
{
    qDebug("GraphicsOval::~GraphicsOval()");
}

QVariant GraphicsOval::elementValue()
{
    return QVariant::fromValue(this);
}

bool GraphicsOval::attributeExists(const QString &name)
{
    if (name == "w" || name == "h" || name == "start" || name == "span" || name == "style") {
        return true;
    }
    return GraphicsItem::attributeExists(name);
}

QVariant GraphicsOval::attribute(const QString &name)
{
    if (name == "w") {
        return w;
    } else if (name == "h") {
        return h;
    } else if (name == "start") {
        return start;
    } else if (name == "span") {
        return span;
    } else if (name == "style") {
        switch (style) {
        case STYLE_ARC: return "arc";
        case STYLE_PIE: return "pie";
        case STYLE_CHORD: return "chord";
        case STYLE_ARCHORD: return "archord";
        }
    }
    return GraphicsItem::attribute(name);
}

void GraphicsOval::setAttribute(const QString &name, const QVariant &val)
{
    if (name == "w") {
        prepareGeometryChange();
        w = val.toReal();
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (name == "h") {
        prepareGeometryChange();
        h = val.toReal();
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (name == "start") {
        prepareGeometryChange();
        start = fmod(val.toReal(), 360.0);
        if (start < 0.0)
            start += 360.0;
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (name == "span") {
        prepareGeometryChange();
        span = val.toReal();
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (name == "style") {
        prepareGeometryChange();
        QString s = val.toString();
        if (s.compare("arc", Qt::CaseInsensitive) == 0) {
            style = STYLE_ARC;
        } else if (s.compare("pie", Qt::CaseInsensitive) == 0) {
            style = STYLE_PIE;
        } else if (s.compare("chord", Qt::CaseInsensitive) == 0) {
            style = STYLE_CHORD;
        } else if (s.compare("archord", Qt::CaseInsensitive) == 0) {
            style = STYLE_ARCHORD;
        }
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else {
        GraphicsItem::setAttribute(name, val);
    }
    qDebug("GraphicsOval: %s=%s", qPrintable(name), qPrintable(attribute(name).toString()));
}

QRectF GraphicsOval::boundingRect() const
{
    if (m_boundingRect.isNull()) {
        m_boundingRect = shape().controlPointRect();
    }
    return m_boundingRect;
}

QPainterPath GraphicsOval::shape() const
{
    QRectF rect(0, 0, w, h);
    if (span <= -360.0 || span >= 360.0) {
        QPainterPath path;
        path.addEllipse(rect);

        QPainterPath res = penShape(path);
        if (m_brush.style() != Qt::NoBrush) {
            res.addEllipse(rect);
        }
        return res;
    }

    QPainterPath path;
    switch (style) {
    case STYLE_ARC:
        path.arcMoveTo(rect, start);
        path.arcTo(rect, start, span);
        break;
    case STYLE_PIE:
        path.moveTo(w/2, h/2);
        path.arcTo(rect, start, span);
        path.closeSubpath();
        break;
    case STYLE_CHORD:
        path.arcMoveTo(rect, start);
        path.arcTo(rect, start, span);
        path.closeSubpath();
        break;
    case STYLE_ARCHORD:
        path.arcMoveTo(rect, start);
        path.arcTo(rect, start, span);
        break;
    }

    QPainterPath res = penShape(path);
    if (m_brush.style() != Qt::NoBrush) {
        QPainterPath path1;
        switch (style) {
        case STYLE_ARC:
            path1.moveTo(w/2, h/2);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            break;
        case STYLE_PIE:
            path1.moveTo(w/2, h/2);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            break;
        case STYLE_CHORD:
            path1.arcMoveTo(rect, start);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            break;
        case STYLE_ARCHORD:
            path1.arcMoveTo(rect, start);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            break;
        }
        res.addPath(path1);
    }
    return res;
}

void GraphicsOval::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QRectF rect(0, 0, w, h);
    if (span <= -360.0 || span >= 360.0) {
        painter->setPen(m_pen);
        painter->setBrush(m_brush);
        painter->drawEllipse(rect);
    } else {
        QPainterPath path1, path2;
        switch (style) {
        case STYLE_ARC:
            path1.moveTo(w/2, h/2);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            painter->fillPath(path1, m_brush);
            path2.arcMoveTo(rect, start);
            path2.arcTo(rect, start, span);
            painter->strokePath(path2, m_pen);
            break;
        case STYLE_PIE:
            path1.moveTo(w/2, h/2);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            painter->fillPath(path1, m_brush);
            painter->strokePath(path1, m_pen);
            break;
        case STYLE_CHORD:
            path1.arcMoveTo(rect, start);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            painter->fillPath(path1, m_brush);
            painter->strokePath(path1, m_pen);
            break;
        case STYLE_ARCHORD:
            path1.arcMoveTo(rect, start);
            path1.arcTo(rect, start, span);
            path1.closeSubpath();
            painter->fillPath(path1, m_brush);
            path2.arcMoveTo(rect, start);
            path2.arcTo(rect, start, span);
            painter->strokePath(path2, m_pen);
            break;
        }
    }
}


GraphicsText::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsText::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsText> graphics(new GraphicsText(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

GraphicsText::GraphicsText(QXmlName name, maksi::schema_type_t type) :
    GraphicsItem(name, type)
{
}

GraphicsText::~GraphicsText()
{
    qDebug("GraphicsText::~GraphicsText()");
}

QVariant GraphicsText::elementValue()
{
    return QVariant::fromValue(this);
}

bool GraphicsText::attributeExists(const QString &name)
{
    if (name == "text") {
        return true;
    }
    return GraphicsItem::attributeExists(name);
}

QVariant GraphicsText::attribute(const QString &name)
{
    if (name == "text") {
        return m_text;
    }
    return GraphicsItem::attribute(name);
}

void GraphicsText::setAttribute(const QString &name, const QVariant &val)
{
    if (name == "text") {
        prepareGeometryChange();
        m_text = val.toString();
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (name == "font") {
        if (val.type() == QVariant::Font) {
            setFont(val.value<QFont>());
        } else {
            maksi::Element::setAttribute(name, val);
            Scene *ps = static_cast<Scene *>(scene());
            if (ps)
                setFont(ps->maket()->font(val.toString()));
        }
    } else {
        GraphicsItem::setAttribute(name, val);
    }
}

void GraphicsText::setFont(QFont val)
{
    prepareGeometryChange();
    m_font = val;
    m_boundingRect.setRect(0, 0, 0, 0);
    update();
}

void GraphicsText::updateStyles()
{
    Scene *ps = static_cast<Scene *>(scene());
    if (ps) {
        if (attributeExists("font")) {
            QVariant val(attribute("font"));
            setFont(ps->maket()->font(val.toString()));
        }
        GraphicsItem::updateStyles();
    }
}

QRectF GraphicsText::boundingRect() const
{
    if (m_boundingRect.isNull() && !m_text.isEmpty()) {
        QPainterPath path;
        path.addText(0, 0, m_font, m_text);
        m_boundingRect = path.controlPointRect();
    }
    return m_boundingRect;
}

QPainterPath GraphicsText::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void GraphicsText::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPainterPath path;
    path.addText(0, 0, m_font, m_text);
    if (m_brush.style() != Qt::NoBrush)
        painter->fillPath(path, m_brush);
    if (m_pen.style() != Qt::NoPen)
        painter->strokePath(path, m_pen);
}



GraphicsPoint::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsPoint::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsPoint> graphics(new GraphicsPoint(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}


GraphicsPoint::GraphicsPoint(QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_poly(0)
{
}

GraphicsPoint::~GraphicsPoint()
{
    qDebug("~GraphicsPoint()");
}

QVariant GraphicsPoint::elementValue()
{
    return QVariant::fromValue(this);
}

bool GraphicsPoint::attributeExists(const QString &name)
{
    if (name == "x" || name == "y") {
        return true;
    }
    return Element::attributeExists(name);
}

QVariant GraphicsPoint::attribute(const QString &name)
{
    if (name == "x") {
        return x();
    } else if (name == "y") {
        return y();
    }
    return Element::attribute(name);
}

void GraphicsPoint::setAttribute(const QString &name, const QVariant &val)
{
    if (name == "x") {
        setX(val.toReal());
    } else if (name == "y") {
        setY(val.toReal());
    }
}

qreal GraphicsPoint::x()
{
    if (m_poly) {
        return m_poly->at(m_index).x();
    }
    return Element::attribute("x").toReal();
}

qreal GraphicsPoint::y()
{
    if (m_poly) {
        return m_poly->at(m_index).y();
    }
    return Element::attribute("y").toReal();
}

void GraphicsPoint::setX(qreal x)
{
    if (m_poly) {
        return m_poly->operator[](m_index).setX(x);
    } else {
        Element::setAttribute("x", x);
    }
}

void GraphicsPoint::setY(qreal y)
{
    if (m_poly) {
        return m_poly->operator[](m_index).setY(y);
    } else {
        Element::setAttribute("y", y);
    }
}

void GraphicsPoint::setPoly(QPolygonF *poly, int index)
{
    if (poly == 0) {
        if (m_poly != 0) {
            Element::setAttribute("x", m_poly->at(m_index).x());
            Element::setAttribute("y", m_poly->at(m_index).y());
        }
    }

    m_poly = poly;
    m_index = index;
}


GraphicsPolyItem::GraphicsPolyItem(QXmlName name, maksi::schema_type_t type) :
    GraphicsItem(name, type)
{
}

int GraphicsPolyItem::countElements()
{
    return m_poly.size() + Element::countElements();
}

QVariant GraphicsPolyItem::element(int index)
{
    if (index < m_poly.size()) {
        if (m_curPoint) {
            QSharedPointer<GraphicsPoint> point(m_curPoint);
            if (point && point->index() == index) {
                return maksi::fromElement(point);
            }
        }

        maksi::schema_type_t type;
        if (Scene *scene = static_cast<Scene *>(this->scene())) {
            static QXmlName type_name = maksi::xml::qname("graphicsPoint");
            type = scene->maket()->maksi()->type(type_name);
        }

        static QXmlName element_name = maksi::xml::qname("point");
        QSharedPointer<GraphicsPoint> point(new GraphicsPoint(element_name, type));
        point->setPoly(&m_poly, index);
        m_curPoint = point;
        return maksi::fromElement(point);
    }
    return Element::element(index - m_poly.size());
}

QVariant GraphicsPolyItem::appendElement(const QVariant &valref)
{
    QVariant val;
    if (valref.canConvert<maksi::element_t>()) {
        maksi::element_t elem(valref.value<maksi::element_t>());
        val = elem->elementValue();
    } else {
        val = valref;
    }

    if (val.canConvert<GraphicsPoint*>()) {
        GraphicsPoint *p(val.value<GraphicsPoint*>());
        QPointF point(p->x(), p->y());
        m_poly.append(point);
        return val;
    } else {
        return GraphicsItem::appendElement(valref);
    }
}

QRectF GraphicsPolyItem::boundingRect() const
{
    if (m_boundingRect.isNull()) {
        if (m_pen.style() == Qt::NoPen || m_pen.widthF() == 0.0)
            m_boundingRect = m_poly.boundingRect();
        else
            m_boundingRect = shape().controlPointRect();
    }
    return m_boundingRect;
}


GraphicsPolygon::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsPolygon::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsPolygon> graphics(new GraphicsPolygon(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

GraphicsPolygon::GraphicsPolygon(QXmlName name, maksi::schema_type_t type) :
    GraphicsPolyItem(name, type)
{
}

GraphicsPolygon::~GraphicsPolygon()
{
    qDebug("GraphicsPolygon::~GraphicsPolygon()");
}

QVariant GraphicsPolygon::elementValue()
{
    return QVariant::fromValue(this);
}

QPainterPath GraphicsPolygon::shape() const
{
    QPainterPath path, res;

    path.addPolygon(m_poly);
    if (m_brush.style() != Qt::NoBrush)
        res = path;

    if (m_pen.style() != Qt::NoPen && m_pen.widthF() > 0.0) {
        path.closeSubpath();

        QPainterPathStroker ps;
        ps.setWidth(m_pen.widthF());
        ps.setCapStyle(m_pen.capStyle());
        ps.setJoinStyle(m_pen.joinStyle());
        ps.setMiterLimit(m_pen.miterLimit());

        QPainterPath p = ps.createStroke(path);
        res.addPath(p);
    }

    return res;
}

void GraphicsPolygon::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setBrush(m_brush);
    painter->setPen(m_pen);
    painter->drawPolygon(m_poly);
}


GraphicsPolyline::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsPolyline::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsPolyline> graphics(new GraphicsPolyline(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

GraphicsPolyline::GraphicsPolyline(QXmlName name, maksi::schema_type_t type) :
    GraphicsPolyItem(name, type)
{
}

GraphicsPolyline::~GraphicsPolyline()
{
    qDebug("GraphicsPolyline::~GraphicsPolyline()");
}

QVariant GraphicsPolyline::elementValue()
{
    return QVariant::fromValue(this);
}

QPainterPath GraphicsPolyline::shape() const
{
    QPainterPath path, res;

    if (m_pen.style() != Qt::NoPen) {
        path.addPolygon(m_poly);

        QPainterPathStroker ps;
        ps.setWidth(m_pen.widthF());
        ps.setCapStyle(m_pen.capStyle());
        ps.setJoinStyle(m_pen.joinStyle());
        ps.setMiterLimit(m_pen.miterLimit());

        QPainterPath p = ps.createStroke(path);
        res.addPath(p);
    }

    return res;
}

void GraphicsPolyline::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(m_pen);
    painter->drawPolyline(m_poly);
}


GraphicsMultiline::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t GraphicsMultiline::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<GraphicsMultiline> graphics(new GraphicsMultiline(conf.name(), type));
        *value = maksi::fromElement(graphics);
    }
    return maksi::RESULT_OK;
}

GraphicsMultiline::GraphicsMultiline(QXmlName name, maksi::schema_type_t type) :
    GraphicsPolyItem(name, type)
{
}

GraphicsMultiline::~GraphicsMultiline()
{
    qDebug("GraphicsMultiline::~GraphicsMultiline()");
}

QVariant GraphicsMultiline::elementValue()
{
    return QVariant::fromValue(this);
}

QPainterPath GraphicsMultiline::shape() const
{
    QPainterPath res;

    if (m_pen.style() != Qt::NoPen) {
        QPainterPath path;

        int poly_size = m_poly.size() - 1;
        for (int i = 0; i < poly_size; i++) {
            path.moveTo(m_poly.at(i));
            i++;
            path.lineTo(m_poly.at(i));
        }

        QPainterPathStroker ps;
        if (m_pen.widthF() <= 0.0)
            ps.setWidth(0.000001);
        else
            ps.setWidth(m_pen.widthF());

        ps.setCapStyle(m_pen.capStyle());
        ps.setJoinStyle(m_pen.joinStyle());
        ps.setMiterLimit(m_pen.miterLimit());

        QPainterPath p = ps.createStroke(path);
        res.addPath(p);
    }

    return res;
}

void GraphicsMultiline::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(m_pen);
    painter->drawLines(m_poly);
}




#if 0
static QList<double> variant2doubles(QVariant val)
{
    QList<double> ds;
    if (val.type() == QVariant::List) {
        QList<QVariant> vs = val.toList();
        for (int i = 0; i < vs.size(); i++) {
            ds.append(vs.at(i).toDouble());
        }
    } else if (val.type() == QVariant::StringList) {
        QList<QString> vs = val.toStringList();
        for (int i = 0; i < vs.size(); i++) {
            ds.append(vs.at(i).toDouble());
        }
    } else {
        QString s = val.toString();
        for (int pos = 0; pos < s.size(); pos++) {
            if (s.at(pos).isSpace())
                continue;

            int end;
            for (end = pos+1; end < s.size(); end++) {
                if (s.at(end).isSpace())
                    break;
            }
            QString m = s.mid(pos, end-pos);
            ds.append(m.toDouble());

            pos = end;
        }
    }

    return ds;
}

static QPainterPath string2path(QString s)
{
    QPainterPath path;

    for (int pos = 0; pos < s.size(); pos++) {
        if (s.at(pos).isSpace())
            continue;

        QChar ch = s.at(pos);
        int end;
        for (end = ++pos; end < s.size(); end++) {
            if (s.at(end).isSpace())
                break;
        }
        QStringList ls = s.mid(pos, end-pos).split(',');
        QList<double> ds;
        for (int i = 0; i < ls.size(); i++) {
            ds.append(ls.at(i).toDouble());
        }
        switch (ch.unicode()) {
        case L'm': case L'M':
            if (ds.size() == 2) {
                path.moveTo(ds.at(0), ds.at(1));
            }
            break;
        case L'l': case L'L':
            for (int i = 1; i < ds.size(); i += 2) {
                path.lineTo(ds.at(i-1), ds.at(i));
            }
            break;
        }

        pos = end;
    }

    return path;
}

static QString path2string(QPainterPath path)
{
    QString str;

    for (int i = 0; i < path.elementCount(); i++) {
        const QPainterPath::Element &e = path.elementAt(i);
        switch (e.type) {
        case QPainterPath::MoveToElement:
            str += QString("m%1,%2 ").arg(e.x).arg(e.y);
            break;
        case QPainterPath::LineToElement:
            str += QString("l%1,%2 ").arg(e.x).arg(e.y);
            break;
        case QPainterPath::CurveToElement:
        case QPainterPath::CurveToDataElement:
            break;
        }
    }
    return str;
}


class GraphicsScriptItem : public QGraphicsItem, public maksi::BaseScriptObject
{
public:
    GraphicsScriptItem(Graphics *owner, const QString &id, const QString &tag, info_t *info);
    ~GraphicsScriptItem();

    virtual QVariant property(const QString &id);
    virtual void setProperty(const QString &id, const QVariant &val);

    virtual bool setFeature(const Feature *feature);

protected:
    static property_t props[];
    static method_t methods[];
    static struct info_t info;
};

bool GraphicsScriptItem::setFeature(const Feature *feature)
{
    bool found = false;
    for (info_t *cur = m_info; cur; cur = cur->base) {
        if (cur->property) {
            for (int i = 0; !cur->property[i].id.isEmpty(); i++) {
                QVariant val = feature->property(cur->property[i].id);
                if (val.isValid()) {
                    setProperty(cur->property[i].id, val);
                    found = true;
                }
            }
        }
    }
    return found;
}


class GraphicsPoint : public GraphicsScriptItem
{
public:
    GraphicsPoint(Graphics *owner, const QString &id);
    ~GraphicsPoint();

    virtual QVariant property(const QString &id);
    virtual void setProperty(const QString &id, const QVariant &val);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    mutable QRectF m_boundingRect;
    QBrush m_brush;
    QPen m_pen;
    QPainterPath m_path;

    static QString tag;
    static property_t props[];
    static struct info_t info;

    friend class Graphics;
    friend class GraphicsGroup;
};

QString GraphicsPoint::tag("point");

GraphicsPoint::property_t GraphicsPoint::props[] = {
    { "x", false },
    { "y", false },
    { "pen", false },
    { "brush", false },
    { "path", false },
    { QString(), false }
};

struct GraphicsPoint::info_t GraphicsPoint::info = {
    &GraphicsScriptItem::info, props, 0
};

GraphicsPoint::GraphicsPoint(Graphics *owner, const QString &id) :
    GraphicsScriptItem(owner, id, tag, &info)
{
    setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
}

GraphicsPoint::~GraphicsPoint()
{
}

QVariant GraphicsPoint::property(const QString &id)
{
    if (id == "x") {
        return x();
    } else if (id == "y") {
        return y();
    } else if (id == "pen") {
        return m_pen;
    } else if (id == "brush") {
        return m_brush;
    } else if (id == "path") {
        return path2string(m_path);
    }
    return GraphicsScriptItem::property(id);
}

void GraphicsPoint::setProperty(const QString &id, const QVariant &val)
{
    //qDebug("id=%s val=%s", qPrintable(id), qPrintable(val.toString()));
    if (id == "x") {
        setX(val.toDouble());
    } else if (id == "y") {
        setY(val.toDouble());
    } else if (id == "pen") {
        prepareGeometryChange();
        if (val.type() == QVariant::Pen) {
            m_pen = val.value<QPen>();
        } else {
            Scene *ps = static_cast<Scene *>(scene());
            m_pen = ps->maket()->pen(val.toString());
        }
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (id == "brush") {
        prepareGeometryChange();
        if (val.type() == QVariant::Brush) {
            m_brush = val.value<QBrush>();
        } else {
            Scene *ps = static_cast<Scene *>(scene());
            m_brush = ps->maket()->brush(val.toString());
        }
        m_boundingRect.setRect(0, 0, 0, 0);
        update();
    } else if (id == "path") {
        prepareGeometryChange();
        m_path = string2path(val.toString());
        update();
    } else {
        GraphicsScriptItem::setProperty(id, val);
    }
}

QRectF GraphicsPoint::boundingRect() const
{
    if (m_boundingRect.isNull() && !m_path.isEmpty()) {
        m_boundingRect = shape().boundingRect();
    }
    return m_boundingRect;
}

QPainterPath GraphicsPoint::shape() const
{
    QPainterPath shape;
    if (!m_path.isEmpty()) {
        if (m_pen.style() != Qt::NoPen) {
            QPainterPathStroker ps;
            ps.setWidth(qMax(0.0000001, m_pen.widthF()));
            ps.setCapStyle(m_pen.capStyle());
            ps.setJoinStyle(m_pen.joinStyle());
            ps.setMiterLimit(m_pen.miterLimit());

            shape.addPath(ps.createStroke(m_path));
        }
        if (m_brush.style() != Qt::NoBrush) {
            shape.addPath(m_path);
        }
    }
    return shape;
}

void GraphicsPoint::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!m_path.isEmpty()) {
        if (m_brush.style() != Qt::NoBrush)
            painter->fillPath(m_path, m_brush);
        if (m_pen.style() != Qt::NoPen)
            painter->strokePath(m_path, m_pen);
    }
}

QPainterPath GraphicsMultiline::shape() const
{
    QPainterPath path;
    int poly_size = m_poly.size() - 1;
    if (poly_size > 0) {
        for (int i = 0; i < poly_size; i++) {
            path.moveTo(m_poly.at(i));
            i++;
            path.lineTo(m_poly.at(i));
        }
    }

    QPainterPath res;
    if (m_pen.style() != Qt::NoPen) {
        QPainterPathStroker ps;
        if (m_pen.widthF() <= 0.0)
            ps.setWidth(0.00000001);
        else
            ps.setWidth(m_pen.widthF());
        ps.setCapStyle(m_pen.capStyle());
        ps.setJoinStyle(m_pen.joinStyle());
        ps.setMiterLimit(m_pen.miterLimit());

        QPainterPath p = ps.createStroke(path);
        res.addPath(p);
    }

    return res;
}

void GraphicsMultiline::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(m_pen);
    painter->drawLines(m_poly);
}


class GraphicsPolypoint : public GraphicsScriptItem
{
public:
    GraphicsPolypoint(Graphics *owner, const QString &id);
    ~GraphicsPolypoint();

    virtual QVariant property(const QString &id);
    virtual void setProperty(const QString &id, const QVariant &val);
    virtual int propertyLength(const QString &id);
    virtual void setPropertyLength(const QString &id, int len);
    virtual QVariant property(const QString &id, int idx);
    virtual void setProperty(const QString &id, int idx, const QVariant &val);
    virtual void insertProperty(const QString &id, int idx, const QVariant &val);
    virtual void removeProperty(const QString &id, int idx);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    class Point : public QGraphicsItem {
    public:
        Point(Graphics *owner, QGraphicsItem *parent);

        QRectF boundingRect() const;
        QPainterPath shape() const;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    };

    void updatePath();
    void updatePoints(int size);

    QPainterPath m_path;
    QPainterPath m_shape;
    QRectF m_boundingRect;
    QBrush m_brush;
    QPen m_pen;

    friend class Point;

    static QString tag;
    static property_t props[];
    static struct info_t info;

    friend class Graphics;
    friend class GraphicsGroup;
};

QString GraphicsPolypoint::tag("polypoint");

GraphicsPolypoint::property_t GraphicsPolypoint::props[] = {
    { "xs", false },
    { "ys", false },
    { "path", false },
    { "pen", false },
    { "brush", false },
    { QString(), false }
};

struct GraphicsPolypoint::info_t GraphicsPolypoint::info = {
    &GraphicsScriptItem::info, props, 0
};

GraphicsPolypoint::Point::Point(Graphics *owner, QGraphicsItem *parent) :
    QGraphicsItem(parent)
{
    setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
    setData(GRAPHICS_DATA_PTR, QVariant::fromValue(owner));
}

QRectF GraphicsPolypoint::Point::boundingRect() const
{
    GraphicsPolypoint *p = static_cast<GraphicsPolypoint *>(parentItem());
    if (p)
        return p->m_boundingRect;
    return QRectF();
}

QPainterPath GraphicsPolypoint::Point::shape() const
{
    GraphicsPolypoint *p = static_cast<GraphicsPolypoint *>(parentItem());
    if (p)
        return p->m_shape;
    return QPainterPath();
}

void GraphicsPolypoint::Point::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    GraphicsPolypoint *p = static_cast<GraphicsPolypoint *>(parentItem());
    if (p) {
        painter->setBrush(p->m_brush);
        painter->setPen(p->m_pen);
        painter->drawPath(p->m_path);
    }
}


GraphicsPolypoint::GraphicsPolypoint(Graphics *owner, const QString &id) :
    GraphicsScriptItem(owner, id, tag, &info)
{
    setFlag(QGraphicsItem::ItemHasNoContents, true);
}

GraphicsPolypoint::~GraphicsPolypoint()
{
}

QRectF GraphicsPolypoint::boundingRect() const
{
    return QRectF();
}

void GraphicsPolypoint::paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *)
{
}

void GraphicsPolypoint::updatePath()
{
    m_shape = m_path;
    if (m_pen.style() != Qt::NoPen) {
        QPainterPathStroker ps;
        if (m_pen.widthF() <= 0.0)
            ps.setWidth(0.00000001);
        else
            ps.setWidth(m_pen.widthF());

        ps.setCapStyle(m_pen.capStyle());
        ps.setJoinStyle(m_pen.joinStyle());
        ps.setMiterLimit(m_pen.miterLimit());

        m_shape.addPath(ps.createStroke(m_path));
    }
    m_boundingRect = m_shape.controlPointRect();

    QList<QGraphicsItem *> ls = childItems();
    for (int i = 0; i < ls.size(); i++) {
        //ls.at(i)->prepareGeometryChange();
        ls.at(i)->update();
    }
}

void GraphicsPolypoint::updatePoints(int size)
{
    Scene *ps = static_cast<Scene *>(scene());

    Graphics *owner = 0;
    QVariant val = this->data(GRAPHICS_DATA_PTR);
    if (val.canConvert<Graphics*>())
        owner = val.value<Graphics*>();

    QList<QGraphicsItem *> ls = childItems();
    while (ls.size() > size) {
        QGraphicsItem *p = ls.takeLast();
        p->setParentItem(0);
        if (ps)
            ps->removeItem(p);
        delete p;
    }

    size -= ls.size();
    while (size > 0) {
        QGraphicsItem *p = new Point(owner, this);
        (void) p;
        --size;
    }
}

QVariant GraphicsPolypoint::property(const QString &id)
{
    if (id == "xs") {
        QList<QGraphicsItem *> ls = childItems();
        QList<QVariant> rs;
        for (int i = 0; i < ls.size(); i++) {
            rs.append(ls.at(i)->x() + x());
        }
        return rs;
    }
    if (id == "ys") {
        QList<QGraphicsItem *> ls = childItems();
        QList<QVariant> rs;
        for (int i = 0; i < ls.size(); i++) {
            rs.append(ls.at(i)->y() + y());
        }
        return rs;
    }
    if (id == "pen") {
        return m_pen;
    }
    if (id == "brush") {
        return m_brush;
    }
    if (id == "path") {
        return path2string(m_path);
    }
    return GraphicsScriptItem::property(id);
}

void GraphicsPolypoint::setProperty(const QString &id, const QVariant &val)
{
    if (id == "path") {
        m_path = string2path(val.toString());
        updatePath();
    } else if (id == "xs") {
        QList<double> ds = variant2doubles(val);
        updatePoints(ds.size());

        QList<QGraphicsItem *> ls = childItems();
        for (int i = 0; i < ls.size(); i++) {
            ls.at(i)->setX(ds.at(i) - x());
        }
    } else if (id == "ys") {
        QList<double> ds = variant2doubles(val);
        updatePoints(ds.size());

        QList<QGraphicsItem *> ls = childItems();
        for (int i = 0; i < ls.size(); i++) {
            ls.at(i)->setY(ds.at(i) - y());
        }
    } else if (id == "pen") {
        if (val.type() == QVariant::Pen) {
            m_pen = val.value<QPen>();
        } else {
            Scene *ps = static_cast<Scene *>(scene());
            m_pen = ps->maket()->pen(val.toString());
        }
        updatePath();
    } else if (id == "brush") {
        if (val.type() == QVariant::Brush) {
            m_brush = val.value<QBrush>();
        } else {
            Scene *ps = static_cast<Scene *>(scene());
            m_brush = ps->maket()->brush(val.toString());
        }

        QList<QGraphicsItem *> ls = childItems();
        for (int i = 0; i < ls.size(); i++) {
            ls.at(i)->update();
        }
    } else {
        GraphicsScriptItem::setProperty(id, val);
    }
}

int GraphicsPolypoint::propertyLength(const QString &id)
{
    if (id == "xs" || id == "ys") {
        QList<QGraphicsItem *> ls = childItems();
        return ls.size();
    }
    return -1;
}

void GraphicsPolypoint::setPropertyLength(const QString &id, int len)
{
    if (id == "xs" || id == "ys") {
        updatePoints(len);
    }
}

QVariant GraphicsPolypoint::property(const QString &id, int idx)
{
    if (id == "xs") {
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx < ls.size()) {
            return ls.at(idx)->x() + x();
        }
    } else if (id == "ys") {
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx < ls.size()) {
            return ls.at(idx)->y() + y();
        }
    }
    return QVariant();
}

void GraphicsPolypoint::setProperty(const QString &id, int idx, const QVariant &val)
{
    if (id == "xs") {
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx < ls.size()) {
            ls.at(idx)->setX(val.toDouble() - x());
        }
    } else if (id == "ys") {
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx < ls.size()) {
            ls.at(idx)->setY(val.toDouble() - y());
        }
    }
}

void GraphicsPolypoint::insertProperty(const QString &id, int idx, const QVariant &val)
{
    Graphics *owner = 0;

    QVariant owner_val = this->data(GRAPHICS_DATA_PTR);
    if (owner_val.canConvert<Graphics*>())
        owner = owner_val.value<Graphics*>();

    if (id == "xs") {
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx <= ls.size()) {
            QGraphicsItem *p = new Point(owner, this);
            p->setX(val.toDouble() - x());
        }
    } else if (id == "ys") {
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx <= ls.size()) {
            QGraphicsItem *p = new Point(owner, this);
            p->setY(val.toDouble() - y());
        }
    }
}

void GraphicsPolypoint::removeProperty(const QString &id, int idx)
{
    if (id == "xs" || id == "ys") {
        Scene *ps = static_cast<Scene *>(scene());
        QList<QGraphicsItem *> ls = childItems();
        if (0 <= idx && idx <= ls.size()) {
            QGraphicsItem *p = ls.takeAt(idx);
            p->setParentItem(0);
            if (ps)
                ps->removeItem(p);
            delete p;
        }
    }
}


class GraphicsGroup : public QGraphicsItemGroup, public maksi::BaseScriptObject
{
public:
    GraphicsGroup(Graphics *owner, const QString &id);
    ~GraphicsGroup();

    virtual QVariant property(const QString &id);
    virtual void setProperty(const QString &id, const QVariant &val);

    virtual int childCount() const;
    virtual ScriptObject *child(int index) const;
    virtual void removeChild(int index);
    virtual ScriptObject *insertChild(int pos, const QString &script_id, const QString &script_tag);

protected:
    static QString tag;
    static property_t props[];
    static struct info_t info;

    friend class Graphics;
};

QString GraphicsGroup::tag("group");

GraphicsGroup::property_t GraphicsGroup::props[] = {
    { "x", false },
    { "y", false },
    { "scale", false },
    { "rotation", false },
    { "visible", false },
    { QString(), false }
};

struct GraphicsGroup::info_t GraphicsGroup::info = {
    0, props, 0
};

GraphicsGroup::GraphicsGroup(Graphics *owner, const QString &id) :
    maksi::BaseScriptObject(id, tag, &info)
{
    if (owner) {
        setData(GRAPHICS_DATA_PTR, QVariant::fromValue(owner));
    }
}

GraphicsGroup::~GraphicsGroup()
{
}

QVariant GraphicsGroup::property(const QString &id)
{
    if (id == "x") {
        return x();
    } else if (id == "y") {
        return y();
    } else if (id == "scale") {
        return scale();
    } else if (id == "rotation") {
        return -rotation();
    } else if (id == "visible") {
        return isVisible();
    }
    return QVariant();
}

void GraphicsGroup::setProperty(const QString &id, const QVariant &val)
{
    if (id == "x") {
        setX(val.toDouble());
    } else if (id == "y") {
        setY(val.toDouble());
    } else if (id == "scale") {
        setScale(val.toDouble());
    } else if (id == "rotation") {
        setRotation(-fmod(val.toDouble(), 360.0));
    } else if (id == "visible") {
        setVisible(val.toBool());
    }
}

int GraphicsGroup::childCount() const
{
    QList<QGraphicsItem *> items = childItems();
    return items.size();
}

maksi::ScriptObject *GraphicsGroup::child(int index) const
{
    QList<QGraphicsItem *> items = childItems();
    if (0 <= index && index < items.size()) {
        QGraphicsItem *p = items.at(index);
        return dynamic_cast<maksi::ScriptObject *>(p);
    }
    return 0;
}

void GraphicsGroup::removeChild(int index)
{
    QList<QGraphicsItem *> items = childItems();
    if (0 <= index && index < items.size()) {
        Scene *scene = static_cast<Scene *>(this->scene());
        QGraphicsItem *item = items.at(index);
        item->setParentItem(0);
        if (scene)
            scene->removeItem(item);
        delete item;
    }
}

maksi::ScriptObject *GraphicsGroup::insertChild(int pos, const QString &script_id, const QString &script_tag)
{
    QGraphicsItem *gi = 0;
    maksi::ScriptObject *so = 0;

    Graphics *owner = 0;
    QVariant owner_val = this->data(GRAPHICS_DATA_PTR);
    if (owner_val.canConvert<Graphics*>())
        owner = owner_val.value<Graphics*>();

    if (owner) {
        if (script_tag == GraphicsPoint::tag) {
            GraphicsPoint *p = new GraphicsPoint(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsRect::tag) {
            GraphicsRect *p = new GraphicsRect(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsOval::tag) {
            GraphicsOval *p = new GraphicsOval(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsText::tag) {
            GraphicsText *p = new GraphicsText(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsPolygon::tag) {
            GraphicsPolygon *p = new GraphicsPolygon(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsPolyline::tag) {
            GraphicsPolygon *p = new GraphicsPolygon(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsMultiline::tag) {
            GraphicsPolygon *p = new GraphicsPolygon(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsPolypoint::tag) {
            GraphicsPolypoint *p = new GraphicsPolypoint(owner, script_id);
            gi = p; so = p;
        } else if (script_tag == GraphicsGroup::tag) {
            GraphicsGroup *p = new GraphicsGroup(owner, script_id);
            gi = p; so = p;
        }
    }

    if (gi) {
        if (pos < 0)
            pos = 0;

        QList<QGraphicsItem *> items = childItems();
        if (pos < items.size()) {
            QGraphicsItem *pc = items.at(pos);
            gi->setGroup(this);
            gi->stackBefore(pc);
        } else {
            gi->setGroup(this);
        }

    }
    return so;
}



QString Graphics::tag("shape");

maksi::BaseScriptObject::property_t Graphics::props[] = {
    { "x", false },
    { "y", false },
    { "scale", false },
    { "rotation", false },
    { "opacity", false },
    { "visible", false },
    { "layer", false },
    { QString(), false }
};

maksi::BaseScriptObject::method_t Graphics::methods[] = {
    { "move" },
    { "rotate" },
    { "setFeature" },
    { "setState" },
    { QString() }
};

maksi::BaseScriptObject::info_t Graphics::info = {
    0, props, methods
};

Graphics::Graphics(const QString &script_id) :
    maksi::BaseScriptObject(script_id, tag, &info),
    m_group(new QGraphicsItemGroup)
{
}

Graphics::~Graphics()
{
    delete m_group;
}

QVariant Graphics::property(const QString &id)
{
    if (id == "x") {
        return m_group->x();
    } else if (id == "y") {
        return m_group->y();
    } else if (id == "scale") {
        return m_group->scale();
    } else if (id == "rotation") {
        return -m_group->rotation();
    } else if (id == "opacity") {
        return m_group->opacity();
    } else if (id == "visible") {
        return m_group->isVisible();
    } else if (id == "layer") {
        Scene *ps = static_cast<Scene *>(m_group->scene());
        if (ps)
            return ps->getGraphicsLayer(this);
    }
    return QVariant();
}

void Graphics::setProperty(const QString &id, const QVariant &val)
{
    if (id == "x") {
        m_group->setX(val.toDouble());
    } else if (id == "y") {
        m_group->setY(val.toDouble());
    } else if (id == "scale") {
        m_group->setScale(val.toDouble());
    } else if (id == "rotation") {
        m_group->setRotation(-fmod(val.toDouble(), 360.0));
    } else if (id == "opacity") {
        m_group->setOpacity(val.toDouble());
    } else if (id == "visible") {
        m_group->setVisible(val.toBool());
    } else if (id == "layer") {
        if (val.type() == QVariant::String) {
            Scene *ps = static_cast<Scene *>(m_group->scene());
            if (ps)
                ps->setGraphicsLayer(this, val.toString());
        }
    }
}

int Graphics::childCount() const
{
    QList<QGraphicsItem *> items = m_group->childItems();
    return items.size();
}

maksi::ScriptObject *Graphics::child(int index) const
{
    QList<QGraphicsItem *> items = m_group->childItems();
    if (0 <= index && index < items.size()) {
        QGraphicsItem *item = items.at(index);
        return dynamic_cast<maksi::ScriptObject *>(item);
    }
    return 0;
}

void Graphics::removeChild(int index)
{
    QList<QGraphicsItem *> items = m_group->childItems();
    if (0 <= index && index < items.size()) {
        Scene *scene = static_cast<Scene *>(m_group->scene());
        QGraphicsItem *item = items.at(index);
        item->setParentItem(0);
        if (scene)
            scene->removeItem(item);
        delete item;
    }
}

maksi::ScriptObject *Graphics::insertChild(int pos, const QString &script_id, const QString &script_tag)
{
    QGraphicsItem *gi = 0;
    maksi::ScriptObject *so;

    if (script_tag == GraphicsPoint::tag) {
        GraphicsPoint *p = new GraphicsPoint(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsRect::tag) {
        GraphicsRect *p = new GraphicsRect(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsOval::tag) {
        GraphicsOval *p = new GraphicsOval(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsText::tag) {
        GraphicsText *p = new GraphicsText(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsPolygon::tag) {
        GraphicsPolygon *p = new GraphicsPolygon(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsPolyline::tag) {
        GraphicsPolygon *p = new GraphicsPolygon(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsMultiline::tag) {
        GraphicsPolygon *p = new GraphicsPolygon(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsPolypoint::tag) {
        GraphicsPolypoint *p = new GraphicsPolypoint(this, script_id);
        gi = p; so = p;
    } else if (script_tag == GraphicsGroup::tag) {
        GraphicsGroup *p = new GraphicsGroup(this, script_id);
        gi = p; so = p;
    }

    if (gi) {
        if (pos < 0)
            pos = 0;

        QList<QGraphicsItem *> items = m_group->childItems();
        if (pos < items.size()) {
            QGraphicsItem *pc = items.at(pos);
            gi->setGroup(m_group);
            gi->stackBefore(pc);
        } else {
            gi->setGroup(m_group);
        }

    }
    return so;
}

QVariant Graphics::callMethod(const QString &id, const QVariantList &args)
{
    if (id == "move") {
        if (args.size() >= 2) {
            move(args.at(0).toDouble(), args.at(1).toDouble());
            return true;
        }
    } else if (id == "rotate") {
        if (args.size() >= 1) {
            rotate(args.at(0).toDouble());
            return true;
        }
    } else if (id == "setFeature") {
        if (args.size() >= 1) {
            return setFeature(args.at(0).toString());
        }
    } else if (id == "setState") {
        if (args.size() >= 1) {
            return setState(args.at(0).toString());
        }
    }
    return false;
}

void Graphics::move(qreal dx, qreal dy)
{
    m_group->setPos(m_group->x() + dx, m_group->y() + dy);
}

void Graphics::rotate(qreal da)
{
    m_group->setRotation(fmod(-m_group->rotation() + da, 360.0));
}


maksi::ScriptObject *Graphics::appendPoint(const QString & id)
{
    return insertChild(childCount(), id, GraphicsPoint::tag);
}

maksi::ScriptObject *Graphics::appendRect(const QString &id)
{
    return insertChild(childCount(), id, GraphicsRect::tag);
}

maksi::ScriptObject *Graphics::appendOval(const QString &id)
{
    return insertChild(childCount(), id, GraphicsOval::tag);
}

maksi::ScriptObject *Graphics::appendText(const QString &id)
{
    return insertChild(childCount(), id, GraphicsText::tag);
}

maksi::ScriptObject *Graphics::appendPolygon(const QString &id)
{
    return insertChild(childCount(), id, GraphicsPolygon::tag);
}

maksi::ScriptObject *Graphics::appendPolyline(const QString &id)
{
    return insertChild(childCount(), id, GraphicsPolyline::tag);
}

maksi::ScriptObject *Graphics::appendMultiline(const QString &id)
{
    return insertChild(childCount(), id, GraphicsMultiline::tag);
}

maksi::ScriptObject *Graphics::appendPolypoint(const QString &id)
{
    return insertChild(childCount(), id, GraphicsPolypoint::tag);
}

maksi::ScriptObject *Graphics::appendGroup(const QString &id)
{
    return insertChild(childCount(), id, GraphicsGroup::tag);
}

bool Graphics::setFeature(const QString &feature_id)
{
    Scene *ps = static_cast<Scene *>(m_group->scene());
    if (ps) {
        const Feature *pf = ps->maket()->feature(feature_id);
        if (pf)
            return setFeature(pf);
    }
    return false;
}

bool Graphics::setFeature(const Feature *feature)
{
    //qDebug("feature: id=%s feature=%s", qPrintable(script_id()), qPrintable(feature->id()));
    bool found = false;
    for (info_t *cur = m_info; cur; cur = cur->base) {
        if (cur->property) {
            for (int i = 0; !cur->property[i].id.isEmpty(); i++) {
                QVariant val = feature->property(cur->property[i].id);
                if (val.isValid()) {
                    setProperty(cur->property[i].id, val);
                    found = true;
                }
            }
        }
    }

    QList<QGraphicsItem *> items = m_group->childItems();
    for (int i = 0; i < items.size(); i++) {
        GraphicsScriptItem *pg = static_cast<GraphicsScriptItem *>(items.at(i));
        const Feature *pf = feature->feature(pg->script_id());
        if (!pf)
            pf = feature;
        //qDebug("  child: id=%s feature=%s", qPrintable(pg->script_id()), qPrintable(pf->id()));
        if (pg->setFeature(pf))
            found = true;
    }
    return found;
}

bool Graphics::setState(const QString &state_id)
{
    Scene *ps = static_cast<Scene *>(m_group->scene());
    if (ps) {
        return ps->setGraphicsState(this, state_id);
    }
    return false;
}
#endif

};

 /* End of code */
