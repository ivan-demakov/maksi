/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    event.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jul  8 12:26:59 2011
 *
 * @brief   events
 *
 */

#include "event.h"
#include "maket.h"

#include <maksi/conf.h>

namespace maket
{

EventElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t EventElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<EventElement> elem(new EventElement(m_maket, conf.name(), type));
        *value = maksi::fromElement(elem);
    }
    return maksi::RESULT_OK;
}

EventElement::EventElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    maksi::Element(name, type),
    m_maket(maket)
{
}

EventElement::~EventElement()
{
}

QVariant EventElement::elementValue()
{
    Value data;
    data.type = attribute("type").toString();
    data.handler = attribute("handler").toString();

    if (attributeExists("keys")) {
        data.keys = QKeySequence(attribute("keys").toString());
    }

    data.modifiers = 0;
    if (attributeExists("modifiers")) {
        QString mods = attribute("modifiers").toString();
        if (mods == "any") {
            data.modifiers = (Qt::KeyboardModifiers) -1;
        } else {
            QKeySequence seq(mods+'+');
            data.modifiers = (Qt::KeyboardModifiers) (seq[0] & (Qt::SHIFT | Qt::META | Qt::CTRL | Qt::ALT));
        }
    }

    data.button = Qt::NoButton;
    if (attributeExists("button")) {
        QString bstr = attribute("button").toString();
        if (bstr == "left") {
            data.button = Qt::LeftButton;
        } else if (bstr == "right") {
            data.button = Qt::RightButton;
        } else if (bstr == "middle") {
            data.button = Qt::MidButton;
        }
    }

    return QVariant::fromValue(data);
}

};

/* End of code */
