/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maket.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 14 21:38:15 2010
 *
 * @brief   defines
 *
 */

#ifndef MAKET_H
#define MAKET_H

#include "defs.h"

#include <QObject>
#include <QString>
#include <QVariant>

#include <maksi/maksi.h>


//! maket namespace
namespace maket
{

class MaketData;
class Scene;
class View;


//! The Maket class provides interface to the maket library
class MAKET_EXPORT Maket : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Maket);

    Maket();                    /**< private constructor */
    Maket(const QString &name); /**< private constructor */
    ~Maket();                   /**< private destructor */

public:

    /** Get instance
     *
     * @param name name of the instance
     *
     * @return maket instance
     */
    static Maket *instance(const QString &name);


    /** destroy instance
     *
     *
     * @param maket maket instance
     */
    static void destroy(Maket *maket);


    /** Get name of the maket instance
     *
     * @return instance name
     */
    const QString &name() const;


    /** Initialize types
     *
     * This function should be called first
     *
     * @param confURL config with type definitions
     *
     * @return result code
     */
    maksi::result_t loadTypes(const QString &confURL);


    /** Load config
     *
     * Read config and initialize data.
     * This function should be called after loadTypes()
     *
     * @param confURL config with data definitions
     *
     * @return result code
     */
    maksi::result_t loadConfig(const QString &confURL);


    /** Get color
     *
     * @param name color name
     *
     * @return color
     */
    QColor color(const QString &name) const;


    /** Add color
     *
     * @param name color name
     * @param val color value
     *
     */
    void addColor(const QString &name, QVariant val);


    /** Get brush
     *
     * @param name brush name
     *
     */
    QBrush brush(const QString &name) const;


    /** Add brush
     *
     * @param name brush name
     * @param val brush value
     *
     */
    void addBrush(const QString &name, QVariant val);


    /** Get pen
     *
     * @param id pen name
     *
     * @return pen
     */
    QPen pen(const QString &name) const;


    /** Add pen
     *
     * @param name pen name
     * @param val pen value
     *
     */
    void addPen(const QString &name, QVariant val);


    /** Get font
     *
     * @param name font name
     *
     * @return font
     */
    QFont font(const QString &name) const;


    /** Add font
     *
     * @param name font name
     * @param val font value
     *
     */
    void addFont(const QString &name, QVariant val);


    /** Get scene
     *
     * Scene is container for the graphics.
     *
     * @param name scene name
     *
     * @return scene
     */
    QSharedPointer<Scene> scene(const QString &name);


    /** Get view
     *
     * View is window for the graphics.
     *
     * @param name view name
     *
     * @return view
     */
    QSharedPointer<View> view(const QString &name);


    /** Get the feature property
     *
     * The function looks up the property in the feature and returns the property value,
     * or invalid value if the property or feature is not found.
     * The features should be defined in one of the config files (\sa maket::Maket::readConfig()).
     *
     * @param feature_id feature id
     * @param property_id property id
     *
     * @return property value
     */
    QVariant featureProperty(const QString &feature_id, const QString &property_id) const;


    /** Get connected maksi
     *
     * @return maksi instance
     */
    maksi::Maksi *maksi() const;

public slots:
    void emitError(QString msg);
    void emitWarn(QString msg);
    void emitInfo(QString msg);
    void emitDebug(QString msg);

signals:
    void error(const QString &msg); /**< emited if error procedure called in script */
    void warn(const QString &msg);  /**< emited if warn procedure called in script */
    void info(const QString &msg);  /**< emited if info procedure called in script */
    void debug(const QString &msg); /**< emited if debug procedure called in script */

private:
    maksi::result_t initColors(const maksi::Conf &conf);
    maksi::result_t initBrushes(const maksi::Conf &conf);
    maksi::result_t initPens(const maksi::Conf &conf);
    maksi::result_t initFonts(const maksi::Conf &conf);
    maksi::result_t initScenes(const maksi::Conf &conf);
    maksi::result_t initViews(const maksi::Conf &conf);

    MaketData *d;
};

};


#endif

/* End of file */
