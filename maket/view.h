/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    view.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Nov 24 11:25:26 2010
 *
 * @brief   view
 *
 */

#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsView>

#include <maksi/elem.h>
#include <maksi/schema.h>
#include <maksi/script.h>

#include "defs.h"
#include "event.h"


class QRubberBand;


//! maket namespace
namespace maket
{

class Maket;
class Scene;


class MAKET_EXPORT View : public QGraphicsView, public maksi::Element
{
    Q_OBJECT
    Q_DISABLE_COPY(View);
public:

    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    enum mode_t {
        MODE_VIEW,
        MODE_SCROLL,
        MODE_DRAG,
        MODE_ZOOM_IN,
        MODE_ZOOM_OUT,
        MODE_SELECT_IN,
        MODE_SELECT_OUT
    };

    View(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~View();

    virtual QVariant elementValue();
    virtual bool attributeExists(const QString &name);
    virtual QVariant attribute(const QString &name);
    virtual void setAttribute(const QString &name, const QVariant &value);
    virtual QVariant appendElement(const QVariant &value);
    virtual QList<QString> methodNames();
    virtual QVariant callMethod(const QString &name, QVariantList args);

public slots:
    void setMouseScrollMode(bool value, int x, int y, int button);
    void setMouseDragMode(bool value, int x, int y, int button);
    void setMouseRubberMode(bool value, mode_t mode, int x, int y, int button);
    void scale(qreal dx);

protected:
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *event);

//    virtual QVariant callMethod(const QString &id, const QVariantList &args);

public:
    QVariantList graphicsAt(bool selected, int x, int y);

private:
    Maket *m_maket;
    QSharedPointer<maket::Scene> m_scene;
    QSharedPointer<maksi::Script> m_script;
    QMultiMap<QString, EventElement::Value> m_events;

    mode_t m_mode;
    QRubberBand *m_rubberBand;

    int m_lastX, m_lastY, m_lastButton;
    int m_keys[4];
};

};

Q_DECLARE_METATYPE(maket::View*)

#endif

/* End of file */
