/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    color.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jun 24 22:56:23 2011
 *
 * @brief   colors
 *
 */

#include "color.h"
#include "maket.h"

#include <maksi/conf.h>


namespace maket
{

ColorElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t ColorElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<ColorElement> elem(new ColorElement(m_maket, conf.name(), type));
        *value = maksi::fromElement(elem);
    }
    return maksi::RESULT_OK;
}

ColorElement::ColorElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_maket(maket)
{
}

ColorElement::~ColorElement()
{
}

QVariant ColorElement::elementValue()
{
    QColor color;

    if (attributeExists("ref")) {
        color = m_maket->color(attribute("ref").toString());
    } else if (attributeExists("value")) {
        color = QColor(attribute("value").toString());
    } else {
        color = QColor(attribute("red").toInt(), attribute("green").toInt(), attribute("blue").toInt());
    }
    if (attributeExists("alpha")) {
        color.setAlpha(attribute("alpha").toInt());
    }
    return color;
}

};

 /* End of code */
