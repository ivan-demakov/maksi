/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maket.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 14 21:39:42 2010
 *
 * @brief   defines
 *
 */

#include "maket.h"
#include "color.h"
#include "brush.h"
#include "pen.h"
#include "font.h"
#include "layer.h"
#include "scene.h"
#include "event.h"
#include "view.h"
#include "graphics.h"

#include <maksi/conf.h>

#include <QMutex>
#include <QMutexLocker>


namespace maket
{

static QMutex maket_mutex;
static QMap<QString, Maket *> maket_instances;


class MaketData
{
public:
    explicit MaketData(const QString &name) : m_name(name), m_maksi(maksi::Maksi::instance(name)) {}
    ~MaketData();

    QString m_name;
    maksi::Maksi *m_maksi;
    QMutex m_mutex;

    QHash<QString, QColor> m_colors;
    QHash<QString, QBrush> m_brushes;
    QHash<QString, QPen> m_pens;
    QHash<QString, QFont> m_fonts;
    QHash<QString, QSharedPointer<Scene> > m_scenes;
    QHash<QString, maksi::Conf> m_views;
};


MaketData::~MaketData()
{
}

maksi::Maksi *Maket::maksi() const
{
    return d ? d->m_maksi : 0;
}

Maket::Maket(const QString &name) : d(new MaketData(name))
{
    connect(d->m_maksi, SIGNAL(error(const QString &)), SIGNAL(error(const QString &)));
    connect(d->m_maksi, SIGNAL(warn(const QString &)), SIGNAL(warn(const QString &)));
    connect(d->m_maksi, SIGNAL(info(const QString &)), SIGNAL(info(const QString &)));
    connect(d->m_maksi, SIGNAL(debug(const QString &)), SIGNAL(debug(const QString &)));
}

Maket::~Maket()
{
    qDebug("delete maket '%s'", qPrintable(d->m_name));
    delete d;
}


void Maket::emitError(QString msg)
{
    d->m_maksi->emitError(msg);
}

void Maket::emitWarn(QString msg)
{
    d->m_maksi->emitWarn(msg);
}

void Maket::emitInfo(QString msg)
{
    d->m_maksi->emitInfo(msg);
}

void Maket::emitDebug(QString msg)
{
    d->m_maksi->emitDebug(msg);
}

Maket *Maket::instance(const QString &name)
{
    QMutexLocker locker(&maket_mutex);

    static int registered = 0;
    if (!registered) {
        registered++;
        qRegisterMetaType<QLinearGradient>("QLinearGradient");
        qRegisterMetaType<QRadialGradient>("QRadialGradient");
        qRegisterMetaType<maket::GradientStopElement::Value>("maket::GradientStopElement::Value");
        qRegisterMetaType<maket::TextureElement::Value>("maket::TextureElement::Value");
        qRegisterMetaType<maket::EventElement::Value>("maket::EventElement::Value");
        qRegisterMetaType<maket::LayerElement*>("maket::LayerElement*");
        qRegisterMetaType<maket::Scene*>("maket::Scene*");
        qRegisterMetaType<maket::View*>("maket::View*");
        qRegisterMetaType<maket::Graphics*>("maket::Graphics*");
        qRegisterMetaType<maket::GraphicsRect*>("maket::GraphicsRect*");
        qRegisterMetaType<maket::GraphicsOval*>("maket::GraphicsOval*");
        qRegisterMetaType<maket::GraphicsText*>("maket::GraphicsText*");
        qRegisterMetaType<maket::GraphicsPoint*>("maket::GraphicsPoint*");
        qRegisterMetaType<maket::GraphicsPolygon*>("maket::GraphicsPolygon*");
        qRegisterMetaType<maket::GraphicsPolyline*>("maket::GraphicsPolyline*");
        qRegisterMetaType<maket::GraphicsMultiline*>("maket::GraphicsMultiline*");
    }

    Maket *maket = maket_instances.value(name);
    if (!maket) {
        maket = new Maket(name);
        maket_instances.insert(name, maket);
    }
    return maket;
}

void Maket::destroy(Maket *maket)
{
    QMutexLocker locker(&maket_mutex);
    maket_instances.remove(maket->d->m_name);
    delete maket;
}

const QString &Maket::name() const
{
    return d->m_name;
}

void Maket::addColor(const QString &name, QVariant val)
{
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem = val.value<maksi::element_t>();
        val = elem->elementValue();
    }
    if (val.canConvert<QColor>()) {
        QColor color = val.value<QColor>();
        QMutexLocker locker(&d->m_mutex);
        d->m_colors.insert(name, color);
    }
}

QColor Maket::color(const QString &id) const
{
    static QColor def(0, 0, 0);

    QMutexLocker locker(&d->m_mutex);
    return d->m_colors.value(id, def);
}


void Maket::addBrush(const QString &name, QVariant val)
{
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem = val.value<maksi::element_t>();
        val = elem->elementValue();
    }
    if (val.canConvert<QBrush>()) {
        QBrush brush = val.value<QBrush>();
        QMutexLocker locker(&d->m_mutex);
        d->m_brushes.insert(name, brush);
    }
}

QBrush Maket::brush(const QString &id) const
{
    static QBrush def(Qt::NoBrush);

    QMutexLocker locker(&d->m_mutex);
    return d->m_brushes.value(id, def);
}


void Maket::addPen(const QString &name, QVariant val)
{
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem = val.value<maksi::element_t>();
        val = elem->elementValue();
    }
    if (val.canConvert<QPen>()) {
        QPen pen = val.value<QPen>();
        QMutexLocker locker(&d->m_mutex);
        d->m_pens.insert(name, pen);
    }
}

QPen Maket::pen(const QString &id) const
{
    static QPen def(Qt::NoPen);

    QMutexLocker locker(&d->m_mutex);
    return d->m_pens.value(id, def);
}


void Maket::addFont(const QString &name, QVariant val)
{
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem = val.value<maksi::element_t>();
        val = elem->elementValue();
    }
    if (val.canConvert<QFont>()) {
        QFont font = val.value<QFont>();
        QMutexLocker locker(&d->m_mutex);
        d->m_fonts.insert(name, font);
    }
}

QFont Maket::font(const QString &name) const
{
    static QFont def;

    QMutexLocker locker(&d->m_mutex);
    return d->m_fonts.value(name, def);
}

QSharedPointer<Scene> Maket::scene(const QString &name)
{
    QMutexLocker locker(&d->m_mutex);
    return d->m_scenes.value(name);
}

QSharedPointer<View> Maket::view(const QString &name)
{
    static QXmlName maksi_view = maksi::xml::qname("view");

    if (d->m_views.contains(name)) {
        d->m_mutex.lock();
        maksi::Conf conf = d->m_views.value(name);
        d->m_mutex.unlock();

        maksi::schema_type_t type = d->m_maksi->type(maksi_view);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_view)));
            return QSharedPointer<View>();
        }

        QVariant var;
        maksi::result_t res = type.getValue(conf, maksi_view, &var, d->m_maksi, true);
        if (maksi::RESULT_OK != res) {
            emitError(QString("%1:%2: invalid view").arg(conf.source()).arg(conf.line()));
            return QSharedPointer<View>();
        }

        if (var.canConvert<maksi::element_t>()) {
            maksi::element_t elem = var.value<maksi::element_t>();
            var = elem->elementValue();
            if (var.canConvert<View*>()) {
                qDebug("create view %s", qPrintable(elem->attribute("name").toString()));
                return elem.staticCast<View>();
            }
        }
    }
    return QSharedPointer<View>();
}

maksi::result_t Maket::loadTypes(const QString &confURL)
{
    static QXmlName maksi_color = maksi::xml::qname("color");
    static QXmlName maksi_brush = maksi::xml::qname("brush");
    static QXmlName maksi_gstop = maksi::xml::qname("gradient-stop");
    static QXmlName maksi_lgradient = maksi::xml::qname("linear-gradient");
    static QXmlName maksi_rgradient = maksi::xml::qname("radial-gradient");
    static QXmlName maksi_texture = maksi::xml::qname("texture");
    static QXmlName maksi_pen = maksi::xml::qname("pen");
    static QXmlName maksi_font = maksi::xml::qname("font");
    static QXmlName maksi_layer = maksi::xml::qname("layer");
    static QXmlName maksi_scene = maksi::xml::qname("scene");
    static QXmlName maksi_event = maksi::xml::qname("event");
    static QXmlName maksi_view = maksi::xml::qname("view");
    static QXmlName maksi_graphics = maksi::xml::qname("graphics");
    static QXmlName maksi_rect = maksi::xml::qname("graphicsRect");
    static QXmlName maksi_oval = maksi::xml::qname("graphicsOval");
    static QXmlName maksi_text = maksi::xml::qname("graphicsText");
    static QXmlName maksi_point = maksi::xml::qname("graphicsPoint");
    static QXmlName maksi_polygon = maksi::xml::qname("graphicsPolygon");
    static QXmlName maksi_polyline = maksi::xml::qname("graphicsPolyline");
    static QXmlName maksi_multiline = maksi::xml::qname("graphicsMultiline");

    maksi::result_t res = d->m_maksi->loadConfig(confURL, 0);
    if (maksi::RESULT_OK != res)
        return res;

    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_color);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_color)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new ColorElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_gstop);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_gstop)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GradientStopElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_lgradient);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_lgradient)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new LinearGradientElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_rgradient);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_rgradient)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new RadialGradientElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_brush);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_brush)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new BrushElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_texture);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_texture)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new TextureElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_pen);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_pen)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new PenElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_font);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_font)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new FontElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_layer);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_layer)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new LayerElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_scene);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_scene)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new Scene::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_event);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_event)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new EventElement::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_view);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_view)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new View::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_graphics);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_graphics)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new Graphics::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_rect);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_rect)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsRect::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_oval);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_oval)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsOval::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_text);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_text)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsText::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_point);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_point)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsPoint::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_polygon);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_polygon)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsPolygon::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_polyline);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_polyline)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsPolyline::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }
    {
        maksi::schema_type_t type = d->m_maksi->type(maksi_multiline);
        if (!type.isValid()) {
            emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_multiline)));
            return maksi::RESULT_ERROR;
        }
        QSharedPointer<maksi::SchemaTypeFactory> factory(new GraphicsMultiline::TypeFactory(this, type.factory()));
        type.setFactory(factory);
    }

    return maksi::RESULT_OK;
}

maksi::result_t Maket::loadConfig(const QString &confURL)
{
    maksi::Conf conf;
    maksi::result_t res = d->m_maksi->loadConfig(confURL, &conf);
    if (maksi::RESULT_OK != res)
        return res;

    res = initColors(conf);
    if (maksi::RESULT_OK != res)
        return res;

    res = initBrushes(conf);
    if (maksi::RESULT_OK != res)
        return res;

    res = initPens(conf);
    if (maksi::RESULT_OK != res)
        return res;

    res = initFonts(conf);
    if (maksi::RESULT_OK != res)
        return res;

    res = initScenes(conf);
    if (maksi::RESULT_OK != res)
        return res;

    res = initViews(conf);
    if (maksi::RESULT_OK != res)
        return res;

    return res;
}

maksi::result_t Maket::initColors(const maksi::Conf &conf)
{
    static QXmlName maksi_color = maksi::xml::qname("color");
    static QXmlName maksi_name = maksi::xml::qname("name");

    for (int i = 0; i < conf.countNodes(); i++) {
        const maksi::Conf &node = conf.node(i);
        if (node.isNode(maksi_color)) {
            QString name = node.attribute(maksi_name);
            if (name.isEmpty()) {
                emitWarn(tr("%1:%2: color has not required attribute %3").arg(node.source()).arg(node.line()).arg(maksi::xml::toString(maksi_name)));
                continue;
            }

            maksi::schema_type_t type = d->m_maksi->type(maksi_color);
            if (!type.isValid()) {
                emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_color)));
                return maksi::RESULT_ERROR;
            }

            QVariant var;
            maksi::result_t res = type.getValue(node, maksi_color, &var, d->m_maksi, true);
            if (maksi::RESULT_OK != res) {
                emitWarn(QString("%1:%2: invalid color '%3'").arg(node.source()).arg(node.line()).arg(name));
                continue;
            }
            addColor(name, var);
        }
    }
    return maksi::RESULT_OK;
}


maksi::result_t Maket::initBrushes(const maksi::Conf &conf)
{
    static QXmlName maksi_brush = maksi::xml::qname("brush");
    static QXmlName maksi_name = maksi::xml::qname("name");

    for (int i = 0; i < conf.countNodes(); i++) {
        const maksi::Conf &node = conf.node(i);
        if (node.isNode(maksi_brush)) {
            QString name = node.attribute(maksi_name);
            if (name.isEmpty()) {
                emitWarn(tr("%1:%2: brush has not required attribute %3").arg(node.source()).arg(node.line()).arg(maksi::xml::toString(maksi_name)));
                continue;
            }

            maksi::schema_type_t type = d->m_maksi->type(maksi_brush);
            if (!type.isValid()) {
                emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_brush)));
                return maksi::RESULT_ERROR;
            }

            QVariant var;
            maksi::result_t res = type.getValue(node, maksi_brush, &var, d->m_maksi, true);
            if (maksi::RESULT_OK != res) {
                emitWarn(QString("%1:%2: invalid brush '%3'").arg(node.source()).arg(node.line()).arg(name));
                continue;
            }

            addBrush(name, var);
        }
    }
    return maksi::RESULT_OK;
}

maksi::result_t Maket::initPens(const maksi::Conf &conf)
{
    static QXmlName maksi_pen = maksi::xml::qname("pen");
    static QXmlName maksi_name = maksi::xml::qname("name");

    for (int i = 0; i < conf.countNodes(); i++) {
        const maksi::Conf &node = conf.node(i);
        if (node.isNode(maksi_pen)) {
            QString name = node.attribute(maksi_name);
            if (name.isEmpty()) {
                emitWarn(tr("%1:%2: pen has not required attribute %3").arg(node.source()).arg(node.line()).arg(maksi::xml::toString(maksi_name)));
                continue;
            }

            maksi::schema_type_t type = d->m_maksi->type(maksi_pen);
            if (!type.isValid()) {
                emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_pen)));
                return maksi::RESULT_ERROR;
            }

            QVariant var;
            maksi::result_t res = type.getValue(node, maksi_pen, &var, d->m_maksi, true);
            if (maksi::RESULT_OK != res) {
                emitWarn(QString("%1:%2: invalid pen '%3'").arg(node.source()).arg(node.line()).arg(name));
                continue;
            }

            addPen(name, var);
        }
    }
    return maksi::RESULT_OK;
}

maksi::result_t Maket::initFonts(const maksi::Conf &conf)
{
    static QXmlName maksi_font = maksi::xml::qname("font");
    static QXmlName maksi_name = maksi::xml::qname("name");

    for (int i = 0; i < conf.countNodes(); i++) {
        const maksi::Conf &node = conf.node(i);
        if (node.isNode(maksi_font)) {
            QString name = node.attribute(maksi_name);
            if (name.isEmpty()) {
                error(tr("%1:%2: brush has not required attribute %3").arg(node.source()).arg(node.line()).arg(maksi::xml::toString(maksi_name)));
                return maksi::RESULT_INVALID_CONFIG;
            }

            maksi::schema_type_t type = d->m_maksi->type(maksi_font);
            if (!type.isValid()) {
                emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_font)));
                return maksi::RESULT_ERROR;
            }

            QVariant var;
            maksi::result_t res = type.getValue(node, maksi_font, &var, d->m_maksi, true);
            if (maksi::RESULT_OK != res) {
                emitWarn(QString("%1:%2: invalid font '%3'").arg(node.source()).arg(node.line()).arg(name));
                continue;
            }

            addFont(name, var);
        }
    }
    return maksi::RESULT_OK;
}

maksi::result_t Maket::initScenes(const maksi::Conf &conf)
{
    static QXmlName maksi_scene = maksi::xml::qname("scene");

    for (int i = 0; i < conf.countNodes(); i++) {
        const maksi::Conf &node = conf.node(i);
        if (node.isNode(maksi_scene)) {
            maksi::schema_type_t type = d->m_maksi->type(maksi_scene);
            if (!type.isValid()) {
                emitError(tr("required type %1 is not defined").arg(maksi::xml::toString(maksi_scene)));
                return maksi::RESULT_ERROR;
            }

            QVariant var;
            maksi::result_t res = type.getValue(node, maksi_scene, &var, d->m_maksi, true);
            if (maksi::RESULT_OK != res) {
                emitWarn(QString("%1:%2: invalid scene").arg(node.source()).arg(node.line()));
                continue;
            }

            if (var.canConvert<maksi::element_t>()) {
                maksi::element_t elem = var.value<maksi::element_t>();
                var = elem->elementValue();
                if (var.canConvert<Scene*>()) {
                    QMutexLocker locker(&d->m_mutex);
                    d->m_scenes.insert(elem->attribute("name").toString(), elem.staticCast<Scene>());
                }
            }
        }
    }
    return maksi::RESULT_OK;
}

maksi::result_t Maket::initViews(const maksi::Conf &conf)
{
    static QXmlName maksi_view = maksi::xml::qname("view");
    static QXmlName maksi_name = maksi::xml::qname("name");

    for (int i = 0; i < conf.countNodes(); i++) {
        const maksi::Conf &node = conf.node(i);
        if (node.isNode(maksi_view)) {
            QString name = node.attribute(maksi_name);
            if (name.isEmpty()) {
                emitWarn(tr("%1:%2: view has not required attribute %3").arg(node.source()).arg(node.line()).arg(maksi::xml::toString(maksi_name)));
                continue;
            }
            QMutexLocker locker(&d->m_mutex);
            d->m_views.insert(name, node);
        }
    }
    return maksi::RESULT_OK;
}


#if 0
QVariant Maket::featureProperty(const QString &feature_id, const QString &property_id) const
{
    const Feature *pf = d->feature(feature_id);
    if (pf)
        return pf->property(property_id);
    return QVariant();
}
#endif

};

 /* End of code */
