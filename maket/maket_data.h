/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maket_data.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Nov 24 12:28:38 2010
 *
 * @brief   internal maket data
 *
 */

#ifndef MAKET_DATA_H
#define MAKET_DATA_H

#include "maket.h"
#include <maksi/maksi.h>

#include <QMap>
#include <QList>
#include <QKeySequence>


class QFileInfo;
class QDomDocument;
class QDomElement;


//! maket namespace
namespace maket
{

class Scene;


enum graphics_data_t {
    GRAPHICS_DATA_PTR
};

class Feature
{
public:
    Feature(const QString &id, const Feature *proto) : m_id(id), m_proto(proto) {}
    ~Feature();

    const QString &id() const { return m_id; }
    const Feature *proto() const { return m_proto; }

    QVariant property(const QString &prop_id) const;
    void setProperty(const QString &prop_id, const QVariant &val) { m_props.insert(prop_id, val); }

    Feature *feature(const QString &id) const { return m_features.value(id); }
    void addFeature(const QString &id, Feature *pf) { m_features.insert(id, pf); }

private:
    QString m_id;
    const Feature *m_proto;
    QMap<QString, QVariant> m_props;
    QMap<QString, Feature *> m_features;
};

struct LayerData
{
    QString id;
    double z;
    bool visible;
    QList<LayerData> m_layers;
};

struct FeatureData
{
    QString id;
    QString state;
};

struct GraphicsData
{
    QList<FeatureData> features;
};

struct SceneData
{
    double x, y, w, h;
    QString background_brush;
    QList<LayerData> layers;
    QMap<QString, GraphicsData> graphics;
};

struct EventData
{
    Qt::KeyboardModifiers modifiers;
    Qt::MouseButton button;
    QKeySequence keys;
    QString handler;
};

struct ViewData
{
    QString script;
    QMultiMap<QString, EventData> events;
};


class MaketData : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(MaketData);

public:
    MaketData(const QString &name);
    ~MaketData();

    const QString &name() const { return m_name; }

    result_t readConfig(const QString &config);
    result_t readConfig(const QFileInfo &info, const QDomElement &root);

    const QColor &color(const QString &id) const;
    const QBrush &brush(const QString &id) const;
    const QPen &pen(const QString &id) const;
    const QFont &font(const QString &id) const;
    const Feature *feature(const QString &id) const { return m_features.value(id); }

    Scene *createScene(const QString &id);
    View *createView(const QString &id, Scene *scene);

    void emitError(const QString &msg) { emit error(QString("%1: %2").arg(m_name).arg(msg)); }
    void emitWarn(const QString &msg) { emit warn(QString("%1: %2").arg(m_name).arg(msg)); }
    void emitInfo(const QString &msg) { emit info(QString("%1: %2").arg(m_name).arg(msg)); }
    void emitDebug(const QString &msg) { emit debug(QString("%1: %2").arg(m_name).arg(msg)); }

    maksi::Maksi *maksi() const { return m_maksi; }

signals:
    void error(const QString &msg);
    void warn(const QString &msg);
    void info(const QString &msg);
    void debug(const QString &msg);

private:
    result_t checkFile(const QFileInfo &info);
    result_t parseConfig(const QFileInfo &info, QDomDocument *doc);
    result_t initColors(const QFileInfo &info, const QDomElement &def);
    result_t initBrushes(const QFileInfo &info,const  QDomElement &def);
    result_t initPens(const QFileInfo &info, const QDomElement &def);
    result_t initFonts(const QFileInfo &info, const QDomElement &def);
    result_t initFeatures(const QFileInfo &info, const QDomElement &def, Feature *parent);
    result_t initViews(const QFileInfo &info, const QDomElement &def);
    result_t initScenes(const QFileInfo &info, const QDomElement &def);
    result_t initLayers(const QFileInfo &info, const QDomElement &def, QList<LayerData> *list);
    result_t initGraphics(const QFileInfo &info, const QDomElement &def, SceneData *scene);

    QString m_name;
    QMap<QString, QColor *> m_colors;
    QMap<QString, QBrush *> m_brushes;
    QMap<QString, QPen *> m_pens;
    QMap<QString, QFont *> m_fonts;
    QMap<QString, Feature *> m_features;
    QMap<QString, SceneData *> m_scenes;
    QMap<QString, ViewData *> m_views;

    maksi::Maksi *m_maksi;

    friend class Maket;
};


};

#endif

/* End of file */
