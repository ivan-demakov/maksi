/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maket_data.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Nov 24 12:30:28 2010
 *
 * @brief   internal data
 *
 */

#include "maket_data.h"
#include "scene.h"
#include "view.h"

#include <QtGui>
#include <QFileInfo>
#include <QDir>
#include <QDomDocument>


namespace maket
{

Feature::~Feature()
{
    {
        QMutableMapIterator<QString, Feature *> i(m_features);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
}

QVariant Feature::property(const QString &pid) const
{
    for (const Feature *pf = this; pf; pf = pf->proto()) {
        //qDebug("get property: id=%s feature=%s", qPrintable(pid), qPrintable(pf->id()));
        QVariant val = pf->m_props.value(pid);
        if (val.isValid())
            return val;
    }
    return QVariant();
}


MaketData::MaketData(const QString &name) :
    m_name(name),
    m_maksi(0)
{
    m_maksi = maksi::Maksi::instance(name);
    if (m_maksi) {
        connect(m_maksi, SIGNAL(error(const QString &)), SIGNAL(error(const QString &)));
        connect(m_maksi, SIGNAL(warn(const QString &)), SIGNAL(warn(const QString &)));
        connect(m_maksi, SIGNAL(info(const QString &)), SIGNAL(info(const QString &)));
        connect(m_maksi, SIGNAL(debug(const QString &)), SIGNAL(debug(const QString &)));
    }
}

MaketData::~MaketData()
{
    maksi::Maksi::destroy(m_maksi);

    {
        QMutableMapIterator<QString, QColor *> i(m_colors);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
    {
        QMutableMapIterator<QString, QBrush *> i(m_brushes);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
    {
        QMutableMapIterator<QString, QPen *> i(m_pens);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
    {
        QMutableMapIterator<QString, QFont *> i(m_fonts);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
    {
        QMutableMapIterator<QString, Feature *> i(m_features);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
    {
        QMutableMapIterator<QString, SceneData *> i(m_scenes);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
    {
        QMutableMapIterator<QString, ViewData *> i(m_views);
        while (i.hasNext()) {
            i.next();
            delete i.value();
            i.remove();
        }
    }
}

result_t MaketData::readConfig(const QString &config_filename)
{
    QFileInfo defs_info(config_filename);
    result_t res = checkFile(defs_info);
    if (RESULT_OK != res)
        return res;

    QDomDocument defs_doc;
    res = parseConfig(defs_info, &defs_doc);
    if (RESULT_OK != res)
        return res;

    QDomElement defs_root = defs_doc.documentElement();
    return readConfig(defs_info, defs_root);
}

result_t MaketData::readConfig(const QFileInfo &defs_info, const QDomElement &defs_root)
{
    result_t res = initColors(defs_info, defs_root);
    if (RESULT_OK != res)
        return res;

    res = initBrushes(defs_info, defs_root);
    if (RESULT_OK != res)
        return res;

    res = initPens(defs_info, defs_root);
    if (RESULT_OK != res)
        return res;

    res = initFonts(defs_info, defs_root);
    if (RESULT_OK != res)
        return res;

    res = initFeatures(defs_info, defs_root, 0);
    if (RESULT_OK != res)
        return res;

    res = initScenes(defs_info, defs_root);
    if (RESULT_OK != res)
        return res;

    res = initViews(defs_info, defs_root);
    if (RESULT_OK != res)
        return res;

    if (m_maksi) {
        return (result_t) m_maksi->readConfig(defs_info, defs_root);
    }

    return RESULT_OK;
}


result_t MaketData::checkFile(const QFileInfo &info)
{
    if (!info.exists()) {
        emitError(QString("file '%1' is not exists").arg(info.absoluteFilePath()));
        return RESULT_CONFIG_NOT_EXISTS;
    }
    if (!info.isFile() || !info.isReadable()) {
        emitError(QString("file '%1' is not readable").arg(info.absoluteFilePath()));
        return RESULT_CONFIG_NOT_READABLE;
    }
    return RESULT_OK;
}

result_t MaketData::parseConfig(const QFileInfo &info, QDomDocument *doc)
{
    QFile file(info.filePath());
    if (!file.open(QIODevice::ReadOnly)) {
        emitError(QString("cannot open config file %1: %2").arg(info.absoluteFilePath()).arg(file.errorString()));
        return RESULT_CONFIG_ERROR;
    }

    QString errmsg;
    int errline;
    if (!doc->setContent(&file, &errmsg, &errline)) {
        emitError(QString("%1:%2: %3").arg(info.filePath()).arg(errline).arg(errmsg));
        return RESULT_CONFIG_ERROR;
    }

    return RESULT_OK;
}

const QColor &MaketData::color(const QString &id) const
{
    static QColor defColor(0, 0, 0);

    QColor *p = m_colors.value(id, &defColor);
    return (p ? *p : defColor);
}

result_t MaketData::initBrushes(const QFileInfo &fileinfo, const QDomElement &root)
{
    for (QDomElement brush = root.firstChildElement("brush"); !brush.isNull(); brush = brush.nextSiblingElement("brush")) {
        QString id = brush.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: brush has no requared attribute 'id'").arg(fileinfo.filePath()).arg(brush.lineNumber()));
            continue;
        }
        QString style = brush.attribute("style", "solid");

        QScopedPointer<QBrush> pbrush(0);
        if (style == "no-brush") {
            pbrush.reset(new QBrush(Qt::NoBrush));
        } else if (style == "solid") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::SolidPattern));
        } else if (style == "hlines") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::HorPattern));
        } else if (style == "vlines") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::VerPattern));
        } else if (style == "cross") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::CrossPattern));
        } else if (style == "bdiag") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::BDiagPattern));
        } else if (style == "fdiag") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::FDiagPattern));
        } else if (style == "diagcross") {
            pbrush.reset(new QBrush(color(brush.attribute("color", "black")), Qt::DiagCrossPattern));
        } else if (style == "linear-gradient") {
            QLinearGradient g(brush.attribute("x1").toDouble(), brush.attribute("y1").toDouble(),
                              brush.attribute("x2").toDouble(), brush.attribute("y2").toDouble());

            QString spread = brush.attribute("spread", "pad");
            if (spread == "pad")
                g.setSpread(QGradient::PadSpread);
            else if (spread == "repeat")
                g.setSpread(QGradient::RepeatSpread);
            else if (spread == "reflect")
                g.setSpread(QGradient::ReflectSpread);

            for (QDomElement gs = brush.firstChildElement("gradient-stop"); !gs.isNull(); gs = gs.nextSiblingElement("gradient-stop")) {
                g.setColorAt(gs.attribute("offset").toDouble(), color(gs.attribute("color", "black")));
            }
            pbrush.reset(new QBrush(g));
        } else if (style == "radial-gradient") {
            QRadialGradient g(brush.attribute("cx").toDouble(), brush.attribute("cy").toDouble(),
                              brush.attribute("r").toDouble(),
                              brush.attribute("fx").toDouble(), brush.attribute("fy").toDouble());

            QString spread = brush.attribute("spread", "pad");
            if (spread == "pad")
                g.setSpread(QGradient::PadSpread);
            else if (spread == "repeat")
                g.setSpread(QGradient::RepeatSpread);
            else if (spread == "reflect")
                g.setSpread(QGradient::ReflectSpread);

            for (QDomElement gs = brush.firstChildElement("gradient-stop"); !gs.isNull(); gs = gs.nextSiblingElement("gradient-stop")) {
                g.setColorAt(gs.attribute("offset").toDouble(), color(gs.attribute("color", "black")));
            }
            pbrush.reset(new QBrush(g));
        } else if (style == "texture") {
            QString image_file = brush.attribute("image");
            if (image_file.isEmpty()) {
                emitError(QString("%1:%2: brush with id=%3 has no requared attribute 'image'").arg(fileinfo.filePath()).arg(brush.lineNumber()).arg(id));
            } else {
                QFileInfo image_info(image_file);
                if (image_info.isRelative()) {
                    image_info.setFile(QDir(fileinfo.path()), image_file);
                }
                QImage image(image_info.filePath());
                if (image.isNull()) {
                    emitError(QString("%1:%2: brush with id=%3 has invalid attribute 'image'").arg(fileinfo.filePath()).arg(brush.lineNumber()).arg(id));
                } else {
                    if (image.depth() == 1) {
                        pbrush.reset(new QBrush(color(brush.attribute("color", "black")), QBitmap::fromImage(image)));
                    } else {
                        pbrush.reset(new QBrush(image));
                    }
                }
            }
        }
        if (!pbrush.isNull()) {
            if (m_brushes.contains(id))
                *m_brushes.value(id) = *pbrush;
            else
                m_brushes.insert(id, pbrush.take());
        } else {
            emitError(QString("%1:%2: invalid brush id='%3'").arg(fileinfo.filePath()).arg(brush.lineNumber()).arg(id));
        }
    }
    return RESULT_OK;
}

const QBrush &MaketData::brush(const QString &id) const
{
    static QBrush defBrush(Qt::NoBrush);

    QBrush *p = m_brushes.value(id);
    return (p ? *p : defBrush);
}

result_t MaketData::initPens(const QFileInfo &fileinfo, const QDomElement &root)
{
    for (QDomElement pen = root.firstChildElement("pen"); !pen.isNull(); pen = pen.nextSiblingElement("pen")) {
        QString id = pen.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: pen has no required attribute 'id'").arg(fileinfo.filePath()).arg(pen.lineNumber()));
            continue;
        }
        QString dashes = pen.attribute("dashes");
        QString style = pen.attribute("style", (dashes.isEmpty() ? "solid" : "dash"));

        QScopedPointer<QPen> pp(0);
        if (style == "no-pen") {
            pp.reset(new QPen(Qt::NoPen));
        } else {
            if (style == "solid") {
                pp.reset(new QPen(Qt::SolidLine));
            } else if (style == "dash") {
                QVector<qreal> ds;
                if (!dashes.isEmpty()) {
                    QStringList sl = dashes.split(QRegExp("\\s+"), QString::SkipEmptyParts);
                    foreach (QString s, sl) {
                        ds << s.toDouble();
                    }
                    if (ds.size() % 2 == 1)
                        ds << 0.0;
                }
                if (ds.size() == 0) {
                    pp.reset(new QPen(Qt::DashLine));
                } else {
                    pp.reset(new QPen(Qt::CustomDashLine));
                    pp->setDashPattern(ds);
                }
            } else if (style == "dot") {
                pp.reset(new QPen(Qt::DotLine));
            } else if (style == "dashdot") {
                pp.reset(new QPen(Qt::DashDotLine));
            }
            QString brush_id = pen.attribute("brush");
            if (brush_id.isEmpty()) {
                QString color_id = pen.attribute("color", "black");
                pp->setColor(color(color_id));
            } else {
                pp->setBrush(brush(brush_id));
            }

            QString width = pen.attribute("width");
            if (!width.isEmpty()) {
                pp->setWidthF(width.toDouble());

                QString cap = pen.attribute("cap");
                if (cap == "square")
                    pp->setCapStyle(Qt::SquareCap);
                else if (cap == "round")
                    pp->setCapStyle(Qt::RoundCap);
                else
                    pp->setCapStyle(Qt::FlatCap);

                QString join = pen.attribute("join");
                if (join == "mitter")
                    pp->setJoinStyle(Qt::MiterJoin);
                else if (join == "round")
                    pp->setJoinStyle(Qt::RoundJoin);
                else
                    pp->setJoinStyle(Qt::BevelJoin);
            }
        }
        if (!pp.isNull()) {
            if (m_pens.contains(id))
                *m_pens.value(id) = *pp;
            else
                m_pens.insert(id, pp.take());
        } else {
            emitError(QString("%1:%2: invalid pen id='%3'").arg(fileinfo.filePath()).arg(pen.lineNumber()).arg(id));
        }
    }
    return RESULT_OK;
}

const QPen &MaketData::pen(const QString &id) const
{
    static QPen defPen(Qt::NoPen);

    QPen *p = m_pens.value(id);
    return (p ? *p : defPen);
}

result_t MaketData::initFonts(const QFileInfo &fileinfo, const QDomElement &root)
{
    for (QDomElement node = root.firstChildElement("font"); !node.isNull(); node = node.nextSiblingElement("font")) {
        QString id = node.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: font has no required attribute 'id'").arg(fileinfo.filePath()).arg(node.lineNumber()));
            continue;
        }
        QString family = node.attribute("family");
        if (family.isEmpty()) {
            emitError(QString("%1:%2: font has no required attribute 'family'").arg(fileinfo.filePath()).arg(node.lineNumber()));
            continue;
        }

        //<font id="serif" family="Times" size="12" weigth="normal" style="italic" />
        QScopedPointer<QFont> font(new QFont(family));

        QString size = node.attribute("size");
        if (!size.isEmpty()) {
            font->setPointSizeF(size.toDouble());
        }

        QString weight = node.attribute("weight");
        if (weight.compare("light", Qt::CaseInsensitive) == 0) {
            font->setWeight(QFont::Light);
        } else if (weight.compare("normal", Qt::CaseInsensitive) == 0) {
            font->setWeight(QFont::Normal);
        } else if (weight.compare("demibold", Qt::CaseInsensitive) == 0) {
            font->setWeight(QFont::DemiBold);
        } else if (weight.compare("bold", Qt::CaseInsensitive) == 0) {
            font->setWeight(QFont::Bold);
        } else if (weight.compare("black", Qt::CaseInsensitive) == 0) {
            font->setWeight(QFont::Black);
        } else if (!weight.isEmpty()) {
            font->setWeight(weight.toInt());
        }

        QString style = node.attribute("style");
        if (style.compare("normal", Qt::CaseInsensitive) == 0) {
            font->setStyle(QFont::StyleNormal);
        } else if (style.compare("italic", Qt::CaseInsensitive) == 0) {
            font->setStyle(QFont::StyleItalic);
        } else if (style.compare("oblique", Qt::CaseInsensitive) == 0) {
            font->setStyle(QFont::StyleOblique);
        }

        if (node.attribute("overline").compare("true", Qt::CaseInsensitive) == 0) {
            font->setOverline(true);
        }
        if (node.attribute("strikeout").compare("true", Qt::CaseInsensitive) == 0) {
            font->setStrikeOut(true);
        }
        if (node.attribute("underline").compare("true", Qt::CaseInsensitive) == 0) {
            font->setUnderline(true);
        }

        if (m_fonts.contains(id))
            *m_fonts.value(id) = *font;
        else
            m_fonts.insert(id, font.take());
    }
    return RESULT_OK;
}

const QFont &MaketData::font(const QString &id) const
{
    static QFont defFont;

    QFont *p = m_fonts.value(id);
    return (p ? *p : defFont);
}


static QVariant propValue(const QString &str, const QString &type)
{
    QVariant val;
    if (!str.isEmpty()) {
        if (type.compare("boolean", Qt::CaseInsensitive) == 0) {
            val.setValue(str.compare("true", Qt::CaseInsensitive) == 0 ? true : false);
        } else if (type.compare("integer", Qt::CaseInsensitive) == 0) {
            val.setValue(str.toLong());
        } else if (type.compare("long", Qt::CaseInsensitive) == 0) {
            val.setValue(str.toLongLong());
        } else if (type.compare("number", Qt::CaseInsensitive) == 0) {
            val.setValue(str.toDouble());
        } else if (type.compare("string", Qt::CaseInsensitive) == 0) {
            val.setValue(str);
        } else {
            val.setValue(str);
        }
    }
    return val;
}

result_t MaketData::initFeatures(const QFileInfo &fileinfo, const QDomElement &root, Feature *parent)
{
    for (QDomElement node = root.firstChildElement("feature"); !node.isNull(); node = node.nextSiblingElement("feature")) {
        QString id = node.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: feature has no required attribute 'id'").arg(fileinfo.filePath()).arg(node.lineNumber()));
            return RESULT_CONFIG_ERROR;
        }

        Feature *proto = parent;
        QString proto_id = node.attribute("prototype");
        if (!proto_id.isEmpty()) {
            if (parent) {
                emitError(QString("%1:%2: child feature (id='%3') cannot have prototype").arg(fileinfo.filePath()).arg(node.lineNumber()).arg(id));
                return RESULT_CONFIG_ERROR;
            } else {
                proto = m_features.value(proto_id);
                if (!proto) {
                    emitError(QString("%1:%2: undefined feature prototype='%3'").arg(fileinfo.filePath()).arg(node.lineNumber()).arg(proto_id));
                    return RESULT_CONFIG_ERROR;
                }
            }
        }

        Feature *pf = (parent ? parent->feature(id) : m_features.value(id));
        if (pf) {
            emitError(QString("%1:%2: feature (id=%3) already defined").arg(fileinfo.filePath()).arg(node.lineNumber()).arg(id));
            return RESULT_CONFIG_ERROR;
        } else {
            pf = new Feature(id, proto);
            if (RESULT_OK != initFeatures(fileinfo, node, pf)) {
                return RESULT_CONFIG_ERROR;
            }
            if (parent)
                parent->addFeature(id, pf);
            else
                m_features.insert(id, pf);
        }

        for (QDomElement prop = node.firstChildElement("property"); !prop.isNull(); prop = prop.nextSiblingElement("property")) {
            QString prop_id = prop.attribute("id");
            if (prop_id.isEmpty()) {
                emitWarn(QString("%1:%2: feature property has no required attribute 'id'").arg(fileinfo.filePath()).arg(prop.lineNumber()));
                continue;
            }
            QVariant val = propValue(prop.attribute("value"), prop.attribute("type"));
            if (val.isValid()) {
                pf->setProperty(prop_id, val);
            }
        }
    }
    return RESULT_OK;
}

result_t MaketData::initScenes(const QFileInfo &fileinfo, const QDomElement &root)
{
    for (QDomElement node = root.firstChildElement("scene"); !node.isNull(); node = node.nextSiblingElement("scene")) {
        QString id = node.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: scene has no required attribute 'id'").arg(fileinfo.filePath()).arg(node.lineNumber()));
            continue;
        }

        SceneData *ps = m_scenes.value(id);
        if (ps) {
            emitWarn(QString("%1:%2: scene (id=%3) already defined (append additional properties)").arg(fileinfo.filePath()).arg(node.lineNumber()).arg(id));

            QString v = node.attribute("x");
            if (!v.isEmpty())
                ps->x = v.toDouble();

            v = node.attribute("y");
            if (!v.isEmpty())
                ps->y = v.toDouble();

            v = node.attribute("w");
            if (!v.isEmpty())
                ps->w = v.toDouble();

            v = node.attribute("h");
            if (!v.isEmpty())
                ps->h = v.toDouble();

            v = node.attribute("background-brush");
            if (!v.isEmpty())
                ps->background_brush = v;

        } else {
            ps = new SceneData;
            ps->x = node.attribute("x", "0").toDouble();
            ps->y = node.attribute("y", "0").toDouble();
            ps->w = node.attribute("w", "0").toDouble();
            ps->h = node.attribute("h", "0").toDouble();
            ps->background_brush = node.attribute("background-brush");

            m_scenes.insert(id, ps);
        }

        initLayers(fileinfo, node, &ps->layers);
        initGraphics(fileinfo, node, ps);
    }
    return RESULT_OK;
}

result_t MaketData::initLayers(const QFileInfo &info, const QDomElement &root, QList<LayerData> *list)
{
    for (QDomElement node = root.firstChildElement("layer"); !node.isNull(); node = node.nextSiblingElement("layer")) {
        QString id = node.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: layer has no required attribute 'id'").arg(info.filePath()).arg(node.lineNumber()));
            return RESULT_CONFIG_ERROR;
        }

        LayerData *layer = 0;
        for (int i = 0; i < list->size(); i++) {
            if (list->at(i).id == id) {
                layer = &list->operator[](i);
                break;
            }
        }
        if (!layer) {
            list->append(LayerData());
            layer = &list->last();
            layer->id = id;
            layer->z = 0.0;
            layer->visible = true;
        }

        QString v = node.attribute("z-order");
        if (!v.isEmpty())
            layer->z = v.toDouble();

        v = node.attribute("visible");
        if (!v.isEmpty())
            layer->visible = (v == "false" || v == "no" ? false : true);

        initLayers(info, node, &layer->m_layers);
    }
    return RESULT_OK;
}


result_t MaketData::initGraphics(const QFileInfo &info, const QDomElement &root, SceneData *scene)
{
    for (QDomElement node = root.firstChildElement("graphics"); !node.isNull(); node = node.nextSiblingElement("graphics")) {
        QString id = node.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: graphics has no required attribute 'id'").arg(info.filePath()).arg(node.lineNumber()));
            continue;
        }

        GraphicsData &data = scene->graphics[id];
        for (QDomElement e = node.firstChildElement("feature"); !e.isNull(); e = e.nextSiblingElement("feature")) {
            QString feature_id = e.attribute("id");
            if (feature_id.isEmpty()) {
                emitError(QString("%1:%2: feature has no required attribute 'id'").arg(info.filePath()).arg(e.lineNumber()));
                continue;
            }
            QString state = e.attribute("state");

            FeatureData feature_data;
            feature_data.id = feature_id;
            feature_data.state = state;

            data.features.append(feature_data);
        }
    }
    return RESULT_OK;
}

result_t MaketData::initViews(const QFileInfo &info, const QDomElement &root)
{
    for (QDomElement node = root.firstChildElement("view"); !node.isNull(); node = node.nextSiblingElement("view")) {
        QString id = node.attribute("id");
        if (id.isEmpty()) {
            emitError(QString("%1:%2: view has no required attribute 'id'").arg(info.filePath()).arg(node.lineNumber()));
            continue;
        }

        ViewData *view = m_views.value(id);
        if (!view) {
            view = new ViewData;
            m_views.insert(id, view);
        }

        QString v = node.attribute("script");
        if (!v.isEmpty())
            view->script = v;

        for (QDomElement e = node.lastChildElement("event"); !e.isNull(); e = e.previousSiblingElement("event")) {
            QString id = e.attribute("id");
            if (id.isEmpty()) {
                emitError(QString("%1:%2: event has no required attribute 'id'").arg(info.filePath()).arg(e.lineNumber()));
                continue;
            }
            QString handler = e.attribute("handler");
            if (handler.isEmpty()) {
                emitError(QString("%1:%2: event has no required attribute 'handler'").arg(info.filePath()).arg(e.lineNumber()));
                continue;
            }

            Qt::MouseButton btn = Qt::NoButton;
            QString bstr = e.attribute("button");
            if (bstr.compare("left", Qt::CaseInsensitive) == 0) {
                btn = Qt::LeftButton;
            } else if (bstr.compare("right", Qt::CaseInsensitive) == 0) {
                btn = Qt::RightButton;
            } else if (bstr.compare("middle", Qt::CaseInsensitive) == 0) {
                btn = Qt::MidButton;
            }

            QString mods = e.attribute("modifiers");
            QString keys = e.attribute("keys");

            EventData data;
            if (mods.compare("any", Qt::CaseInsensitive) == 0) {
                data.modifiers = (Qt::KeyboardModifiers) -1;
            } else {
                QKeySequence seq(mods+'+');
                data.modifiers = (Qt::KeyboardModifiers) (seq[0] & (Qt::SHIFT | Qt::META | Qt::CTRL | Qt::ALT));
            }
            if (!keys.isEmpty()) {
                data.keys = QKeySequence(keys);
            }
            data.button = btn;
            data.handler = handler;

            view->events.insert(id, data);
        }
    }
    return RESULT_OK;
}


Scene *MaketData::createScene(const QString &id)
{
    SceneData *data = m_scenes.value(id);
    if (data) {
        return new Scene(this, id, *data);
    }
    return 0;
}

View *MaketData::createView(const QString &id, Scene *scene)
{
    ViewData *data = m_views.value(id);
    if (data) {
        View *view = new View(this, id, *data, scene);
        return view;
    }
    return 0;
}


};

 /* End of code */
