/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    font.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jul  1 06:30:11 2011
 *
 * @brief   fonts
 *
 */

#ifndef MAKET_FONT_H
#define MAKET_FONT_H

#include <QFont>

#include <maksi/elem.h>
#include <maksi/schema.h>


#include "defs.h"


//! maket namespace
namespace maket
{

class Maket;

class MAKET_EXPORT FontElement : public maksi::Element
{
public:
    class TypeFactory : public maksi::StructTypeFactory {
    public:
        TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory);
        virtual maksi::result_t createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const;
    private:
        Maket *m_maket;
    };

    FontElement(Maket *maket, QXmlName name, maksi::schema_type_t type);
    virtual ~FontElement();

    virtual QVariant elementValue();

private:
    Maket *m_maket;
};

};

#endif

/* End of file */
