/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    layer.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Sat Jul  2 23:26:39 2011
 *
 * @brief   layers
 *
 */

#include "layer.h"
#include "maket.h"

#include <maksi/conf.h>


namespace maket
{

LayerElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory.isNull() ? maksi::schema_type_t() : oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t LayerElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<LayerElement> elem(new LayerElement(m_maket, conf.name(), type));
        *value = maksi::fromElement(elem);
    }
    return maksi::RESULT_OK;
}

LayerElement::LayerElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_maket(maket)
{
    setFlag(QGraphicsItem::ItemNegativeZStacksBehindParent, true);
    setFlag(QGraphicsItem::ItemHasNoContents, true);
}

LayerElement::~LayerElement()
{
    qDebug("delete layer %s", qPrintable(attribute("name").toString()));
}

QVariant LayerElement::elementValue()
{
    return QVariant::fromValue(this);
}

bool LayerElement::attributeExists(const QString &name)
{
    if (name == "z-order") {
        return true;
    } else if (name == "visible") {
        return true;
    }
    return Element::attributeExists(name);
}

QVariant LayerElement::attribute(const QString &name)
{
    if (name == "z-order") {
        return zValue();
    } else if (name == "visible") {
        return isVisible();
    }
    return Element::attribute(name);
}

void LayerElement::setAttribute(const QString &name, const QVariant &value)
{
    if (name == "z-order") {
        setZValue(value.toReal());
    } else if (name == "visible") {
        setVisible(value.toBool());
    } else {
        Element::setAttribute(name, value);
    }
}

QVariant LayerElement::appendElement(const QVariant &value)
{
    QVariant val(Element::appendElement(value));
    if (val.canConvert<maksi::element_t>()) {
        maksi::element_t elem(val.value<maksi::element_t>());
        val = elem->elementValue();
    }
    if (val.canConvert<LayerElement*>()) {
        LayerElement *layer = val.value<LayerElement*>();
        addToGroup(layer);
    }
    return val;
}

QList<LayerElement*> LayerElement::layers()
{
    QList<LayerElement*> ls;
    for (int i = 0; i < countElements(); ++i) {
        QVariant val(element(i));
        if (val.canConvert<maksi::element_t>()) {
            maksi::element_t elem(val.value<maksi::element_t>());
            val = elem->elementValue();
        }
        if (val.canConvert<LayerElement*>()) {
            LayerElement* layer = val.value<LayerElement*>();
            ls.append(layer);
        }
    }
    return ls;
}

};

/* End of code */
