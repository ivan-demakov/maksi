/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    view.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Nov 24 11:27:21 2010
 *
 * @brief   view implementation
 *
 */

#include "view.h"
#include "scene.h"
#include "maket.h"
//#include "graphics.h"

#include <maksi/conf.h>

#include <QMouseEvent>
#include <QApplication>
#include <QRubberBand>
#include <QScrollBar>
#include <QStyleHintReturnMask>


namespace maket
{

static const QString keyPress_id("keyPress");
static const QString keyRelease_id("keyRelease");
static const QString mousePress_id("mousePress");
static const QString mouseRelease_id("mouseRelease");
static const QString mouseMove_id("mouseMove");
static const QString mouseDoubleClick_id("mouseDoubleClick");
static const QString mouseWheel_id("mouseWheel");
static const QString contextMenu_id("contextMenu");


View::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t View::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        QSharedPointer<View> view(new View(m_maket, conf.name(), type));
        *value = maksi::fromElement(view);
    }
    return maksi::RESULT_OK;
}

View::View(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_maket(maket),
    m_mode(MODE_VIEW),
    m_rubberBand(new QRubberBand(QRubberBand::Rectangle, viewport()))
{
    m_keys[0] = m_keys[1] = m_keys[2] = m_keys[3] = 0;

    setTransformationAnchor(AnchorUnderMouse);
    setMouseTracking(true);
    viewport()->setCursor(Qt::CrossCursor);
}

View::~View()
{
    qDebug("delete view %s", qPrintable(attribute("name").toString()));
}

QVariant View::elementValue()
{
    return QVariant::fromValue(this);
}

bool View::attributeExists(const QString &name)
{
    return Element::attributeExists(name);
}

QVariant View::attribute(const QString &name)
{
    return Element::attribute(name);
}

void View::setAttribute(const QString &name, const QVariant &value)
{
    Element::setAttribute(name, value);
    if (name == "scene") {
        m_scene = m_maket->scene(value.toString());
        setScene(m_scene.data());
    } else if (name == "script") {
        m_script = m_maket->maksi()->script(value.toString());
    }
}

QVariant View::appendElement(const QVariant &value)
{
    QVariant val(Element::appendElement(value));
    if (value.canConvert<maksi::element_t>()) {
        maksi::element_t elem(value.value<maksi::element_t>());
        val = elem->elementValue();
    }
    if (val.canConvert<EventElement::Value>()) {
        EventElement::Value event(val.value<EventElement::Value>());
        m_events.insert(event.type, event);
    }
    return val;
}

QList<QString> View::methodNames()
{
    static QList<QString> ms;
    if (ms.isEmpty()) {
        ms << "graphicsAt";
        ms << "hasSelected";
        ms << "setMouseDragMode";
        ms << "setMouseScrollMode";
        ms << "setMouseZoomInMode";
        ms << "setMouseZoomOutMode";
        ms << "setMouseSelectInMode";
        ms << "setMouseSelectOutMode";
    }
    return ms;
}

QVariant View::callMethod(const QString &id, QVariantList args)
{
    if (id == "graphicsAt") {
        if (args.size() >= 3) {
            maksi::Array ar(graphicsAt(args.at(0).toBool(), args.at(1).toInt(), args.at(2).toInt()));
            return QVariant::fromValue(ar);
        }
    } else if (id == "hasSelected") {
        return m_scene->hasSelected();
    } else if (id == "setMouseDragMode") {
        if (args.size() >= 1) {
            if (args.at(0).toBool()) {
                if (args.size() >= 4) {
                    setMouseDragMode(true, args.at(1).toInt(), args.at(2).toInt(), args.at(3).toInt());
                    return true;
                }
            } else {
                setMouseDragMode(false, 0, 0, 0);
                return true;
            }
        }
    } else if (id == "setMouseScrollMode") {
        if (args.size() >= 1) {
            if (args.at(0).toBool()) {
                if (args.size() >= 4) {
                    setMouseScrollMode(true, args.at(1).toInt(), args.at(2).toInt(), args.at(3).toInt());
                    return true;
                }
            } else {
                setMouseScrollMode(false, 0, 0, 0);
                return true;
            }
        }
    } else {
        mode_t mode = MODE_VIEW;
        if (id == "setMouseZoomInMode") {
            mode = MODE_ZOOM_IN;
        } else if (id == "setMouseZoomOutMode") {
            mode = MODE_ZOOM_OUT;
        } else if (id == "setMouseSelectInMode") {
            mode = MODE_SELECT_IN;
        } else if (id == "setMouseSelectOutMode") {
            mode = MODE_SELECT_OUT;
        }
        if (mode != MODE_VIEW) {
            if (args.size() >= 1) {
                if (args.at(0).toBool()) {
                    if (args.size() >= 4) {
                        setMouseRubberMode(true, mode, args.at(1).toInt(), args.at(2).toInt(), args.at(3).toInt());
                        return true;
                    }
                } else {
                    setMouseRubberMode(false, mode, 0, 0, 0);
                    return true;
                }
            }
        }
    }
    return false;
}


void View::setMouseDragMode(bool value, int x, int y, int button)
{
    if (value) {
        m_lastX = x;
        m_lastY = y;
        m_lastButton = button;
        m_mode = MODE_DRAG;
        viewport()->setCursor(Qt::ClosedHandCursor);
    } else {
        m_lastButton = 0;
        m_mode = MODE_VIEW;
        viewport()->setCursor(Qt::CrossCursor);
    }
}

void View::setMouseScrollMode(bool value, int x, int y, int button)
{
    if (value) {
        m_lastX = x;
        m_lastY = y;
        m_lastButton = button;
        m_mode = MODE_SCROLL;
        viewport()->setCursor(Qt::OpenHandCursor);
    } else {
        m_lastButton = 0;
        m_mode = MODE_VIEW;
        viewport()->setCursor(Qt::CrossCursor);
    }
}

void View::setMouseRubberMode(bool value, mode_t mode, int x, int y, int button)
{
    if (value) {
        m_lastX = x;
        m_lastY = y;
        m_lastButton = button;
        m_mode = mode;
        if (mode == MODE_ZOOM_IN || mode == MODE_ZOOM_OUT)
            viewport()->setCursor(Qt::UpArrowCursor);
        else
            viewport()->setCursor(Qt::PointingHandCursor);

        QRect rubberRect(x, y, 0, 0);
        m_rubberBand->setGeometry(rubberRect);
        m_rubberBand->show();
    } else {
        m_rubberBand->hide();

        m_lastButton = 0;
        m_mode = MODE_VIEW;
        viewport()->setCursor(Qt::CrossCursor);
    }
}

void View::scale(qreal dx)
{
    QGraphicsView::scale(dx, dx);
}


void View::keyPressEvent(QKeyEvent *event)
{
    if (m_script) {
        QPoint pos = viewport()->mapFromGlobal(QCursor::pos());
        uint key = event->key() | event->modifiers();

        if (m_keys[0] == 0) {
            m_keys[0] = key;
        } else if (m_keys[1] == 0) {
            m_keys[1] = key;
        } else if (m_keys[2] == 0) {
            m_keys[2] = key;
        } else if (m_keys[3] == 0) {
            m_keys[3] = key;
        } else {
            m_keys[0] = m_keys[1]; m_keys[1] = m_keys[2]; m_keys[2] = m_keys[3]; m_keys[3] = key;
        }

        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(keyPress_id);
        while (it != m_events.end() && it.key() == keyPress_id) {
            if (it->keys.isEmpty()) {
                QVariantList args;
                args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                if (event->key() == Qt::Key_Shift || event->key() == Qt::Key_Control || event->key() == Qt::Key_Meta || event->key() == Qt::Key_Alt) {
                    QString s = QKeySequence(event->modifiers()).toString();
                    s.chop(1);
                    args << s;
                } else {
                    args << QKeySequence(key).toString();
                }
                args << (uint) event->modifiers();
                args << event->text();
                args << pos.x();
                args << pos.y();

                QVariant val = m_script->callProc(it->handler, args);
                if (val.isValid() && val.toBool()) {
                    m_keys[0] = m_keys[1] = m_keys[2] = m_keys[3] = 0;
                    event->accept();
                    return;
                }
            } else {
                QKeySequence seq(m_keys[0], m_keys[1], m_keys[2], m_keys[3]);
                QKeySequence::SequenceMatch m = seq.matches(it->keys);
                if (QKeySequence::ExactMatch == m) {
                    m_keys[0] = m_keys[1] = m_keys[2] = m_keys[3] = 0;

                    QVariantList args;
                    args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                    args << seq.toString();
                    args << (uint) event->modifiers();
                    args << pos.x();
                    args << pos.y();

                    QVariant val = m_script->callProc(it->handler, args);
                    if (val.isValid() && val.toBool()) {
                        event->accept();
                        return;
                    }
                } else if (QKeySequence::PartialMatch == m) {
                    event->accept();
                    return;
                }
            }
            ++it;
        }

        m_keys[0] = m_keys[1] = m_keys[2] = m_keys[3] = 0;
    }

    QAbstractScrollArea::keyPressEvent(event);
}

void View::keyReleaseEvent(QKeyEvent *event)
{
    if (m_script) {
        QPoint pos = viewport()->mapFromGlobal(QCursor::pos());
        uint key = event->key();

        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(keyRelease_id);
        while (it != m_events.end() && it.key() == keyRelease_id) {
            QVariantList args;
            args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
            if (event->key() == Qt::Key_Shift) {
                args << "Shift";
            } else if (event->key() == Qt::Key_Control) {
                args << "Ctrl";
            } else if (event->key() == Qt::Key_Meta) {
                args << "Meta";
            } else if (event->key() == Qt::Key_Alt) {
                args << "Alt";
            } else {
                args << QKeySequence(key).toString();
            }
            args << (uint) event->modifiers();
            args << pos.x();
            args << pos.y();

            QVariant val = m_script->callProc(it->handler, args);
            if (val.isValid() && val.toBool()) {
                event->accept();
                return;
            }
            ++it;
        }
    }

    QAbstractScrollArea::keyReleaseEvent(event);
}

void View::mousePressEvent(QMouseEvent *event)
{
    if (m_mode != MODE_VIEW) {
        event->accept();
        return;
    }

    if (m_script) {
        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(mousePress_id);
        while (it != m_events.end() && it.key() == mousePress_id) {
            if ((it->modifiers == -1 || it->modifiers == event->modifiers()) && (it->button == 0 || it->button == event->button())) {
                QVariantList args;
                args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                args << (uint) event->button();
                args << (uint) event->modifiers();
                args << event->x();
                args << event->y();

                QVariant val = m_script->callProc(it->handler, args);
                if (val.isValid() && val.toBool()) {
                    event->accept();
                    return;
                }
            }
            it++;
        }
    }
}

void View::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_mode == MODE_SCROLL) {
        if (event->button() == m_lastButton) {
            setMouseScrollMode(false, 0, 0, 0);
        }
        event->accept();
        return;
    } else if (m_mode == MODE_DRAG) {
        if (event->button() == m_lastButton) {
            setMouseDragMode(false, 0, 0, 0);
        }
        event->accept();
        return;
    } else if (m_mode == MODE_ZOOM_IN || m_mode == MODE_ZOOM_OUT) {
        if (event->button() == m_lastButton) {
            QPoint lastPos(m_lastX, m_lastY);
            if ((lastPos - event->pos()).manhattanLength() > QApplication::startDragDistance()) {
                QRect rubberRect(lastPos, event->pos());
                rubberRect = rubberRect.normalized();
                if (m_mode == MODE_ZOOM_OUT) {
                    double sx = ((double) rubberRect.width()) / viewport()->rect().width();
                    double sy = ((double) rubberRect.height()) / viewport()->rect().height();
                    double ss = qMax(sx, sy);
                    scale(ss);
                } else {
                    QRectF rect = mapToScene(rubberRect).boundingRect();
                    fitInView(rect, Qt::KeepAspectRatio);
                }
            }
            setMouseRubberMode(false, MODE_VIEW, 0, 0, 0);
        }
        event->accept();
        return;
    } else if (m_mode == MODE_SELECT_IN || m_mode == MODE_SELECT_OUT) {
        if (event->button() == m_lastButton) {
            Scene::select_t stype = (m_mode == MODE_SELECT_IN ? Scene::SELECT_IN : Scene::SELECT_OUT);
            QPoint lastPos(m_lastX, m_lastY);
            if ((lastPos - event->pos()).manhattanLength() > QApplication::startDragDistance()) {
                QRect rect(lastPos, event->pos());
                rect = rect.normalized();
                m_scene->selectItems(this, rect, Qt::ContainsItemShape, stype);
            } else {
                QRect rect(event->x()-3, event->y()-3, 6, 6);
                m_scene->selectItems(this, rect, Qt::IntersectsItemShape, stype);
            }
            setMouseRubberMode(false, MODE_VIEW, 0, 0, 0);
        }
        event->accept();
        return;
    }

    if (m_script) {
        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(mouseRelease_id);
        while (it != m_events.end() && it.key() == mouseRelease_id) {
            if ((it->modifiers == -1 || it->modifiers == event->modifiers()) && (it->button == 0 || it->button == event->button())) {
                QVariantList args;
                args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                args << (uint) event->button();
                args << (uint) event->modifiers();
                args << event->x();
                args << event->y();

                QVariant val = m_script->callProc(it->handler, args);
                if (val.isValid() && val.toBool()) {
                    event->accept();
                    return;
                }
            }
            it++;
        }
    }
}

void View::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mode == MODE_SCROLL) {
        if (!(event->buttons() & m_lastButton)) { // button released already
            setMouseScrollMode(false, 0, 0, 0);
        } else {
            QScrollBar *hBar = horizontalScrollBar();
            QScrollBar *vBar = verticalScrollBar();
            int deltaX = event->x() - m_lastX;
            int deltaY = event->y() - m_lastY;
            hBar->setValue(hBar->value() + (isRightToLeft() ? deltaX : -deltaX));
            vBar->setValue(vBar->value() - deltaY);
            m_lastX = event->x();
            m_lastY = event->y();
        }
        event->accept();
        return;
    } else if (m_mode == MODE_DRAG) {
        if (!(event->buttons() & m_lastButton)) { // button released already
            setMouseDragMode(false, 0, 0, 0);
        } else {
            QPointF lastPos = mapToScene(QPoint(m_lastX, m_lastY));
            QPointF thisPos = mapToScene(event->pos());
            qreal deltaX = thisPos.x() - lastPos.x();
            qreal deltaY = thisPos.y() - lastPos.y();
            m_scene->moveSelected(deltaX, deltaY);
            m_lastX = event->x();
            m_lastY = event->y();
        }
        event->accept();
        return;
    } else if (m_mode == MODE_ZOOM_IN || m_mode == MODE_ZOOM_OUT || m_mode == MODE_SELECT_IN || m_mode == MODE_SELECT_OUT) {
        if (!(event->buttons() & m_lastButton)) { // button released already
            setMouseRubberMode(false, MODE_VIEW, 0, 0, 0);
        } else {
            QPoint lastPos(m_lastX, m_lastY);
            QRect rubberRect(lastPos, event->pos());
            rubberRect = rubberRect.normalized();
            m_rubberBand->setGeometry(rubberRect);
        }
        event->accept();
        return;
    }

    if (m_script) {
        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(mouseMove_id);
        while (it != m_events.end() && it.key() == mouseMove_id) {
            if (it->modifiers == -1 || it->modifiers == event->modifiers()) {
                QVariantList args;
                args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                args << (uint) event->buttons();
                args << (uint) event->modifiers();
                args << event->x();
                args << event->y();

                QVariant val = m_script->callProc(it->handler, args);
                if (val.isValid() && val.toBool()) {
                    event->accept();
                    return;
                }
            }
            it++;
        }
    }
}

void View::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (m_mode != MODE_VIEW) {
        event->accept();
        return;
    }

    if (m_script) {
        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(mouseDoubleClick_id);
        while (it != m_events.end() && it.key() == mouseDoubleClick_id) {
            if ((it->modifiers == -1 || it->modifiers == event->modifiers()) && (it->button == 0 || it->button == event->button())) {
                QVariantList args;
                args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                args << (uint) event->button();
                args << (uint) event->modifiers();
                args << event->x();
                args << event->y();

                QVariant val = m_script->callProc(it->handler, args);
                if (val.isValid() && val.toBool()) {
                    event->accept();
                    return;
                }
            }
            it++;
        }
    }
}

void View::wheelEvent(QWheelEvent *event)
{
    if (m_script) {
        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(mouseWheel_id);
        while (it != m_events.end() && it.key() == mouseWheel_id) {
            if (it->modifiers == -1 || it->modifiers == event->modifiers()) {
                QVariantList args;
                args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
                args << (((double) event->delta()) / 8.0);
                args << (uint) event->orientation();
                args << (uint) event->modifiers();
                args << event->x();
                args << event->y();

                QVariant val = m_script->callProc(it->handler, args);
                if (val.isValid() && val.toBool()) {
                    event->accept();
                    return;
                }
            }
            it++;
        }
    }

    if (event->modifiers() == Qt::ShiftModifier) {
        double d = 1.2 * event->delta() / (8.0 * 15.0);
        if (d < 0.0) {
            scale(-d);
        } else {
            scale(1.0/d);
        }
        event->accept();
        return;
    }
    if (event->modifiers() == Qt::ControlModifier) {
        rotate(event->delta() / (8.0 * 15.0));
        event->accept();
        return;
    }
    QAbstractScrollArea::wheelEvent(event);
}

void View::contextMenuEvent(QContextMenuEvent *event)
{
    if (m_mode != MODE_VIEW) {
        event->accept();
        return;
    }

    if (m_script) {
        QMultiMap<QString, EventElement::Value>::const_iterator it = m_events.find(contextMenu_id);
        while (it != m_events.end() && it.key() == contextMenu_id) {
            QVariantList args;
            args << maksi::fromElement(QWeakPointer<View>(this).toStrongRef());
            args << event->reason();
            args << event->x();
            args << event->y();

            QVariant val = m_script->callProc(it->handler, args);
            if (val.isValid() && val.toBool()) {
                event->accept();
                return;
            }
            it++;
        }
    }
}

QVariantList View::graphicsAt(bool selected, int x, int y)
{
    QRect rect(x-3, y-3, 6, 6);
    QVariantList ls = m_scene->itemsAt(this, selected, rect, Qt::IntersectsItemShape);
    qDebug("found items %d", ls.size());
    return ls;
}

};

 /* End of code */
