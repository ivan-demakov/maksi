/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    font.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jul  1 06:35:31 2011
 *
 * @brief   fonts
 *
 */

#include "font.h"
#include "maket.h"

#include <maksi/conf.h>


namespace maket
{

FontElement::TypeFactory::TypeFactory(Maket *maket, QSharedPointer<maksi::SchemaTypeFactory> oldFactory) :
    maksi::StructTypeFactory(oldFactory->contentType()),
    m_maket(maket)
{
}

maksi::result_t FontElement::TypeFactory::createNew(maksi::schema_type_t type, const maksi::Conf &conf, QVariant *value) const
{
    if (value) {
        maksi::element_t elem(new FontElement(m_maket, conf.name(), type));
        *value = QVariant::fromValue(elem);
    }
    return maksi::RESULT_OK;
}

FontElement::FontElement(Maket *maket, QXmlName name, maksi::schema_type_t type) :
    Element(name, type),
    m_maket(maket)
{
}

FontElement::~FontElement()
{
}

QVariant FontElement::elementValue()
{
    if (attributeExists("ref"))
        return m_maket->font(attribute("ref").toString());

    QFont font(attribute("family").toString());
    if (attributeExists("size"))
        font.setPointSizeF(attribute("size").toReal());

    if (attributeExists("weight")) {
        QVariant val(attribute("weight"));
        if (val.canConvert<int>()) {
            font.setWeight(val.toInt());
        } else {
            QString weight = val.toString();
            if (weight == "light") {
                font.setWeight(QFont::Light);
            } else if (weight == "normal") {
                font.setWeight(QFont::Normal);
            } else if (weight == "demibold") {
                font.setWeight(QFont::DemiBold);
            } else if (weight == "bold") {
                font.setWeight(QFont::Bold);
            } else if (weight == "black") {
                font.setWeight(QFont::Black);
            }
        }
    }

    if (attributeExists("style")) {
        QString style = attribute("style").toString();
        if (style == "normal") {
            font.setStyle(QFont::StyleNormal);
        } else if (style == "italic") {
            font.setStyle(QFont::StyleItalic);
        } else if (style == "oblique") {
            font.setStyle(QFont::StyleOblique);
        }
    }

    if (attributeExists("overline"))
        font.setOverline(attribute("overline").toBool());
    if (attributeExists("strikeout"))
        font.setOverline(attribute("strikeout").toBool());
    if (attributeExists("underline"))
        font.setOverline(attribute("underline").toBool());

    return font;
}

};

/* End of code */
