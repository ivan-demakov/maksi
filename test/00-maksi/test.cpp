/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012, 2013, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <QtTest/QtTest>
#include <maksi/iface.h>


class Test: public QObject
{
    Q_OBJECT;

    maksi_t m_maksi;

    static void message(int type, const QString &msg) {
        if (maksi::MESSAGE_ERROR >= type)
            qCritical("%s", qPrintable(msg));
        else if (maksi::MESSAGE_NOTE >= type)
            qWarning("%s", qPrintable(msg));
        else
            qDebug("%s", qPrintable(msg));
    }

private slots:

    void initTestCase() {
        qDebug("create maksi");
        m_maksi = maksi_newInstance("test");
        if (m_maksi) {
            maksi_setMessageHandler(m_maksi, message);
        }

        QVERIFY(m_maksi != 0);
    }

    void cleanupTestCase() {
        QCoreApplication::processEvents();

        qDebug("destroy maksi");
        maksi_freeInstance(m_maksi);
    }
};


QTEST_MAIN(Test)
#include "test.moc"


 /* End of code */
