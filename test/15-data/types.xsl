<?xml version="1.0" encoding="UTF-8"?>

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns="http://maksi.su/maksi">

  <xs:simpleType name='real'>
    <xs:restriction base='xs:float'/>
  </xs:simpleType>

  <xs:complexType name='color'>
    <xs:attribute name='ref' type='xs:NCName'/>
    <xs:attribute name='value' type='xs:token'/>
    <xs:attribute name='red' type='xs:unsignedByte'/>
    <xs:attribute name='green' type='xs:unsignedByte'/>
    <xs:attribute name='blue' type='xs:unsignedByte'/>
    <xs:attribute name='alpha' type='xs:unsignedByte' default='255'/>
  </xs:complexType>


  <xs:complexType name='gradient-stop'>
    <xs:attribute name='offset' type='real' use='required'/>
    <xs:attribute name='color' type='xs:NCName'/>
    <xs:sequence>
      <xs:element name='color' type='color' minOccurs='0'/>
    </xs:sequence>
  </xs:complexType>


  <xs:simpleType name='gradient-spread'>
    <xs:restriction base='xs:NCName'>
      <xs:enumeration value='pad'/>
      <xs:enumeration value='repeat'/>
      <xs:enumeration value='reflect'/>
    </xs:restriction>
  </xs:simpleType>


  <xs:complexType name='linear-gradient'>
    <xs:attribute name='x1' type='real' default='0'/>
    <xs:attribute name='y1' type='real' default='0'/>
    <xs:attribute name='x2' type='real' default='1'/>
    <xs:attribute name='y2' type='real' default='1'/>
    <xs:attribute name='spread' type='gradient-spread' default='pad'/>
    <xs:sequence>
      <xs:element name='gradient-stop' type='gradient-stop' minOccurs='0' maxOccurs='unbounded'/>
    </xs:sequence>
  </xs:complexType>


  <xs:complexType name='radial-gradient'>
    <xs:attribute name='cx' type='real' default='0'/>
    <xs:attribute name='cy' type='real' default='0'/>
    <xs:attribute name='fx' type='real' default='0'/>
    <xs:attribute name='fy' type='real' default='0'/>
    <xs:attribute name='r' type='real' default='1'/>
    <xs:attribute name='spread' type='gradient-spread' default='pad'/>
    <xs:sequence>
      <xs:element name='gradient-stop' type='gradient-stop' minOccurs='0' maxOccurs='unbounded'/>
    </xs:sequence>
  </xs:complexType>


  <xs:complexType name='texture'>
    <xs:attribute name='href' type='xs:anyURI' use='required'/>
    <xs:attribute name='color' type='xs:NCName'/>
    <xs:sequence>
      <xs:element name='color' type='color' minOccurs='0'/>
    </xs:sequence>
  </xs:complexType>


  <xs:simpleType name='brush-style'>
    <xs:restriction base='xs:NCName'>
      <xs:enumeration value='no-brush'/>
      <xs:enumeration value='solid'/>
      <xs:enumeration value='hlines'/>
      <xs:enumeration value='vlines'/>
      <xs:enumeration value='cross'/>
      <xs:enumeration value='bdiag'/>
      <xs:enumeration value='fdiag'/>
      <xs:enumeration value='diag-cross'/>
    </xs:restriction>
  </xs:simpleType>


  <xs:complexType name='brush'>
    <xs:attribute name='ref' type='xs:NCName'/>
    <xs:attribute name='style' type='brush-style'/>
    <xs:attribute name='color' type='xs:NCName'/>
    <xs:choice minOccurs='0'>
      <xs:element name='color' type='color'/>
      <xs:element name='linear-gradient' type='linear-gradient'/>
      <xs:element name='radial-gradient' type='radial-gradient'/>
      <xs:element name='texture' type='texture'/>
    </xs:choice>
  </xs:complexType>


  <xs:complexType name='pen'>
    <xs:attribute name='ref' type='xs:NCName'/>

    <xs:attribute name='style'>
      <xs:simpleType>
        <xs:restriction base='xs:NCName'>
          <xs:enumeration value='no-pen'/>
          <xs:enumeration value='solid'/>
          <xs:enumeration value='dash'/>
          <xs:enumeration value='dot'/>
          <xs:enumeration value='dashdot'/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name='dashes'>
      <xs:simpleType>
        <xs:list itemType='real'/>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name='width' type='real'/>

    <xs:attribute name='cap' default='flat'>
      <xs:simpleType>
        <xs:restriction base='xs:NCName'>
          <xs:enumeration value='square'/>
          <xs:enumeration value='round'/>
          <xs:enumeration value='flat'/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name='join' default='bevel'>
      <xs:simpleType>
        <xs:restriction base='xs:NCName'>
          <xs:enumeration value='mitter'/>
          <xs:enumeration value='round'/>
          <xs:enumeration value='bevel'/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name='color' type='xs:NCName'/>
    <xs:attribute name='brush' type='xs:NCName'/>

    <xs:choice minOccurs='0'>
      <xs:element name='color' type='color'/>
      <xs:element name='brush' type='brush'/>
    </xs:choice>
  </xs:complexType>


  <xs:complexType name='font'>
    <xs:attribute name='family' type='xs:token' use='required'/>
    <xs:attribute name='size' type='real'/>

    <xs:attribute name='weight'>
      <xs:simpleType>
        <xs:union memberTypes='xs:int'>
          <xs:simpleType>
            <xs:restriction base='xs:NCName'>
              <xs:enumeration value='light'/>
              <xs:enumeration value='normal'/>
              <xs:enumeration value='demibold'/>
              <xs:enumeration value='bold'/>
              <xs:enumeration value='black'/>
            </xs:restriction>
          </xs:simpleType>
        </xs:union>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name='style'>
      <xs:simpleType>
        <xs:restriction base='xs:NCName'>
          <xs:enumeration value='normal'/>
          <xs:enumeration value='italic'/>
          <xs:enumeration value='oblique'/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name='overline' type='xs:boolean'/>
    <xs:attribute name='underline' type='xs:boolean'/>
    <xs:attribute name='strikeout' type='xs:boolean'/>
  </xs:complexType>

</xs:schema>
