/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <QtTest/QtTest>

#include <maksi/job.h>
#include <maksi/maksi.h>


class Job : public maksi::Job
{
    Q_OBJECT;
public:

    Job() : maksi::Job(true), m_first(true) { qDebug("Job::Job()"); }
    ~Job() { qDebug("Job::~Job()"); }

    bool run();

    bool m_first;
};

bool Job::run()
{
    qDebug("Job::run()");
    if (m_first) {
        m_first = false;
        restart();
        return false;
    }
    return true;
}

class Test: public QObject
{
    Q_OBJECT;

    maksi::Maksi *m_maksi;
    Job *m_job;

public slots:
    void message(int type, const QString msg);
    void jobFinished() { qDebug("jobFinished"); }

private slots:
    void initTestCase();
    void cleanupTestCase();
};

void Test::message(int type, const QString msg)
{
    if (maksi::MESSAGE_ERROR >= type)
        qCritical("%s", qPrintable(msg));
    else if (maksi::MESSAGE_NOTE >= type)
        qWarning("%s", qPrintable(msg));
    else
        qDebug("%s", qPrintable(msg));
}

void Test::initTestCase()
{
    m_maksi = maksi::Maksi::instance("test");
    connect(m_maksi, SIGNAL(message(int, QString)), SLOT(message(int, QString)));

    m_job = new Job();
    connect(m_job, SIGNAL(finished()), SLOT(jobFinished()));

    maksi::Worker *w = m_maksi->startJob(m_job);
    QVERIFY(w != 0);
}

void Test::cleanupTestCase()
{
    while (!m_maksi->wait(100)) {
        QCoreApplication::processEvents();
    }
    //sleep(1);
    QCoreApplication::processEvents();
    qDebug("destroy maksi");
    maksi::Maksi::destroy(m_maksi);
}

QTEST_MAIN(Test)
#include "test.moc"


 /* End of code */
