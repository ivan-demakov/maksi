######################################################################
# Automatically generated by qmake (2.01a) Fri May 21 20:58:25 2010
######################################################################

CONFIG += qtestlib
TEMPLATE = app
TARGET = 
DEPENDPATH += .

QT -= gui

INCLUDEPATH += .
INCLUDEPATH += ../..

win32 {
    release {
        LIBS += ..\..\maksi\release\maksi1.dll
    } else {
        LIBS += ..\..\maksi\debug\maksi1.dll
    }
}

unix {
    LIBS += -L../../maksi -lmaksi
}

# Input
SOURCES += test.cpp
