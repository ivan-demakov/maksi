TEMPLATE = app
DEPENDPATH += .

QT -= gui
QT += testlib

INCLUDEPATH += ../..

unix {
  CONFIG += debug
  QMAKE_LIBDIR += ../../maksi
  LIBS += -lmaksi
  CONFIG += debug
}
win32 {
  DESTDIR = ../../win32
  QMAKE_LIBDIR += ../../win32
  LIBS += -lmaksi
  CONFIG += release
}
