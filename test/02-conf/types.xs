<?xml version="1.0" encoding="UTF-8"?>

<schema xmlns="http://www.w3.org/2001/XMLSchema">

  <simpleType name='str'>
    <restriction>
      <simpleType>
        <restriction base='string'>
        </restriction>
      </simpleType>
    </restriction>
  </simpleType>

  <simpleType name='enum'>
    <restriction base='Name'>
      <enumeration value='one'/>
      <enumeration value='two'/>
      <enumeration value='three'/>
    </restriction>
  </simpleType>

  <simpleType name='ints'>
    <list itemType='int' />
  </simpleType>

  <simpleType name='bis'>
    <list>
      <simpleType>
        <union memberTypes='boolean int' />
      </simpleType>
    </list>
  </simpleType>

</schema>
