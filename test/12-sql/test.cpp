/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, 2012, 2014, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <QtTest/QtTest>

#include <maksi/iface.h>
#include <QtDebug>


class Test: public QObject
{
    Q_OBJECT;
public:

    Test() : m_maksi(0), m_script(0), m_context(0) {}

    maksi_t m_maksi;
    maksi::script_t m_script;
    maksi::context_t m_context;

    static void message(int type, const QString &msg) {
        if (maksi::MESSAGE_ERROR >= type)
            qCritical("%s", qPrintable(msg));
        else if (maksi::MESSAGE_NOTE >= type)
            qWarning("%s", qPrintable(msg));
        else
            qDebug("%s", qPrintable(msg));
    }

private slots:

    void initTestCase() {
        qDebug("create maksi");
        m_maksi = maksi_newInstance("test");
        if (m_maksi) {
            maksi_setMessageHandler(m_maksi, message);
        }

        QVERIFY(m_maksi != 0);
    }

    void cleanupTestCase() {
        QCoreApplication::processEvents();

        qDebug("destroy maksi");
        maksi_freeContext(m_context);
        maksi_freeInstance(m_maksi);
    }

    void loadConfig();
    void testScript();
    void testSql();
};

void Test::loadConfig()
{
    maksi_loadConfig(m_maksi, "test.xml");
    QVERIFY(1);
}

void Test::testScript()
{
    m_script = maksi_script(m_maksi, "test");
    QVERIFY(m_script != 0);
}

void Test::testSql()
{
    QVariant res;
    if (m_script) {
        m_context = maksi_newContext(m_script);
        if (m_context) {
            maksi_setArg(m_context, "a", 1);
            res = maksi_eval(m_context);
        }
    }
    QVERIFY(res.isValid());
}

QTEST_MAIN(Test)
#include "test.moc"

 /* End of code */
