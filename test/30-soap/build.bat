echo off
echo Setting up environment for Qt usage...
set QTDIR=\\VBOXSVR\win32\qt\Desktop\Qt\4.8.1\mingw
set MINGW=\\VBOXSVR\win32\qt\mingw

set PATH=%QTDIR%\bin;%PATH%
set PATH=%MINGW%\bin;%PATH%

qmake
mingw32-make clean
mingw32-make
