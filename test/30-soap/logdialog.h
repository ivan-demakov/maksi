/* -*-mode:C++-*- */
/*
 * logdialog.h
 *
 * Copyright (C) 2008, 2009, 2012, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Mon Feb 25 19:16:58 2008
 *
 * $Id: logdialog.h,v 1.4 2009/01/11 10:22:31 ksion Exp $
 *
 */

#ifndef LOGDIALOG_H
#define LOGDIALOG_H

#include <QObject>
#include <QString>
#include <QDialog>

#include <maksi/defs.h>

class QTextEdit;
class QComboBox;
class QPushButton;


class LogDialog : public QDialog
{
    Q_OBJECT
public:
    LogDialog(const QString &title, QWidget *parent = 0);

    int getVerbosity() const { return verbose_level; }

private:
    QTextEdit   *logEdit;
    QComboBox   *verbCombo;

    int verbose_level;

    QString time() const;
    void showText(const QString &);

public slots:
    void setVerbosity(int level);
    void message(int type, QString msg);
};

#endif

/* End of file */
