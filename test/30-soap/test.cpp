/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <QApplication>

#include <maksi/maksi.h>
#include <maksi/script.h>

#include "logdialog.h"


class Test: public QApplication
{
    Q_OBJECT
public:
    Test(int argc, char **argv);

private:
    maksi::Maksi *m_maksi;
    LogDialog *m_log;
};


Test::Test(int argc, char **argv) :
    QApplication(argc, argv),
    m_maksi(maksi::Maksi::instance("test")),
    m_log(new LogDialog("SoapSrv"))
{
    connect(m_maksi, SIGNAL(message(int, QString)), m_log, SLOT(message(int, QString)));
    m_log->show();

    m_maksi->loadConfig("soap.xml");
}


int main(int argc, char **argv)
{
    Test app(argc, argv);
    return app.exec();
}

#include "test.moc"


 /* End of code */
