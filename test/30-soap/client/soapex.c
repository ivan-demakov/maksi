#include "soapH.h"
#include "SoapExampleBinding.nsmap"

main(int argc, char **argv)
{
  char *addr = 0;
  struct ns1__selectResponse selectRes;
  int i;

  if (argc >= 1) {
    addr = argv[1];
  }

//  while (1)
  {
    struct soap *soap = soap_new(); 
    float res;
    printf("<add>\n");
    if (SOAP_OK == soap_call_ns1__add(soap, addr, NULL, 1.2, 3.4, &res)) {
      printf("res = %g\n", res);
    } else {
      soap_print_fault(soap, stderr);
    }
    printf("</add>\n");

    printf("<select>\n");
    if (SOAP_OK == soap_call_ns1__select(soap, addr, NULL, &selectRes)) {
      for (i = 0; selectRes->reply; i++) {
      }
    } else {
      soap_print_fault(soap, stderr);
    }
    printf("</select>\n");

    soap_free(soap);
  }
}
