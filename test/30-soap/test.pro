TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += ../..

TARGET = soap

unix {
  CONFIG += debug
  QMAKE_LIBDIR += ../../maksi
  LIBS += -lmaksi
  CONFIG += debug
}
win32 {
  DESTDIR = ../../win32
  QMAKE_LIBDIR += ../../win32
  LIBS += -lmaksi
  CONFIG += release
}

SOURCES += logdialog.cpp test.cpp
HEADERS += logdialog.h
