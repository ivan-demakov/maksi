<?xml version="1.0" encoding="UTF-8"?>

<schema xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:ds="http://tgrad.ru/soap"
        targetNamespace='http://tgrad.ru/soap'>

  <complexType name='row'>
    <attribute name='name' type='string' default=''/>
    <attribute name='val' type='int' default='0' />
  </complexType>

  <complexType name='table'>
    <sequence>
      <element name='row' type='ds:row' minOccurs='0' maxOccur='unbounded' />
    </sequence>
  </complexType>

</schema>
