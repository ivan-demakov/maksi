/*
 * logdialog.cpp
 *
 * Copyright (C) 2008, 2009, 2012, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Mon Feb 25 19:18:32 2008
 *
 */

#include <QtGui>

#include "logdialog.h"

static char rcsid[] = "$Id: logdialog.cpp,v 1.4 2009/01/11 10:22:31 ksion Exp $";
static void *rcs[] = { rcsid, &rcs };


LogDialog::LogDialog(const QString &title, QWidget *parent) : QDialog(parent)
{
    setWindowTitle(title);

    QVBoxLayout *dlgLayout = new QVBoxLayout;
    setLayout(dlgLayout);

    QHBoxLayout *optLayout = new QHBoxLayout;
    QWidget *optWidget = new QWidget;
    optWidget->setLayout(optLayout);
    dlgLayout->addWidget(optWidget);

    verbCombo = new QComboBox;
    verbCombo->addItem("Quiet");
    verbCombo->addItem("Error");
    verbCombo->addItem("Warning");
    verbCombo->addItem("Note");
    verbCombo->addItem("Info");
    verbCombo->addItem("Debug");
    verbCombo->addItem("All");

    optLayout->addWidget(verbCombo);

    connect(verbCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(setVerbosity(int)));

    logEdit = new QTextEdit;
    logEdit->setReadOnly(true);
    dlgLayout->addWidget(logEdit);

    QDialogButtonBox *buttonBox = new QDialogButtonBox();
    dlgLayout->addWidget(buttonBox);

    QPushButton *clearButton = buttonBox->addButton("Clear", QDialogButtonBox::ActionRole);
    connect(clearButton, SIGNAL(clicked()), logEdit, SLOT(clear()));

    setVerbosity(maksi::MESSAGE_DEBUG);
}

QString LogDialog::time() const
{
    return QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
}

void LogDialog::setVerbosity(int level)
{
    if (level >= 0 && level < verbCombo->count()) {
        verbose_level = level;
        verbCombo->setCurrentIndex(level);
    }
}

void LogDialog::showText(const QString &msg)
{
    logEdit->moveCursor(QTextCursor::End);
    logEdit->insertHtml(msg);

//    QCoreApplication::processEvents();
}


void LogDialog::message(int type, QString msg)
{
    if (verbose_level >= type) {
        QString s = Qt::escape(msg);
        s.replace('\n', "<br>");
        switch (type) {
        case maksi::MESSAGE_ALARM:
        case maksi::MESSAGE_ERROR:
            showText(QString("<tr><td>[<u>%1</u>]:</td><td><font color=\"red\"><b>%2</b></font></td></tr>").arg(time()).arg(s));
            break;                                                \
        case maksi::MESSAGE_WARN:
            showText(QString("<tr><td>[<u>%1</u>]:</td><td><font color=\"red\">%2</font></td></tr>").arg(time()).arg(s));
            break;
        case maksi::MESSAGE_NOTE:
            showText(QString("<tr><td>[<u>%1</u>]:</td><td><b>%2</b></td></tr>").arg(time()).arg(s));
            break;
        case maksi::MESSAGE_INFO:
            showText(QString("<tr><td>[<u>%1</u>]:</td><td>%2</td></tr>").arg(time()).arg(s));
            break;
        default:
            showText(QString("<tr><td>[<u>%1</u>]:</td><td><font color=\"gray\">%2</font></td></tr>").arg(time()).arg(s));
            break;
        }
    }
}

/* End of code */
