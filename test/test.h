/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2013, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    test.h
 * @author  ivan demakov <ivan.demakov@gmail.com>
 * @date    Thu Nov 21 11:05:44 2013
 *
 */

#ifndef TEST_H
#define TEST_H

#include <QtTest/QtTest>
#include <maksi/iface.h>


class Test: public QObject
{
    Q_OBJECT;

    maksi_t m_maksi;

    static void message(int type, const wchar_t *msg) {
        if (MAKSI_MESSAGE_ERROR >= type)
            qCritical("%ls", msg);
        else if (MAKSI_MESSAGE_NOTE >= type)
            qWarning("%ls", msg);
        else
            qDebug("%ls", msg);
    }

private slots:

    void initTestCase() {
        qDebug("create maksi");
        m_maksi = maksi_newInstance("test");
        if (m_maksi) {
            maksi_setMessageHandler(m_maksi, message);
        }

        QVERIFY(m_maksi != 0);
    }

    void cleanupTestCase() {
        QCoreApplication::processEvents();

        qDebug("destroy maksi");
        maksi_freeInstance(m_maksi);
    }
};


#endif

/* End of file */
