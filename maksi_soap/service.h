/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2014, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    service.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Nov 25 06:40:17 2011
 *
 * @brief   Service Job
 *
 */

#ifndef MAKSI_SOAP_SERVICE_H
#define MAKSI_SOAP_SERVICE_H

#include <maksi/maksi.h>
#include <maksi/xmlelem.h>

#include <QHostAddress>
#include <QTcpServer>
#include <QLocalServer>
#include <QUdpSocket>
#include <QBuffer>
#include <QThread>


namespace maksi {

class ServiceJob : public QThread
{
    Q_OBJECT;
public:
    struct OpArg {
        QString m_name;
        QString m_requestPart;
        QString m_responsePart;
        MaksiElementPtr m_val;
    };

    struct OpCall {
        MaksiScript *m_script;
        QList<OpArg> m_args;
    };

    struct OpPart {
        QString m_name;
        maksi_name_t m_type;
        MaksiElementPtr m_val;
    };

    struct Op {
        Op() {}
        Op(const QString &name) : m_name(name) {}

        QString m_name;
        QList<OpPart> m_request;
        QList<OpPart> m_response;
        QList<OpCall> m_calls;
    };

    ServiceJob(Maksi *maksi, const QString &name);
    ~ServiceJob();

    const QString &name() const { return m_name; }
    const QString &doc() const { return m_doc; }
    const QString &address() const { return m_address; }
    const QString &ns() const { return m_ns; }
    const QHash<QString, Op> &ops() const { return m_ops; }
    const QList<MaksiElementPtr> &types() const { return m_types; }

    bool initService(MaksiElementPtr conf);

protected slots:
    void typesLoaded(MaksiElementPtr val);

protected:
    void run();

    struct TcpSocket {
        TcpSocket() : m_addr(QHostAddress::Any), m_port(0) {}

        QHostAddress m_addr;
        int m_port;
    };

    Maksi *m_maksi;
    QString m_name;
    QString m_doc;
    QString m_address;
    QString m_ns;
    QList<TcpSocket> m_tcpSockets;
    QList<QTcpServer *> m_tcpServers;
    QHash<QString, Op> m_ops;
    QList<MaksiElementPtr> m_types;
};


class TcpServer : public QTcpServer
{
    Q_OBJECT;
public:
    TcpServer(Maksi *maksi, ServiceJob *service);

protected:
    void incomingConnection(int socketDescriptor);

private:
    Maksi *m_maksi;
    ServiceJob *m_service;
};


class RequestJob : public QThread
{
    Q_OBJECT;
public:
    enum state_t {
        STATE_INIT,             /**< initial state */
        STATE_REQUEST,          /**< get request */
        STATE_HEADER,           /**< get http header  */
        STATE_CONTENT,          /**< get content */
        STATE_DATA,             /**< get data */
        STATE_XML,              /**< parse xml message */
        STATE_READY,            /**< result ready */
        STATE_WSDL,             /**< send WSDL */
        STATE_REPLY,            /**< send reply */
        STATE_END,

        STATE_ERROR,
        STATE_CLOSED,           /**< connection closed */
        STATE_FAIL,             /**< socket error */
        STATE_HTTP_400,         /**< http error */
        STATE_HTTP_404,         /**< http error */
        STATE_HTTP_405,         /**< http error */
        STATE_HTTP_415,         /**< http error */
        STATE_HTTP_500          /**< http error */
    };
    enum xml_state_t {
        XML_INTERNAL_ELEMENT,
        XML_SOAP_ENVELOP,
        XML_SOAP_HEADER,
        XML_SOAP_HEADER_ELEMENT,
        XML_SOAP_BODY,
        XML_SOAP_BODY_ELEMENT,
        XML_USER_STATE
    };
    enum soap_version_t {
        SOAP_11,
        SOAP_12
    };
    enum http_version_t {
        HTTP_10,
        HTTP_11,
    };

    RequestJob(Maksi *maksi, ServiceJob *service);
    ~RequestJob();

protected slots:
    void processXml(MaksiElementPtr data);

protected:
    bool processRequest(QIODevice *dev);
    bool parseXml();
    bool readLine(QIODevice *dev);
    bool parseHttpRequest(QIODevice *dev);
    bool parseHttpHeader(QIODevice *dev);
    bool parseHttpContent(QIODevice *dev);

    void sendGoodReply(QIODevice *dev);
    void sendBadReply(QIODevice *dev);
    void sendWsdl(QIODevice *dev);

    QByteArray httpReply(const char *hdr, const char *type, int len);

    Maksi *m_maksi;
    ServiceJob *m_service;
    QByteArray m_buf;
    QString m_httpRequest;
    QUrl m_httpUrl;
    QString m_charset;
    QString m_soapAction;
    QList<QByteArray> m_httpHeaders;
    QList<QByteArray> m_httpValues;
    QList<MaksiElementPtr> m_soapHeaders;
    //QList<element_t> m_soapRequest;
    MaksiElementPtr m_soapRequest;
    QHash<QString, QVariant> m_soapResponse;

    int m_contentLength;
    http_version_t m_httpVersion;
    soap_version_t m_soapVersion;
    state_t m_state;
};

class TcpJob : public RequestJob
{
    Q_OBJECT;
public:
    TcpJob(Maksi *maksi, ServiceJob *service, int socketDescriptor);
    ~TcpJob();

protected slots:
    void disconnected();
    void error(QAbstractSocket::SocketError socketError);
    void written(qint64 bytes);
    void ready();
    void ended();

protected:
    virtual void run();

    int m_socketDescriptor;
    QTcpSocket *m_socket;
};


};

#endif

/* End of file */
