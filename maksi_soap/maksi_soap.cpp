/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2013, 2014, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_soap.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:50 2011
 *
 * @brief   SOAP engine
 *
 */

#include "maksi_soap.h"
#include "service.h"

#include <maksi/xml.h>
#include <maksi/maksi.h>

#include <QtPlugin>


SOAP_MaksiEnginePlugin::~SOAP_MaksiEnginePlugin()
{
}

QString SOAP_MaksiEnginePlugin::description() const
{
    return "SOAP";
}

MaksiEngine *SOAP_MaksiEnginePlugin::newEngine(Maksi *maksi, const QString &name, const QString &baseUrl) const
{
    return new SOAP_MaksiEngine(maksi, name, baseUrl);
}


SOAP_MaksiEngine::SOAP_MaksiEngine(Maksi *maksi, const QString &name, const QString &baseUrl) :
    MaksiEngine(maksi, name, baseUrl)
{
}

SOAP_MaksiEngine::~SOAP_MaksiEngine()
{
}

bool SOAP_MaksiEngine::preinitEngine(const MaksiElement *conf)
{
    static maksi_name_t maksi_service = maksi::xml::qname("service");
    static maksi_name_t maksi_name = maksi::xml::qname("name");

    int count = conf->countElements();
    for (int i = 0; i < count; i++) {
        MaksiElementPtr node = conf->element(i);
        if (node->elementName() == maksi_service) {
            if (node->hasAttribute(maksi_name)) {
                QString name = node->attribute(maksi_name)->elementText();
                maksi::ServiceJob *job = new maksi::ServiceJob(maksi(), name);
                if (job->initService(node)) {
                    job->start();
                } else {
                    delete job;
                    maksi()->sendMessage(MAKSI_MESSAGE_WARN, node.data(), tr("service %1 do not started").arg(name));
                }
            } else {
                maksi()->sendMessage(MAKSI_MESSAGE_ERROR, node.data(), tr("service has not required attribute 'name'"));
            }
        }
    }

    return true;
}

bool SOAP_MaksiEngine::evalCode(const QString &, const QUrl &, int)
{
    return false;
}

MaksiScript *SOAP_MaksiEngine::newScript(const QString &)
{
    return 0;
}


 /* End of code */
