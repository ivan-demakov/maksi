/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2014, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_soap.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:43 2011
 *
 * @brief   SOAP engine
 *
 */

#ifndef MAKSI_SOAP_H
#define MAKSI_SOAP_H

#include <QObject>

#include <maksi/engine.h>
#include <maksi/script.h>
#include <maksi/elem.h>


class SOAP_MaksiEnginePlugin: public QObject, MaksiEnginePlugin
{
    Q_OBJECT;
    Q_PLUGIN_METADATA(IID MaksiEnginePlugin_IID FILE "maksi-soap.json");
    Q_INTERFACES(MaksiEnginePlugin);

public:
    ~SOAP_MaksiEnginePlugin();

    virtual QString description() const;
    virtual MaksiEngine *newEngine(Maksi *maksi, const QString &name, const QString &baseUrl) const;
};


class SOAP_MaksiEngine : public MaksiEngine
{
    Q_OBJECT
    Q_DISABLE_COPY(SOAP_MaksiEngine);

public:
    SOAP_MaksiEngine(Maksi *maksi, const QString &name, const QString &baseUrl);
    ~SOAP_MaksiEngine();

    virtual MaksiScript *newScript(const QString &scriptName);
    virtual bool preinitEngine(const MaksiElement *conf);
    virtual bool evalCode(const QString &code, const QUrl &url, int line);
};


#endif

/* End of file */
