include(../maksi.pri)

MAKSI_SOAP_VERSION=2.0
DEFINES += MAKSI_SOAP_VERSION=\\\"$${MAKSI_SOAP_VERSION}\\\"

TEMPLATE = lib
CONFIG += plugin

QT += network
QT -= gui

unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksiSOAP)
  DESTDIR = ../maksi
  CONFIG += debug
}
win32 {
  TARGET = maksiSOAP
  DESTDIR = ../win32
  QMAKE_LIBDIR += ../win32
  LIBS += -lmaksi
  CONFIG += release
}

INCLUDEPATH += ..

HEADERS = maksi_soap.h service.h
SOURCES = maksi_soap.cpp service.cpp

OTHER_FILES  += maksi-soap.json
