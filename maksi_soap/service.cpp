/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2013, 2014, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    service.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Nov 25 06:44:16 2011
 *
 * @brief   Service Job
 *
 */

#include "service.h"

#include <maksi/xmloader.h>
#include <maksi/script.h>

#include <QTcpSocket>
#include <QLocalSocket>
#include <QXmlStreamReader>
#include <QDateTime>


namespace maksi {

static QString ns_xsd(QString::fromLatin1("http://www.w3.org/2001/XMLSchema"));

static QString ns_wsdl(QString::fromLatin1("http://schemas.xmlsoap.org/wsdl/"));
static QString ns_soap(QString::fromLatin1("http://schemas.xmlsoap.org/wsdl/soap/"));
static QString ns_http(QString::fromLatin1("http://schemas.xmlsoap.org/wsdl/http/"));
static QString ns_mime(QString::fromLatin1("http://schemas.xmlsoap.org/wsdl/mime/"));
static QString ns_soapenc(QString::fromLatin1("http://schemas.xmlsoap.org/soap/encoding/"));
static QString ns_soapenv(QString::fromLatin1("http://schemas.xmlsoap.org/soap/envelope/"));

static QString ns_wsdl12(QString::fromLatin1("http://www.w3.org/2003/06/wsdl"));
static QString ns_soap12(QString::fromLatin1("http://www.w3.org/2003/06/wsdl/soap12"));
static QString ns_http12(QString::fromLatin1("http://www.w3.org/2003/06/wsdl/http"));
static QString ns_mime12(QString::fromLatin1("http://www.w3.org/2003/06/wsdl/mime"));
static QString ns_soapenc12(QString::fromLatin1("http://www.w3.org/2003/05/soap-encoding"));
static QString ns_soapenv12(QString::fromLatin1("http://www.w3.org/2003/05/soap-envelope"));


ServiceJob::ServiceJob(Maksi *maksi, const QString &name) :
    m_maksi(maksi),
    m_name(name)
{
}

ServiceJob::~ServiceJob()
{
    while (!m_tcpServers.isEmpty()) {
        QTcpServer *ts = m_tcpServers.takeFirst();
        ts->close();
        delete ts;
    }
}

bool ServiceJob::initService(MaksiElementPtr conf)
{
    static maksi_name_t maksi_tcp = xml::qname("tcp-socket");
    static maksi_name_t maksi_addr = xml::qname("address");
    static maksi_name_t maksi_port = xml::qname("port");
    static maksi_name_t maksi_doc = xml::qname("documentation");
    static maksi_name_t maksi_ns = xml::qname("namespace");
    static maksi_name_t maksi_name = xml::qname("name");
    static maksi_name_t maksi_type = xml::qname("type");
    static maksi_name_t maksi_types = xml::qname("types");
    static maksi_name_t maksi_loc = xml::qname("location");

    static maksi_name_t maksi_op = xml::qname("operation");
    static maksi_name_t maksi_request = xml::qname("request");
    static maksi_name_t maksi_response = xml::qname("response");
    static maksi_name_t maksi_part = xml::qname("part");
    static maksi_name_t maksi_call = xml::qname("call");
    static maksi_name_t maksi_script = xml::qname("script");
    static maksi_name_t maksi_arg = xml::qname("arg");
    static maksi_name_t maksi_requestPart = xml::qname("request-part");
    static maksi_name_t maksi_responsePart = xml::qname("response-part");

    for (int i = 0; i < conf->countElements(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (node->elementName() == maksi_tcp) {
            TcpSocket sock;
            if (node->hasAttribute(maksi_addr)) {
                QString val;
                if (node->attribute(maksi_addr)->elementToString(&val))
                    sock.m_addr.setAddress(val);
            }
            if (node->hasAttribute(maksi_port)) {
                int val;
                if (node->attribute(maksi_port)->elementToInt(&val))
                    sock.m_port = val;
            }
            m_tcpSockets.append(sock);
        } else if (node->elementName() == maksi_doc) {
            m_doc += node->elementText();
        } else if (node->elementName() == maksi_addr) {
            m_address = node->elementText();
        } else if (node->elementName() == maksi_ns) {
            m_ns = node->elementText();
        } else if (node->elementName() == maksi_types) {
            if (node->hasAttribute(maksi_loc)) {
                QString loc;
                if (node->attribute(maksi_loc)->elementToString(&loc)) {
                    QUrl url(loc, QUrl::StrictMode);
                    if (url.isValid()) {
                        if (url.isRelative()) {
                            QUrl base(node->elementSourceUrl());
                            if (base.isValid())
                                url = base.resolved(url);
                        }

                        XmlLoader loader(m_maksi, url);
                        connect(&loader, SIGNAL(loaded(element_t)), this, SLOT(typesLoaded(element_t)), Qt::DirectConnection);
                        loader.load();
                    } else {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node.data(), tr("invalid url '%1'").arg(loc));
                    }
                }
            } else {
                m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node.data(), tr("types has not required attribute '%1'").arg(maksi_loc->toString()));
                return false;
            }
        }
    }

    if (m_address.isEmpty()) {
        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, conf.data(), tr("service has not required element 'address'"));
        return false;
    }
    if (m_ns.isEmpty()) {
        m_ns = xml::maksi_uri;
    }
    if (m_tcpSockets.isEmpty()) {
        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, conf.data(), tr("service has not listen any socket"));
        return false;
    }

    maksi_name_t targetName = xml::qname("name", m_ns);
    for (int i = 0; i < conf->countElements(); i++) {
        MaksiElementPtr node = conf->element(i);

        if (node->elementName() == maksi_op) {
            QString name = node->attribute(maksi_name)->elementText();
            if (name.isEmpty()) {
                m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("operation has not required attribute 'name'"));
                goto next;
            }

            Op op(name);
            for (int i = 0; i < node->countElements(); i++) {
                MaksiElementPtr node = node->element(i);
                if (node->elementName() == maksi_request) {
                    OpPart part;
                    part.m_name = node->attribute(maksi_part)->elementText();
                    if (part.m_name.isEmpty()) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("request has not required attribute 'part'"));
                        goto next;
                    }
                    if (!node->hasAttribute(maksi_type)) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("request has not required attribute 'type'"));
                        goto next;
                    }
                    part.m_type = xml::qname(node->attribute(maksi_type)->elementText(), node.data(), targetName);
                    MaksiType type = m_maksi->type(part.m_type);
                    if (!type.isValid()) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("request has undefined type '%1'").arg(part.m_type->toString()));
                        goto next;
                    }
                    part.m_val = type.createNew(m_maksi, node.data(), part.m_type, false);
                    op.m_request.append(part);
                } else if (node->elementName() == maksi_response) {
                    OpPart part;
                    part.m_name = node->attribute(maksi_part)->elementText();
                    if (part.m_name.isEmpty()) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("response has not required attribute 'part'"));
                        goto next;
                    }
                    if (!node->hasAttribute(maksi_type)) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("response has not required attribute 'type'"));
                        goto next;
                    }
                    part.m_type = xml::qname(node->attribute(maksi_type)->elementText(), node.data(), targetName);
                    MaksiType type = m_maksi->type(part.m_type);
                    if (!type.isValid()) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("response has undefined type '%1'").arg(part.m_type->toString()));
                        goto next;
                    }
                    part.m_val = type.createNew(m_maksi, node.data(), part.m_type, false);
                    op.m_response.append(part);
                } else if (node->elementName() == maksi_call) {
                    if (!node->hasAttribute(maksi_script)) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("call has not required attribute 'script'"));
                        goto next;
                    }
                    QString scriptName(node->attribute(maksi_script)->elementText());
                    OpCall call;
                    call.m_script = m_maksi->script(scriptName);
                    if (!call.m_script) {
                        m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("call has undefined script '%1'").arg(scriptName));
                        goto next;
                    }

                    for (int i = 0; i < node->countElements(); i++) {
                        MaksiElementPtr node = node->element(i);
                        if (node->elementName() == maksi_arg) {
                            OpArg arg;
                            arg.m_name = node->attribute(maksi_name)->elementText();
                            if (arg.m_name.isEmpty()) {
                                m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("arg has not required attribute 'name'"));
                                goto next;
                            }
                            if (node->hasAttribute(maksi_type)) {
                                maksi_name_t typeName = xml::qname(node->attribute(maksi_type)->elementText(), node.data(), targetName);
                                MaksiType type = m_maksi->type(typeName);
                                if (!type.isValid()) {
                                    m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, node, tr("arg has undefined type '%1'").arg(typeName->toString()));
                                    goto next;
                                }
                                arg.m_val = type.createNew(m_maksi, node.data(), typeName, false);
                            }
                            arg.m_requestPart = node->attribute(maksi_requestPart)->elementText();
                            arg.m_responsePart = node->attribute(maksi_responsePart)->elementText();

                            call.m_args.append(arg);
                        }
                    }

                    op.m_calls.append(call);
                }
            }

            m_ops.insert(name, op);
        }
    next:;
    }

    return true;
}

void ServiceJob::typesLoaded(MaksiElementPtr conf)
{
    m_types.append(conf);
}

void ServiceJob::run()
{
    while (!m_tcpSockets.isEmpty()) {
        TcpSocket sock = m_tcpSockets.takeFirst();
        TcpServer *ts = new TcpServer(m_maksi, this);
        if (ts->listen(sock.m_addr, sock.m_port)) {
            m_tcpServers.append(ts);
            m_maksi->sendMessage(MAKSI_MESSAGE_INFO, tr("service %1: listen on address %2 port %3").arg(m_name).arg(ts->serverAddress().toString()).arg(ts->serverPort()));
        } else {
            m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, tr("service %1: socket error: %2").arg(m_name).arg(ts->errorString()));
            delete ts;
        }
    }

    exec();
}


TcpServer::TcpServer(Maksi *maksi, ServiceJob *service) :
    m_maksi(maksi),
    m_service(service)
{
}

void TcpServer::incomingConnection(int socketDescriptor)
{
    TcpJob *job = new TcpJob(m_maksi, m_service, socketDescriptor);
    connect(job, SIGNAL(finished()), job, SLOT(ended()));

    job->start();
}


RequestJob::RequestJob(Maksi *maksi, ServiceJob *service) :
    m_maksi(maksi),
    m_service(service),
    m_state(STATE_INIT)
{
}

RequestJob::~RequestJob()
{
}

bool RequestJob::processRequest(QIODevice *dev)
{
    for (;;) {
        switch (m_state) {
        case STATE_INIT:
            m_state = STATE_REQUEST;
            break;

        case STATE_REQUEST:
            if (!parseHttpRequest(dev))
                return false;
            break;

        case STATE_HEADER:
            if (!parseHttpHeader(dev))
                return false;
            break;

        case STATE_CONTENT:
            if (!parseHttpContent(dev))
                return false;
            break;

        case STATE_DATA:
            if (m_contentLength == 0) {
                m_state = STATE_XML;
            } else {
                QByteArray b = dev->read(m_contentLength);
                if (b.size() == 0)
                    return false;
                m_buf += b;
                m_contentLength -= b.size();
            }
            break;

        case STATE_XML:
            if (!parseXml())
                return false;
            break;

        case STATE_READY:
        case STATE_WSDL:
        case STATE_REPLY:
        case STATE_END:
        case STATE_ERROR:
        case STATE_HTTP_400:
        case STATE_HTTP_404:
        case STATE_HTTP_405:
        case STATE_HTTP_415:
        case STATE_HTTP_500:
        case STATE_FAIL:
        case STATE_CLOSED:
            return true;
        }
    }
}

bool RequestJob::readLine(QIODevice *dev)
{
    if (m_buf.isEmpty()) {
        m_buf.reserve(512);
    }

    for(;;) {
        char ch;
        int num = dev->read(&ch, 1);
        if (num == 0) {
            return false;
        } else if (num < 0) {
            m_state = STATE_CLOSED;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("unexpected end of file"));
            return false;
        } else if (ch == '\n') {
            if (m_buf.endsWith('\r'))
                m_buf.chop(1);
            //qDebug("%s", m_buf.data());
            return true;
        } else {
            if (m_buf.size() > 16*1024) {
                m_state = STATE_HTTP_400;
                m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("input buffer overflow"));
                return false;
            }
            if (ch == '\r' && m_buf.size() == 0)
                continue;
            m_buf.append(ch);
        }
    }
}

bool RequestJob::parseHttpRequest(QIODevice *dev)
{
    if (readLine(dev)) {
        QList<QByteArray> req = m_buf.split(' ');
        if (req.size() < 3) {
            m_state = STATE_HTTP_400;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("malformed HTTP request: %1").arg(QString::fromLatin1(m_buf)));
            return true;
        }

        m_httpRequest = req.at(0);
        m_httpUrl = QUrl::fromEncoded(req.at(1), QUrl::StrictMode);

        if (req.at(2) == "HTTP/1.0") {
            m_httpVersion = HTTP_10;
        } else if (req.at(2) == "HTTP/1.1") {
            m_httpVersion = HTTP_11;
        } else {
            m_state = STATE_HTTP_400;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("unsupported HTTP version: %1").arg(m_httpVersion));
            return true;
        }

        if (m_httpRequest != "GET" && m_httpRequest != "POST") {
            m_state = STATE_HTTP_405;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("unsupported request: %1").arg(m_httpRequest));
            return true;
        }

        if (!m_httpUrl.isValid()) {
            m_state = STATE_HTTP_404;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("invalid URL: %1").arg(m_httpUrl.toString()));
            return true;
        }

        QUrl baseUrl(m_service->address());
        if (m_httpUrl.scheme().isEmpty()) {
            m_httpUrl.setScheme("http");
        }
        if (m_httpUrl.scheme() == "http") {
            m_httpUrl.setAuthority(baseUrl.authority());
        }

        if (m_httpUrl.path() == baseUrl.path()) {
            static QString wsdl(QString::fromLatin1("wsdl"));
            if (m_httpUrl.query() == wsdl) {
                m_state = STATE_WSDL;
                return true;
            }
        } else {
            m_state = STATE_HTTP_404;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("unknown URL: %1").arg(m_httpUrl.toString()));
            return true;
        }

        m_buf.clear();
        m_state = STATE_HEADER;
        return true;
    }
    return (m_state >= STATE_ERROR ? true : false);
}

bool RequestJob::parseHttpHeader(QIODevice *dev)
{
    while (readLine(dev)) {
        if (m_buf.isEmpty()) { // empty line, so end of http header
            //m_buf.clear();
            m_state = STATE_CONTENT;
            return true;
        }

        if (m_buf.at(0) == ' ' || m_buf.at(0) == '\t') {
            if (m_httpValues.isEmpty()) {
                m_state = STATE_HTTP_400;
                m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("malformed HTTP header: %1").arg(QString::fromLatin1(m_buf)));
                return true;
            } else {
                if (!m_httpValues[m_httpValues.size() - 1].isEmpty())
                    m_httpValues[m_httpValues.size() - 1].append(' ');
                m_httpValues[m_httpValues.size() - 1].append(m_buf.trimmed());
                m_buf.clear();
                continue;
            }
        }

        int pos = m_buf.indexOf(':');
        if (pos < 0) {
            m_state = STATE_HTTP_400;
            m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("malformed HTTP header: %1").arg(QString::fromLatin1(m_buf)));
            return true;
        }

        QByteArray header(m_buf.left(pos).trimmed());
        QByteArray value(m_buf.mid(pos+1).trimmed());
        m_httpHeaders.append(header);
        m_httpValues.append(value);

        m_buf.clear();
    }
    return (m_state >= STATE_ERROR ? true : false);
}

static int ncIndexOf(QList<QByteArray> ba, const char *val)
{
    for (int i = 0; i < ba.size(); ++i) {
        if (qstricmp(ba.at(i), val) == 0)
            return i;
    }
    return -1;
}

bool RequestJob::parseHttpContent(QIODevice *)
{
    int id = m_httpHeaders.indexOf("Content-Type");
    if (id < 0) {
        m_state = STATE_HTTP_415;
        m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("invalid http header (Content-Type must present)"));
        return true;
    }

    QByteArray contentType = m_httpValues.at(id);
    if (contentType.startsWith("text/xml")) {
        // Example: text/xml; charset=utf-8
        m_soapVersion = SOAP_11;
        id = ncIndexOf(m_httpHeaders, "SoapAction");
        if (id >= 0) {
            m_soapAction = m_httpValues.at(id).simplified();
        }
    } else if (contentType.startsWith("application/soap+xml")) {
        // Example: application/soap+xml; charset=utf-8; action=Action
        m_soapVersion = SOAP_12;
    } else {
        m_state = STATE_HTTP_415;
        m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("unsupported Content-Type: %1").arg(QString::fromLatin1(contentType)));
        return true;
    }

    QList<QByteArray> parts = contentType.split(';');
    for (int i = 0; i < parts.size(); ++i) {
        QByteArray val = parts.at(i).trimmed();
        if (val.startsWith("charset=")) {
            m_charset = QString::fromLatin1(val.mid(sizeof("charset")));
        } else {
            if (m_soapVersion == SOAP_12) {
                if (val.startsWith("action="))
                    m_soapAction = val.mid(sizeof("action"));
            }
        }
    }

    if (m_soapAction.startsWith('"') && m_soapAction.endsWith('"')) {
        m_soapAction = m_soapAction.mid(1, m_soapAction.size()-2).simplified();
    }

    QString prefix(m_service->ns());
    prefix += "/";
    if (m_soapAction.startsWith(prefix)) {
        m_soapAction = m_soapAction.mid(prefix.size());
        //qDebug("prefix=%s action=%s", qPrintable(prefix), qPrintable(m_soapAction));
    }

    if (!m_service->ops().contains(m_soapAction)) {
        m_state = STATE_HTTP_500;
        m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("unsupported soap action: %1").arg(m_soapAction));
        return true;
    }

    id = m_httpHeaders.indexOf("Content-Length");
    if (id < 0) {
        m_state = STATE_HTTP_415;
        m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("invalid http header (Content-Length must present)"));
        return true;
    }

    QByteArray contentLength = m_httpValues.at(id);
    bool ok;
    m_contentLength = contentLength.toInt(&ok);
    if (!ok || m_contentLength < 0) {
        m_state = STATE_HTTP_415;
        m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("invalid Content-Length: %1").arg(QString::fromLatin1(contentLength)));
        return true;
    }

    m_state = STATE_DATA;
    return true;
}

bool RequestJob::parseXml()
{
    XmlLoader loader(m_maksi, QUrl());
    connect(&loader, SIGNAL(loaded(element_t)), this, SLOT(processXml(element_t)), Qt::DirectConnection);

    //m_maksi->sendMessage(MESSAGE_DEBUG, tr("load xml"));
    if (!loader.load(m_buf)) {
        m_state = STATE_ERROR;
    }

    m_buf.clear();
    return true;
}

void RequestJob::processXml(MaksiElementPtr conf)
{
    static maksi_name_t soap_envelop = xml::qname("Envelope", ns_soapenv);
    static maksi_name_t soap_header = xml::qname("Header", ns_soapenv);
    static maksi_name_t soap_body = xml::qname("Body", ns_soapenv);

    static maksi_name_t soap_envelop12 = xml::qname("Envelope", ns_soapenv12);
    static maksi_name_t soap_header12 = xml::qname("Header", ns_soapenv12);
    static maksi_name_t soap_body12 = xml::qname("Body", ns_soapenv12);

    //m_maksi->sendMessage(MAKSI_MESSAGE_DEBUG, tr("process xml"));

    maksi_name_t nodeName = conf->elementName();
    if (soap_envelop != nodeName && soap_envelop12 != nodeName) {
        m_state = STATE_ERROR;
        m_maksi->sendMessage(MAKSI_MESSAGE_WARN, tr("SOAP Envelope expected (but get: %1)").arg(nodeName->toString()));
        return;
    }

    int count = conf->countElements();
    for (int i = 0; i < count; i++) {
        QVariant elem = conf->element(i);
        if (elem.canConvert<element_t>()) {
            element_t node = elem.value<element_t>();
            nodeName = node->elementName();
            //qDebug("node=%s", qPrintable(xml::toString(nodeName)));

            if (soap_header == nodeName || soap_header12 == nodeName) {
                //m_soapHeaders.append(node);
            } else if (soap_body == nodeName || soap_body12 == nodeName) {
                for (int j = 0; j < node->countElements(); ++j) {
                    QVariant v = node->element(j);
                    if (v.canConvert<element_t>()) {
                        element_t e = v.value<element_t>();
                        QString name(xml::toLocalName(e->elementName()));
                        //qDebug("name=%s", qPrintable(name));
                        if (name == m_soapAction) {
                            m_soapRequest = e;
                            break;
                        } else {
                            m_state = STATE_HTTP_500;
                            m_maksi->sendMessage(MESSAGE_WARN, tr("unknown SOAP Body part: '%1'").arg(xml::toString(e->elementName())));
                            return;
                        }
                    }
                }
            } else {
                m_state = STATE_ERROR;
                m_maksi->sendMessage(MESSAGE_WARN, tr("SOAP Header or Body expected (but get: %1)").arg(xml::toString(nodeName)));
                return;
            }
        }
    }

    const ServiceJob::Op &op(m_service->ops()[m_soapAction]);
    //qDebug("action='%s''", qPrintable(m_soapAction));

    QHash<QString, QVariant> args;
    count = m_soapRequest->countElements();
    for (int i = 0; i < count; ++i) {
        QVariant elem(m_soapRequest->element(i));
        if (elem.canConvert<element_t>()) {
            element_t node = elem.value<element_t>();
            QString name(xml::toLocalName(node->elementName()));
            for (int j = 0; j < op.m_request.size(); ++j) {
                //qDebug("name='%s'; op='%s'", qPrintable(name), qPrintable(op.m_request.at(j).m_name));
                if (op.m_request.at(j).m_name == name) {
                    maksi_name_t typeName = op.m_request.at(j).m_type;
                    schema_type_t type = m_maksi->type(typeName);
                    if (!type.isValid()) {
                        m_state = STATE_HTTP_500;
                        m_maksi->sendMessage(MESSAGE_WARN, tr("unknown SOAP request part type: '%1'").arg(xml::toString(typeName)));
                        return;
                    }
                    QVariant val = type.getValue(m_maksi, node, typeName);
                    if (!val.isValid()) {
                        m_state = STATE_HTTP_500;
                        m_maksi->sendMessage(MESSAGE_WARN, tr("invalid SOAP request part value: '%1'").arg(node->toString()));
                        return;
                    }
                    //qDebug("set %s=%s", qPrintable(name), qPrintable(val.toString()));
                    args.insert(name, val);
                    goto nextArg;
                }
            }

//            m_state = STATE_HTTP_500;
//            m_maksi->sendMessage(MESSAGE_WARN, tr("unknown SOAP request part: '%1'").arg(xml::toString(node->elementName())));
//            return;
        }
    nextArg:;
    }

    m_soapResponse.clear();
    for (int i = 0; i < op.m_response.size(); ++i) {
        if (op.m_response.at(i).m_val.isValid()) {
            m_soapResponse.insert(op.m_response.at(i).m_name, op.m_response.at(i).m_val);
        }
    }

    for (int i = 0; i < op.m_calls.size(); ++i) {
        Context *ctx = op.m_calls.at(i).m_script->newContext();
        if (ctx) {
            for (int j = 0; j < op.m_calls.at(i).m_args.size(); ++j) {
                QString partName(op.m_calls.at(i).m_args.at(j).m_requestPart);
                if (!partName.isEmpty()) {
                    QVariant val;
                    if (args.contains(partName)) {
                        val = args.value(partName);
                    } else {
                        val = op.m_calls.at(i).m_args.at(j).m_val;
                    }
                    ctx->setArg(op.m_calls.at(i).m_args.at(j).m_name, val);
                }
            }
            ctx->eval();

            for (int j = 0; j < op.m_calls.at(i).m_args.size(); ++j) {
                QString partName(op.m_calls.at(i).m_args.at(j).m_responsePart);
                if (!partName.isEmpty()) {
                    QVariant val(ctx->arg(op.m_calls.at(i).m_args.at(j).m_name));
                    if (val.isValid()) {
                        m_soapResponse.insert(partName, val);
                    }
                }
            }
            delete ctx;
        }
    }

    m_state = STATE_READY;
}

void RequestJob::sendWsdl(QIODevice *dev)
{
    QByteArray body;
    QXmlStreamWriter stream(&body);
    stream.writeStartDocument();
    stream.writeDefaultNamespace(ns_wsdl);

    QHash<QString, QString> ns;
    ns.insert(ns_xsd, "xsd");
    ns.insert(ns_soap, "soap");
    ns.insert(ns_http, "http");
    ns.insert(m_service->ns(), "tns");

    int num = 0;
    for (QHash<QString, ServiceJob::Op>::const_iterator i = m_service->ops().constBegin(); i != m_service->ops().constEnd(); ++i) {
        for (int j = 0; j < i->m_request.size(); ++j) {
            QString n(xml::toNamespace(i->m_request.at(j).m_type));
            if (!ns.contains(n)) {
                ++num;
                ns.insert(n, QString("n%1").arg(num));
            }
        }
        for (int j = 0; j < i->m_response.size(); ++j) {
            QString n(xml::toNamespace(i->m_response.at(j).m_type));
            if (!ns.contains(n)) {
                ++num;
                ns.insert(n, QString("n%1").arg(num));
            }
        }
    }

    for (QHash<QString, QString>::const_iterator i = ns.constBegin(); i != ns.constEnd(); ++i) {
        stream.writeNamespace(i.key(), i.value());
    }

    stream.writeStartElement(ns_wsdl, "definitions");
    stream.writeAttribute("targetNamespace", m_service->ns());

    stream.writeStartElement(ns_wsdl, "types");
    for (int i = 0; i < m_service->types().size(); ++i) {
        element_t xml = m_service->types().at(i);
        QList<QString> nl(ns.keys());
        xml->writeXml(stream, xml->elementName(), nl);
    }
    stream.writeEndElement(); // types

    for (QHash<QString, ServiceJob::Op>::const_iterator i = m_service->ops().constBegin(); i != m_service->ops().constEnd(); ++i) {
        stream.writeStartElement(ns_wsdl, "message");
        stream.writeAttribute("name", i.key() + "Request");
        for (int j = 0; j < i->m_request.size(); ++j) {
            QString n(xml::toNamespace(i->m_request.at(j).m_type));
            QString s(xml::toLocalName(i->m_request.at(j).m_type));

            stream.writeStartElement(ns_wsdl, "part");
            stream.writeAttribute("name", i->m_request.at(j).m_name);
            stream.writeAttribute("type", ns.value(n) + ":" + s);
            stream.writeEndElement(); // part
        }
        stream.writeEndElement(); // message

        stream.writeStartElement(ns_wsdl, "message");
        stream.writeAttribute("name", i.key() + "Response");
        for (int j = 0; j < i->m_response.size(); ++j) {
            QString n(xml::toNamespace(i->m_response.at(j).m_type));
            QString s(xml::toLocalName(i->m_response.at(j).m_type));

            stream.writeStartElement(ns_wsdl, "part");
            stream.writeAttribute("name", i->m_response.at(j).m_name);
            stream.writeAttribute("type", ns.value(n) + ":" + s);
            stream.writeEndElement(); // part
        }
        stream.writeEndElement(); // message
    }

    stream.writeStartElement(ns_wsdl, "portType");
    stream.writeAttribute("name",  m_service->name() + "Type");
    for (QHash<QString, ServiceJob::Op>::const_iterator i = m_service->ops().constBegin(); i != m_service->ops().constEnd(); ++i) {
        stream.writeStartElement(ns_wsdl, "operation");
        stream.writeAttribute("name", i.key());

        stream.writeStartElement(ns_wsdl, "input");
        stream.writeAttribute("message", "tns:" + i.key() + "Request");
        stream.writeEndElement(); // input

        stream.writeStartElement(ns_wsdl, "output");
        stream.writeAttribute("message", "tns:" + i.key() + "Response");
        stream.writeEndElement(); // output
        stream.writeEndElement(); // operation
    }
    stream.writeEndElement(); // portType

    stream.writeStartElement(ns_wsdl, "binding");
    stream.writeAttribute("name",  m_service->name() + "Binding");
    stream.writeAttribute("type", "tns:" + m_service->name() + "Type");
    stream.writeStartElement(ns_soap, "binding");
    stream.writeAttribute("style", "rpc");
    stream.writeAttribute("transport", ns_http);
    stream.writeEndElement(); // soap:binding
    for (QHash<QString, ServiceJob::Op>::const_iterator i = m_service->ops().constBegin(); i != m_service->ops().constEnd(); ++i) {
        stream.writeStartElement(ns_wsdl, "operation");
        stream.writeAttribute("name", i.key());

        stream.writeStartElement(ns_soap, "operation");
        stream.writeAttribute("soapAction", m_service->ns() + "/" + i.key());
        stream.writeEndElement(); // soap:operation

        stream.writeStartElement(ns_wsdl, "input");
        stream.writeStartElement(ns_soap, "body");
        stream.writeAttribute("use", "encoded");
        stream.writeAttribute("encodingStyle", ns_soapenc);
        stream.writeAttribute("namespace", m_service->ns());
        stream.writeEndElement(); // soap:body
        stream.writeEndElement(); // input

        stream.writeStartElement(ns_wsdl, "output");
        stream.writeStartElement(ns_soap, "body");
        stream.writeAttribute("use", "encoded");
        stream.writeAttribute("encodingStyle", ns_soapenc);
        stream.writeAttribute("namespace", m_service->ns());
        stream.writeEndElement(); // soap:body
        stream.writeEndElement(); // output

        stream.writeEndElement(); // operation
    }
    stream.writeEndElement(); // binding

    stream.writeStartElement(ns_wsdl, "service");
    stream.writeAttribute("name",  m_service->name());
    if (!m_service->doc().isEmpty()) {
        stream.writeStartElement(ns_wsdl, "documentation");
        stream.writeCharacters(m_service->doc());
        stream.writeEndElement();
    }
    stream.writeStartElement(ns_wsdl, "port");
    stream.writeAttribute("name",  m_service->name() + "Port");
    stream.writeAttribute("binding",  "tns:" + m_service->name() + "Binding");
    stream.writeStartElement(ns_soap, "address");
    stream.writeAttribute("location", m_service->address());
    stream.writeEndElement(); // soap:address
    stream.writeEndElement(); // port

    stream.writeEndElement(); // service
    stream.writeEndElement(); // definitions
    stream.writeEndDocument();

    QByteArray reply = httpReply("200 OK", "text/xml", body.size());
    //qDebug("%s", reply.data());
    //qDebug("%s", body.data());

    dev->write(reply);
    dev->write(body);
    m_state = STATE_REPLY;
}

void RequestJob::sendGoodReply(QIODevice *dev)
{
    QByteArray body;
    QXmlStreamWriter stream(&body);
    stream.writeStartDocument();

    stream.writeDefaultNamespace(ns_soapenv);
//    stream.writeNamespace(ns_xsd, "xsd");
    stream.writeNamespace(m_service->ns(), "tns");

    stream.writeStartElement(ns_soapenv, "Envelope");
    stream.writeAttribute("encodingStyle", ns_soapenc);

    stream.writeStartElement("Header");
    stream.writeEndElement(); // Header

    stream.writeStartElement("Body");

    stream.writeStartElement(m_service->ns(), m_soapAction + "Response");
    for (QHash<QString, QVariant>::const_iterator i = m_soapResponse.constBegin(); i != m_soapResponse.constEnd(); ++i) {
        QString name(i.key());
        QVariant val(i.value());
        //qDebug("%s=%s", qPrintable(name), qPrintable(val.toString()));
        if (val.canConvert<element_t>()) {
            element_t e = val.value<element_t>();
            maksi_name_t xmlName(xml::qname(name, m_service->ns()));
            e->writeXml(stream, xmlName);
        } else {
            stream.writeStartElement(name);
            stream.writeCharacters(val.toString());
            stream.writeEndElement();
        }
    }
    stream.writeEndElement();

    stream.writeEndElement(); // Body
    stream.writeEndElement(); // Envelope
    stream.writeEndDocument();

    QByteArray reply = httpReply("200 OK", "text/xml", body.size());
    //qDebug("%s", reply.data());
    //qDebug("%s", body.data());

    dev->write(reply);
    dev->write(body);
    m_state = STATE_REPLY;
}

void RequestJob::sendBadReply(QIODevice *dev)
{
    QByteArray body;
    QXmlStreamWriter stream(&body);
    stream.writeStartDocument();

    stream.writeDefaultNamespace(ns_soapenv);

    stream.writeStartElement(ns_soapenv, "Envelope");
    stream.writeAttribute(ns_soapenv, "encodingStyle", ns_soapenc);

    stream.writeStartElement(ns_soapenv, "Header");
    stream.writeEndElement(); // Header

    stream.writeStartElement(ns_soapenv, "Body");
    stream.writeStartElement(ns_soapenv, "Fault");
    stream.writeStartElement(ns_soapenv, "Code");
    stream.writeStartElement(ns_soapenv, "Value");
    stream.writeCharacters("Sender");
    stream.writeEndElement(); // Value
    stream.writeEndElement(); // Code

    stream.writeStartElement(ns_soapenv, "Reason");
    stream.writeStartElement(ns_soapenv, "Text");
    stream.writeAttribute("xml:lang", "en-US");
    stream.writeCharacters("Your application sent a request that this server could not understand");
    stream.writeEndElement(); // Text
    stream.writeEndElement(); // Reason

    stream.writeEndElement(); // Fault
    stream.writeEndElement(); // Body
    stream.writeEndElement(); // Envelope
    stream.writeEndDocument();

    QByteArray reply;
    switch (m_state) {
    case STATE_HTTP_404:
        reply = httpReply("404 Not Found", "text/xml", body.size());
        break;
    case STATE_HTTP_405:
        reply = httpReply("405 Method Not Allowed", "text/xml", body.size());
        break;
    case STATE_HTTP_415:
        reply = httpReply("415 Unsupported Media", "text/xml", body.size());
        break;
    case STATE_HTTP_500:
        reply = httpReply("500 Internal Server Error", "text/xml", body.size());
        break;
    default:
        reply = httpReply("400 Bad Request", "text/xml", body.size());
        break;
    }

    dev->write(reply);
    dev->write(body);
    m_state = STATE_REPLY;
}

QByteArray RequestJob::httpReply(const char *hdr, const char *type, int len)
{
    QByteArray reply;

    if (m_httpVersion == HTTP_10) {
        reply = "HTTP/1.0 ";
        reply += hdr;
        reply += "\r\n";
    } else {
        reply = "HTTP/1.1 ";
        reply += hdr;
        reply += "\r\n";

        reply += "Date: ";
        reply += QDateTime::currentDateTimeUtc().toString("ddd, dd MMM yyyy hh:mm:ss UTC");
        reply += "\r\n";

        reply += "Server: MaksiSoapEngine/";
        reply += MAKSI_SOAP_VERSION;
        reply += "\r\n";
    }

    reply += "Content-Type: ";
    reply += type;
    reply += "; charset=utf-8\r\n";

    reply += "Content-Length: ";
    reply += QByteArray::number(len);
    reply += "\r\n\r\n";

    return reply;
}


TcpJob::TcpJob(Maksi *maksi, ServiceJob *service, int socketDescriptor) :
    RequestJob(maksi, service),
    m_socketDescriptor(socketDescriptor),
    m_socket(0)
{
}

TcpJob::~TcpJob()
{
    delete m_socket;
}

void TcpJob::disconnected()
{
    if (m_state != STATE_END) {
        m_state = STATE_CLOSED;
        m_maksi->sendMessage(MESSAGE_WARN, tr("service %1: client %2: connection closed by peer").arg(m_service->name()).arg(m_socket->peerAddress().toString()));
        quit();
    }
}

void TcpJob::error(QAbstractSocket::SocketError)
{
    m_state = STATE_FAIL;
    m_maksi->sendMessage(MESSAGE_WARN, tr("service %1: client %2: socket error: %3").arg(m_service->name()).arg(m_socket->peerAddress().toString()).arg(m_socket->errorString()));
    quit();
}

void TcpJob::written(qint64)
{
    if (m_state == STATE_REPLY) {
        if (m_socket->bytesToWrite() == 0) {
            m_state = STATE_END;
            m_maksi->sendMessage(MESSAGE_INFO, tr("service %1: client %2: close connection").arg(m_service->name()).arg(m_socket->peerAddress().toString()));
            m_socket->close();
            quit();
        }
    }
}

void TcpJob::ready()
{
    if (processRequest(m_socket)) {
        switch (m_state) {
        case STATE_WSDL:
            sendWsdl(m_socket);
            break;

        case STATE_READY:
            sendGoodReply(m_socket);
            break;

        case STATE_ERROR:
        case STATE_HTTP_400:
        case STATE_HTTP_404:
        case STATE_HTTP_405:
        case STATE_HTTP_415:
        case STATE_HTTP_500:
            sendBadReply(m_socket);
            break;

        default:
            break;
        }
    }
}

void TcpJob::ended()
{
//    qDebug("job finished");
    delete this;
}

void TcpJob::run()
{
    if (!m_socket) {
        m_socket = new QTcpSocket();
        m_socket->setSocketDescriptor(m_socketDescriptor);
        connect(m_socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);
        connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)), Qt::DirectConnection);
        connect(m_socket, SIGNAL(bytesWritten(qint64)), this, SLOT(written(qint64)), Qt::DirectConnection);
        connect(m_socket, SIGNAL(readyRead()), this, SLOT(ready()), Qt::DirectConnection);
        m_maksi->sendMessage(MESSAGE_INFO, tr("service %1: client %2: connection opened").arg(m_service->name()).arg(m_socket->peerAddress().toString()));
    }

    exec();
}


};

 /* End of code */
