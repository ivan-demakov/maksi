/*
 * mainwindow.cpp
 *
 * Copyright (C) 2009, 2010, 2011, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Sep 27 15:48:31 2009
 *
 */

#include "mainwindow.h"
#include "maksiapp.h"
#include "logdialog.h"
#include "prefdialog.h"

#include <QtGui>

#include <maket/graphics.h>
#include <maket/view.h>
#include <maket/scene.h>


MainWindow::MainWindow() :
    m_view(0),
    m_scene(0)
{
    setWindowTitle(MaksiApp::applicationName());
    readSettings();

    maket::Maket *mak = MaksiApp::app()->maket();

    m_scene = mak->scene("main");
    if (m_scene) {
        m_view = mak->view("main");
        setCentralWidget(m_view.data());
    } else {
        MaksiApp::log()->warn(QString("cannot get scene"));
    }

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();
}

MainWindow::~MainWindow()
{
    if (m_view)
        m_view->setParent(0);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    maket::Maket *mak = MaksiApp::app()->maket();
    mak->maksi()->wait();

    writeSettings();
    event->accept();
}

void MainWindow::createActions()
{
    open_act = new QAction(tr("&Open"), this);
    open_act->setShortcut(QKeySequence::Open);
    connect(open_act, SIGNAL(triggered()), this, SLOT(open()));

    close_act = new QAction(tr("&Close"), this);
    close_act->setShortcut(QKeySequence::Close);
    connect(close_act, SIGNAL(triggered()), this, SLOT(close()));

    exit_act = new QAction(tr("E&xit"), this);
    exit_act->setShortcut(tr("Ctrl+Q"));
    connect(exit_act, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

    preference_act = new QAction(tr("&Preferences"), this);
    preference_act->setShortcut(tr("Ctrl+P"));
    connect(preference_act, SIGNAL(triggered()), this, SLOT(preferences()));

    clear_act = new QAction(tr("Cl&ear"), this);
    clear_act->setShortcut(tr("Ctrl+E"));
    if (m_scene) {
        connect(clear_act, SIGNAL(triggered()), this, SLOT(clearScene()));
    }

    about_act = new QAction(tr("&About") + " " + MaksiApp::applicationName(), this);
    connect(about_act, SIGNAL(triggered()), this, SLOT(about()));

    aboutQt_act = new QAction(tr("About &Qt"), this);
    connect(aboutQt_act, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    showlog_act = new QAction(tr("&Log"), this);
    showlog_act->setShortcut(tr("Ctrl+L"));
    connect(showlog_act, SIGNAL(triggered()), MaksiApp::log(), SLOT(show()));

    zoomin_act = new QAction(tr("Zoom In"), this);
    zoomin_act->setShortcut(tr("*"));
    connect(zoomin_act, SIGNAL(triggered()), this, SLOT(zoomin()));

    zoomout_act = new QAction(tr("Zoom Out"), this);
    zoomout_act->setShortcut(tr("/"));
    connect(zoomout_act, SIGNAL(triggered()), this, SLOT(zoomout()));

    selectMode_act = new QAction(tr("&Select"), this);
    selectMode_act->setCheckable(true);
    connect(selectMode_act, SIGNAL(triggered()), this, SLOT(selectMode()));
}

bool MainWindow::createLayerMenu(QList<maket::LayerElement*> ls)
{
    for (int i = 0; i < ls.size(); i++) {
        QAction *act = new QAction(ls.at(i)->attribute("name").toString(), this);
        act->setData(QVariant::fromValue(ls.at(i)));
        act->setCheckable(true);

        bool visible = ls.at(i)->isVisible();
        act->setChecked(visible);

        layer_menu->addAction(act);
        connect(act, SIGNAL(triggered()), this, SLOT(toggleLayerVisible()));

        bool sublayers = createLayerMenu(ls.at(i)->layers());
        if (sublayers) {
            if (i < ls.size()-1)
                layer_menu->addSeparator();
        }
    }
    return ls.size() > 0;
}

void MainWindow::createMenus()
{
    file_menu = menuBar()->addMenu(tr("&File"));
    file_menu->addAction(open_act);
    file_menu->addAction(close_act);
    file_menu->addSeparator();
    file_menu->addAction(exit_act);

    action_menu = menuBar()->addMenu(tr("&Action"));
    action_menu->addAction(zoomin_act);
    action_menu->addAction(zoomout_act);
    action_menu->addAction(selectMode_act);

    layer_menu = menuBar()->addMenu(tr("&Layer"));
    if (m_scene) {
        createLayerMenu(m_scene->layers());
    }

    shape_menu = menuBar()->addMenu(tr("&Shape"));
    shape_menu->addAction(clear_act);

#if 0
    maksi::Maksi *maksi = MaksiApp::app()->maksi();
    if (maksi) {
        shape_menu->addSeparator();
        for (int i = 0; i < maksi->shapeCount(); i++) {
            QString id = maksi->shapeId(i);
            QAction *act = new QAction(id, this);
            act->setData(id);
            connect(act, SIGNAL(triggered()), this, SLOT(appendShape()));

            shape_menu->addAction(act);
        }
    }
#endif

    tool_menu = menuBar()->addMenu(tr("&Tools"));
    tool_menu->addAction(showlog_act);
    tool_menu->addSeparator();
    tool_menu->addAction(preference_act);

    menuBar()->addSeparator();
    help_menu = menuBar()->addMenu(tr("&Help"));
    help_menu->addAction(about_act);
    help_menu->addAction(aboutQt_act);
}

void MainWindow::createToolBars()
{
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::readSettings()
{
    QSettings settings;
    QString section = QString("mainwindow");

    if (settings.value(section+"/maximized", false).toBool()) {
        if (MaksiApp::app()->saveWindowSize) {
            setWindowState(Qt::WindowMaximized);
        }
    } else {
        if (MaksiApp::app()->saveWindowSize) {
            QSize size = settings.value(section+"/size", QSize(800, 600)).toSize();
            resize(size);
        }
        if (MaksiApp::app()->saveWindowPos) {
            QPoint pos  = settings.value(section+"/pos").toPoint();
            move(pos);
        }
    }
}

void MainWindow::writeSettings()
{
    QSettings settings;
    QString section = QString("mainwindow");

    if (MaksiApp::app()->saveWindowPos) {
        settings.setValue(section+"/pos",  pos());
    }
    if (MaksiApp::app()->saveWindowSize) {
        settings.setValue(section+"/maximized", isMaximized());
        settings.setValue(section+"/size", size());
    }
}

void MainWindow::about()
{
    MaksiApp::about(this);
}

void MainWindow::preferences()
{
    PrefDialog dlg(this);
    dlg.exec();

    if (dlg.result() == QDialog::Accepted) {
        //m_scene->clearShapes();
    }
}

void MainWindow::toggleLayerVisible()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action) {
        maket::LayerElement *layer = action->data().value<maket::LayerElement*>();
        if (action->isChecked()) {
            layer->setVisible(true);
        } else {
            layer->setVisible(false);
        }

        action->setChecked(layer->isVisible());
    }
}

void MainWindow::open()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Load Graphics"), QString(), tr("XML-Graphics (*.xml)"));
    if (m_scene) {
        m_scene->loadGraphics(filename);
    }
}

void MainWindow::clearScene()
{
    if (m_scene) {
        m_scene->clearGraphics();
    }
}

void MainWindow::appendShape()
{
#if 0
    QAction *action = qobject_cast<QAction *>(sender());
    if (action) {
        QString shape_id = action->data().toString();

        //maket::Graphics *pg = m_scene->addGraphics(shape_id);
        //m_scene->appendShape(shape_id);
    } else {
        MaksiApp::log()->warn(QString("append shape: action == 0"));
    }
#endif
}

void MainWindow::zoomin()
{
    if (m_view)
        m_view->scale(1.2);
}

void MainWindow::zoomout()
{
    if (m_view)
        m_view->scale(1.0/1.2);
}

void MainWindow::selectMode()
{
    if (m_view) {
        QAction *action = qobject_cast<QAction *>(sender());
        if (action) {
            if (action->isChecked()) {
                //m_view->setSelectMode(true);
            } else {
                //m_view->setSelectMode(false);
            }
        }
    }
}



/* End of code */
