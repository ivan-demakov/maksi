TARGET = mapet
TEMPLATE = app

QT += xml

INCLUDEPATH += ..

SOURCES += main.cpp maksiapp.cpp mainwindow.cpp logdialog.cpp prefdialog.cpp

HEADERS  += maksiapp.h mainwindow.h logdialog.h prefdialog.h

win32 {
  CONFIG += release
}

unix {
  CONFIG += debug
#  QT += opengl
}

win32 {
    RC_FILE = mapet.rc
    release {
        LIBS += ..\maksi\release\maksi1.lib
        LIBS += ..\maksi\release\maket1.lib
    } else {
        LIBS += ..\maksi\debug\maksi1.lib
        LIBS += ..\maksi\debug\maket1.lib
    }
}

unix {
    LIBS += -L../maksi -lmaksi -lmaket
}

RESOURCES += mapet.qrc
