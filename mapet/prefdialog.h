/* -*-mode:C++-*- */
/*
 * prefdailog.h
 *
 * Copyright (C) 2007, 2009, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sat Apr  7 01:07:59 2007
 * Last Update:Mon Oct 29 07:05:52 2007
 *
 * $Id: prefdlg.h,v 1.15 2009/01/07 10:00:43 ksion Exp $
 *
 */

#ifndef PREFDIALOG_H
#define PREFDIALOG_H

#include <QDialog>


class QLabel;
class QLineEdit;
class QTextEdit;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QListView;
class QTableView;
class QModelIndex;

class GeneralPrefTab;

class Database;
class DatabasesModel;
class DatabasePrefTab;

class HandHistory;
class HandHistoriesModel;
class HandHistoryPrefTab;


class PrefDialog : public QDialog
{
    Q_OBJECT
public:
    PrefDialog(QWidget *parent = 0);
    ~PrefDialog();

    void accept();

private:

    GeneralPrefTab     *generalPrefTab;
};

class GeneralPrefTab : public QWidget
{
    Q_OBJECT
public:
    GeneralPrefTab(PrefDialog *dlg, QWidget *parent = 0);
    ~GeneralPrefTab();

private:
    PrefDialog *prefDlg;
    QCheckBox *saveWindowSizeBox;
    QCheckBox *saveWindowPosBox;
    QLineEdit *configFilename;

private slots:
    friend class PrefDialog;
};

#endif

/* End of file */
