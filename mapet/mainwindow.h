/* -*-mode:C++-*- */
/*
 * mainwindow.h
 *
 * Copyright (C) 2009, 2010, 2011, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Sep 27 15:48:23 2009
 *
 * $Id$
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QAction>
#include <QMenu>

#include <maket/maket.h>
#include <maket/layer.h>


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

private:
    void createActions();
    bool createLayerMenu(QList<maket::LayerElement*> ls);
    void createMenus();
    void createToolBars();
    void createStatusBar();

    void readSettings();
    void writeSettings();

    QAction *open_act;
    QAction *close_act;
    QAction *exit_act;
    QAction *preference_act;
    QAction *about_act;
    QAction *aboutQt_act;
    QAction *showlog_act;
    QAction *clear_act;
    QAction *zoomin_act;
    QAction *zoomout_act;
    QAction *selectMode_act;

    QMenu *file_menu;
    QMenu *action_menu;
    QMenu *layer_menu;
    QMenu *tool_menu;
    QMenu *help_menu;
    QMenu *shape_menu;

    QSharedPointer<maket::View> m_view;
    QSharedPointer<maket::Scene> m_scene;

protected:
    void closeEvent(QCloseEvent *event);

public slots:
    void open();
    void clearScene();

    void about();
    void preferences();
    void toggleLayerVisible();
    void appendShape();
    void zoomin();
    void zoomout();
    void selectMode();
};



#endif

/* End of file */
