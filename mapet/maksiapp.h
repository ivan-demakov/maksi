/* -*-mode:C++-*- */
/*
 * maksiapp.h
 *
 * Copyright (C) 2009, 2010, 2011, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Sep 27 15:22:18 2009
 *
 */

#ifndef MAKSIAPP_H
#define MAKSIAPP_H

#include <QApplication>
#include <QMessageBox>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QHash>


#include <maksi/maksi.h>
#include <maket/maket.h>


class LogDialog;
class QSettings;
class QFileInfo;


class MaksiApp : public QApplication
{
    Q_OBJECT
public:
    MaksiApp(int &argc, char **argv);
    ~MaksiApp();

    static MaksiApp *app() { return theApp; };
    static LogDialog *log() { return theLog; };

private:
    static MaksiApp *theApp;
    static LogDialog *theLog;

public:
    bool saveWindowSize;
    bool saveWindowPos;
    bool expertOptions;
    QString configFilename;
    QString imageDir;

public:
    maket::Maket *maket() { return (m_maket ? m_maket : initMaket()); };

public slots:
    void error(const QString &msg); /**< emited if error */
    void warn(const QString &msg);  /**< emited if warn */
    void info(const QString &msg);  /**< emited if info */
    void debug(const QString &msg); /**< emited if debug */

private:
    maket::Maket *initMaket();
    maket::Maket *m_maket;

public:
    static void about(QWidget *parent);

    static QMessageBox::StandardButton crit(QWidget *parent, QString text, QMessageBox::StandardButtons buttons = QMessageBox::Ok);
    static QMessageBox::StandardButton warn(QWidget *parent, QString text, QMessageBox::StandardButtons buttons = QMessageBox::Ok);
    static QMessageBox::StandardButton info(QWidget *parent, QString text, QMessageBox::StandardButtons buttons = QMessageBox::Ok);
    static QMessageBox::StandardButton quest(QWidget *parent, QString text, QMessageBox::StandardButtons buttons = QMessageBox::Ok);
};


#endif

/* End of file */
