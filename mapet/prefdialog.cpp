/*
 * prefdialog.cpp
 *
 * Copyright (C) 2009, 2010, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sat Apr  7 01:13:22 2007
 *
 */

#include <QtGui>

#include "maksiapp.h"
#include "prefdialog.h"


PrefDialog::PrefDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Preferences: ") + MaksiApp::applicationName());

    QVBoxLayout *mainLayout = new QVBoxLayout;
    setLayout(mainLayout);

    QTabWidget *tabWidget = new QTabWidget;
    mainLayout->addWidget(tabWidget);

    generalPrefTab = new GeneralPrefTab(this);
    tabWidget->addTab(generalPrefTab, tr("General"));

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    mainLayout->addWidget(buttonBox);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

PrefDialog::~PrefDialog()
{
}

void PrefDialog::accept()
{
    QDialog::accept();

    MaksiApp::app()->saveWindowSize = generalPrefTab->saveWindowSizeBox->isChecked();
    MaksiApp::app()->saveWindowPos  = generalPrefTab->saveWindowPosBox->isChecked();
    MaksiApp::app()->configFilename  = generalPrefTab->configFilename->text().trimmed();
}


GeneralPrefTab::GeneralPrefTab(PrefDialog *dlg, QWidget *parent)
    : QWidget(parent),
      prefDlg(dlg)
{
    QVBoxLayout *tabLayout = new QVBoxLayout;
    setLayout(tabLayout);

    saveWindowSizeBox = new QCheckBox(tr("Save window size"));
    saveWindowSizeBox->setCheckState(MaksiApp::app()->saveWindowSize ? Qt::Checked : Qt::Unchecked);
    tabLayout->addWidget(saveWindowSizeBox);

    saveWindowPosBox  = new QCheckBox(tr("Save window position"));
    saveWindowPosBox->setCheckState(MaksiApp::app()->saveWindowPos ? Qt::Checked : Qt::Unchecked);
    tabLayout->addWidget(saveWindowPosBox);

    configFilename = new QLineEdit;
    configFilename->setText(MaksiApp::app()->configFilename);
    tabLayout->addWidget(configFilename);
}

GeneralPrefTab::~GeneralPrefTab()
{
}


/* End of code */
