/*
 * maksiapp.cpp
 *
 * Copyright (C) 2009, 2010, 2011, ivan demakov.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 * Author:        ivan demakov <ksion@users.sourceforge.net>
 * Creation date: Sun Sep 27 15:26:11 2009
 *
 */

#include <QIcon>
#include <QBitmap>
#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QDomDocument>


#include "maksiapp.h"
#include "logdialog.h"


MaksiApp *MaksiApp::theApp;
LogDialog *MaksiApp::theLog;


MaksiApp::MaksiApp(int &argc, char **argv)
    : QApplication(argc, argv),
      m_maket(0)
{
    theApp = this;

    setOrganizationName("Technograd");
    setOrganizationDomain("maksi.tgrad.ru");
    setApplicationName("Mapet");

//    QIcon icon("images/32x32/ksi.png");
    QIcon icon(":/images/ksi.png");
    setWindowIcon(icon);

    QSettings settings;
    saveWindowSize = settings.value("general/saveWindowSize", true).toBool();
    saveWindowPos = settings.value("general/saveWindowPos", false).toBool();
    expertOptions = settings.value("general/expertOptions", false).toBool();

    int verbosity = settings.value("general/verbosity", LogDialog::INFO).toInt();

    theLog = new LogDialog("Log: MaPate");
    theLog->setModal(false);
    theLog->setVerbosity(verbosity);
    if (saveWindowSize) {
        QSize size = settings.value("logwindow/size").toSize();
        theLog->resize(size);
    }
    if (saveWindowPos) {
        QPoint pos  = settings.value("logwindow/pos").toPoint();
        theLog->move(pos);
    }

    configFilename = settings.value("data/config", "../maksi.ini").toString();
}

MaksiApp::~MaksiApp()
{
    QSettings settings;
    settings.setValue("general/saveWindowSize",  saveWindowSize);
    settings.setValue("general/saveWindowPos",   saveWindowPos);
    settings.setValue("general/expertOptions",   expertOptions);
    settings.setValue("general/verbosity",       theLog->getVerbosity());
    if (saveWindowPos) {
        settings.setValue("logwindow/pos",  theLog->pos());
    }
    if (saveWindowSize) {
        settings.setValue("logwindow/size", theLog->size());
    }

    settings.setValue("data/config", configFilename);

    if (m_maket)
        maket::Maket::destroy(m_maket);
}


maket::Maket *MaksiApp::initMaket()
{
    if (!m_maket) {
        QFileInfo configInfo(configFilename);
        if (!configInfo.exists()) {
            warn(0, QString("'%1' is not exists").arg(configInfo.absoluteFilePath()));
            return 0;
        }
        if (!configInfo.isFile() || !configInfo.isReadable()) {
            warn(0, QString("'%1' is not readable or not a regular file").arg(configInfo.absoluteFilePath()));
            return 0;
        }

        QString maketName(configInfo.fileName());
        if (maketName.endsWith(".ini")) {
            maketName.chop(4);
        }
        m_maket = maket::Maket::instance(maketName);
        if (m_maket) {
            connect(m_maket, SIGNAL(error(const QString &)), SLOT(error(const QString &)));
            connect(m_maket, SIGNAL(warn(const QString &)), SLOT(warn(const QString &)));
            connect(m_maket, SIGNAL(info(const QString &)), SLOT(info(const QString &)));
            connect(m_maket, SIGNAL(debug(const QString &)), SLOT(debug(const QString &)));

            QSettings config(configInfo.filePath(), QSettings::IniFormat);

            {
                QString fileName = config.value("maksi/configTypes").toString();
                if (fileName.isEmpty()) {
                    warn(0, QString("[maksi] section do not contain 'configTypes' parameter in file '%1'").arg(configFilename));
                    return 0;
                }
                QFileInfo fileInfo(fileName);
                if (fileInfo.isRelative()) {
                    fileName = configInfo.absolutePath() + "/" + fileName;
                }
                m_maket->loadTypes(fileName);
            }
            {
                QString fileName = config.value("maksi/configScripts").toString();
                if (fileName.isEmpty()) {
                    warn(0, QString("[maksi] section do not contain 'configScripts' parameter in file '%1'").arg(configFilename));
                    return 0;
                }
                QFileInfo fileInfo(fileName);
                if (fileInfo.isRelative()) {
                    fileName = configInfo.absolutePath() + "/" + fileName;
                }
                m_maket->loadConfig(fileName);
            }
            {
                QString fileName = config.value("maksi/configStyles").toString();
                if (fileName.isEmpty()) {
                    warn(0, QString("[maksi] section do not contain 'configStyles' parameter in file '%1'").arg(configFilename));
                    return 0;
                }
                QFileInfo fileInfo(fileName);
                if (fileInfo.isRelative()) {
                    fileName = configInfo.absolutePath() + "/" + fileName;
                }
                m_maket->loadConfig(fileName);
            }
            {
                QString fileName = config.value("maksi/configScenes").toString();
                if (fileName.isEmpty()) {
                    warn(0, QString("[maksi] section do not contain 'configScenes' parameter in file '%1'").arg(configFilename));
                    return 0;
                }
                QFileInfo fileInfo(fileName);
                if (fileInfo.isRelative()) {
                    fileName = configInfo.absolutePath() + "/" + fileName;
                }
                m_maket->loadConfig(fileName);
            }
            {
                QString fileName = config.value("maksi/configViews").toString();
                if (fileName.isEmpty()) {
                    warn(0, QString("[maksi] section do not contain 'configViews' parameter in file '%1'").arg(configFilename));
                    return 0;
                }
                QFileInfo fileInfo(fileName);
                if (fileInfo.isRelative()) {
                    fileName = configInfo.absolutePath() + "/" + fileName;
                }
                m_maket->loadConfig(fileName);
            }
        }
    }
    return m_maket;
}


void MaksiApp::error(const QString &msg)
{
    if (log()) {
        log()->error(msg);
    } else {
        app()->crit(0, msg);
    }
}

void MaksiApp::warn(const QString &msg)
{
    if (log()) {
        log()->warn(msg);
    } else {
        app()->warn(0, msg);
    }
}

void MaksiApp::info(const QString &msg)
{
    if (log()) {
        log()->info(msg);
    } else {
        app()->info(0, msg);
    }
}

void MaksiApp::debug(const QString &msg)
{
    if (log()) {
        log()->debug(msg);
    }
}


void MaksiApp::about(QWidget *parent)
{
    QMessageBox::about(parent,
                       tr("About ") + applicationName(),

                       QString("<b>") + applicationName() + QString("</b><br>") +
                       QString("<table>") +

                       QString("<tr><td>") +
                       tr("Authors:") + QString("</td><td>") + QString("Ivan Demakov (ksion@users.sourceforge.net)") +
                       QString("</td></tr>") +

                       QString("<tr><td>") +
                       tr("License:") + QString("</td><td>") + tr("GNU General Public License") +
                       QString("</td></tr>") +

                       QString("</table>")
        );
}

QMessageBox::StandardButton MaksiApp::crit(QWidget *parent, QString text, QMessageBox::StandardButtons buttons)
{
    return QMessageBox::critical(parent, tr("Error"), text, buttons);
}

QMessageBox::StandardButton MaksiApp::info(QWidget *parent, QString text, QMessageBox::StandardButtons buttons)
{
    return QMessageBox::information(parent, tr("Information"), text, buttons);
}

QMessageBox::StandardButton MaksiApp::quest(QWidget *parent, QString text, QMessageBox::StandardButtons buttons)
{
    return QMessageBox::question(parent, tr("Question"), text, buttons);
}

QMessageBox::StandardButton MaksiApp::warn(QWidget *parent, QString text, QMessageBox::StandardButtons buttons)
{
    return QMessageBox::warning(parent, tr("Warning"), text, buttons);
}


 /* End of code */
