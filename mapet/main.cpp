#include <QtGui/QApplication>
#include "mainwindow.h"
#include "maksiapp.h"

int main(int argc, char *argv[])
{
    MaksiApp app(argc, argv);

    MainWindow w;
    w.show();
    return app.exec();
}
