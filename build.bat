echo off
echo Setting up environment for Qt usage...
set QTDIR=\\VBOXSVR\win32\qt\Desktop\Qt\4.8.1\mingw
set MINGW=\\VBOXSVR\win32\qt\mingw

set PATH=%QTDIR%\bin;%PATH%
set PATH=%MINGW%\bin;%PATH%

cd maksi
qmake
mingw32-make clean
mingw32-make
cd ..

::cd maksi_v8
::qmake
::mingw32-make clean
::mingw32-make
::cd ..

cd maksi_js
qmake
mingw32-make
cd ..

cd maksi_sql
qmake
mingw32-make
cd ..

cd maksi_soap
qmake
mingw32-make
cd ..
