/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    xmlelem.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 13 18:27:27 2011
 *
 * @brief   xml element
 *
 */

#include "xmlelem.h"

#include <QUrl>
#include <QSharedPointer>


//! maksi namespace
namespace maksi
{

XmlElement::TypeFactory::TypeFactory() :
    StructTypeFactory(Type::anyType())
{
}

MaksiElementPtr XmlElement::TypeFactory::createNew(Type type, const MaksiElement *from) const
{
    name_t xmlName = 0;
    const Source *src = 0;
    if (from) {
        xmlName = from->elementName();
        src = from->elementSource();
    }
    if (!src) {
        static Source no;
        src = &no;
    }
    return MaksiElementPtr(new XmlElement(xmlName, src->namespaces(), src->url(), src->line(), type));
}

Type XmlElement::schemaType()
{
    static Type type;
    if (!type.isValid()) {
        static QSharedPointer<TypeFactory> factory(new TypeFactory());
        type.setFactory(factory);
    }
    return type;
}

XmlElement::XmlElement(name_t name, QMap<QString, QString> ns, const QUrl &srcUrl, int srcLine, Type type) :
    MaksiElement_Complex(name, type),
    m_src(ns, srcUrl, srcLine)
{
}

XmlElement::~XmlElement()
{
}

const Source *XmlElement::elementSource() const
{
    return &m_src;
}


XmlString::TypeFactory::TypeFactory()
{
}

MaksiElementPtr XmlString::TypeFactory::createNew(Type /*type*/, const MaksiElement *from) const
{
    QString data;
    const Source *src = 0;
    if (from) {
        data = from->elementText();
        src = from->elementSource();
    }
    if (!src) {
        static Source no;
        src = &no;
    }
    return MaksiElementPtr(new XmlString(data, src->namespaces(), src->url(), src->line()));
}

Type XmlString::schemaType()
{
    static Type type;
    if (!type.isValid()) {
        static QSharedPointer<TypeFactory> factory(new TypeFactory());
        type.setFactory(factory);
    }
    return type;
}

XmlString::XmlString(const QString &str, QMap<QString, QString> ns, const QUrl &srcUrl, int srcLine) :
    MaksiElement_String(0, schemaType(), str),
    m_src(ns, srcUrl, srcLine)
{
}

XmlString::~XmlString()
{
}

const Source *XmlString::elementSource() const
{
    return &m_src;
}

};

 /* End of code */
