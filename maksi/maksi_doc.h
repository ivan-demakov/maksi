/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_doc.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Tue Feb  1 08:52:57 2011
 *
 * @brief   qt interface to maksi
 *
 * @mainpage MaKsi
 *
 * Библиотека Maksi предназаначена для построения объектов, имеющих иерархическую структуру.
 * Объект (\c shape) в Maksi состоит из набора(множества?) свойств и вложенных объектов.
 * Значения свойств объекта определяются в конфиге с помощью скриптового языка.
 *
 * Объекты и скрипты описываюся в конфигурационном файле в формате XML.
 * Maksi обеспечивает взаимодействие между программой на \c C++, и скриптами.
 *
 * Библиотека MaKsi написана с использыванием библиотеки Qt (http://qt.nokia.com.)
 *
 * @section init Инициализация
 *
 * В программе может быть создано несколько экземпляров класса \c maksi::Maksi.
 * Каждый экземпляр \c maksi::Maksi должен иметь уникальное имя.
 * Список всех экземпляров хранится во внутрених структурах библиотеки.
 *
 * Функция maksi::Maksi::instance() возвращает и, при отсутствии, создает экземпляр класса
 * для последующего использования.
 * Для загрузки файла конфигурации используется функция maksi::Maksi::readConfig().
 *
 *
 * @section script Скрипты
 *
 * Скрипты определяются в файле конфигурации, с помощью элемента \c script.
 *
 * Скрипт имеет атрибут \c id, по которому на него можно ссылаться,
 * а так же атрибут \c engine, который указывает интерпретатор для исполнения скрипта.
 *
 * В настоящий момент реализованы следующие интерпретаторы (атрибут \c engine):
 * \li \c javascript javascript
 * \li \c ksi ksi scheme
 *
 * Скрипты загружаются и исполняются при вызове функции maksi::Maksi::readConfig().
 * Каждый скрипт загружается в отдельное окружение.
 * Окружение представляет собой набор переменных и функций, в котором можно выполнить выражение на скриптовом языке.
 *
 * В окружении скрипта (перед исполнением) определяются следующие функции:
 * \li error вывести сообщение о ошибке
 * \li warn вывести предупреждение
 * \li info вывести информационное сообщение
 * \li debug вывести отладочное сообщение
 *
 * Эти функции можно испоьзовать в скриптах для вывода сообщений пользователю.
 *
 * Скрипт (\c script) может содержать следующие дочерние элементы (эти элементы выполняются последовательно, в порядке перечисления в конфиге):
 * \li \c load загрузить файл
 * \li \c code выполнить код
 * \li \c var определить переменную
 * \li \c proc определить процедуру
 *
 * @subsection script_load Загрузка файла
 * Элемент \c load загружает файл, указанный в атрибуте \c file.
 *
 * @subsubsection script_load_1 Пример загрузки файла
 * \code
 *   <script id="js" engine="javascript">
 *     <load file="file.js" />
 *   </script>
 * \endcode
 *
 * Загрузить файл file.js в скрипт js.
 *
 *
 * @subsection script_code Загрузка кода
 * Элемент \c code загружает(выполняет?) код, указанный непосредственно в элементе.
 *
 * @subsubsection script_code_1 Пример загрузки кода
 * \code
 *   <script id="js" engine="javascript">
 *     <code><![CDATA[
 *       debug("code evaluated.");
 *     ]]></code>
 *   </script>
 * \endcode
 *
 * Выполнить код при загрузке скрипта js.
 *
 *
 * @subsection script_var Определение переменной
 *
 * Определяет в скрипте переменную, на которую, в дальнейшем, можно ссылаться как из скрипта,
 * так и из программы. \sa maksi::Maksi::getVar(), maksi::Maksi::setVar().
 *
 * @subsubsection script_var_1 Пример определения переменной
 * \code
 *   <script id="js" engine="javascript">
 *     <var id="a" value="0" />
 *   </script>
 * \endcode
 *
 * Определить в скрипте js переменную a и записать в нее значение 0.
 *
 *
 * @subsection script_proc Определение процедуры
 *
 * Определяет в скрипте процедуру, которую, в дальнейшем, можно вызывать как из скрипта,
 * так и из программы. \sa maksi::Maksi::callProc().
 *
 * Процедура должна иметь атрибут \c id, который задает имя процедуры в скрипте и по которому ее можно вызвать из программы.
 * А также следующие  дочерние элементы:
 * \li \c arg аргумент
 * \li \c code код
 * \li \c call вызов функции или процедуры
 *
 * Элементы \c arg определяют аргументы процедуры. Каждый аргумент должен иметь атрибут \c id.
 * В вызове процедуры (из скрипта или программы) должно быть передано нужное количество аргументов.
 * (В случае несоответствия количества аргументов в вызове и в определении процедуры, поведение неопределено.)
 * При этом создается локальное окружение (context), в котором создаются локальные переменные для каждого аргумента,
 * с именем \c id аргумента и значением, переданным в вызове.
 *
 * Далее выполняются (последовательно) элементы \c code и \c call.
 * Элемент \c code содержит выполняемый текст.
 * Элемент \c call описывает вызов процеды (возможно, определенной в другом скрипте).
 *
 *
 * @subsubsection script_proc_1 Пример определения процедуры
 * \code
 *   <script id="js" engine="javascript">
 *     <proc id="proc" >
 *       <arg id="a" />
 *
 *       <code><![CDATA[
 *         debug("a="+a);
 *       ]]></code>
 *     </proc>
 *   </script>
 * \endcode
 *
 * Определить процедуру \c proc с аргументом \c a.
 *
 *
 * @subsubsection script_proc_2 Вызов процедуры из процедуры
 * \code
 *   <script id="js" engine="javascript">
 *     <proc id="call_other_script" >
 *       <arg id="a" />
 *
 *        <call function="func" script="other_srcipt" >
 *          <param value="a" />
 *          <param value="0" />
 *       </call>
 *     </proc>
 *   </script>
 * \endcode
 *
 * Определить процедуру \c call_other_script с аргументом \c a.
 * Тело процедуры состоит из вызова функции \c func (определенной в скрипте \c other_script) с параметрами a, 0.
 *
 *
 * @section scheme Структура объектов
 *
 * Элемент \c scheme конфигурационного файла описывает структуру объектов.
 *
 * Элемент \c scheme имеет следующие атрибуты:
 * \li \c id идентификатор, по которому на \c scheme можно ссылаться
 * \li \c script идентификатор скрипта, с помощью которого выполняются вычисления,
 * необходимые для получения значений свойств.
 *
 * Элемент \c scheme содержит набор дочерних элементов \c object,
 * каждый из которых описывает структуру объекта.
 *
 * @subsection object Структура объекта
 *
 * Элемент \c object имеет следующие атрибуты:
 * \li \c id идентификатор объекта
 *
 * Также \c object содержит набор свойств и дочерних объектов.
 *
 * @subsection property Свойство объекта
 *
 * Элемент \c property имеет следующие атрибуты:
 * \li \c id идентификатор свойства
 * \li \c type тип свойства
 * \li \c value значение свойства
 *
 * Атрибут \c type задает тип свойства и может быть одним из:
 * \li \c boolean свойство содержит булево значение (false or true).
 * \li \c int свойство содержит числовое значение (int in C, usually 32-bit).
 * \li \c long свойство содержит числовое значение (long long in C, usually 64-bit).
 * \li \c number свойство содержит числовое значение (double in C).
 * \li \c string свойство содержит строковое значение.
 * \li \c value свойство содержит значение (тип значения неопределен).
 * \li \c list свойство содержит список значений.
 *
 * Атрибут \c value должен содержать выражение, которое задает начальное значение свойства.
 * Если \c value отсутствует, то начальное значение неопределено.
 *
 *
 * @section shape объект
 *
 * Элемент \c shape описывает шаблон объекта и имеет следующие атрибуты:
 * \li \c id идентификатор, по которому на \c shape можно ссылаться
 * \li \c script идентификатор скрипта, с помощью которого выполняются вычисления, необходимые для получения значений свойств.
 * \li \c scheme идентификатор \c scheme, с помощью которого описана структура \c shape.
 *
 * Объект состоит из набора свойств и дочерних объектов.
 *
 * Следующие свойства являются встроенными и имеются у любого объекта:
 * \li \c id идентификатор объекта, по которому на объект можно ссылатся в скрипте.
 * \li \c tag тип (вид? класс?) объекта (должен быть равен \c id одного из \c object в \c scheme)
 * \li \c valid имеет булево значение \c true, если все свойста объекта и его дочерних объектов определены
 * \li \c length количество дочерних объектов
 *
 * Эти свойства можно только читать и нельзя менять.
 *
 * Кроме того, объект может иметь дополнительные свойства, описанные с помощью элемента \c property.
 *
 * В элементе \c scheme задается набор обязательных свойств и их начальные значения, для каждого типа (\c tag) объектов.
 * Дополнительные свойства, также, могут быть описаны с помощью элемента \c property непосредственно в \c shape.
 *
 * @subsection property Свойства
 *
 * Элемент \c property описывет свойство объекта.
 * Свойство объекта имеет уникальный (внутри объекта) идентификатор, а также,
 * может иметь значение, либо быть неопределенным.
 *
 *
 * Элемент \c property имеет следующие атрибуты:
 * \li \c id идентификатор свойства
 * \li \c value значение свойства.
 * \li \c type тип свойства
 *
 * Каждое свойство объекта должно иметь атрибут \c id, содержащий уникальный (внутри объекта) идентификатор.
 * \note Чтобы избежать проблем, связанным со скриптовым языком, идентификаторы должны удовлетворять
 * правилам для идентификаторов в этом языке.
 *
 * Атрибут \c value должен содержать выражение, которое используется в зависимости от типа свойства.
 *
 * Атрибут \c type задает тип свойства и может быть одним из:
 * \li \c value свойство содержит значение.
 *   Если атрибут \c value задан, выражение вычислется один раз, при создании объекта,
 *   для задания начального значения свойства.
 *   Если атрибут \c value не задан в описании \c shape, но, при этом, это свойство описано в \c scheme,
 *   то начачалное значение береться из описания \c scheme.
 *   В противном случае, значение считается неопределенным,
 *   до тех пор пока значение свойству не будет присвоено явно (в скрипте или программе).
 * \li \c const свойство содержит константу.
 *   Атрибут \c value должен присутствовать и содержать значение свойства.
 *   Попытки изменить значение свойства этого типа игнорируются.
 *   В случае если это свойство описано, также, в \c scheme, то начачалное значение в описании \c shape
 *   можно не указывать.
 * \li \c eval значение свойства вычисляется каждый раз при его получении.
 *   Атрибут \c value должен присутствовать и должен содержать выражение для вычисления значения.
 *   Попытки изменить значение свойства этого типа игнорируются.
 * \li \c list свойство содержит список значений.
 *   Атрибут \c value игнорируется.
 *   Само свойство менять нельзя, но можно менять значения элементов списка, а также,
 *   добавлять в список новые элементы, и удалять ненужные.
 *
 * Если \c type не указан в описании свойства, то, по умолчанию, используется тип \c value.
 *
 * Элемент описания \c property может содержать следующие вложенные элементы:
 * \li \c valid
 * \li \c action
 *
 * Эти элементы используются для задания дополнительных характеристик свойств.
 *
 * @subsubsection valid Элемент valid
 *
 * Элемент \c valid задает дополнительное условие для свойства и имеет следующие атрибуты:
 * \li \c if выражение с условием.
 *
 * Свойство типа \c value считается неопределенным, до тех пор пока этому свойству не будет присвоено значение,
 * при этом, если при описании свойства задано начальное значение, это значение не вычисляется до тех пор,
 * пока не будут выполнены все условия (если они, конечно, есть).
 *
 * Значения свойств типа \c eval и \c const считаются неопределенными, если хотя бы одно из условий не выполнено.
 *
 * Значение свойства типа \c list считается определенным, если все дополнительные условия выполнены.
 *
 * \note Следует избегать циклических зависимостей.
 *
 * Выражение заданное в атрибуте \c if должно вернуть значение, указывающее выполнено условие или нет.
 * При этом, если выражение вычисляется с ошибкой, возвращает неопределенное значение,
 * либо возвращает булево значение \c false,
 * считается, что условие не выполнено.
 *
 * \note Неопределенное значение можно получить, если выражение состоит из ссылки на неопределенное свойство,
 * либо выражение вычисляется в некоторое значение, считающееся неопределенным в скриптовом языке.
 * Например, \code undefined \endcode в javascript или \code (if #f #f) \endcode в ksi.
 *
 * @subsubsection action Элемент action
 *
 * Элемент \c action задает дополнительное действие при присваивании значения и имеет следующие атрибуты:
 * \li \c if выражение с условием (может отсутствовать).
 * \li \c do выражение с дополнительным действием.
 *
 * Выражение заданное в атрибуте \c if должно вернуть значение, указывающее выполнено условие или нет.
 * При этом, если выражение вычисляется с ошибкой, возвращает неопределенное значение,
 * либо возвращает булево значение \c false,
 * считается что условие не выполнено.
 *
 * В случае, если условие выполнено или отсутствует,
 * то, после присваивания нового значения,
 * выполняется выражение указанное в атрибуте \c do.
 *
 * \note При присваивании свойств типа \c eval и \c const и \c list,
 * их значения не меняются, однако, действия выполняются (если выполнены условия действия).
 *
 * \note При присваивании значения элементу списка,
 * а также, вставке нового элемента в список или удалении элемента из списка,
 * действия, также, выполняются (если выполнены условия действия).
 *
 * @subsection ScriptObject объект
 *
 * Для работы с объектом \c shape предназначен интерфейс maksi::ScriptObject.
 * Функция maksi::Maksi::createShape() создает объект \c shape и возвращает указатель на него.
 *
 * При этом, для вычисления значений свойств типа \c eval, а также вычисления дополнительных условий
 * и действий, создается новое окружение, в котором окружение скрипта используется в качестве родительского.
 * То есть, на переменные и функции определенные в скрипте можно ссылатся из этого окружения (но не наоборот).
 * На сам объект в окружении можно ссылатся с помощью идентификатора \c this.
 *
 *
 * В программе может существовать одновременно произвольное количество объектов, каждый из которых
 * предоставляет отдельное окружение, со с своим набором переменных.
 * Таким образом, объекты не могут взаимодействовать между собой напрямую,
 * но могут взаимодействовать через окружение скрипта (если у них общий скрипт).
 *
 * Кроме того, в программе можно создавать собственные програмные объекты,
 * с помощью наследования от абстрактного класса maksi::ScriptObject и определения
 * всех виртуальных функций.  Эти объекты можно использовать наравне с созданными с помощью
 * библиотеки объектами \c shape.  То есть передавать в скрипт в качестве аргумента функции,
 * добавлять в качестве дочерних в другие объекты и так далее.
 *
 * @subsection shape_js Shape для скрипта javascript
 *
 * Описание языка можно найти, например,
 * здесь: http://www.ecma-international.org/publications/standards/Ecma-262
 *
 * Для \c shape в javascript создается (встроенный) объект с некотрыми особенностями.
 *
 * Доступ к свойствам \c shape выполняется стандартным для javascript образом.
 *
 * \code
 * // get shape id
 * function getId(shape) {
 *   return shape.id;
 * }
 *
 * // set shape x
 * function setX(shape, x) {
 *   shape.x = x;
 * }
 *
 * // print shape properties
 * function print_properties(shape) {
 *   for(i in shape) {
 *     print("property " + i + "=" + shape[i]);
 *   }
 * }
 * \endcode
 *
 * В прототипе объекта \c shape (см. описание языка javascript) имеются следующие свойства:
 * \li \c canSetValue функция, проверяющая можно ли менять значение свойства объекта
 * \li \c insert функция, добавляющая новый дочерний объект
 *
 * \code
 * // set shape property (if can)
 * function set(shape, id, val) {
 *   if (shape.canSetValue(id)) {
 *     shape[id] = val;
 *   } else {
 *     warn("property " + id + " is read-only");
 *   }
 * }
 * \endcode
 *
 * \note Следует отметить, что в javascript допускается динимическое добавление и удаление свойств объекта.
 * Однако, эти действия не предусмотрены для объектов \c shape.
 * Подобные действия либо игнорируются, либо приводят к странным результатам.
 *
 * Доступ к дочерним объектам может выполняться либо по идентификатору (если он есть у объекта),
 * либо по порядковому номеру.  При этом, дочерние объекты доступны только для чтения и удаления.
 *
 * \code
 * // find child by id
 * function findChild(shape, id) {
 *   for (i = 0; i < shape.length; i++) {
 *     if (shape[i].id == id) {
 *       return shape[i];
 *     }
 *   }
 *   return null;
 * }
 *
 * // delete child
 * function deleteChild(shape, id) {
 *   delete shape[id];
 * }
 * \endcode
 *
 * Добавление новых дочерних объектов выполняется с помощью функции insert:
 * \code
 *   shape.insert(index, { tag: theTag });
 * \endcode
 *
 * первый аргумент указывает позицию для вставки,
 * второй - произвольный объект, имеющий, хотя бы, свойство \c tag.
 * При этом, свойства объекта, переданного во втором аргументе, копируются в создаваемый дочерний объект.
 *
 * @subsection shape_ksi Shape для скрипта ksi
 *
 * Для \c shape в ksi создается замыкание (closure).
 *
 * То есть, доступ к свойствам и дочерним объектам выполняется, синтаксически, в виде вызова функции
 * с одним или более аргументами.
 *
 * Результат вызова различается в зависимости от типа первого аргумента и других аргументов:
 * \li Если первый аргумент является символом,
 *     то возвращается дочерний объект с таким идентификатором.
 *     Второй аргумент (если он есть) игнорируется.
 * \li Если первый аргумент является целым точным числом,
 *     то возвращается дочерний объект, с таким порядковым номером.
 *     Второй аргумент (если он есть) игнорируется.
 * \li Если первый агрумент является ключом (keyword),
 *     и у объекта имеется свойство с таким именем,
 *     и это свойство имеет любой тип, кроме \c list,
 *     то возвращается значение этого свойства
 *     Если, при этом, имеется второй аргумент, то свойству присваивается новое значение.
 * \li Если первый агрумент является ключом (keyword),
 *     и у объекта имеется свойство с таким именем,
 *     и это свойство имеет тип \c list,
 *     то возвращается значение элемента этого списка с номером, заданным во втором аргументе.
 *     Если, при этом, имеется третий аргумент, то элементу списка присваивается новое значение.
 *
 * Кроме того, если первый агрумент является ключом (keyword), но, при этом, в объекте нет указанного свойства,
 * а ключ равен:
 * \li \c properties: возвращается список с именами свойств.
 * \li \c has-property: проверяет имеется ли в объекте свойство с именем, переданным во втором аргументе
 * \li \c can-set-value: проверяет можно ли менять значение свойства с именем, переданным во втором аргументе
 * \li \c delete: удаляет дочерний объект, идентификатор или порядковый номер которого, передан во втором аргументе
 * \li \c insert: добавляет новый дочерний объект порядковый номер которого, передан во втором аргументе, а структура -- в третьем.
 *
 * В случае добавления нового объекта, в последнем аргументе должен быть список следующего вида:
 * \li для каждого свойства нового объекта, в списке должны быть два элемента, первый -- имя свойства, второй -- значение
 * \li обязательно должено быть указано свойство с именем \c tag.
 *
 * \code
 * ; получить значение свойства x
 * (shape x:)
 *
 * ; изменить значение свойства x
 * (shape x: 10)
 *
 * ; получить дочерний элемент с идентификатором K1
 * (shape 'K1)
 *
 * ; получить список свойств
 * (shape properties:)
 *
 * ; проверить, есть ли у объекта свойство x
 * (shape has-property: x:)
 *
 * ; проверить, можно ли менять свойство x
 * (shape can-set-value: x:)
 *
 * ; удалить дочерний элемент K1
 * (shape delete: 'K1)
 *
 * ; добавить прямоугольник в конец списка дочерних объектов
 * (shape insert: (shape length:) '(tag: rect x: 0 y: 0 w: 100 h: 100))
 *
 * \endcode
 *
 */

#ifndef MAKSI_DOC_H
#define MAKSI_DOC_H



#endif

/* End of file */
