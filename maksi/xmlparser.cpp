/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    xmlparser.cpp
 * @author  ivan demakov <ivan@demakov.net>
 * @date    Tue Nov 14 05:18:26 +07 2017
 *
 * @brief   xml parser
 *
 */

#include "xmlparser.h"
//#include "maksi.h"

#include <QXmlStreamReader>
#include <QStack>


namespace maksi
{

XmlParser::XmlParser(Maksi *maksi, const QUrl &url, int line) :
    m_maksi(maksi),
    m_url(url),
    m_startLine(line)
{
}

XmlParser::~XmlParser()
{
}

void XmlParser::sendMessage(message_t type, int lineNumber, const QString &msg)
{
    if (m_url.isValid()) {
        //m_maksi->sendMessage(type, m_url, lineNumber+m_startLine);
    } else {
        //m_maksi->sendMessage(type, msg);
    }
}

bool XmlParser::parse(const QByteArray &data)
{
    QXmlStreamReader reader(data);
    return parse(reader);
}

bool XmlParser::parse(const QString &data)
{
    QXmlStreamReader reader(data);
    return parse(reader);
}

bool XmlParser::parse(QXmlStreamReader &reader)
{
    QSharedPointer<xml::Doc> doc(new xml::Doc(m_url, m_startLine));
    QSharedPointer<xml::NodeItem> node;
    QSharedPointer<xml::NodeItem> parent;
    QStack<QSharedPointer<xml::NodeItem>> stackNode;
    QString chars;
    QString nodeName;

    bool finished = false;
    while (!reader.atEnd()) {
        QXmlStreamReader::TokenType token = reader.readNext();

        switch (token) {
        case QXmlStreamReader::NoToken:
            break;

        case QXmlStreamReader::StartDocument:
	    node = doc;
            break;

        case QXmlStreamReader::EndDocument:
            finished = true;
            goto cleanup;

        case QXmlStreamReader::Invalid:
            sendMessage(MESSAGE_ERROR, reader.lineNumber(), reader.errorString());
            goto cleanup;

        case QXmlStreamReader::Characters:
            chars += reader.text().toString();
            break;

        case QXmlStreamReader::StartElement:
            if (!chars.isEmpty()) {
                if (node) {
		    node->addText(chars, m_url, m_startLine+reader.lineNumber());
                }
                chars.clear();
            }

            nodeName = reader.name().toString();
            if (!nodeName.isEmpty()) {
                parent = node;
                stackNode.push(parent);

                QString nodeNS(reader.namespaceUri().toString());
                QString nodePrefix(reader.prefix().toString());

                name_t xmlName = xml::qname(nodeName, nodeNS, nodePrefix);
                node.reset(new xml::Node(xmlName, m_url, m_startLine+reader.lineNumber()));

		node->m_nspaces = parent->m_nspaces;
                QXmlStreamNamespaceDeclarations decl(reader.namespaceDeclarations());
                for (int i = 0; i < decl.size(); ++i) {
                    QString p(decl.at(i).prefix().toString());
                    QString n(decl.at(i).namespaceUri().toString());
                    //qDebug("add prefix='%s' ns='%s'", qPrintable(p), qPrintable(n));
                    node->m_nspaces.insert(p, n);
                }

                QXmlStreamAttributes as(reader.attributes());
                for (int i = 0; i < as.size(); i++) {
                    const QXmlStreamAttribute &attr = as.at(i);
                    QString attrNS(attr.namespaceUri().toString());
                    QString attrName(attr.name().toString());
                    QString attrPrefix(attr.prefix().toString());
                    QString attrValue(attr.value().toString());
                    if (attrNS.isEmpty()) {
                        attrNS = nodeNS;
                        attrPrefix = nodePrefix;
                    }

                    name_t xname = xml::qname(attrName, attrNS, attrPrefix);
		    node->addAttr(xname, attrValue, m_url, m_startLine+reader.lineNumber());
                } /* for */
            } else {
                sendMessage(MESSAGE_ERROR, reader.lineNumber(), QString::fromLatin1("unnamed xml element"));
                goto cleanup;
            }
            break;

        case QXmlStreamReader::EndElement:
            if (!chars.isEmpty()) {
		node->addText(chars, m_url, m_startLine+reader.lineNumber());
                chars.clear();
            }

	    parent->m_items.append(node);
            node = parent;
            parent = stackNode.pop();
            break;

        case QXmlStreamReader::Comment:
            break;

        case QXmlStreamReader::DTD:
        case QXmlStreamReader::EntityReference:
        case QXmlStreamReader::ProcessingInstruction:
        default:
            //warn(tr("%1:%2: xml element '%3' skipped").arg(m_url.toString()).arg(reader->lineNumber()).arg(reader->name().toString()));
            break;
        } /* switch() */
    } /* while() */

cleanup:
    return finished;
}


};
