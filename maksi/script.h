/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2009, 2010, 2011, 2012, 2013, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    script.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Sat Oct 31 14:53:57 2009
 *
 * @brief   Maksi script interface
 *
 */

#ifndef MAKSI_SCRIPT_H
#define MAKSI_SCRIPT_H

#include "iface.h"
#include "elem.h"

#include <QObject>


class Maksi;


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT Context : public QObject
{
    Q_OBJECT

public:
    Context() : m_permanent(false) {}
    virtual ~Context();

    virtual QVariant arg(const QString &name) = 0;
    virtual void setArg(const QString &name, const QVariant &val) = 0;
    virtual QVariant eval() = 0;

    bool isPermanent() const { return m_permanent; }
    void setPermanent() { m_permanent = true; }

private:
    bool m_permanent;
};


/** Script class
 *
 * \c maksi::Script is abstarct class that should be implemented in engine plugin (see \c maksi::Engine class).
 * The script is created in 2 steps.
 *
 * First step is to call the \c maksi::Script constructor that called from function \a maksi::Engine::newScript().
 * Plugin should create new instanse of the script that can be used in the maksi library.
 *
 * Second step is to call \a maksi::Script::initScript() that parse the configuration of the script.
 * The maksi library calls this function with configuration that is read from xml configuration file.
 *
 * Plugin should implement all pure virtual functions of the \c maksi::Script class.
 *
 */
class MAKSI_EXPORT Script : public QObject
{
    Q_OBJECT

public:
    Script(Maksi *maksi, const QString &name); /**< Constructor */
    virtual ~Script();          /**< Destructor */

    /** Initialize script
     *
     * @param conf the script configuration
     */
    void initScript(const MaksiElement *conf);


    /** Create new script context
     *
     * @return Script context
     */
    virtual Context *newContext() = 0;


    /** Get script name
     *
     * @return script name
     */
    const QString &name() const { return m_name; }


    /** Get script maksi instance
     *
     * @return maksi instance
     */
    Maksi *maksi() const { return m_maksi; }

    bool isPermanent() { return m_permanent; }

protected:
    virtual bool addArg(const QString &name, const QVariant &val, const MaksiElement *conf) = 0;
    virtual void addCode(const QString &code, const MaksiElement *conf) = 0;

private:
    Maksi *m_maksi;
    QString m_name;
    bool m_permanent;

    friend class Conf;
};


#if 0
class ScriptContext : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ScriptContext);
public:

    struct Feature {
        Feature(const QString &name) : m_name(name) {}

        QString m_name;
        QString m_state;
        QVariant m_value;
    };

    struct Version {
        Version(int number) : m_number(number) {}

        result_t addFeature(const QString &name, element_t conf);
        result_t addVar(const QString &name, element_t conf);

        int m_number;
        script_t m_script;
        QList<Feature> m_features;
        QMap<QString, QVariant> m_vars;
    };


    ScriptContext(script_t script);
    virtual ~ScriptContext();

    result_t addFeature(const QString &name, element_t conf);

    //virtual QVariant getVar(const QString &name) const = 0;
    //virtual result_t setVar(const QString &name, const QVariant &val, bool define) = 0;

    script_t script() const { return m_script; }

private:
    script_t m_script;
    QList<Feature> m_features;
    QMap<int, Version> m_versions;
};
#endif

};

#endif

/* End of file */
