/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    type.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Apr 20 15:56:29 2011
 *
 * @brief   XSchema interface
 *
 */

#ifndef MAKSI_TYPE_H
#define MAKSI_TYPE_H

#include "iface.h"
#include "elem.h"

#include <QUrl>
#include <QSharedPointer>


//! maksi namespace
namespace maksi
{

class TypeAttribute;
class TypeElement;
class TypeGroup;
class TypeFactory;
class TypePartition;
class TypePrivate;
class TypeAttributePrivate;
class TypeParticle;
class TypeGroupPrivate;
class TypeElementPrivate;


//! Use mode
enum use_t {
    USE_OPTIONAL,
    USE_REQUIRED,
    USE_PROHIBITED,
    USE_FIXED,
    USE_FIXED_REQUIRED,
    USE_DEFAULT
};


class MAKSI_EXPORT Type
{
public:
    enum ws_facet_t {
        WS_PRESERVE,
        WS_REPLACE,
        WS_COLLAPSE
    };

    Type();
    ~Type();

    Type(const Type &x);
    Type &operator=(const Type &x);
    Type clone();
    void clear();

    bool isValid() const;
    bool isAtomic() const;
    bool isSimple() const;
    bool isStruct() const;
    bool isEmpty() const;
    bool isMixed() const;
    bool isName() const;

    void setFactory(QSharedPointer<TypeFactory> factory);
    QSharedPointer<TypeFactory> factory() const;

    Element::Ptr createNew(const Element *from) const;
    Element::Ptr createNew(Maksi *maksi, const Element *conf, QXmlName elemName, bool hereNode) const;

    static Type anyType();
    static Type stdType(QXmlName name);
    static bool simpleType(Maksi *maksi, const Element *conf, QXmlName targetName, Type *declareType);
    static bool complexType(Maksi *maksi, const Element *conf, QXmlName targetName, Type *declareType, bool define);

    static TypeAttribute attribute(Maksi *maksi, const Element *conf, QXmlName targetName, bool global);
    static TypeElement element(Maksi *maksi, const Element *conf, QXmlName targetName);
    static TypeGroup group(Maksi *maksi, const Element *conf, QXmlName targetName);

    QList<TypeAttribute> attributes() const;
    Type contentType() const;

    bool operator==(const Type &x) const { return d == x.d; }
    bool operator!=(const Type &x) const { return d != x.d; }

private:
    Type(TypePrivate *x);

    TypePrivate *d;

    void setWsFacet(ws_facet_t val);
    void addEnum(ElementPtr val);
    void addAttribute(TypeAttribute attr);

    QSharedPointer<TypeParticle> particle() const;
    void setParticle(QSharedPointer<TypeParticle> p);

    bool createElements(Maksi *maksi, const Element *conf, QSharedPointer<TypePartition> part, Element *element) const;

    static bool restrictionType(Maksi *maksi, const Element *conf, QXmlName targetName, bool simple, Type *declareType);
    static bool listType(Maksi *maksi, const Element *conf, QXmlName targetName, Type *declareType);
    static bool unionType(Maksi *maksi, const Element *conf, QXmlName targetName, Type *declareType);
    static bool structType(Maksi *maksi, const Element *conf, QXmlName targetName, Type baseType, Type *declareType);

    static bool simpleContent(Maksi *maksi, const Element *conf, QXmlName targetName, Type *declareType, bool define);
    static bool complexContent(Maksi *maksi, const Element *conf, QXmlName targetName, Type *declareType, bool define);
    static bool complexRestriction(Maksi *maksi, const Element *conf, QXmlName targetName, Type baseType, bool mixed, Type *declareType, bool define);
    static bool complexExtension(Maksi *maksi, const Element *conf, QXmlName targetName, Type baseType, bool mixed, Type *declareType, bool define);

    static bool setMinMaxOccurs(Maksi *maksi, const Element *conf, TypeParticle *p);

    static QSharedPointer<TypeParticle> modelGroup(Maksi *maksi, const Element *conf, QXmlName targetName);
    static TypeGroup allGroup(Maksi *maksi, const Element *conf, QXmlName targetName);
    static TypeGroup choiceGroup(Maksi *maksi, const Element *conf, QXmlName targetName);
    static TypeGroup sequenceGroup(Maksi *maksi, const Element *conf, QXmlName targetName);
};


class MAKSI_EXPORT TypeAttribute
{
public:
    TypeAttribute();
    TypeAttribute(QXmlName name, Type type);
    ~TypeAttribute();

    TypeAttribute(const TypeAttribute &x);
    TypeAttribute &operator=(const TypeAttribute &x);

    bool isValid() const;

    void setUse(use_t use);
    void setFixed(ElementPtr value);
    void setDefault(ElementPtr value);

    QXmlName name() const;
    Type type() const;
    use_t use() const;
    ElementPtr value() const;

private:
    void detach();

    TypeAttributePrivate *d;
};


class MAKSI_EXPORT TypeGroup
{
public:
    enum compositor_t {
        COMPOSITOR_INVALID,
        COMPOSITOR_ALL,
        COMPOSITOR_CHOICE,
        COMPOSITOR_SEQUENCE
    };

    TypeGroup();
    ~TypeGroup();

    TypeGroup(const TypeGroup &x);
    TypeGroup &operator=(const TypeGroup &x);

    bool isValid() const;
    bool isEmpty() const;

    compositor_t compositor() const;
    void setCompositor(compositor_t val);

    void appendParticle(QSharedPointer<TypeParticle> p);
    void appendChildParticles(QSharedPointer<TypeParticle> p);

private:
    void detach();

    TypeGroupPrivate *d;
    friend class TypeParticle_Group;
};


class MAKSI_EXPORT TypeElement
{
public:
    TypeElement();
    TypeElement(QXmlName name, Type type);
    ~TypeElement();

    TypeElement(const TypeElement &x);
    TypeElement &operator=(const TypeElement &x);

    bool isValid() const;
    QXmlName elementName() const;
    const Type elementType() const;

private:
    void detach();

    TypeElementPrivate *d;
    friend class TypeParticle_Elem;
};


class MAKSI_EXPORT TypeFactory
{
public:
    TypeFactory();
    virtual ~TypeFactory();

    virtual bool isSimple() const;
    virtual bool isAtomic() const;
    virtual bool isStruct() const;
    virtual bool isName() const;

    virtual Type contentType() const;

    virtual ElementPtr createNew(Type type, const Element *from) const;
    virtual QString textContent(const Element *from) const;
};


class MAKSI_EXPORT SimpleTypeFactory : public TypeFactory
{
public:
    virtual bool isSimple() const;
};

class MAKSI_EXPORT AtomicTypeFactory : public SimpleTypeFactory
{
public:
    virtual bool isAtomic() const;
};

class MAKSI_EXPORT StringTypeFactory : public AtomicTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT NormStringTypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT TokenTypeFactory : public NormStringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT NMTokenTypeFactory : public TokenTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT NameTypeFactory : public NMTokenTypeFactory
{
public:
    virtual bool isName() const;
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT NCNameTypeFactory : public NameTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT QNameTypeFactory : public NameTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT HexTypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
    virtual QString textContent(const Element *from) const;
};

class MAKSI_EXPORT Base64TypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
    virtual QString textContent(const Element *from) const;
};

class MAKSI_EXPORT URITypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT DateTimeTypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT DateTypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT TimeTypeFactory : public StringTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT BoolTypeFactory : public TokenTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT NumberTypeFactory : public TokenTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT LongTypeFactory : public NumberTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT ULongTypeFactory : public NumberTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT IntTypeFactory : public LongTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT UIntTypeFactory : public ULongTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT FloatTypeFactory : public NumberTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT DoubleTypeFactory : public NumberTypeFactory
{
public:
    virtual ElementPtr createNew(Type type, const Element *from) const;
};

class MAKSI_EXPORT ListTypeFactory : public SimpleTypeFactory
{
public:
    explicit ListTypeFactory(const Type &itemType) : m_itemType(itemType) {}

    virtual ElementPtr createNew(Type type, const Element *from) const;

    Type m_itemType;
};

class MAKSI_EXPORT UnionTypeFactory : public SimpleTypeFactory
{
public:
    explicit UnionTypeFactory(QList<Type> types) : m_memberTypes(types) {}

    virtual ElementPtr createNew(Type type, const Element *from) const;

    QList<Type> m_memberTypes;
};

class MAKSI_EXPORT StructTypeFactory : public TypeFactory
{
public:
    explicit StructTypeFactory(const Type &baseType) : m_baseType(baseType) {}

    virtual bool isStruct() const;
    virtual Type contentType() const;

    virtual ElementPtr createNew(Type type, const Element *from) const;

    Type m_baseType;
};

};

Q_DECLARE_METATYPE(maksi::Type);


#endif

/* End of file */
