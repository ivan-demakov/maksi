/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    elem.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri May 13 17:12:44 2011
 *
 * @brief   maksi element interface
 *
 */

#ifndef MAKSI_ELEM_H
#define MAKSI_ELEM_H

#include "iface.h"
#include "xml.h"

#include <QMap>
#include <QUrl>
#include <QVariant>
#include <QDateTime>


class QXmlStreamWriter;


//! maksi namespace
namespace maksi
{

class Type;


//! Maski Elem interface
class MAKSI_EXPORT Element : public QObject
{
    Q_OBJECT;

public:
    virtual ~Element();    /**< Destructor */


    typedef QSharedPointer<Element> Ptr;


    /** Get element name.
     *
     * @return element name
     */
    name_t elementName() const;


    /** Set element name.
     *
     * @param name
     */
    void setElementName(name_t name);


    /** Get element type
     *
     * @return element type
     */
    const Type &elementType() const;


    /** Get element source
     *
     * Valid if element was readed from xml file
     *
     * @return element source
     */
    virtual const xml::Src *elementSource() const;


    /** Get element source url
     *
     * @return element url
     */
    QUrl elementSourceUrl() const;


    /** Get element source line
     *
     * @return element url
     */
    int elementSourceLine() const;


    /** Get attribute name
     *
     * Convert string to attribute name.
     *
     * @param name string with attribute name
     *
     * @return attribute name
     */
    name_t attributeName(const QString &name) const;


    /** Get attribute names
     *
     * Default implementation returns list of attributes defined in the type of element.
     *
     * @return list of attribute names
     */
    virtual QList<name_t> attributeNames() const;


    /** Test if the attribute exists
     *
     * @param name attribute name
     *
     * @return is the attribute exists in the element?
     */
    virtual bool hasAttribute(name_t name) const;
    bool hasAttribute(const QString &name) const;


    /** Test if the attribute has fixed value (i.e. read-only)
     *
     * @param name attribute name
     *
     * @return is the attribute read-only?
     */
    virtual bool attributeFixed(name_t name) const;
    bool attributeFixed(const QString &name) const;


    /** Get attribute value
     *
     * @param name attribute name
     *
     * @return attribute value
     */
    virtual Ptr attribute(name_t name) const;
    Ptr attribute(const QString &name) const;


    /** Set attribute value
     *
     * @param name attribute name
     * @param value new value
     */
    virtual bool setAttribute(name_t name, Ptr value);
    bool setAttribute(const QString &name, Ptr value);


    /** Get the number of child elements
     *
     * @return the number of elements
     */
    virtual int elementCount() const;


    /** Get the child element
     *
     * @param index the order number of element
     *
     * @return the element value
     */
    virtual Ptr element(int index) const;


    /** Append new child element
     *
     * @param value value for element initialization
     *
     * @return is append successful?
     */
    virtual bool elementAppend(const Ptr &value);


    /// conversion to/from QVariant
    virtual QVariant elementToVariant() const;
    static Ptr newVariant(const QVariant &value);

    /// conversion to/from byte array
    virtual bool elementToByteArray(QByteArray *val) const;
    static Ptr newByteArray(name_t name, Type type, const QByteArray &val);
    static Ptr newByteArray(const QByteArray &val);

    /// conversion to/from string
    virtual bool elementToString(QString *val) const;
    static Ptr newString(name_t name, Type type, const QString &val);
    static Ptr newString(const QString &val);
    static Ptr newToken(const QString &val);
    static Ptr newName(const QString &val);

    /// conversion to/from QName
    virtual bool elementToQName(name_t *val) const;
    static Ptr newQName(name_t name, Type type, name_t val);
    static Ptr newQName(name_t val);

    /// conversion to/from bool
    virtual bool elementToBool(bool *val) const;
    static Ptr newBool(name_t name, Type type, bool val);
    static Ptr newBool(bool val);

    /// conversion to/from int
    virtual bool elementToInt(int32_t *val) const;
    static Ptr newInt(name_t name, Type type, int32_t val);
    static Ptr newInt(int32_t val);

    /// conversion to/from uint
    virtual bool elementToUInt(uint32_t *val) const;
    static Ptr newUInt(name_t name, Type type, uint32_t val);
    static Ptr newUInt(uint32_t val);

    /// conversion to/from long
    virtual bool elementToLong(int64_t *val) const;
    static Ptr newLong(name_t name, Type type, int64_t val);
    static Ptr newLong(int64_t val);

    /// conversion to/from ulong
    virtual bool elementToULong(uint64_t *val) const;
    static Ptr newULong(name_t name, Type type, uint64_t val);
    static Ptr newULong(uint64_t val);

    /// conversion to/from float
    virtual bool elementToFloat(float *val) const;
    static Ptr newFloat(name_t name, Type type, float val);
    static Ptr newFloat(float val);

    /// conversion to/from double
    virtual bool elementToDouble(double *val) const;
    static Ptr newDouble(name_t name, Type type, double val);
    static Ptr newDouble(double val);

    /// conversion to/from Url
    virtual bool elementToUrl(QUrl *val) const;
    static Ptr newUrl(name_t name, Type type, QUrl val);
    static Ptr newUrl(QUrl val);

    /// conversion to/from DateTime
    virtual bool elementToDate(QDateTime *val) const;
    static Ptr newDate(name_t name, Type type, const QDateTime &val);
    static Ptr newDate(const QDateTime &val);

    /// conversion to/from Date
    virtual bool elementToDate(QDate *val) const;
    static Ptr newDate(name_t name, Type type, const QDate &val);
    static Ptr newDate(const QDate &val);

    /// conversion to/from Time
    virtual bool elementToTime(QTime *val) const;
    static Ptr newTime(name_t name, Type type, const QTime &val);
    static Ptr newTime(const QTime &val);


    /** Get copy of the element
     *
     * @return copy of the element
     */
    virtual Ptr elementClone() const = 0;


    /** Get content of the element
     *
     * @return content of the element
     */
    virtual Ptr elementContent() const;


    /** Get text of the element
     *
     * @return text of the element
     */
    virtual QString elementText() const;


    /** Test if the element is valid.
     *
     * @return is element valid?
     */
    virtual bool isElementValid() const;


    /** Is element simple?
     *
     * @return true if element have not attributes, and does not contains children or all children are simple, false otherwise
     */
    virtual bool isElementSimple() const;


    /** Is empty?
     *
     * @return true if element does not contain children or all children are empty, false otherwise
     */
    virtual bool isElementEmpty() const;


    /** Is element mixed?
     *
     * @return true if element contains children that are simple and that are not simple (i.e. both), false otherwise
     */
    bool isElementMixed() const;


    /** Is element list?
     *
     * @return true if element contains children, false otherwise
     */
    virtual bool isElementList() const;


    /** Compare elements
     *
     * @param elem element to compare with
     *
     * @return true if element is equal to \a elem, false otherwise
     */
    virtual bool isElementEqual(const Ptr elem) const;


    /** Convert element to xml
     *
     * @return
     */
    QString elementXmlText() const;


    /** Write element to xml stream
     *
     */
    void elementWriteXml(QXmlStreamWriter &stream, name_t name, QList<QString> ns=QList<QString>()) const;


    /** Get method names
     *
     * @return list of methods
     */
    virtual QList<QString> elementMethods() const;


    /** Call method
     *
     * @param name method name
     * @param args method arguments
     *
     * @return result
     */
    virtual QVariant callMethod(const QString &name, QList<QVariant> args);

protected:
    Element(name_t name, Type type); /**< Constructor */

    name_t m_name;
    Type m_type;

    friend class Element_List;
};


class Element_Variant : public Element
{
    Q_OBJECT

public:
    Element_Variant(const QVariant &val);
    ~Element_Variant();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

protected:
    Element_Variant(name_t name, Type type, const QVariant &val) : Element(name, type), m_val(val) {}

    QVariant m_val;

    friend class Element;
};

struct Element_ByteArray : public Element
{
    Q_OBJECT

public:
    Element_ByteArray(name_t name, Type type, const QByteArray &val) : Element(name, type), m_val(val) {}
    ~Element_ByteArray();

    virtual QVariant elementToVariant() const;
    virtual bool elementToByteArray(QByteArray *val) const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    QByteArray m_val;
};

class MAKSI_EXPORT  Element_String : public Element
{
    Q_OBJECT

public:
    Element_String(const QString &val);
    virtual ~Element_String();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;

    virtual bool isElementEmpty() const;
    virtual bool isElementEqual(const Ptr val) const;

    virtual Ptr elementClone() const;

protected:
    Element_String(name_t name, Type type, const QString &val) : Element(name, type), m_val(val) {}

    QString m_val;

    friend class Element;
};

struct Element_QName : public Element
{
    Q_OBJECT

public:
    Element_QName(name_t name, Type type, name_t val) : Element(name, type), m_val(val) {}
    ~Element_QName();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToQName(name_t *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    name_t m_val;
};

struct Element_Bool : public Element
{
    Q_OBJECT

public:
    Element_Bool(name_t name, Type type, bool val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToBool(bool *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    bool m_val;
};

struct Element_Int : public Element
{
    Q_OBJECT

public:
    Element_Int(name_t name, Type type, int32_t val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToBool(bool *val) const;
    virtual bool elementToInt(int32_t *val) const;
    virtual bool elementToUInt(uint32_t *val) const;
    virtual bool elementToLong(int64_t *val) const;
    virtual bool elementToULong(uint64_t *val) const;
    virtual bool elementToFloat(float *val) const;
    virtual bool elementToDouble(double *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    int32_t m_val;
};

struct Element_UInt : public Element
{
    Q_OBJECT

public:
    Element_UInt(name_t name, Type type, uint32_t val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToBool(bool *val) const;
    virtual bool elementToInt(int32_t *val) const;
    virtual bool elementToUInt(uint32_t *val) const;
    virtual bool elementToLong(int64_t *val) const;
    virtual bool elementToULong(uint64_t *val) const;
    virtual bool elementToFloat(float *val) const;
    virtual bool elementToDouble(double *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    uint32_t m_val;
};

struct Element_Long : public Element
{
    Q_OBJECT

public:
    Element_Long(name_t name, Type type, int64_t val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToBool(bool *val) const;
    virtual bool elementToInt(int32_t *val) const;
    virtual bool elementToUInt(uint32_t *val) const;
    virtual bool elementToLong(int64_t *val) const;
    virtual bool elementToULong(uint64_t *val) const;
    virtual bool elementToFloat(float *val) const;
    virtual bool elementToDouble(double *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    int64_t m_val;
};

struct Element_ULong : public Element
{
    Q_OBJECT

public:
    Element_ULong(name_t name, Type type, int64_t val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToBool(bool *val) const;
    virtual bool elementToInt(int32_t *val) const;
    virtual bool elementToUInt(uint32_t *val) const;
    virtual bool elementToLong(int64_t *val) const;
    virtual bool elementToULong(uint64_t *val) const;
    virtual bool elementToFloat(float *val) const;
    virtual bool elementToDouble(double *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    uint64_t m_val;
};

struct Element_Float : public Element
{
    Q_OBJECT

public:
    Element_Float(name_t name, Type type, float val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToInt(int32_t *val) const;
    virtual bool elementToUInt(uint32_t *val) const;
    virtual bool elementToLong(int64_t *val) const;
    virtual bool elementToULong(uint64_t *val) const;
    virtual bool elementToFloat(float *val) const;
    virtual bool elementToDouble(double *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    float m_val;
};

struct Element_Double : public Element
{
    Q_OBJECT

public:
    Element_Double(name_t name, Type type, double val) : Element(name, type), m_val(val) {}

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual bool elementToInt(int32_t *val) const;
    virtual bool elementToLong(int64_t *val) const;
    virtual bool elementToFloat(float *val) const;
    virtual bool elementToDouble(double *val) const;
    virtual QString elementText() const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    double m_val;
};

struct Element_Url : public Element
{
    Q_OBJECT

public:
    Element_Url(name_t name, Type type, const QUrl &val) : Element(name, type), m_val(val) {}
    ~Element_Url();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool elementToURI(QUrl *val) const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    QUrl m_val;
};

struct Element_DateTime : public Element
{
    Q_OBJECT

public:
    Element_DateTime(name_t name, Type type, const QDateTime &val) : Element(name, type), m_val(val) {}
    ~Element_DateTime();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool elementToDate(QDateTime *val) const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    QDateTime m_val;
};

struct Element_Date : public Element
{
    Q_OBJECT

public:
    Element_Date(name_t name, Type type, const QDate &val) : Element(name, type), m_val(val) {}
    ~Element_Date();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool elementToDate(QDate *val) const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    QDate m_val;
};

struct Element_Time : public Element
{
    Q_OBJECT

public:
    Element_Time(name_t name, Type type, const QTime &val) : Element(name, type), m_val(val) {}
    ~Element_Time();

    virtual QVariant elementToVariant() const;
    virtual bool elementToString(QString *val) const;
    virtual QString elementText() const;
    virtual bool isElementEmpty() const;
    virtual bool elementToTime(QTime *val) const;
    virtual bool isElementEqual(const Ptr val) const;
    virtual Ptr elementClone() const;

    QTime m_val;
};


class MAKSI_EXPORT Element_List : public Element
{
    Q_OBJECT

public:
    Element_List(); /**< Constructor */

    virtual ~Element_List();    /**< Destructor */

    virtual int elementCount() const;
    virtual Ptr element(int index) const;
    virtual bool elementAppend(const Ptr &value);

    virtual bool elementToString(QString *val) const;

    virtual Ptr elementContent() const;
    virtual QString elementText() const;

    virtual bool isElementSimple() const;
    virtual bool isElementEmpty() const;
    virtual bool isElementList() const;
    virtual bool isElementEqual(const Ptr &elem) const;

    virtual Ptr elementClone() const;

protected:
    Element_List(name_t name, Type type); /**< Constructor */
    Element_List(name_t name, Type type, const QList<Ptr> &elements); /**< Constructor */

    QList<Ptr> m_elements;

    friend class ListTypeFactory;
};


class MAKSI_EXPORT Element_Complex : public Element_List
{
    Q_OBJECT

public:
    virtual ~Element_Complex();    /**< Destructor */

    virtual QList<name_t> attributeNames() const;
    virtual bool hasAttribute(name_t name) const;
    virtual bool attributeFixed(name_t name) const;
    virtual Ptr attribute(name_t name) const;
    virtual bool setAttribute(name_t name, Ptr value);

    virtual bool elementToString(QString *val) const;

    virtual bool isElementSimple() const;
    virtual bool isElementEmpty() const;
    virtual bool isElementEqual(const Ptr elem) const;

    virtual Ptr elementClone() const;

protected:
    Element_Complex(name_t name, Type type); /**< Constructor */
    Element_Complex(name_t name, Type type, const QList<Ptr> &elements, const QMap<name_t, Ptr> &attributes); /**< Constructor */

    QMap<name_t, Element::Ptr> m_attributes;

    friend class StructTypeFactory;
};

};

inline bool operator==(const maksi::Element::Ptr x, const maksi::Element::Ptr y) { return x && x->isElementEqual(y); }
inline bool operator!=(const maksi::Element::Ptr x, const maksi::Element::Ptr y) { return x && !x->isElementEqual(y); }


Q_DECLARE_METATYPE(maksi::Element::Ptr);


#endif

/* End of file */
