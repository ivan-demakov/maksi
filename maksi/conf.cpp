/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012, 2013, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    conf.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Dec  5 17:01:35 2012
 *
 * @brief   conf
 *
 */

#include "conf.h"
#include "xmloader.h"
#include "maksi.h"
#include "engine.h"
#include "script.h"
#include "package.h"


//! maksi namespace
namespace maksi
{

Conf::Conf(Maksi *maksi, const QUrl &url) :
    m_import(0),
    m_defaultNamespace(xml::maksi_uri),
    m_maksi(maksi),
    m_url(url)
{
}

Conf::~Conf()
{
}

bool Conf::load()
{
    QByteArray data;
    if (m_maksi->loadPackageData(m_url, &data)) {
        maksi::XmlLoader xl(m_maksi, m_url);
        connect(&xl, SIGNAL(loaded(const MaksiElement *)), this, SLOT(loaded(const MaksiElement *)));
        return xl.loaded(data);
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, tr("cannot load config: %1").arg(m_url.toString()));
    }
    return false;
}

bool Conf::loaded(const MaksiElement *conf)
{
    static name_t maksi = xml::qname("maksi");
    static name_t maksi_engine = xml::qname("engine");
    static name_t maksi_script = xml::qname("script");
    static name_t maksi_package = xml::qname("package");
    static name_t xs_schema = xml::qname("schema", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = xml::qname("targetNamespace", maksi::xml::xschema_uri);

    m_maksi->sendMessage(MESSAGE_TRACE, tr("Conf::loaded: %1").arg(conf->elementSourceUrl().toString()));
    //qDebug("Conf::loaded: %s", qPrintable(conf->elementSourceUrl().toString()));

    if (conf->elementName() == maksi) {
        int count = conf->elementCount();
        for (int i = 0; i < count; ++i) {
            MaksiElementPtr e = conf->element(i);
            name_t name = e->elementName();
            if (name == xs_schema) {
                QString targetNamespace = m_defaultNamespace;
                if (e->hasAttribute(xs_targetNamespace)) {
                    targetNamespace = e->attribute(xs_targetNamespace)->elementText();
                }

                int typeCount = e->elementCount();
                for (int k = 0; k < typeCount; ++k) {
                    MaksiElementPtr val = e->element(k);
                    if (!val->isElementSimple()) {
                        initType(val, targetNamespace);
                    }
                }
            } else if (name == maksi_package) {
                initPackage(e.data());
            }
        }

        while (!m_types.isEmpty()) {
            Type t = m_types.takeFirst();
            //qDebug("Conf::loaded: %p: %s", t->m_conf.data(), qPrintable(t->m_conf->elementName()->toString()));
            maksi::Type::complexType(m_maksi, t.m_conf.data(), t.m_targetName, t.m_type, true);
        }

        for (int i = 0; i < count; ++i) {
            MaksiElementPtr e = conf->element(i);
            name_t name = e->elementName();
            if (name == maksi_engine) {
                initEngine(e.data());
            } else if (name == maksi_script) {
                initScript(e.data());
            }
        }
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("cannot load unsupported type of xml document: %1").arg(conf->elementName()->toString()));
    }

    return true;
}

bool Conf::initType(const MaksiElementPtr conf, const QString &targetNamespace)
{
    static name_t xs_include = xml::qname("include", maksi::xml::xschema_uri);
    static name_t xs_location = xml::qname("schemaLocation", maksi::xml::xschema_uri);
    static name_t xs_simpleType = xml::qname("simpleType", maksi::xml::xschema_uri);
    static name_t xs_complexType = xml::qname("complexType", maksi::xml::xschema_uri);
    static name_t xs_attribute = xml::qname("attribute", maksi::xml::xschema_uri);
    static name_t xs_group = xml::qname("group", maksi::xml::xschema_uri);
    static name_t xs_element = xml::qname("element", maksi::xml::xschema_uri);
    static name_t xs_name = xml::qname("name", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = xml::qname("targetNamespace", maksi::xml::xschema_uri);

    name_t element_name = conf->elementName();
    if (xs_include == element_name) {
        if (!conf->hasAttribute(xs_location)) {
            m_maksi->sendMessage(MESSAGE_WARN, conf, tr("missing attribute 'schemaLocation'"));
        } else {
            QString loc = conf->attribute(xs_location)->elementText();
            QUrl url(loc, QUrl::StrictMode);
            if (url.isValid()) {
                const MaksiElement *saveImport = m_import;
                QString saveNamespace = m_defaultNamespace;
                m_import = conf.data();
                m_defaultNamespace = targetNamespace;

                QByteArray data;
                if (m_maksi->loadPackageData(url, &data)) {
                    maksi::XmlLoader xl(m_maksi, url);
                    connect(&xl, SIGNAL(loaded(const MaksiElement *)), this, SLOT(imported(const MaksiElement *)));
                    xl.loaded(data);
                } else {
                    m_maksi->sendMessage(MESSAGE_WARN, conf, tr("cannot find url: %1").arg(url.toString()));
                }

                m_import = saveImport;
                m_defaultNamespace = targetNamespace;
            } else {
                m_maksi->sendMessage(MESSAGE_WARN, conf, tr("invalid url in schemaLocation: %1").arg(loc));
            }
        }
    } else {
        if (conf->hasAttribute(xs_name)) {
            name_t defName, targetName;
            if (conf->hasAttribute(xs_targetNamespace)) {
                targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
            } else {
                targetName = maksi::xml::qname("name", targetNamespace);
            }
            defName = maksi::xml::qname(conf->attribute(xs_name)->elementText(), conf->elementSource()->namespaces(), targetName);

            if (xs_simpleType == element_name) {
                maksi::Type type;
                if (maksi::Type::simpleType(m_maksi, conf.data(), targetName, &type)) {
                    m_maksi->addType(defName, type);
                }
            } else if (xs_complexType == element_name) {
                maksi::Type type;
                if (maksi::Type::complexType(m_maksi, conf.data(), targetName, &type, false)) {
                    maksi::Type *tp = m_maksi->addType(defName, type);
                    Type tt(tp, targetName, conf);
                    //qDebug("Conf::initType: %p: %s", tt->m_conf.data(), qPrintable(tt->m_conf->elementName()->toString()));
                    m_types.append(tt);
                }
            } else if (xs_attribute == element_name) {
                TypeAttribute attr = maksi::Type::attribute(m_maksi, conf.data(), targetName, true);
                if (attr.isValid()) {
                    m_maksi->addAttribute(defName, attr);
                }
            } else if (xs_group == element_name) {
                TypeGroup gr = maksi::Type::group(m_maksi, conf.data(), targetName);
                if (gr.isValid()) {
                    m_maksi->addGroup(defName, gr);
                }
            } else if (xs_element == element_name) {
                TypeElement el = maksi::Type::element(m_maksi, conf.data(), targetName);
                if (el.isValid()) {
                    m_maksi->addElement(defName, el);
                }
            } else {
                m_maksi->sendMessage(MESSAGE_WARN, conf, tr("skip definition %1: not implemented").arg(element_name->toString()));
            }
        } else {
            m_maksi->sendMessage(MESSAGE_WARN, conf, tr("skip definition %1: do not have attribute 'name'").arg(element_name->toString()));
        }
    }
    return true;
}

bool Conf::imported(const MaksiElement *conf)
{
    m_maksi->sendMessage(MESSAGE_TRACE, tr("Conf::imported: %1").arg(conf->elementSourceUrl().toString()));
    //qDebug("Conf::imported(): %s", qPrintable(conf->elementSourceUrl().toString()));

    static name_t xs_schema = xml::qname("schema", maksi::xml::xschema_uri);
    static name_t xs_import = xml::qname("import", maksi::xml::xschema_uri);
    static name_t xs_namespace = xml::qname("namespace", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->elementName() == xs_schema) {
        QString targetNamespace = m_defaultNamespace;
        if (conf->hasAttribute(xs_targetNamespace)) {
            targetNamespace = conf->attribute(xs_targetNamespace)->elementText();
        } else if (m_import) {
            if (m_import->hasAttribute(xs_targetNamespace)) {
                targetNamespace = m_import->attribute(xs_targetNamespace)->elementText();
            }
            if (m_import->elementName() == xs_import) {
                if (m_import->hasAttribute(xs_namespace)) {
                    targetNamespace = m_import->attribute(xs_namespace)->elementText();
                }
            }
        }

        int count = conf->elementCount();
        for (int i = 0; i < count; ++i) {
            MaksiElementPtr e = conf->element(i);
            if (!e->isElementSimple())
                initType(e, targetNamespace);
        }
    } else {
        m_maksi->sendMessage(MESSAGE_WARN, m_import, tr("cannot import unsupported type of document: %1").arg(conf->elementName()->toString()));
        return false;
    }

    return true;
}


bool Conf::initEngine(const MaksiElement *conf)
{
    static name_t maksi_name = xml::qname("name");
    static name_t maksi_plugin = xml::qname("plugin");

    QString name;
    if (conf->hasAttribute(maksi_name)) {
        name = conf->attribute(maksi_name)->elementText();
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("engine do not have required attribute %1").arg(maksi_name->toString()));
        return false;
    }

    QString lib;
    if (conf->hasAttribute(maksi_plugin)) {
        lib = conf->attribute(maksi_plugin)->elementText();
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("engine do not have required attribute %1").arg(maksi_plugin->toString()));
        return false;
    }

    Engine *engine = Engine::load(m_maksi, lib, name);
    if (engine) {
        if (engine->preinitEngine(conf) && engine->initEngine(conf) && engine->postinitEngine(conf)) {
            m_maksi->addEngine(engine);
        } else {
            delete engine;
            return false;
        }
    }

    return true;
}

bool Conf::initScript(const MaksiElement *conf)
{
    static name_t maksi_name = xml::qname("name");
    static name_t maksi_engine = xml::qname("engine");
    static name_t maksi_perm = xml::qname("permanent");

    QString scriptName;
    if (conf->hasAttribute(maksi_name)) {
        scriptName = conf->attribute(maksi_name)->elementText();
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("script do not have required attribute %1").arg(maksi_name->toString()));
        return false;
    }

    QString engineName;
    if (conf->hasAttribute(maksi_engine)) {
        engineName = conf->attribute(maksi_engine)->elementText();
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("script do not have required attribute %1").arg(maksi_engine->toString()));
        return false;
    }

    Engine *engine = m_maksi->engine(engineName);
    if (!engine) {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("undefined engine '%1'").arg(engineName));
        return false;
    }

    Script *script = engine->newScript(scriptName);
    if (script) {
	if (conf->hasAttribute(maksi_perm)) {
	    QString val = conf->attribute(maksi_perm)->elementText();
	    val = val.simplified().toLower();
	    if (val == "true" || val.toInt() != 0) {
		script->m_permanent = true;
	    }
	}

        m_maksi->addScript(script);
        script->initScript(conf);
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("cannot create script '%1'").arg(scriptName));
        return false;
    }
    return true;
}

bool Conf::initPackage(const MaksiElement *conf)
{
    static name_t maksi_location = xml::qname("location");
    static name_t maksi_type = xml::qname("type");

    QString type;
    if (conf->hasAttribute(maksi_type)) {
        type = conf->attribute(maksi_type)->elementText();
    } else {
        type = "dir";
    }

    QString location;
    if (conf->hasAttribute(maksi_location)) {
        location = conf->attribute(maksi_location)->elementText();
    } else {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("package do not have required attribute %1").arg(maksi_location->toString()));
        return false;
    }

    QUrl url(location);
    if (url.isRelative()) {
        QUrl base(conf->elementSourceUrl().toString());
        url = base.resolved(url);
    }
    //m_maksi->sendMessage(MESSAGE_DEBUG_CORE, tr("package: url='%1' type='%2'").arg(url.toString()).arg(type));

    Package *pack = 0;
    if (type == "dir") {
        pack = new PackageDir(m_maksi, url);
    } else if (type == "zip") {
        pack = new PackageZip(m_maksi, url);
    }
    if (pack == 0) {
        m_maksi->sendMessage(MESSAGE_ERROR, conf, tr("package has unknown type '%1'").arg(type));
        return false;
    }

    if (!pack->init()) {
        m_maksi->sendMessage(MESSAGE_WARN, conf, tr("cannot init package '%1'").arg(location));
        delete pack;
        return false;
    }

    m_maksi->addPackage(pack);
    return true;
}

};

 /* End of code */
