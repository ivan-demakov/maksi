/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    xmlelem.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Oct 13 18:24:52 2011
 *
 * @brief   xml element
 *
 */

#ifndef MAKSI_XMLELEM_H
#define MAKSI_XMLELEM_H

#include "elem.h"
#include "type.h"
#include "xml.h"

#include <QUrl>


//! maksi namespace
namespace maksi
{

//! xml element
class MAKSI_EXPORT XmlElement : public MaksiElement_Complex
{
    Q_OBJECT

public:
    class TypeFactory : public StructTypeFactory {
    public:
        TypeFactory();
        virtual MaksiElementPtr createNew(Type type, const MaksiElement *from) const;
    };

    static Type schemaType();

    XmlElement(maksi::name_t name, QMap<QString, QString> ns, const QUrl &srcUrl, int srcLine, Type type=schemaType());

    virtual ~XmlElement();

    virtual const maksi::Source *elementSource() const;

protected:
    Source m_src;
};


class MAKSI_EXPORT XmlString : public MaksiElement_String
{
    Q_OBJECT

public:
    class TypeFactory : public StringTypeFactory {
    public:
        TypeFactory();
        virtual MaksiElementPtr createNew(Type type, const MaksiElement *from) const;
    };

    static Type schemaType();

    XmlString(const QString &str, QMap<QString, QString> ns, const QUrl &srcUrl, int srcLine);

    virtual ~XmlString();

    virtual const maksi::Source *elementSource() const;

protected:
    Source m_src;
};

};

#endif

/* End of file */
