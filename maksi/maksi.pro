include(../maksi.pri)

TEMPLATE = lib

QT -= gui
QT += network


unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksi)
  LIBS += -larchive
  CONFIG += debug
}
win32 {
#  VERSION = $${MAKSI_VERSION}
  TARGET = maksi
  DEFINES += MAKSI_LIB=1
  RC_FILE  = maksi.rc
  CONFIG += release
}


HEADERS += xml.h
SOURCES += xml.cpp

HEADERS += xmlparser.h
SOURCES += xmlparser.cpp

#HEADERS += xmlelem.h
#SOURCES += xmlelem.cpp

#HEADERS += elem.h
#SOURCES += elem.cpp

#HEADERS += type.h
#SOURCES += type.cpp

#HEADERS += package.h
#SOURCES += package.cpp

#HEADERS += engine.h
#SOURCES += engine.cpp

#HEADERS += script.h
#SOURCES += script.cpp

#HEADERS += conf.h
#SOURCES += conf.cpp

#HEADERS += maksi.h
#SOURCES += maksi.cpp

#HEADERS += iface.h
#SOURCES += iface.cpp
