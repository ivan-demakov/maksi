/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010-2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri May 21 19:29:08 2010
 *
 * @brief   implementation of qt interface to maksi
 *
 */

#include "maksi.h"
#include "elem.h"
#include "conf.h"
#include "engine.h"
#include "script.h"
#include "package.h"

#include <QUrl>
#include <QFileInfo>
#include <QNetworkAccessManager>


Maksi::Maksi(const QString &name) :
    m_name(name),
    m_defPackage(0),
    m_message(0),
    m_network(0)
{
}

Maksi::~Maksi()
{
    for (auto i = m_contexts.begin(); i != m_contexts.end(); ++i) {
	delete i.value();
    }
    for (auto i = m_scripts.begin(); i != m_scripts.end(); ++i) {
	delete i.value();
    }
    for (auto i = m_engines.begin(); i != m_engines.end(); ++i) {
        delete i.value();
    }
    while (!m_packages.isEmpty()) {
        delete m_packages.takeFirst();
    }
    delete m_defPackage;
}

void Maksi::sendMessage(maksi::message_t type, const MaksiElement *e, QString msg)
{
    if (e) {
        sendMessage(type, e->elementSourceUrl(), e->elementSourceLine(), msg);
    } else {
        sendMessage(type, msg);
    }
}

void Maksi::sendMessage(maksi::message_t type, const QUrl &url, int line, QString msg)
{
    if (m_message) {
        if (!url.isValid()) {
            sendMessage(type, msg);
        } else {
            QString str(url.toString());
            str += ':';
            str += QString::number(line);
            str += ": ";
            str += msg;
            sendMessage(type, str);
        }
    }
}

void Maksi::sendMessage(maksi::message_t type, QString msg)
{
    if (m_message) {
        QString str(m_name);
        str += ": ";
        str += msg;

        m_message(type, str);
    }
}

maksi::Type Maksi::type(maksi::name_t name) const
{
    if (m_types.contains(name)) {
        return m_types.value(name);
    }
    return maksi::Type::stdType(name);
}


maksi::Type *Maksi::addType(maksi::name_t name, maksi::Type data)
{
    m_types.insert(name, data);
    return &m_types[name];
}


maksi::TypeAttribute Maksi::attribute(maksi::name_t name) const
{
    return m_attributes.value(name);
}


void Maksi::addAttribute(maksi::name_t name, maksi::TypeAttribute data)
{
    m_attributes.insert(name, data);
}


maksi::TypeElement Maksi::element(maksi::name_t name) const
{
    return m_elements.value(name);
}

void Maksi::addElement(maksi::name_t name, maksi::TypeElement data)
{
    m_elements.insert(name, data);
}

maksi::TypeGroup Maksi::group(maksi::name_t name) const
{
    return m_groups.value(name);
}

void Maksi::addGroup(maksi::name_t name, maksi::TypeGroup data)
{
    m_groups.insert(name, data);
}

maksi::Engine *Maksi::engine(const QString &name) const
{
    return m_engines.value(name);
}

void Maksi::addEngine(maksi::Engine *engine)
{
    if (engine)
        m_engines.insert(engine->name(), engine);
}

maksi::Script *Maksi::script(const QString &name) const
{
    return m_scripts.value(name);
}

void Maksi::addScript(maksi::Script *script)
{
    if (script)
        m_scripts.insert(script->name(), script);
}

maksi::Context *Maksi::scriptContext(const QString &name) const
{
    return m_contexts.value(name);
}

void Maksi::addScriptContext(const QString &name, maksi::Context *context)
{
    if (context) {
	context->setPermanent();
        m_contexts.insert(name, context);
    }
}

QList<QString> Maksi::scriptNames() const
{
    QList<QString> ls;
    for (auto mi = m_contexts.constBegin(); mi != m_contexts.constEnd(); ++mi) {
        ls << mi.key();
    }
    for (auto mi = m_scripts.constBegin(); mi != m_scripts.constEnd(); ++mi) {
        ls << mi.key();
    }
    return ls;
}

void Maksi::addPackage(maksi::Package *package)
{
    if (package)
        m_packages.append(package);
}

bool Maksi::loadPackageData(const QUrl &url, QByteArray *data)
{
    //qDebug("Maksi::package: %s", qPrintable(url.toString()));
    if (url.isRelative()) {
        for (int i = 0; i < m_packages.size(); ++i) {
            maksi::Package *pack = m_packages.at(i);
            if (pack->load(url, data)) {
                return true;
            }
        }
    } else {
        if (!m_defPackage) {
            m_defPackage = new maksi::PackageDir(this, url);
            m_defPackage->init();
        }
        if (m_defPackage->load(url, data)) {
            return true;
        }
    }

    return false;
}

QNetworkAccessManager *Maksi::network()
{
    if (!m_network) {
        m_network = new QNetworkAccessManager(this);
    }
    return m_network;
}


bool Maksi::loadConfig(const QString &url_)
{
    QUrl url(url_);

    if (!url.isValid())
        return false;

    if (url.isRelative()) {
        QFileInfo info(url_);
        url.setScheme("file");
        url.setPath(info.absoluteFilePath());
    }
    //qDebug("Maksi::loadConfig(): %s", qPrintable(url.toString()));

    maksi::Conf conf(this, url);
    return conf.load();
}

void Maksi::clearConfig()
{
    for (int i = 0; i < m_packages.size(); ++i) {
        m_packages.at(i)->clear();
    }
}


 /* End of code */
