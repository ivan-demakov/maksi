/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    xmloader.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Mon Nov  5 22:24:39 2012
 *
 * @brief   xml loader
 *
 */

#ifndef MAKSI_XMLOADER_H
#define MAKSI_XMLOADER_H

#include <QUrl>

#include "xmlelem.h"


class QXmlStreamReader;

class Maksi;


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT XmlLoader : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(XmlLoader);
public:
    enum xml_state_t {
        XML_DOCUMENT = 0,
        XML_ELEMENT,
        XML_ERROR,
        XML_UNKNOWN,
        XML_USER_STATE
    };

    struct XmlNode {
        XmlNode(maksi::name_t name, QMap<QString, QString> ns, const QUrl &url, int line) : m_state(XML_ELEMENT), m_xml(new XmlElement(name, ns, url, line)) {}

        int m_state;
        MaksiElementPtr m_xml;
    };

    XmlLoader(Maksi *maksi, const QUrl &url, int line=0);
    virtual ~XmlLoader();

signals:
    void loaded(const MaksiElement *xml);

public slots:
    bool loaded(QByteArray data);
    bool loaded(QString data);

protected:
    virtual int startElement(int state, XmlNode *node);
    virtual bool endElement(int state, XmlNode *node);

    void sendMessage(message_t type, int lineNumber, const QString &msg);

    bool load(QXmlStreamReader &reader);

private:
    Maksi *m_maksi;
    QUrl m_url;
    bool m_finished;
    int m_startLine;
};


};


#endif

/* End of file */
