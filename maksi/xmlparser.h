/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    xmlparser.h
 * @author  ivan demakov <ivan@demakov.net>
 * @date    Tue Nov 14 05:18:26 +07 2017
 *
 * @brief   xml parser
 *
 */

#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "xml.h"

#include <QUrl>


class QXmlStreamReader;

class Maksi;


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT XmlParser : public QObject
{
    Q_OBJECT;
    Q_DISABLE_COPY(XmlParser);

public:
    XmlParser(Maksi *maksi, const QUrl &url, int line=0);
    virtual ~XmlParser();

    bool parse(const QByteArray &data);
    bool parse(const QString &data);
    bool parse(QXmlStreamReader &reader);

protected:
    void sendMessage(message_t type, int lineNumber, const QString &msg);

    Maksi *m_maksi;
    QUrl m_url;
    int m_startLine;
};


};


#endif
