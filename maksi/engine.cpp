/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    engine.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Feb  2 20:50:30 2011
 *
 * @brief   engine interface
 *
 */

#include "engine.h"
#include "elem.h"
#include "maksi.h"
#include "xml.h"
#include "package.h"

#include <QPluginLoader>
#include <QFileInfo>
#include <QStringList>
#include <QTextStream>
#include <QCoreApplication>


//! maksi namespace
namespace maksi
{

EnginePlugin::~EnginePlugin()
{
}

Engine::Engine() :
    m_internal(0), m_maksi(0)
{
}

Engine::~Engine()
{
}

Engine *Engine::load(Maksi *maksi, const QString &pluginName, const QString &engineName)
{
    static QHash<QString, EnginePlugin *> plugins;

    EnginePlugin *plug = plugins.value(pluginName);
    if (plug) {
        return plug->newEngine(maksi, engineName);
    }

#if 1
    QString lib = pluginName;
    QPluginLoader loader(lib);
    QObject *obj = loader.instance();
    if (obj) {
        plug = qobject_cast<EnginePlugin *>(obj);
        if (plug) {
            maksi->sendMessage(MESSAGE_INFO, QString("%1: loaded plugin '%2'").arg(plug->description()).arg(lib));
            plugins.insert(pluginName, plug);
            return plug->newEngine(maksi, engineName);
        } else {
            maksi->sendMessage(MESSAGE_ERROR, QString("'%1' is not maksi plugin").arg(lib));
        }
    } else {
        maksi->sendMessage(MESSAGE_ERROR, QString("cannot load '%1': %2").arg(lib).arg(loader.errorString()));
    }

#else
    QFileInfo fi(pluginName);
#if defined(Q_OS_SYMBIAN)
    QString path; // In Symbian, always resolve with just the filename
    QString name;
    // Replace possible ".qtplugin" suffix with ".dll"
    if (fi.suffix() == "qtplugin")
        name = fi.completeBaseName() + ".dll";
    else
        name = fi.fileName();
#else
    QString path = fi.path();
    QString name = fi.fileName();
    if (path == "." && !pluginName.startsWith(path))
        path.clear();
    else
        path += '/';
#endif

    QStringList suffixes(""), prefixes(""), pathes(path);
#if !defined(Q_OS_SYMBIAN) && !defined(Q_OS_WIN)
    prefixes << "lib";
#endif
#if defined(Q_OS_SYMBIAN) || defined(Q_OS_WIN)
    suffixes << ".dll";
#else
    suffixes << ".so";
#endif
#ifdef Q_OS_MAC
    suffixes << ".bundle" << ".dylib";
#endif
    if (path.isEmpty()) {
        pathes << QCoreApplication::libraryPaths();
    }

    for (int k = 0; k < pathes.size(); k++) {
        QString path = pathes.at(k);
        if (!path.isEmpty()) {
#if defined(Q_OS_WIN)
            if (!path.endsWith("/") && !path.endsWith("\\"))
                path += '/';
#else
            if (!path.endsWith("/"))
                path += '/';
#endif
        }

        for (int p = 0; p < prefixes.size(); p++) {
            for (int s = 0; s < suffixes.size(); s++) {
                if (!prefixes.at(p).isEmpty() && name.startsWith(prefixes.at(p)))
                    continue;
                if (!suffixes.at(s).isEmpty() && name.endsWith(suffixes.at(s)))
                    continue;

                QString lib = path + prefixes.at(p) + name + suffixes.at(s);
                //maksi->sendMessage(MAKSI_MESSAGE_INFO, QString("try plugin: %1").arg(lib));
                QFileInfo info(lib);
                if (!info.exists())
                    continue;

                QPluginLoader loader(lib);
                QObject *obj = loader.instance();
                if (obj) {
                    plug = qobject_cast<EnginePlugin *>(obj);
                    if (plug) {
                        maksi->sendMessage(MESSAGE_INFO, QString("%1: loaded plugin '%2'").arg(plug->description()).arg(lib));
                        plugins.insert(pluginName, plug);
                        return plug->newEngine(maksi, engineName);
                    } else {
                        maksi->sendMessage(MESSAGE_ERROR, QString("'%1' is not maksi plugin").arg(lib));
                    }
                } else {
                    maksi->sendMessage(MESSAGE_ERROR, QString("cannot load '%1': %2").arg(lib).arg(loader.errorString()));
                }
            }
        }
    }
#endif

    maksi->sendMessage(MESSAGE_ERROR, QString("plugin '%1' is not exists").arg(pluginName));
    return 0;
}


bool Engine::preinitEngine(const MaksiElement *)
{
    return true;
}

bool Engine::initEngine(const MaksiElement *conf)
{
    static name_t maksi_load = maksi::xml::qname("load");
    static name_t maksi_href = maksi::xml::qname("location");
    static name_t maksi_eval = maksi::xml::qname("eval");

    //qDebug("MaksiEngine::initEngine");
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (node->elementName() == maksi_load) {
            if (node->hasAttribute(maksi_href)) {
                QString href = node->attribute(maksi_href)->elementText();
                QUrl url(href, QUrl::StrictMode);
                QByteArray data;
                if (m_maksi->loadPackageData(url, &data)) {
                    QTextStream stream(&data, QIODevice::ReadOnly);
                    stream.setAutoDetectUnicode(true);
                    QString code = stream.readAll();

                    evalCode(code, url, 0, 0);
                } else {
                    maksi()->sendMessage(MESSAGE_WARN, node.data(), tr("cannot load code '%1'").arg(href));
                }
            }
        } else if (node->elementName() == maksi_eval) {
            QString code = node->elementText();
            evalCode(code, node->elementSourceUrl(), node->elementSourceLine(), 0);
        }
    }

    return true;
}

bool Engine::postinitEngine(const MaksiElement *)
{
    return true;
}

EngineInternal *Engine::getInternal() const
{
    return m_internal;
}

};

 /* End of code */
