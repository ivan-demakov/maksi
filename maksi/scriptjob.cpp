/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    scriptjob.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jan 27 10:38:11 2012
 *
 * @brief   script job
 *
 */



#include "scriptjob.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QByteArray>


namespace maksi
{

ScriptJob::ScriptJob(Maksi *maksi, ScriptContext *ctx) :
    Job(true),
    m_maksi(maksi),
    m_ctx(ctx),
    m_reply(0),
    m_pos(0)
{
    m_res = RESULT_OK;
}

ScriptJob::~ScriptJob()
{
    delete m_reply;
}

void ScriptJob::subres(result_t res, QVariant val)
{
    m_res = res;
    m_val = val;
}

void ScriptJob::argres(result_t res, QVariant val)
{
    if (RESULT_OK == res) {
        m_args.append(val);
    } else {
        m_res = res;
        m_val = val;
    }
}

bool ScriptJob::run()
{

    resultReady(m_res, m_val);
    return true;
}


};

 /* End of code */
