/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, 2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    xml.h
 * @author  ivan demakov <ivan@demakov.net>
 * @date    Sun Oct  2 15:42:17 2011
 *
 * @brief   xml interface
 *
 */

#ifndef MAKSI_XML_H
#define MAKSI_XML_H

#include "iface.h"

#include <QUrl>
#include <QMap>
#include <QHash>
#include <QVector>


//! maksi namespace
namespace maksi
{

//! xml utils
class MAKSI_EXPORT xml
{
public:

    class Src {
    public:
	Src(const QUrl &url, int line) : m_url(url), m_line(line) {}

	const QUrl &url() const { return m_url; }
	int line() const { return m_line; }

    private:
	QUrl m_url;
	int m_line;
    };

    struct Item {
	enum type_t { TEXT, ATTR, NODE, DOC };

	Item(type_t type, const QUrl &srcUrl, int srcLine) : m_type(type), m_src(srcUrl, srcLine) {}

	type_t m_type;
	Src m_src;
    };

    struct Text : public Item {
	Text(const QString &val, const QUrl &srcUrl, int srcLine) : Item(TEXT, srcUrl, srcLine), m_val(val) {}

	QString m_val;
    };

    struct Attr : public Item {
	Attr(name_t name, const QString &val, const QUrl &srcUrl, int srcLine) : Item(ATTR, srcUrl, srcLine), m_name(name), m_val(val) {}

	name_t m_name;
	QString m_val;
    };

    struct NodeItem : public Item {
	NodeItem(type_t type, const QUrl &srcUrl, int srcLine) : Item(type, srcUrl, srcLine) {}

	void addText(const QString &val, const QUrl &url, int line) {
	    QSharedPointer<Item> x(new Text(val, url, line));
	    m_items.append(x);
	}

	void addAttr(name_t name, const QString &val, const QUrl &url, int line) {
	    QSharedPointer<Attr> x(new Attr(name, val, url, line));
	    m_items.append(x);
	}

	QVector<QSharedPointer<Item>> m_items;
	QVector<QSharedPointer<Attr>> m_attrs;
	QMap<QString, QString> m_nspaces;
    };

    struct Node : public NodeItem {
	Node(name_t name, const QUrl &srcUrl, int srcLine) : NodeItem(NODE, srcUrl, srcLine), m_name(name) {}

	name_t m_name;
    };

    struct Doc : public NodeItem {
	Doc(const QUrl &srcUrl, int srcLine) : NodeItem(DOC, srcUrl, srcLine) {}
    };

    static bool isNMToken(const QString &name);
    static bool isName(const QString &name);
    static bool isNCName(const QString &name);

    static QString toClarkName(const QString &localName, const QString &namespaceUri, const QString &prefix);
    static name_t fromClarkName(const QString &clarkName);
    static QString hashName(const QString &localName, const QString &namespaceUri);

    static name_t qname(const QString &localName, const QString &namespaceURI=maksi_uri, const QString &prefix=QString());
    static name_t qname(const QString &localName, const Name *targetName);
    static name_t qname(const QString &localName, const QMap<QString, QString> &namespaces, const Name *targetName=0);

    static const QString maksi_uri;
    static const QString xschema_uri;
};


//! maksi xml Name
class Name
{
public:
    Name(const QString &name, const QString &ns, const QString &pr) : m_name(name), m_namespace(ns), m_prefix(pr) {}

    QString toString() const { return xml::toClarkName(m_name, m_namespace, m_prefix); }

    const QString &name() const { return m_name; }
    const QString &namespaceUri() const { return m_namespace; }
    const QString &prefix() const { return m_prefix; }

    bool operator==(const Name &x) const { return m_name == x.m_name && m_namespace == x.m_namespace; }
    bool operator!=(const Name &x) const { return m_name != x.m_name || m_namespace != x.m_namespace; }

private:
    QString m_name;
    QString m_namespace;
    QString m_prefix;
};


};


MAKSI_EXPORT uint qHash(const maksi::Name &x);

MAKSI_EXPORT uint qHash(maksi::name_t x);


#endif

/* End of file */
