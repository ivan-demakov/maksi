/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2015, MakLibsi Lop.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    package.cpp
 * @author  ivan demakov <ivan@demakov.net>
 * @date    Thu Jun 11 18:45:18 2015
 *
 * @brief   package
 *
 */


#include "package.h"
#include "maksi.h"

#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <archive.h>
#include <archive_entry.h>

#if defined(_MSC_VER)
#  pragma comment(lib, "archive")
#endif

//! Maksi namespace
namespace maksi
{

Package::Package(Maksi *maksi, const QUrl &baseUrl) :
    m_maksi(maksi),
    m_baseUrl(baseUrl)
{
}

Package::~Package()
{
}

bool Package::init()
{
    return true;
}

void Package::clear()
{
}

void Package::progress(qint64 recv, qint64 total)
{
    if (m_maksi) {
	QString msg(QStringLiteral("received %1").arg(recv));
	if (total > 0)
	    msg += QStringLiteral("/%1").arg(total);
	m_maksi->sendMessage(MESSAGE_PROGRESS, msg);
    }
}

bool Package::loadData(const QUrl &url, QByteArray *data, maksi::message_t msg)
{
    QNetworkAccessManager *nam = 0;
    bool del = false;

    if (m_maksi) {
        nam = m_maksi->network();
    }
    if (!nam) {
        nam = new QNetworkAccessManager(this);
        del = true;
    }

    m_maksi->sendMessage(MESSAGE_TRACE, tr("maksi::Package::loadData: %1").arg(url.toString()));

    QEventLoop loop;
    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::NoLessSafeRedirectPolicy);
    req.setRawHeader("User-Agent", "Maksi");

    QNetworkReply *reply = nam->get(req);
    connect(reply, SIGNAL(preSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *)),
	    this, SLOT(preSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *)));
    connect(reply, SIGNAL(sslErrors(const QList<QSslError> &)), this, SLOT(sslErrors(const QList<QSslError> &)));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), &loop, SLOT(quit()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    if (!url.isLocalFile()) {
	connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progress(qint64, qint64)));
    }
    loop.exec(QEventLoop::ExcludeUserInputEvents);
    //loop.processEvents();

    bool res = false;
    if (reply->error()) {
        m_maksi->sendMessage(msg, reply->errorString());
    } else {
	if (data) {
	    *data = reply->readAll();
	}
	res = true;
    }

    delete reply;
    if (del) {
        delete nam;
    }
    return res;
}

void Package::sslErrors(const QList<QSslError> &errors)
{
    m_maksi->sendMessage(MESSAGE_TRACE, tr("maksi::PackageDir::sslErrors: %1").arg(errors.size()));
    for(int i = 0; i < errors.size(); ++i) {
        m_maksi->sendMessage(MESSAGE_WARN, tr("maksi::PackageDir::sslErrors: %1").arg(errors.at(i).errorString()));
    }
}

void Package::preSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *auth)
{
    m_maksi->sendMessage(MESSAGE_TRACE, tr("maksi::PackageDir::Auth: %1").arg(m_maksi->name()));

    auth->setIdentity("Maksi");

    //const QByteArray key = deriveKey(auth->identityHint(), passphrase);
    //auth->setPreSharedKey(key);
}

PackageDir::PackageDir(Maksi *maksi, const QUrl &baseUrl) :
    Package(maksi, baseUrl)
{
}

PackageDir::~PackageDir()
{
}

bool PackageDir::load(const QUrl &url_, QByteArray *data)
{
    QUrl url(url_);
    if (url.isRelative()) {
        if (m_baseUrl.isValid())
            url = m_baseUrl.resolved(url);
    }

    bool res = loadData(url, data, MESSAGE_TRACE);
    if (res) {
	m_maksi->sendMessage(MESSAGE_TRACE, tr("maksi::PackageDir: loaded %1").arg(url.toString()));
    }
    return res;
}


PackageZip::PackageZip(Maksi *maksi, const QUrl &baseUrl) :
    Package(maksi, baseUrl)
{
}

PackageZip::~PackageZip()
{
}

bool PackageZip::init()
{
    if (!m_baseUrl.isValid())
        return false;

    QByteArray zip;
    bool res = loadData(m_baseUrl, &zip, MESSAGE_WARN);
    if (!res) {
        return false;
    }

    struct archive *archive;
    archive = archive_read_new();
    archive_read_support_filter_all(archive);
    archive_read_support_format_all(archive);

    int err = archive_read_open_memory(archive, zip.data(), zip.size());
    if (err != ARCHIVE_OK) {
        m_maksi->sendMessage(MESSAGE_WARN, tr("maksi::PackageZip(%1): %2").arg(m_baseUrl.toString()).arg(archive_error_string(archive)));
        archive_read_free(archive);
        return false;
    }

    struct archive_entry *entry;
    while (archive_read_next_header(archive, &entry) == ARCHIVE_OK) {
        int64_t size = archive_entry_size(entry);
        if (size < 0) {
            m_maksi->sendMessage(MESSAGE_WARN, tr("maksi::PackageZip(%1): %2").arg(m_baseUrl.toString()).arg(archive_error_string(archive)));
            archive_read_free(archive);
            return false;
        }

        QString name = QString::fromWCharArray(archive_entry_pathname_w(entry));
        QByteArray data(size, 0);
        archive_read_data(archive, data.data(), size);

        m_data.insert(name, data);
    }
    archive_read_free(archive);

    m_maksi->sendMessage(MESSAGE_TRACE, tr("maksi::PackageZip: loaded package %1").arg(m_baseUrl.toString()));
    return true;
}

bool PackageZip::load(const QUrl &url, QByteArray *data)
{
    QString name = url.toString();
    if (m_data.contains(name)) {
        if (data)
            *data = m_data.value(name);
        m_maksi->sendMessage(MESSAGE_TRACE, QString("maksiPackageZip: %1 found in package %2").arg(url.toString()).arg(m_baseUrl.toString()));
        return true;
    }

    m_maksi->sendMessage(MESSAGE_TRACE, QString("maksi::PackageZip: %1 not found in package %2").arg(url.toString()).arg(m_baseUrl.toString()));
    return false;
}

void PackageZip::clear()
{
    m_data.clear();
}


};

 /* End of code */
