/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    script.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri May 21 22:34:26 2010
 *
 * @brief   script interface
 *
 */

#include "script.h"
#include "maksi.h"
#include "elem.h"
#include "xmlelem.h"

#include <QString>


//! maksi namespace
namespace maksi
{

Context::~Context()
{
}


Script::Script(Maksi *maksi, const QString &name) :
    m_maksi(maksi),
    m_name(name),
    m_permanent(false)
{
}

Script::~Script()
{
}

void Script::initScript(const MaksiElement *conf)
{
    static name_t maksi_arg = xml::qname("arg");
    static name_t maksi_name = xml::qname("name");
    static name_t maksi_type = xml::qname("type");
    static name_t maksi_eval = xml::qname("eval");

    for (int i = 0; i < conf->elementCount(); ++i) {
        MaksiElementPtr e = conf->element(i);
        //m_maksi->sendMessage(MESSAGE_ERROR, e, tr("%1").arg(e->toString()));
        if (e->elementName() == maksi_arg) {
            if (e->hasAttribute(maksi_name)) {
                QString name = e->attribute(maksi_name)->elementText();
                if (e->hasAttribute(maksi_type)) {
                    name_t typeName = xml::qname(e->attribute(maksi_type)->elementText(), conf);
                    //qDebug("%s: %s", qPrintable(e->attribute(maksi_type)->elementText()), qPrintable(typeName->toString()));
                    Type type = m_maksi->type(typeName);
                    if (type.isValid()) {
                        MaksiElementPtr val = type.createNew(m_maksi, e.data(), typeName, false);
                        if (val) {
                            addArg(name, val->elementToVariant(), e.data());
                        }
                    } else {
                        m_maksi->sendMessage(MESSAGE_ERROR, e.data(), tr("undefined type %1").arg(typeName->toString()));
                    }
                } else {
                    //m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, e.data(), tr("arg do not have required attribute 'type'"));
                    addArg(name, QVariant(), e.data());
                }
            } else {
                m_maksi->sendMessage(MESSAGE_ERROR, e.data(), tr("arg do not have required attribute 'name'"));
            }
        } else if (e->elementName() == maksi_eval) {
            addCode(e->elementText(), e.data());
        }
    }
}


#if 0
result_t ScriptContext::Version::addVar(const QString &name, element_t conf)
{
    QVariant val;
    result_t res = m_script->getValue(conf, &val, true);
    if (RESULT_OK == res) {
        m_vars.insert(name, val);
    }
    return res;
}

result_t ScriptContext::Version::addFeature(const QString &name, element_t conf)
{
    static QXmlName maksi_state = xml::qname("state");

    Feature feature(name);
    if (conf->hasAttribute(maksi_state)) {
        feature.m_state = conf->attribute(maksi_state).toString();
    }

    result_t res = RESULT_OK;//m_script->getValue(conf, &feature.m_value, true);
    if (RESULT_OK == res) {
        m_features.append(feature);
    }
    return res;
}


result_t ScriptContext::addFeature(const QString &name, element_t conf)
{
    static QXmlName maksi_state = xml::qname("state");

    Feature feature(name);
    if (conf->hasAttribute(maksi_state)) {
        feature.m_state = conf->attribute(maksi_state).toString();
    }

    result_t res = RESULT_OK;//m_script->getValue(conf, &feature.m_value, true);
    if (RESULT_OK == res) {
        m_features.append(feature);
    }
    return res;
}


ScriptContext::ScriptContext(script_t script) :
    m_script(script)
{
}

ScriptContext::~ScriptContext()
{
}
#endif

};

/* End of code */
