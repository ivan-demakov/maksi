/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    elem.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri May 13 17:26:31 2011
 *
 * @brief   maksi element
 *
 */

#include "elem.h"
#include "xml.h"
#include "type.h"

#include <QXmlStreamWriter>
#include <QSharedPointer>
#include <QMetaMethod>


namespace maksi
{

Element::Element(QXmlName name, Type type) :
    m_name(name), m_type(type)
{
}

Element::~Element()
{
}

QXmlName Element::elementName() const
{
    return m_name;
}

void Element::setElementName(QXmlName name)
{
    m_name = name;
}

const Type &Element::elementType() const
{
    return m_type;
}

const QSourceLocation *Element::elementSource() const
{
    return 0;
}

QUrl Element::elementSourceUrl() const
{
    const QSourceLocation *src = elementSource();
    if (src) {
        return src->uri();
    }
    return QUrl();
}

int Element::elementSourceLine() const
{
    const QSourceLocation *src = elementSource();
    if (src) {
        return src->line();
    }
    return 0;
}

QXmlName Element::attributeName(const QString &name) const
{
    if (name.isEmpty()) {
        return QXmlName();
    } else if (name.at(0) == QLatin1Char('{')) {
        return xml::fromClarkName(name);
    } else if (!m_name.isNull()) {
        return xml::qname(name, m_name.namespaceUri(), m_name.prefix());
    }

    return xml::qname(name);
}

QList<QXmlName> Element::attributeNames() const
{
    QList<QXmlName> names;
    if (m_type.isValid()) {
        QList<TypeAttribute> as = m_type.attributes();
        for (int i = 0; i < as.size(); i++) {
            names << as.at(i).name();
        }
    }
    return names;
}

bool Element::hasAttribute(QXmlName ) const
{
    return false;
}

bool Element::hasAttribute(const QString &name) const
{
    QXmlName xmlName = attributeName(name);
    if (!xmlName)
        return false;
    return hasAttribute(xmlName);
}

bool Element::attributeFixed(QXmlName ) const
{
    return false;
}

bool Element::attributeFixed(const QString &name) const
{
    QXmlName xmlName = attributeName(name);
    if (!xmlName)
        return false;
    return attributeFixed(xmlName);
}

ElementPtr Element::attribute(QXmlName ) const
{
    return ElementPtr();
}

ElementPtr Element::attribute(const QString &name) const
{
    QXmlName xmlName = attributeName(name);
    if (!xmlName)
        return ElementPtr();
    return attribute(xmlName);
}

bool Element::setAttribute(QXmlName , ElementPtr )
{
    return false;
}

bool Element::setAttribute(const QString &name, ElementPtr value)
{
    QXmlName xmlName = attributeName(name);
    if (xmlName)
        return setAttribute(xmlName, value);
    return false;
}

int Element::elementCount() const
{
    return 0;
}

ElementPtr Element::element(int ) const
{
    return ElementPtr();
}

bool Element::elementAppend(const ElementPtr &)
{
    return false;
}

bool Element::elementToByteArray(QByteArray *) const
{
    return false;
}

bool Element::elementToString(QString *) const
{
    //qDebug("Element::elementToString()");
    return false;
}

bool Element::elementToQName(QXmlName * ) const
{
    return false;
}

bool Element::elementToBool(bool *) const
{
    return false;
}

bool Element::elementToInt(int32_t *) const
{
    return false;
}

bool Element::elementToUInt(uint32_t *) const
{
    return false;
}

bool Element::elementToLong(int64_t *) const
{
    return false;
}

bool Element::elementToULong(uint64_t *) const
{
    return false;
}

bool Element::elementToFloat(float *) const
{
    return false;
}

bool Element::elementToDouble(double *) const
{
    return false;
}

bool Element::elementToUrl(QUrl *) const
{
    return false;
}

bool Element::elementToDate(QDateTime *) const
{
    return false;
}

bool Element::elementToDate(QDate *) const
{
    return false;
}

bool Element::elementToTime(QTime *) const
{
    return false;
}

QVariant Element::elementToVariant() const
{
    return QVariant::fromValue(elementClone());
}

ElementPtr Element::newVariant(const QVariant &val)
{
    switch (static_cast<QMetaType::Type>(val.type())) {
    case QMetaType::Bool:
        return newBool(val.toBool());

    case QMetaType::Int:
    case QMetaType::Short:
    case QMetaType::SChar:
        return newInt(val.toInt());

    case QMetaType::UInt:
    case QMetaType::UShort:
    case QMetaType::UChar:
        return newUInt(val.toInt());

    case QMetaType::Long:
    case QMetaType::LongLong:
        return newLong(val.toLongLong());

    case QMetaType::ULong:
    case QMetaType::ULongLong:
        return newLong(val.toULongLong());

    case QMetaType::Float:
        return newFloat(val.toFloat());

    case QMetaType::Double:
        return newDouble(val.toDouble());

    case QMetaType::QChar:
    case QMetaType::QString:
        return newString(val.toString());

    case QMetaType::QByteArray:
        return newByteArray(val.toByteArray());

    case QMetaType::QDate:
        return newDate(val.toDate());

    case QMetaType::QTime:
        return newTime(val.toTime());

    case QMetaType::QDateTime:
        return newDate(val.toDateTime());

    case QMetaType::QUrl:
        return newUrl(val.toUrl());

    default:
        if (val.canConvert<ElementPtr>()) {
            return val.value<ElementPtr>();
        }
        break;
    }

    return ElementPtr(new Element_Variant(val));
}

ElementPtr Element::newByteArray(QXmlName name, Type type, const QByteArray &val)
{
    return ElementPtr(new Element_ByteArray(name, type, val));
}

ElementPtr Element::newByteArray(const QByteArray &val)
{
    static QXmlName Name = xml::qname("hexBinary", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_ByteArray(0, Type, val));
}

ElementPtr Element::newString(QXmlName name, Type type, const QString &val)
{
    return ElementPtr(new Element_String(name, type, val));
}

ElementPtr Element::newString(const QString &val)
{
    static QXmlName Name = xml::qname("string", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_String(0, Type, val));
}

ElementPtr Element::newToken(const QString &val)
{
    static QXmlName Name = xml::qname("NMTOKEN", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_String(0, Type, val));
}

ElementPtr Element::newName(const QString &val)
{
    static QXmlName Name = xml::qname("NCName", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_String(0, Type, val));
}

ElementPtr Element::newQName(QXmlName name, Type type, QXmlName val)
{
    return ElementPtr(new Element_QName(name, type, val));
}

ElementPtr Element::newQName(QXmlName val)
{
    static QXmlName Name = xml::qname("QName", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_QName(0, Type, val));
}

ElementPtr Element::newBool(QXmlName name, Type type, bool val)
{
    return ElementPtr(new Element_Bool(name, type, val));
}

ElementPtr Element::newBool(bool val)
{
    static QXmlName Name = xml::qname("boolean", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Bool(0, Type, val));
}

ElementPtr Element::newInt(QXmlName name, Type type, int32_t val)
{
    return ElementPtr(new Element_Int(name, type, val));
}

ElementPtr Element::newInt(int32_t val)
{
    static QXmlName Name = xml::qname("int", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Int(0, Type, val));
}

ElementPtr Element::newUInt(QXmlName name, Type type, uint32_t val)
{
    return ElementPtr(new Element_UInt(name, type, val));
}

ElementPtr Element::newUInt(uint32_t val)
{
    static QXmlName Name = xml::qname("unsignedInt", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_UInt(0, Type, val));
}

ElementPtr Element::newLong(QXmlName name, Type type, int64_t val)
{
    return ElementPtr(new Element_Long(name, type, val));
}

ElementPtr Element::newLong(int64_t val)
{
    static QXmlName Name = xml::qname("long", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Long(0, Type, val));
}

ElementPtr Element::newULong(QXmlName name, Type type, uint64_t val)
{
    return ElementPtr(new Element_ULong(name, type, val));
}

ElementPtr Element::newULong(uint64_t val)
{
    static QXmlName Name = xml::qname("unsignedLong", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_ULong(0, Type, val));
}

ElementPtr Element::newFloat(QXmlName name, Type type, float val)
{
    return ElementPtr(new Element_Float(name, type, val));
}

ElementPtr Element::newFloat(float val)
{
    static QXmlName Name = xml::qname("float", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Float(0, Type, val));
}

ElementPtr Element::newDouble(QXmlName name, Type type, double val)
{
    return ElementPtr(new Element_Double(name, type, val));
}

ElementPtr Element::newDouble(double val)
{
    static QXmlName Name = xml::qname("double", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Double(0, Type, val));
}

ElementPtr Element::newUrl(QXmlName name, Type type, QUrl val)
{
    return ElementPtr(new Element_Url(name, type, val));
}

ElementPtr Element::newUrl(QUrl val)
{
    static QXmlName Name = xml::qname("anyURI", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Url(0, Type, val));
}

ElementPtr Element::newDate(QXmlName name, Type type, const QDateTime &val)
{
    return ElementPtr(new Element_DateTime(name, type, val));
}

ElementPtr Element::newDate(const QDateTime &val)
{
    static QXmlName Name = xml::qname("DateTime", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_DateTime(0, Type, val));
}

ElementPtr Element::newDate(QXmlName name, Type type, const QDate &val)
{
    return ElementPtr(new Element_Date(name, type, val));
}

ElementPtr Element::newDate(const QDate &val)
{
    static QXmlName Name = xml::qname("Date", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Date(0, Type, val));
}

ElementPtr Element::newTime(QXmlName name, Type type, const QTime &val)
{
    return ElementPtr(new Element_Time(name, type, val));
}

ElementPtr Element::newTime(const QTime &val)
{
    static QXmlName Name = xml::qname("Time", xml::xschema_uri);
    static Type Type = Type::stdType(Name);

    return ElementPtr(new Element_Time(0, Type, val));
}


ElementPtr Element::elementContent() const
{
    return elementClone();
}

QString Element::elementText() const
{
    return QString();
}


bool Element::isElementValid() const
{
    return true;
}

bool Element::isElementSimple() const
{
    return true;
}

bool Element::isElementEmpty() const
{
    return false;
}

bool Element::isElementMixed() const
{
    bool hasElem = false;
    bool hasText = false;

    int count = elementCount();
    for (int i = 0; i < count; i++) {
        if (element(i)->isElementSimple())
            hasText = true;
        else
            hasElem = true;
    }
    return (hasElem && hasText);
}

bool Element::isElementList() const
{
    return false;
}

bool Element::isElementEqual(const ElementPtr ) const
{
    return false;
}

QString Element::elementXmlText() const
{
    QString s;
    QXmlStreamWriter stream(&s);
    elementWriteXml(stream, m_name, QList<QString>());
    return s;
}

void Element::elementWriteXml(QXmlStreamWriter &stream, QXmlName xmlName, QList<QString> ns) const
{
    if (xmlName == 0) {
        stream.writeCharacters(elementText());
    } else {
        QString uri(xmlName->namespaceUri());
        if (ns.isEmpty()) {
            stream.writeDefaultNamespace(uri);
            ns.append(uri);
        } else if (!ns.contains(uri)) {
            QString prefix(xmlName->prefix());
            stream.writeNamespace(uri, prefix);
            ns.append(uri);
        }

        if (isElementSimple()) {
            stream.writeStartElement(xmlName->namespaceUri(), xmlName->name());
            stream.writeCharacters(elementText());
            stream.writeEndElement();
        } else {
            QList<QXmlName> attrNames = attributeNames();
            int countElems = elementCount();

            for (int i = 0; i < attrNames.size(); ++i) {
                if (hasAttribute(attrNames.at(i))) {
                    uri = attrNames.at(i)->namespaceUri();
                    if (!ns.contains(uri)) {
                        QString prefix = attrNames.at(i)->prefix();
                        stream.writeNamespace(uri, prefix);
                        ns.append(uri);
                    }
                }
            }

            for (int i = 0; i < countElems; i++) {
                ElementPtr e = element(i);
                QXmlName name = e->elementName();
                if (name) {
                    uri = name->namespaceUri();
                    if (!ns.contains(uri)) {
                        QString prefix = name->prefix();
                        stream.writeNamespace(uri, prefix);
                        ns.append(uri);
                    }
                }
            }

            stream.writeStartElement(xmlName->namespaceUri(), xmlName->name());
            //qDebug("elem: %s", qPrintable(xmlName->localName()));

            for (int i = 0; i < attrNames.size(); ++i) {
                if (hasAttribute(attrNames.at(i))) {
                    QString uri(attrNames.at(i)->namespaceUri());
                    if (ns.size() > 0 && ns.at(0) == uri) {
                        // fix default namespace for attributes
                        stream.writeAttribute(attrNames.at(i)->name(), attribute(attrNames.at(i))->elementText());
                    } else {
                        stream.writeAttribute(uri, attrNames.at(i)->name(), attribute(attrNames.at(i))->elementText());
                    }
                }
            }

            bool appendSpace = false;
            for (int i = 0; i < countElems; i++) {
                ElementPtr e = element(i);
                if (e->elementName() == 0) {
                    if (appendSpace)
                        stream.writeCharacters(" ");

                    QString txt = e->elementText();
                    if (!txt.isEmpty()) {
                        stream.writeCharacters(txt);
                        if (txt.at(txt.size()-1).isSpace())
                            appendSpace = false;
                        else
                            appendSpace = true;
                    }
                } else {
                    e->elementWriteXml(stream, e->elementName(), ns);
                    appendSpace = false;
                }
            }
            stream.writeEndElement();
        }
    }
}

QList<QString> Element::elementMethods() const
{
    static QList<QString> ls;

    if (ls.isEmpty()) {
        const QMetaObject *mo = metaObject();
        int cnt = mo->methodCount();
        for (int i = 0; i < cnt; ++i) {
            QMetaMethod md = mo->method(i);
            if (md.access() == QMetaMethod::Public) {
                ls.append(QString::fromLatin1(md.name()));
            }
        }
    }
    return ls;
}


QVariant Element::callMethod(const QString &name, QList<QVariant> args)
{
    const QMetaObject *mo = metaObject();
    int mcnt = mo->methodCount();
    for (int i = 0; i < mcnt; ++i) {
        QMetaMethod md = mo->method(i);
        if (md.access() == QMetaMethod::Public) {
            int pcnt = md.parameterCount();
            if (pcnt <= args.size() && name == QString::fromLatin1(md.name())) {
                QGenericArgument arg[10];
                QGenericReturnArgument ret;
                union {
                    bool v_bool;
                    char v_char;
                    signed char v_schar;
                    unsigned char v_uchar;
                    short v_short;
                    unsigned short v_ushort;
                    int v_int;
                    unsigned int v_uint;
                    long v_long;
                    unsigned long v_ulong;
                    int64_t v_longlong;
                    uint64_t v_ulonglong;
                    float v_float;
                    double v_double;
                } x;
                QByteArray v_bytearray;
                QString v_string;
                QUrl v_url;
                QDate v_date;
                QTime v_time;
                QDateTime v_datetime;
                QVariant v_variant;
                ElementPtr v_elem;

                for (int i = 0; i < pcnt; ++i) {
                    int tid = md.parameterType(i);
                    switch (tid) {
                    case QMetaType::Bool:
                        if (args.at(i).canConvert<bool>()) {
                            arg[i] = Q_ARG(bool, args.at(i).value<bool>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::Char:
                        if (args.at(i).canConvert<char>()) {
                            arg[i] = Q_ARG(char, args.at(i).value<char>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::SChar:
                        if (args.at(i).canConvert<signed char>()) {
                            arg[i] = Q_ARG(signed char, args.at(i).value<signed char>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::UChar:
                        if (args.at(i).canConvert<unsigned char>()) {
                            arg[i] = Q_ARG(unsigned char, args.at(i).value<unsigned char>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::Short:
                        if (args.at(i).canConvert<short>()) {
                            arg[i] = Q_ARG(short, args.at(i).value<short>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::UShort:
                        if (args.at(i).canConvert<unsigned short>()) {
                            arg[i] = Q_ARG(unsigned short, args.at(i).value<unsigned short>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::Int:
                        if (args.at(i).canConvert<int>()) {
                            arg[i] = Q_ARG(int, args.at(i).value<int>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::UInt:
                        if (args.at(i).canConvert<unsigned int>()) {
                            arg[i] = Q_ARG(unsigned int, args.at(i).value<unsigned int>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::Long:
                        if (args.at(i).canConvert<long>()) {
                            arg[i] = Q_ARG(long, args.at(i).value<long>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::ULong:
                        if (args.at(i).canConvert<unsigned long>()) {
                            arg[i] = Q_ARG(unsigned long, args.at(i).value<unsigned long>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::LongLong:
                        if (args.at(i).canConvert<qlonglong>()) {
                            arg[i] = Q_ARG(qlonglong, args.at(i).value<qlonglong>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::ULongLong:
                        if (args.at(i).canConvert<qulonglong>()) {
                            arg[i] = Q_ARG(qulonglong, args.at(i).value<qulonglong>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::Float:
                        if (args.at(i).canConvert<float>()) {
                            arg[i] = Q_ARG(float, args.at(i).value<float>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::Double:
                        if (args.at(i).canConvert<double>()) {
                            arg[i] = Q_ARG(double, args.at(i).value<double>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::QByteArray:
                        if (args.at(i).canConvert<QByteArray>()) {
                            arg[i] = Q_ARG(QByteArray, args.at(i).value<QByteArray>());
                        } else {
                            goto next;
                        }
                        break;
                        //case QMetaType::QChar:
                    case QMetaType::QString:
                        if (args.at(i).canConvert<QString>()) {
                            arg[i] = Q_ARG(QString, args.at(i).value<QString>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::QUrl:
                        if (args.at(i).canConvert<QUrl>()) {
                            arg[i] = Q_ARG(QUrl, args.at(i).value<QUrl>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::QDateTime:
                        if (args.at(i).canConvert<QDateTime>()) {
                            arg[i] = Q_ARG(QDateTime, args.at(i).value<QDateTime>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::QDate:
                        if (args.at(i).canConvert<QDate>()) {
                            arg[i] = Q_ARG(QDate, args.at(i).value<QDate>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::QTime:
                        if (args.at(i).canConvert<QTime>()) {
                            arg[i] = Q_ARG(QTime, args.at(i).value<QTime>());
                        } else {
                            goto next;
                        }
                        break;
                    case QMetaType::QVariant:
                        arg[i] = Q_ARG(QVariant, args.at(i));
                        break;
                    default:
                        if (args.at(i).canConvert<ElementPtr>()) {
                            arg[i] = Q_ARG(ElementPtr, args.at(i).value<ElementPtr>());
                        } else {
                            goto next;
                        }
                        break;
                    }
                } /* for */

                int tid = md.returnType();
                switch (tid) {
                case QMetaType::Bool:
                    ret = Q_RETURN_ARG(bool, x.v_bool);
                    break;
                case QMetaType::Char:
                    ret = Q_RETURN_ARG(char, x.v_char);
                    break;
                case QMetaType::SChar:
                    ret = Q_RETURN_ARG(signed char, x.v_schar);
                    break;
                case QMetaType::UChar:
                    ret = Q_RETURN_ARG(unsigned char, x.v_uchar);
                    break;
                case QMetaType::Short:
                    ret = Q_RETURN_ARG(short, x.v_short);
                    break;
                case QMetaType::UShort:
                    ret = Q_RETURN_ARG(unsigned short, x.v_ushort);
                    break;
                case QMetaType::Int:
                    ret = Q_RETURN_ARG(int, x.v_int);
                    break;
                case QMetaType::UInt:
                    ret = Q_RETURN_ARG(unsigned int, x.v_uint);
                    break;
                case QMetaType::Long:
                    ret = Q_RETURN_ARG(long, x.v_long);
                    break;
                case QMetaType::ULong:
                    ret = Q_RETURN_ARG(unsigned long, x.v_ulong);
                    break;
                case QMetaType::LongLong:
                    ret = Q_RETURN_ARG(int64_t, x.v_longlong);
                    break;
                case QMetaType::ULongLong:
                    ret = Q_RETURN_ARG(uint64_t, x.v_ulonglong);
                    break;
                case QMetaType::Float:
                    ret = Q_RETURN_ARG(float, x.v_float);
                    break;
                case QMetaType::Double:
                    ret = Q_RETURN_ARG(double, x.v_double);
                    break;
                case QMetaType::QByteArray:
                    ret = Q_RETURN_ARG(QByteArray, v_bytearray);
                    break;
                    //case QMetaType::QChar:
                case QMetaType::QString:
                    ret = Q_RETURN_ARG(QString, v_string);
                    break;
                case QMetaType::QUrl:
                    ret = Q_RETURN_ARG(QUrl, v_url);
                    break;
                case QMetaType::QDateTime:
                    ret = Q_RETURN_ARG(QDateTime, v_datetime);
                    break;
                case QMetaType::QDate:
                    ret = Q_RETURN_ARG(QDate, v_date);
                    break;
                case QMetaType::QTime:
                    ret = Q_RETURN_ARG(QTime, v_time);
                    break;
                case QMetaType::QVariant:
                    ret = Q_RETURN_ARG(QVariant, v_variant);
                    break;
                default:
                    if (tid == qMetaTypeId<ElementPtr>()) {
                        ret = Q_RETURN_ARG(ElementPtr, v_elem);
                    }
                    break;
                }

                if (md.invoke(this, ret, arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8], arg[9])) {
                    switch (tid) {
                    case QMetaType::Bool:
                        return x.v_bool;
                    case QMetaType::Char:
                        return x.v_char;
                    case QMetaType::SChar:
                        return x.v_schar;
                    case QMetaType::UChar:
                        return x.v_uchar;
                    case QMetaType::Short:
                        return x.v_short;
                    case QMetaType::UShort:
                        return x.v_ushort;
                    case QMetaType::Int:
                        return QVariant::fromValue(x.v_int);
                    case QMetaType::UInt:
                        return QVariant::fromValue(x.v_uint);
                    case QMetaType::Long:
                        return QVariant::fromValue(x.v_long);
                    case QMetaType::ULong:
                        return QVariant::fromValue(x.v_ulong);
                    case QMetaType::LongLong:
                        return QVariant::fromValue(x.v_longlong);
                    case QMetaType::ULongLong:
                        return QVariant::fromValue(x.v_ulonglong);
                    case QMetaType::Float:
                        return QVariant::fromValue(x.v_float);
                    case QMetaType::Double:
                        return QVariant::fromValue(x.v_double);
                    case QMetaType::QByteArray:
                        return QVariant::fromValue(v_bytearray);
                    case QMetaType::QString:
                        return QVariant::fromValue(v_string);
                    case QMetaType::QUrl:
                        return QVariant::fromValue(v_url);
                    case QMetaType::QDateTime:
                        return QVariant::fromValue(v_datetime);
                    case QMetaType::QDate:
                        return QVariant::fromValue(v_date);
                    case QMetaType::QTime:
                        return QVariant::fromValue(v_time);
                    case QMetaType::QVariant:
                        return v_variant;
                    default:
                        if (tid == qMetaTypeId<ElementPtr>()) {
                            return QVariant::fromValue(v_elem);
                        }
                        break;
                    }
                    return QVariant();
                }
            }
        }
    next:;
    }

    return QVariant();
}


Element_ByteArray::~Element_ByteArray()
{
}

QVariant Element_ByteArray::elementToVariant() const
{
    return m_val;
}

bool Element_ByteArray::elementToByteArray(QByteArray *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_ByteArray::elementToString(QString *val) const
{
    //qDebug("Element_ByteArray::elementToString()");
    if (val) {
        *val = elementText();
    }
    return true;
}

QString Element_ByteArray::elementText() const
{
    if (m_type.isValid()) {
        QSharedPointer<TypeFactory> fac = m_type.factory();
        return fac->textContent(this);
    }
    return QString::fromLatin1(m_val.toHex());
}

bool Element_ByteArray::isElementEmpty() const
{
    return m_val.isEmpty();
}

bool Element_ByteArray::isElementEqual(const ElementPtr elem) const
{
    QByteArray val;
    if (elem && elem->elementToByteArray(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_ByteArray::elementClone() const
{
    return ElementPtr(new Element_ByteArray(m_name, m_type, m_val));
}


Element_QName::~Element_QName()
{
}

QVariant Element_QName::elementToVariant() const
{
    return QVariant::fromValue(m_val);
}

bool Element_QName::elementToString(QString *val) const
{
    if (val)
        *val = m_val->toString();
    return true;
}

bool Element_QName::elementToQName(QXmlName *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_QName::elementText() const
{
    return m_val->toString();
}

bool Element_QName::isElementEmpty() const
{
    return (m_val == 0);
}

bool Element_QName::isElementEqual(const ElementPtr elem) const
{
    QXmlName val;
    if (elem && elem->elementToQName(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_QName::elementClone() const
{
    return ElementPtr(new Element_QName(m_name, m_type, m_val));
}


QVariant Element_Bool::elementToVariant() const
{
    return m_val;
}

bool Element_Bool::elementToString(QString *val) const
{
    if (val)
        *val = elementText();
    return true;
}

bool Element_Bool::elementToBool(bool *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Bool::elementText() const
{
    static QString s_true = QString::fromLatin1("true");
    static QString s_false = QString::fromLatin1("false");

    return (m_val ? s_true : s_false);
}

bool Element_Bool::isElementEqual(const ElementPtr elem) const
{
    bool val;
    if (elem && elem->elementToBool(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Bool::elementClone() const
{
    return ElementPtr(new Element_Bool(m_name, m_type, m_val));
}



QVariant Element_Int::elementToVariant() const
{
    return m_val;
}

bool Element_Int::elementToString(QString *val) const
{
    if (val)
        val->setNum(m_val);
    return true;
}

bool Element_Int::elementToBool(bool *val) const
{
    if (val)
        *val = (m_val != 0);
    return true;
}

bool Element_Int::elementToInt(int32_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Int::elementToUInt(uint32_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Int::elementToLong(int64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Int::elementToULong(uint64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Int::elementToFloat(float *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Int::elementToDouble(double *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Int::elementText() const
{
    return QString::number(m_val);
}

bool Element_Int::isElementEqual(const ElementPtr elem) const
{
    int32_t val;
    if (elem && elem->elementToInt(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Int::elementClone() const
{
    return ElementPtr(new Element_Int(m_name, m_type, m_val));
}


QVariant Element_UInt::elementToVariant() const
{
    return m_val;
}

bool Element_UInt::elementToString(QString *val) const
{
    if (val)
        val->setNum(m_val);
    return true;
}

bool Element_UInt::elementToBool(bool *val) const
{
    if (val)
        *val = (m_val != 0);
    return true;
}

bool Element_UInt::elementToInt(int32_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_UInt::elementToUInt(uint32_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_UInt::elementToLong(int64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_UInt::elementToULong(uint64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_UInt::elementToFloat(float *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_UInt::elementToDouble(double *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_UInt::elementText() const
{
    return QString::number(m_val);
}

bool Element_UInt::isElementEqual(const ElementPtr elem) const
{
    uint32_t val;
    if (elem && elem->elementToUInt(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_UInt::elementClone() const
{
    return ElementPtr(new Element_UInt(m_name, m_type, m_val));
}


QVariant Element_Long::elementToVariant() const
{
    return QVariant::fromValue(m_val);
}

bool Element_Long::elementToString(QString *val) const
{
    if (val)
        val->setNum(m_val);
    return true;
}

bool Element_Long::elementToBool(bool *val) const
{
    if (val)
        *val = (m_val != 0);
    return true;
}

bool Element_Long::elementToInt(int32_t *val) const
{
    if (val)
        *val = (int32_t) m_val;
    return true;
}

bool Element_Long::elementToUInt(uint32_t *val) const
{
    if (val)
        *val = (uint32_t) m_val;
    return true;
}

bool Element_Long::elementToLong(int64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Long::elementToULong(uint64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Long::elementToFloat(float *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Long::elementToDouble(double *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Long::elementText() const
{
    return QString::number(m_val);
}

bool Element_Long::isElementEqual(const ElementPtr elem) const
{
    int64_t val;
    if (elem && elem->elementToLong(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Long::elementClone() const
{
    return ElementPtr(new Element_Long(m_name, m_type, m_val));
}


QVariant Element_ULong::elementToVariant() const
{
    return QVariant::fromValue(m_val);
}

bool Element_ULong::elementToString(QString *val) const
{
    if (val)
        val->setNum(m_val);
    return true;
}

bool Element_ULong::elementToBool(bool *val) const
{
    if (val)
        *val = (m_val != 0);
    return true;
}

bool Element_ULong::elementToInt(int32_t *val) const
{
    if (val)
        *val = (int32_t) m_val;
    return true;
}

bool Element_ULong::elementToUInt(uint32_t *val) const
{
    if (val)
        *val = (uint32_t) m_val;
    return true;
}

bool Element_ULong::elementToLong(int64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_ULong::elementToULong(uint64_t *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_ULong::elementToFloat(float *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_ULong::elementToDouble(double *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_ULong::elementText() const
{
    return QString::number(m_val);
}

bool Element_ULong::isElementEqual(const ElementPtr elem) const
{
    uint64_t val;
    if (elem && elem->elementToULong(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_ULong::elementClone() const
{
    return ElementPtr(new Element_ULong(m_name, m_type, m_val));
}


QVariant Element_Float::elementToVariant() const
{
    return m_val;
}

bool Element_Float::elementToString(QString *val) const
{
    if (val)
        val->setNum(m_val);
    return true;
}

bool Element_Float::elementToInt(int32_t *val) const
{
    if (val)
        *val = (int32_t) m_val;
    return true;
}

bool Element_Float::elementToUInt(uint32_t *val) const
{
    if (val)
        *val = (uint32_t) m_val;
    return true;
}

bool Element_Float::elementToLong(int64_t *val) const
{
    if (val)
        *val = (int64_t) m_val;
    return true;
}

bool Element_Float::elementToULong(uint64_t *val) const
{
    if (val)
        *val = (uint64_t) m_val;
    return true;
}

bool Element_Float::elementToFloat(float *val) const
{
    if (val)
        *val = m_val;
    return true;
}

bool Element_Float::elementToDouble(double *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Float::elementText() const
{
    return QString::number(m_val);
}

bool Element_Float::isElementEqual(const ElementPtr elem) const
{
    float val;
    if (elem && elem->elementToFloat(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Float::elementClone() const
{
    return ElementPtr(new Element_Float(m_name, m_type, m_val));
}


QVariant Element_Double::elementToVariant() const
{
    return m_val;
}

bool Element_Double::elementToString(QString *val) const
{
    if (val)
        val->setNum(m_val);
    return true;
}

bool Element_Double::elementToInt(int32_t *val) const
{
    if (val)
        *val = (int32_t) m_val;
    return true;
}

bool Element_Double::elementToLong(int64_t *val) const
{
    if (val)
        *val = (int64_t) m_val;
    return true;
}

bool Element_Double::elementToFloat(float *val) const
{
    if (val)
        *val = (float) m_val;
    return true;
}

bool Element_Double::elementToDouble(double *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Double::elementText() const
{
    return QString::number(m_val);
}

bool Element_Double::isElementEqual(const ElementPtr elem) const
{
    double val;
    if (elem && elem->elementToDouble(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Double::elementClone() const
{
    return ElementPtr(new Element_Double(m_name, m_type, m_val));
}


Element_Url::~Element_Url()
{
}

QVariant Element_Url::elementToVariant() const
{
    return m_val;
}

bool Element_Url::elementToString(QString *val) const
{
    if (val)
        *val = m_val.toString();
    return true;
}

bool Element_Url::elementToURI(QUrl *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Url::elementText() const
{
    return m_val.toString();
}

bool Element_Url::isElementEmpty() const
{
    return !m_val.isValid();
}

bool Element_Url::isElementEqual(const ElementPtr elem) const
{
    QUrl val;
    if (elem && elem->elementToUrl(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Url::elementClone() const
{
    return ElementPtr(new Element_Url(m_name, m_type, m_val));
}


Element_DateTime::~Element_DateTime()
{
}

QVariant Element_DateTime::elementToVariant() const
{
    return m_val;
}

bool Element_DateTime::elementToString(QString *val) const
{
    if (val)
        *val = m_val.toString(Qt::ISODate);
    return true;
}

bool Element_DateTime::elementToDate(QDateTime *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_DateTime::elementText() const
{
    return m_val.toString(Qt::ISODate);
}

bool Element_DateTime::isElementEmpty() const
{
    return !m_val.isValid();
}

bool Element_DateTime::isElementEqual(const ElementPtr elem) const
{
    QDateTime val;
    if (elem && elem->elementToDate(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_DateTime::elementClone() const
{
    return ElementPtr(new Element_DateTime(m_name, m_type, m_val));
}


Element_Date::~Element_Date()
{
}

QVariant Element_Date::elementToVariant() const
{
    return m_val;
}

bool Element_Date::elementToString(QString *val) const
{
    if (val)
        *val = m_val.toString(Qt::ISODate);
    return true;
}

bool Element_Date::elementToDate(QDate *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Date::elementText() const
{
    return m_val.toString(Qt::ISODate);
}

bool Element_Date::isElementEmpty() const
{
    return !m_val.isValid();
}

bool Element_Date::isElementEqual(const ElementPtr elem) const
{
    QDate val;
    if (elem && elem->elementToDate(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Date::elementClone() const
{
    return ElementPtr(new Element_Date(m_name, m_type, m_val));
}


Element_Time::~Element_Time()
{
}

QVariant Element_Time::elementToVariant() const
{
    return m_val;
}

bool Element_Time::elementToString(QString *val) const
{
    if (val)
        *val = m_val.toString();
    return true;
}

bool Element_Time::elementToTime(QTime *val) const
{
    if (val)
        *val = m_val;
    return true;
}

QString Element_Time::elementText() const
{
    return m_val.toString();
}

bool Element_Time::isElementEmpty() const
{
    return !m_val.isValid();
}

bool Element_Time::isElementEqual(const ElementPtr elem) const
{
    QTime val;
    if (elem && elem->elementToTime(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Time::elementClone() const
{
    return ElementPtr(new Element_Time(m_name, m_type, m_val));
}



Element_Variant::Element_Variant(const QVariant &val) :
    Element(0, Type::anyType()),
    m_val(val)
{
}

Element_Variant::~Element_Variant()
{
}

QVariant Element_Variant::elementToVariant() const
{
    return m_val;
}

bool Element_Variant::elementToString(QString *val) const
{
    if (val)
        *val = m_val.toString();
    return true;
}

QString Element_Variant::elementText() const
{
    return m_val.toString();
}

bool Element_Variant::isElementEmpty() const
{
    return !m_val.isValid();
}

bool Element_Variant::isElementEqual(const ElementPtr elem) const
{
    QTime val;
    if (elem && elem->elementToTime(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_Variant::elementClone() const
{
    return ElementPtr(new Element_Variant(m_name, m_type, m_val));
}



Element_String::Element_String(const QString &val) :
    Element(0, Type::stdType(xml::qname("string", xml::xschema_uri))),
    m_val(val)
{
}

Element_String::~Element_String()
{
}

QVariant Element_String::elementToVariant() const
{
    return m_val;
}

bool Element_String::elementToString(QString *val) const
{
    //qDebug("Element_String::elementToString()");
    if (val)
        *val = m_val;
    return true;
}

QString Element_String::elementText() const
{
    return m_val;
}

bool Element_String::isElementEmpty() const
{
    for (int i = 0; i < m_val.size(); ++i) {
        if (!m_val.at(i).isSpace())
            return false;
    }
    return true;
}

bool Element_String::isElementEqual(const ElementPtr elem) const
{
    QString val;
    if (elem && elem->elementToString(&val)) {
        return m_val == val;
    }
    return false;
}

ElementPtr Element_String::elementClone() const
{
    return ElementPtr(new Element_String(m_name, m_type, m_val));
}



Element_List::Element_List() :
    Element(0, Type::anyType())
{
}

Element_List::Element_List(QXmlName name, Type type) :
    Element(name, type)
{
}

Element_List::Element_List(QXmlName name, Type type, const QList<ElementPtr> &elems) :
    Element(name, type),
    m_elements(elems)
{
}

Element_List::~Element_List()
{
}

bool Element_List::isElementList() const
{
    return true;
}

int Element_List::elementCount() const
{
    return m_elements.size();
}

ElementPtr Element_List::element(int index) const
{
    return m_elements.value(index);
}

bool Element_List::elementAppend(const ElementPtr &value)
{
    m_elements.append(value);
    return true;
}

bool Element_List::elementToString(QString *val) const
{
    QString text;

    int count = elementCount();
    for (int i = 0; i < count; i++) {
        ElementPtr e = element(i);
        if (e->isElementSimple()) {
            QString t;
            if (e->elementToString(&t)) {
                if (!t.isEmpty()) {
                    if (!text.isEmpty() && !text.at(text.size()-1).isSpace() && !t.at(0).isSpace()) {
                        text += QChar::fromLatin1(' ');
                    }
                    text += t;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    if (val) {
        *val = text;
    }
    return true;
}

ElementPtr Element_List::elementContent() const
{
    ElementPtr data(new Element_List());

    int count = elementCount();
    for (int i = 0; i < count; i++) {
        data->elementAppend(element(i));
    }
    return data;
}

QString Element_List::elementText() const
{
    QString text;

    int count = elementCount();
    for (int i = 0; i < count; i++) {
        QString t = element(i)->elementText();
        if (!t.isEmpty()) {
            if (!text.isEmpty() && !text.at(text.size()-1).isSpace() && !t.at(0).isSpace()) {
                text += QChar::fromLatin1(' ');
            }
            text += t;
        }
    }
    return text;
}

bool Element_List::isElementSimple() const
{
    int count = elementCount();
    for (int i = 0; i < count; i++) {
        if (!element(i)->isElementSimple()) {
            return false;
        }
    }
    return true;
}

bool Element_List::isElementEmpty() const
{
    int count = elementCount();
    for (int i = 0; i < count; i++) {
        if (!element(i)->isElementEmpty())
            return false;
    }
    return true;
}

bool Element_List::isElementEqual(const ElementPtr &val) const
{
    if (m_type != val->m_type)
        return true;

    if (!val->isElementList())
        return false;

    int count = elementCount();
    if (val->elementCount() != count)
        return false;

    for (int i = 0; i < count; i++) {
        if (!element(i)->isElementEqual(val->element(i))) {
            return false;
        }
    }
    return true;
}

ElementPtr Element_List::elementClone() const
{
    return ElementPtr(new Element_List(m_name, m_type, m_elements));
}


Element_Complex::Element_Complex(QXmlName name, Type type) :
    Element_List(name, type)
{
}

Element_Complex::Element_Complex(QXmlName name, Type type, const QList<ElementPtr> &elements, const QMap<QXmlName, ElementPtr> &attr) :
    Element_List(name, type, elements),
    m_attributes(attr)
{
}

Element_Complex::~Element_Complex()
{
}

QList<QXmlName> Element_Complex::attributeNames() const
{
    QList<QXmlName> names = Element_List::attributeNames();

    for (QMap<QXmlName, ElementPtr>::const_iterator i = m_attributes.begin(); i != m_attributes.end(); ++i) {
        if (!names.contains(i.key()))
            names << i.key();
    }
    return names;
}

bool Element_Complex::hasAttribute(QXmlName name) const
{
    return m_attributes.contains(name);
}

bool Element_Complex::attributeFixed(QXmlName name) const
{
    if (m_type.isValid()) {
        QList<TypeAttribute> as = m_type.attributes();
        for (int i = 0; i < as.size(); i++) {
            TypeAttribute attr = as.at(i);
            if (name == attr.name()) {
                if (attr.use() == USE_FIXED || attr.use() == USE_FIXED_REQUIRED)
                    return true;
            }
        }
    }
    return false;
}

ElementPtr Element_Complex::attribute(QXmlName name) const
{
    return m_attributes.value(name);
}

bool Element_Complex::setAttribute(QXmlName name, ElementPtr value)
{
    if (!attributeFixed(name)) {
        m_attributes.insert(name, value);
        return true;
    }
    return false;
}

bool Element_Complex::elementToString(QString *val) const
{
    //qDebug("Element_Complex::elementToString()");
    if (!m_attributes.isEmpty())
        return false;

    return Element_List::elementToString(val);
}

bool Element_Complex::isElementSimple() const
{
    if (!m_attributes.isEmpty())
        return false;

    return Element_List::isElementSimple();
}

bool Element_Complex::isElementEmpty() const
{
    if (!m_attributes.isEmpty())
        return false;

    return Element_List::isElementEmpty();
}

bool Element_Complex::isElementEqual(const ElementPtr val) const
{
    if (!val || !Element_List::isElementEqual(val))
        return false;

    QList<QXmlName> names = val->attributeNames();
    QList<QXmlName> ownNames = attributeNames();
    if (names.size() != ownNames.size())
        return false;

    for (int i = 0; i < names.size(); ++i) {
        QXmlName name = names.at(i);
        if (!hasAttribute(name))
            return false;
        if (attribute(name)->isElementEqual(val->attribute(name)))
            return false;
    }

    return true;
}

ElementPtr Element_Complex::elementClone() const
{
    return ElementPtr(new Element_Complex(m_name, m_type, m_elements, m_attributes));
}

};

 /* End of code */
