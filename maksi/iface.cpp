/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2013-2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    iface.cpp
 * @author  ivan demakov <ivan.demakov@gmail.com>
 * @date    Thu Aug 15 05:00:28 2013
 *
 * @brief   maksi public interface
 *
 */

#include "iface.h"
#include "maksi.h"
#include "elem.h"
#include "script.h"
#include "xml.h"


#include <QMutex>
#include <QMap>


static QMutex maksi_mutex;
static QMap<QString, maksi_t> maksi_instances;

namespace maksi
{

maksi_t
newInstance(const QString &name)
{
    QMutexLocker locker(&maksi_mutex);

    static int registered = 0;
    if (!registered) {
        registered++;

        qRegisterMetaType<maksi_t>();

        qRegisterMetaType<maksi::name_t>();
        qRegisterMetaType<maksi::context_t>();

        qRegisterMetaType<maksi::Type>();
        qRegisterMetaType<maksi::Element::Ptr>();
    }

    maksi_t maksi = maksi_instances.value(name);
    if (!maksi) {
        maksi = new Maksi(name);
        maksi_instances.insert(name, maksi);
    }
    return maksi;
}


void
freeInstance(maksi_t maksi)
{
    if (maksi) {
        QMutexLocker locker(&maksi_mutex);
        maksi_instances.remove(maksi->name());
        delete maksi;
    }
}


void
setMessageHandler(maksi_t maksi, void (*message)(int, const QString &))
{
    if (maksi) {
        maksi->setMessageHandler(message);
    }
}

bool
loadConfig(maksi_t maksi, const QString &conf)
{
    if (maksi) {
        return maksi->loadConfig(conf);
    }
    return false;
}


maksi::name_t
qname(const QString &name, const QString &ns_)
{
    QString ns(ns_.isEmpty() ? maksi::xml::maksi_uri : ns_);

    return maksi::xml::qname(name, ns);
}


maksi::context_t
scriptContext(maksi_t maksi, const QString &scriptName)
{
    maksi::context_t ctx = 0;
    if (maksi) {
	ctx = maksi->scriptContext(scriptName);
	if (!ctx) {
	    maksi::script_t script = maksi->script(scriptName);
	    if (script) {
		ctx = script->newContext();
		if (script->isPermanent()) {
		    maksi->addScriptContext(scriptName, ctx);
		}
	    }
	}
    }
    return ctx;
}

void
freeContext(maksi::context_t context)
{
    if (context) {
	if (!context->isPermanent()) {
	    delete context;
	}
    }
}

QVariant
arg(maksi::context_t context, const QString &name)
{
    if (context) {
        return context->arg(name);
    }
    return QVariant();
}

void
setArg(maksi::context_t context, const QString &name, const QVariant &val)
{
    //qDebug("maksi_setArg(%s, %s)", qPrintable(name), !val ? "[null]" : qPrintable(val->elementText()));
    if (context) {
        context->setArg(name, val);
    }
}

QVariant
eval(maksi::context_t context)
{
    if (context) {
        return context->eval();
    }
    return QVariant();
}

}

 /* End of code */
