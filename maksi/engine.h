/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2013, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_engine.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Tue Feb  1 16:30:53 2011
 *
 * @brief   engine interface
 *
 */

#ifndef MAKSI_ENGINE_H
#define MAKSI_ENGINE_H

#include "iface.h"
#include "elem.h"


//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT EngineInternal
{
};


class MAKSI_EXPORT Engine : public QObject
{
    Q_OBJECT;

    Engine();
public:

    /** Destructor
     */
    virtual ~Engine();


    /** Load engine
     *
     * @param maksi current maksi object
     * @param pluginName name of plugin library
     * @param engineName user defined name of the engine
     * @param baseUrl base url to load code
     *
     * @return new engine
     */
    static Engine *load(Maksi *maksi, const QString &pluginName, const QString &engineName);


    /** Get engine name
     *
     * @return engine name
     */
    const QString &name() const { return m_name; }


    /** Get engine maksi instance
     *
     * @return maksi instance
     */
    Maksi *maksi() const { return m_maksi; }


    /** Create script
     *
     * This function must be implemented in plugin class
     *
     * @param scriptName user defined name of the script
     *
     * @return new script
     */
    virtual Script *newScript(const QString &scriptName) = 0;


    /** Eval engine code
     *
     * This function must be implemented in plugin class
     *
     * @param code string with text that need be evaluated
     * @param url file where code from
     * @param line line number in the file where code from
     * @param val result of \a code evaluation stored here
     *
     * @return \a true if evaluation is succesfull, \a false otherwice
     */
    virtual bool evalCode(const QString &code, const QUrl &url, int line, QVariant *val) = 0;


    virtual EngineInternal *getInternal() const;

protected:
    /** Constructor
     *
     * @param maksi current maksi object
     * @param name engine name
     */
    Engine(Maksi *maksi, const QString &name) : m_internal(0), m_maksi(maksi), m_name(name) {}


    /** First stage of the engine initialization
     *
     * This function can be implemented in plugin class.
     * If the function returns \a false, initialization of the engine canceled.
     *
     * Default version of the function do nothing and return \a true.
     *
     * @param conf engine configuration
     */
    virtual bool preinitEngine(const MaksiElement *conf);


    /** Middle stage of the engine initialization
     *
     * The function loads engine code from the \a conf.
     *
     * @param conf engine configuration
     */
    virtual bool initEngine(const MaksiElement *conf);


    /** Last stage of the engine initialization
     *
     * This function can be implemented in plugin class.
     * If the function returns \a false, initialization of the engine canceled.
     *
     * Default version of the function do nothing and return \a true.
     *
     * @param conf engine configuration
     */
    virtual bool postinitEngine(const MaksiElement *conf);


    EngineInternal *m_internal;

private:
    Maksi *m_maksi;
    QString m_name;

    friend class Conf;
};


class MAKSI_EXPORT EnginePlugin
{
public:
    /** Destructor
     */
    virtual ~EnginePlugin();


    /** Plugin description
     *
     * @return string with short descrition (for example, name of the language implemented in plugin)
     */
    virtual QString description() const = 0;


    /** Create engine
     *
     *
     * @param engineName user name of the engine
     * @param baseUrl base url to load code
     *
     * @return new engine
     */
    virtual Engine *newEngine(Maksi *maksi, const QString &engineName) const = 0;
};

};


#define MaksiEnginePlugin_IID "su.maksi.plugin.MaksiEnginePlugin/1.0"

Q_DECLARE_INTERFACE(maksi::EnginePlugin, MaksiEnginePlugin_IID);

#endif

/* End of file */
