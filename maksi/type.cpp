/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    type.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Apr 20 15:57:08 2011
 *
 * @brief   types
 *
 */

#include "type.h"
#include "maksi.h"
#include "elem.h"
#include "xmlelem.h"

#include <QStringList>
#include <QSharedData>
#include <QDateTime>


//! maksi namespace
namespace maksi
{

class TypeParticle;

class TypeElementPrivate : public QSharedData
{
public:
    TypeElementPrivate() {}
    ~TypeElementPrivate();

    TypeGroup::compositor_t m_compositor;
    QList<QSharedPointer<TypeParticle> > m_particles;

    name_t m_name;
    Type m_type;
};

TypeElementPrivate::~TypeElementPrivate()
{
}


class TypeGroupPrivate : public QSharedData
{
public:
    TypeGroupPrivate() : m_compositor(TypeGroup::COMPOSITOR_INVALID) {}
    ~TypeGroupPrivate();

    TypeGroup::compositor_t m_compositor;
    QList<QSharedPointer<TypeParticle> > m_particles;
};

TypeGroupPrivate::~TypeGroupPrivate()
{
}


class TypePartition
{
public:
    TypePartition(TypeParticle *p, int begin) : m_begin(begin), m_end(begin), m_particle(p) {}

    int m_begin, m_end;
    TypeParticle *m_particle;
    QList<QSharedPointer<TypePartition> > m_subsequences;
};

class TypeParticle
{
    Q_DISABLE_COPY(TypeParticle);
public:
    TypeParticle() : m_minOccurs(1), m_maxOccurs(1) {}
    virtual ~TypeParticle();

    virtual bool isEmpty() const;
    virtual TypeGroup::compositor_t compositor() const;
    virtual name_t name() const;
    virtual Type type() const;
    virtual QSharedPointer<TypePartition> partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed);

    int m_minOccurs, m_maxOccurs;
};

TypeParticle::~TypeParticle()
{
}

bool TypeParticle::isEmpty() const
{
    return true;
}

TypeGroup::compositor_t TypeParticle::compositor() const
{
    return TypeGroup::COMPOSITOR_INVALID;
}

name_t TypeParticle::name() const
{
    return 0;
}

Type TypeParticle::type() const
{
    return Type();
}

QSharedPointer<TypePartition> TypeParticle::partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed)
{
    //qDebug("particle()");
    QSharedPointer<TypePartition> part(new TypePartition(this, beginPos));

    for (int i = beginPos; i < endPos; ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->isElementSimple()) {
            if (mixed || node->isElementEmpty()) {
                part->m_end = i + 1;
            } else {
                break;
            }
        } else {
            if (mixed) {
                break;
            } else {
                return QSharedPointer<TypePartition>();
            }
        }
    }
    return part;
}


class TypeParticle_Elem : public TypeParticle
{
    Q_DISABLE_COPY(TypeParticle_Elem);
public:
    explicit TypeParticle_Elem(TypeElement &e);
    virtual ~TypeParticle_Elem();

    virtual bool isEmpty() const;
    virtual name_t name() const;
    virtual Type type() const;
    virtual QSharedPointer<TypePartition> partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed);

    name_t m_elemName;
    Type m_elemType;
};

TypeParticle_Elem::TypeParticle_Elem(TypeElement &e) : m_elemName(0)
{
    m_elemName = e.elementName();
    m_elemType = e.elementType();
}

TypeParticle_Elem::~TypeParticle_Elem()
{
}

bool TypeParticle_Elem::isEmpty() const
{
    return m_maxOccurs <= 0 ? true : false;
}

name_t TypeParticle_Elem::name() const
{
    return m_elemName;
}

Type TypeParticle_Elem::type() const
{
    return m_elemType;
}

QSharedPointer<TypePartition> TypeParticle_Elem::partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed)
{
    if (!m_elemName)
        return QSharedPointer<TypePartition>();

    QSharedPointer<TypePartition> part(new TypePartition(this, beginPos));

    int num = 0;
    for (int i = beginPos; i < endPos; ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->isElementSimple()) {
            if (mixed || node->isElementEmpty())
                part->m_end = i + 1;
            else
                break;
        } else {
            if (num >= m_maxOccurs)
                break;
            if (node->elementName() == m_elemName) {
                part->m_end = i + 1;
                num += 1;
            } else {
                break;
            }
        }
    }
    if (num >= m_minOccurs) {
        return part;
    }
    return QSharedPointer<TypePartition>();
}


class TypeParticle_Group : public TypeParticle
{
    Q_DISABLE_COPY(TypeParticle_Group);
public:
    explicit TypeParticle_Group(TypeGroup g);
    virtual ~TypeParticle_Group();

    virtual bool isEmpty() const;
    virtual TypeGroup::compositor_t compositor() const = 0;

    int particleSize() const { return d ? d->m_particles.size() : 0; }
    QSharedPointer<TypeParticle> particle(int i) const { return d ? d->m_particles.at(i) : QSharedPointer<TypeParticle>(); }

protected:
    TypeGroupPrivate *d;
};

TypeParticle_Group::TypeParticle_Group(TypeGroup g) : d(g.d)
{
    if (d) d->ref.ref();
}

TypeParticle_Group::~TypeParticle_Group()
{
    if (d && !d->ref.deref())
        delete d;
}

bool TypeParticle_Group::isEmpty() const
{
    return d ? d->m_particles.isEmpty() : true;
}


class TypeParticle_All : public TypeParticle_Group
{
public:
    explicit TypeParticle_All(TypeGroup g) : TypeParticle_Group(g) {}

    virtual TypeGroup::compositor_t compositor() const;
    virtual QSharedPointer<TypePartition> partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed);
};

TypeGroup::compositor_t TypeParticle_All::compositor() const
{
    return TypeGroup::COMPOSITOR_ALL;
}

QSharedPointer<TypePartition> TypeParticle_All::partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed)
{
    if (!d)
        return QSharedPointer<TypePartition>();

    QSharedPointer<TypePartition> part(new TypePartition(this, beginPos));

    int num = 0;
    for (int i = beginPos; i < endPos; ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->isElementSimple()) {
            if (mixed || node->isElementEmpty())
                part->m_end = i + 1;
            else
                break;
        } else {
            if (num >= m_maxOccurs)
                break;

            QList<QSharedPointer<TypeParticle> > particles = d->m_particles;
            while (!particles.isEmpty()) {
                for (int n = 0; n < particles.size(); n++) {
                    QSharedPointer<TypeParticle> p(particles.at(n));
                    QSharedPointer<TypePartition> sn(p->partition(conf, part->m_end, endPos, mixed));
                    if (sn) {
                        //qDebug("all: num=%d beg=%d end=%d", num, sn->m_begin, sn->m_end);
                        part->m_subsequences.append(sn);
                        part->m_end = sn->m_end;
                        num += 1;
                        i = sn->m_end - 1;
                        particles.removeAt(n);
                        goto next;
                    }
                }
                return QSharedPointer<TypePartition>();
            next:;
            }
        }
    }
    if (num >= m_minOccurs) {
        return part;
    }
    return QSharedPointer<TypePartition>();
}


class TypeParticle_Choice : public TypeParticle_Group
{
public:
    explicit TypeParticle_Choice(TypeGroup g) : TypeParticle_Group(g) {}

    virtual TypeGroup::compositor_t compositor() const;
    virtual QSharedPointer<TypePartition> partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed);
};

TypeGroup::compositor_t TypeParticle_Choice::compositor() const
{
    return TypeGroup::COMPOSITOR_CHOICE;
}

QSharedPointer<TypePartition> TypeParticle_Choice::partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed)
{
    if (!d)
        return QSharedPointer<TypePartition>();

    QSharedPointer<TypePartition> part(new TypePartition(this, beginPos));

    int num = 0;
    for (int i = beginPos; i < endPos; ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->isElementSimple()) {
            //qDebug("choice: text='%s'", qPrintable(node.text()));
            if (mixed || node->isElementEmpty())
                part->m_end = i + 1;
            else
                break;
        } else {
            if (num >= m_maxOccurs)
                break;

            for (int n = 0; n < d->m_particles.size(); n++) {
                QSharedPointer<TypeParticle> p(d->m_particles.at(n));
                QSharedPointer<TypePartition> sn(p->partition(conf, part->m_end, endPos, mixed));
                if (sn) {
                    //qDebug("choice: num=%d beg=%d end=%d", num, sn->m_begin, sn->m_end);
                    part->m_subsequences.append(sn);
                    part->m_end = sn->m_end;
                    num += 1;
                    i = sn->m_end - 1;
                    goto next;
                }
            }
            break;
        }
    next:;
    }
    if (num >= m_minOccurs) {
        return part;
    }
    return QSharedPointer<TypePartition>();
}



class TypeParticle_Sequence : public TypeParticle_Group
{
public:
    explicit TypeParticle_Sequence(TypeGroup g) : TypeParticle_Group(g) {}

    virtual TypeGroup::compositor_t compositor() const;
    virtual QSharedPointer<TypePartition> partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed);
};

TypeGroup::compositor_t TypeParticle_Sequence::compositor() const
{
    return TypeGroup::COMPOSITOR_SEQUENCE;
}

QSharedPointer<TypePartition> TypeParticle_Sequence::partition(const MaksiElement *conf, int beginPos, int endPos, bool mixed)
{
    if (!d)
        return QSharedPointer<TypePartition>();

    QSharedPointer<TypePartition> part(new TypePartition(this, beginPos));

    int num = 0;
    for (int i = beginPos; i < endPos; ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->isElementSimple()) {
            if (mixed || node->isElementEmpty())
                part->m_end = i + 1;
            else
                break;
        } else {
            if (num >= m_maxOccurs)
                break;

            for (int n = 0; n < d->m_particles.size(); n++) {
                QSharedPointer<TypeParticle> p(d->m_particles.at(n));
                QSharedPointer<TypePartition> sn(p->partition(conf, part->m_end, endPos, mixed));
                if (sn) {
                    part->m_subsequences.append(sn);
                    part->m_end = sn->m_end;
                } else {
                    return QSharedPointer<TypePartition>();
                }
            }
            num += 1;
            i = part->m_end - 1;
        }
    }

    // try match empty subsequences
    while (num < m_minOccurs) {
        for (int n = 0; n < d->m_particles.size(); n++) {
            QSharedPointer<TypeParticle> p(d->m_particles.at(n));
            QSharedPointer<TypePartition> sn(p->partition(conf, part->m_end, part->m_end, mixed));
            if (sn) {
                part->m_subsequences.append(sn);
            } else {
                return QSharedPointer<TypePartition>();
            }
        }
        num += 1;
    }
    return part;
}


TypeElement::TypeElement() : d(0)
{
}

TypeElement::TypeElement(name_t name, Type type) : d(new TypeElementPrivate)
{
    d->ref.ref();
    d->m_name = name;
    d->m_type = type;
}

TypeElement::~TypeElement()
{
    if (d && !d->ref.deref())
        delete d;
}

TypeElement::TypeElement(const TypeElement &x) : d(x.d)
{
    if (d) d->ref.ref();
}

TypeElement &TypeElement::operator=(const TypeElement &x)
{
    if (x.d != d) {
        if (x.d)
            x.d->ref.ref();
        TypeElementPrivate *old = d;
        d = x.d;
        if (old && !old->ref.deref())
            delete old;
    }
    return *this;
}

void TypeElement::detach()
{
    if (!d) {
        d = new TypeElementPrivate;
        d->ref.ref();
    } else if (d->ref.load() != 1) {
        TypeElementPrivate *x = new TypeElementPrivate(*d);
        x->ref.ref();
        TypeElementPrivate *old = d;
        d = x;
        if (!old->ref.deref())
            delete old;
    }
}

bool TypeElement::isValid() const
{
    return d && d->m_name;
}

name_t TypeElement::elementName() const
{
    if (d)
        return d->m_name;
    return 0;
}

const Type TypeElement::elementType() const
{
    if (d)
        return d->m_type;
    return Type();
}


TypeGroup::TypeGroup() : d(0)
{
}

TypeGroup::~TypeGroup()
{
    if (d && !d->ref.deref())
        delete d;
}

TypeGroup::TypeGroup(const TypeGroup &x) : d(x.d)
{
    if (d) d->ref.ref();
}

TypeGroup &TypeGroup::operator=(const TypeGroup &x)
{
    if (x.d != d) {
        if (x.d)
            x.d->ref.ref();
        TypeGroupPrivate *old = d;
        d = x.d;
        if (old && !old->ref.deref())
            delete old;
    }
    return *this;
}

void TypeGroup::detach()
{
    if (!d) {
        d = new TypeGroupPrivate;
        d->ref.ref();
    } else if (d->ref.load() != 1) {
        TypeGroupPrivate *x = new TypeGroupPrivate(*d);
        x->ref.ref();
        TypeGroupPrivate *old = d;
        d = x;
        if (!old->ref.deref())
            delete old;
    }
}

bool TypeGroup::isValid() const
{
    return d && d->m_compositor != TypeGroup::COMPOSITOR_INVALID;
}

bool TypeGroup::isEmpty() const
{
    return d ? d->m_particles.isEmpty() : true;
}

TypeGroup::compositor_t TypeGroup::compositor() const
{
    return d ? d->m_compositor : COMPOSITOR_INVALID;
}

void TypeGroup::setCompositor(compositor_t val)
{
    detach();
    d->m_compositor = val;
}

void TypeGroup::appendParticle(QSharedPointer<TypeParticle> p)
{
    if (!p.isNull()) {
        detach();
        d->m_particles.append(p);
    }
}

void TypeGroup::appendChildParticles(QSharedPointer<TypeParticle> p)
{
    if (!p.isNull() && p->compositor() != TypeGroup::COMPOSITOR_INVALID && !p->isEmpty()) {
        detach();
        QSharedPointer<TypeParticle_Group> gr(p.staticCast<TypeParticle_Group>());
        for (int i = 0; i < gr->particleSize(); i++) {
            d->m_particles.append(gr->particle(i));
        }
    }
}


struct TypeAttributePrivate : public QSharedData
{
    name_t m_name;
    Type m_type;
    MaksiElementPtr m_value;
    use_t m_use;
};

TypeAttribute::TypeAttribute() : d(0)
{
}

TypeAttribute::TypeAttribute(name_t name, Type type) : d(new TypeAttributePrivate)
{
    d->ref.ref();
    d->m_name = name;
    d->m_type = type;
    d->m_use = USE_OPTIONAL;
}

TypeAttribute::~TypeAttribute()
{
    if (d && !d->ref.deref())
        delete d;
}

TypeAttribute::TypeAttribute(const TypeAttribute &x) : d(x.d)
{
    if (d) d->ref.ref();
}

TypeAttribute &TypeAttribute::operator=(const TypeAttribute &x)
{
    if (x.d != d) {
        if (x.d)
            x.d->ref.ref();
        TypeAttributePrivate *old = d;
        d = x.d;
        if (old && !old->ref.deref())
            delete old;
    }
    return *this;
}

void TypeAttribute::detach()
{
    if (!d) {
        d = new TypeAttributePrivate;
        d->ref.ref();
    } else if (d->ref.load() != 1) {
        TypeAttributePrivate *x = new TypeAttributePrivate(*d);
        x->ref.ref();
        TypeAttributePrivate *old = d;
        d = x;
        if (!old->ref.deref())
            delete old;
    }
}

bool TypeAttribute::isValid() const
{
    return d && d->m_name;
}

name_t TypeAttribute::name() const
{
    return d ? d->m_name : 0;
}

void TypeAttribute::setUse(use_t use)
{
    detach();
    d->m_value.clear();
    d->m_use = use;
}

void TypeAttribute::setFixed(MaksiElementPtr value)
{
    detach();
    d->m_value = value;
    d->m_use = USE_FIXED;
}

void TypeAttribute::setDefault(MaksiElementPtr value)
{
    detach();
    d->m_value = value;
    d->m_use = USE_DEFAULT;
}

Type TypeAttribute::type() const
{
    return d ? d->m_type : Type();
}

use_t TypeAttribute::use() const
{
    return d ? d->m_use : USE_OPTIONAL;
}

MaksiElementPtr TypeAttribute::value() const
{
    return d ? d->m_value : MaksiElementPtr();
}


TypeFactory::TypeFactory()
{
}

TypeFactory::~TypeFactory()
{
}

bool TypeFactory::isSimple() const
{
    return false;
}

bool TypeFactory::isAtomic() const
{
    return false;
}

bool TypeFactory::isStruct() const
{
    return false;
}

bool TypeFactory::isName() const
{
    return false;
}

Type TypeFactory::contentType() const
{
    return Type();
}

MaksiElementPtr TypeFactory::createNew(Type /*type*/, const MaksiElement * /*from*/) const
{
    return MaksiElementPtr();
}

QString TypeFactory::textContent(const MaksiElement *from) const
{
    return from->elementText();
}



bool SimpleTypeFactory::isSimple() const
{
    return true;
}


bool AtomicTypeFactory::isAtomic() const
{
    return true;
}


MaksiElementPtr StringTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString data;
    if (!from || from->elementToString(&data)) {
        return MaksiElement::newString(0, type, data);
    }
    //qDebug("from=%p, data=%s", from, qPrintable(data));
    return MaksiElementPtr();
}

MaksiElementPtr NormStringTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString data;
    if (!from) {
        return MaksiElement::newString(0, type, data);
    }
    if (from->elementToString(&data)) {
        for (int i = 0; i < data.size(); i++) {
            if (data.at(i).isSpace()) {
                data[i] = ' ';
            }
        }
        return MaksiElement::newString(0, type, data);
    }
    return MaksiElementPtr();
}

MaksiElementPtr TokenTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString data;
    if (!from) {
        return MaksiElement::newString(0, type, data);
    }
    if (from->elementToString(&data)) {
        return MaksiElement::newString(0, type, data.simplified());
    }
    return MaksiElementPtr();
}

MaksiElementPtr NMTokenTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString data;
    if (!from) {
        return MaksiElement::newString(0, type, data);
    }
    if (from->elementToString(&data)) {
        data = data.simplified();
        if (!maksi::xml::isNMToken(data))
            return MaksiElementPtr();
        return MaksiElement::newString(0, type, data);
    }
    return MaksiElementPtr();
}

bool NameTypeFactory::isName() const
{
    return true;
}

MaksiElementPtr NameTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString data;
    if (!from) {
        return MaksiElement::newString(0, type, data);
    }
    if (from->elementToString(&data)) {
        data = data.simplified();
        if (!maksi::xml::isName(data))
            return MaksiElementPtr();
        return MaksiElement::newString(0, type, data);
    }
    return MaksiElementPtr();
}

MaksiElementPtr NCNameTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString data;
    if (!from) {
        return MaksiElement::newString(0, type, data);
    }
    if (from->elementToString(&data)) {
        data = data.simplified();
        if (!maksi::xml::isNCName(data))
            return MaksiElementPtr();
        return MaksiElement::newString(0, type, data);
    }
    return MaksiElementPtr();
}

MaksiElementPtr QNameTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    name_t qname = 0;
    if (!from) {
        return MaksiElement::newQName(0, type, qname);
    }
    if (!from->elementToQName(&qname)) {
        QString name;
        if (from->elementToString(&name)) {
            name = name.simplified();
            if (name.at(0) == QLatin1Char('{')) {
                qname = maksi::xml::fromClarkName(name);
            } else {
                const Source *src = from->elementSource();
                if (!src)
                    return MaksiElementPtr();

                qname = xml::qname(name, src->namespaces());
            }
        }

        if (!qname)
            return MaksiElementPtr();
    }
    return MaksiElement::newQName(0, type, qname);
}

MaksiElementPtr HexTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QByteArray data;
    if (!from || from->elementToByteArray(&data)) {
        return MaksiElement::newByteArray(0, type, data);
    }

    QString sval;
    if (from->elementToString(&sval)) {
        data = QByteArray::fromHex(sval.simplified().toLatin1());
        return MaksiElement::newByteArray(0, type, data);
    }

    return MaksiElementPtr();
}

QString HexTypeFactory::textContent(const MaksiElement *from) const
{
    QByteArray data;
    if (from->elementToByteArray(&data)) {
        return QString::fromLatin1(data.toHex());
    }
    return QString();
}

MaksiElementPtr Base64TypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QByteArray data;
    if (!from || from->elementToByteArray(&data)) {
        return MaksiElement::newByteArray(0, type, data);
    }

    QString sval;
    if (from->elementToString(&sval)) {
        data = QByteArray::fromBase64(sval.simplified().toLatin1());
        return MaksiElement::newByteArray(0, type, data);
    }

    return MaksiElementPtr();
}

QString Base64TypeFactory::textContent(const MaksiElement *from) const
{
    QByteArray data;
    if (from->elementToByteArray(&data)) {
        return QString::fromLatin1(data.toBase64());
    }
    return QString();
}


MaksiElementPtr URITypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QUrl url;
    if (!from) {
        return MaksiElement::newUrl(0, type, url);
    }
    if (!from->elementToUrl(&url)) {
        QString sval;
        if (from->elementToString(&sval)) {
            url = QUrl(sval.trimmed(), QUrl::StrictMode);
        }
    }
    if (url.isValid()) {
        const Source *src = from->elementSource();
        if (src && url.isRelative()) {
            url = src->url().resolved(url);
        }
        return MaksiElement::newUrl(0, type, url);
    }
    return MaksiElementPtr();
}

MaksiElementPtr DateTimeTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QDateTime data;
    if (!from || from->elementToDate(&data)) {
        return MaksiElement::newDate(0, type, data);
    }

    QString sval;
    if (from->elementToString(&sval)) {
        data = QDateTime::fromString(sval.simplified(), Qt::ISODate);
        return MaksiElement::newDate(0, type, data);
    }

    return MaksiElementPtr();
}

MaksiElementPtr DateTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QDate data;
    if (!from || from->elementToDate(&data)) {
        return MaksiElement::newDate(0, type, data);
    }

    QString sval;
    if (from->elementToString(&sval)) {
        data = QDate::fromString(sval.simplified(), Qt::ISODate);
        return MaksiElement::newDate(0, type, data);
    }

    return MaksiElementPtr();
}

MaksiElementPtr TimeTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QTime data;
    if (!from || from->elementToTime(&data)) {
        return MaksiElement::newTime(0, type, data);
    }

    QString sval;
    if (from->elementToString(&sval)) {
        data = QTime::fromString(sval.simplified(), Qt::ISODate);
        return MaksiElement::newTime(0, type, data);
    }

    return MaksiElementPtr();
}

MaksiElementPtr BoolTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    bool val = false;
    if (!from || from->elementToBool(&val)) {
        return MaksiElement::newBool(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        if (str == "false" || str == "0") {
            return MaksiElement::newBool(0, type, false);
        } else if (str == "true" || str == "1") {
            return MaksiElement::newBool(0, type, true);
        }
    }

    return MaksiElementPtr();
}

MaksiElementPtr NumberTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    QString str;
    if (!from || from->elementToString(&str)) {
        return MaksiElement::newDouble(0, type, str.toDouble());
    }

    return MaksiElementPtr();
}

MaksiElementPtr LongTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    int64_t val = 0;
    if (!from || from->elementToLong(&val)) {
        return MaksiElement::newLong(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        return MaksiElement::newLong(0, type, str.toLongLong());
    }

    return MaksiElementPtr();
}

MaksiElementPtr ULongTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    uint64_t val = 0;
    if (!from || from->elementToULong(&val)) {
        return MaksiElement::newULong(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        return MaksiElement::newULong(0, type, str.toULongLong());
    }

    return MaksiElementPtr();
}

MaksiElementPtr IntTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    int32_t val = 0;
    if (!from || from->elementToInt(&val)) {
        return MaksiElement::newInt(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        return MaksiElement::newInt(0, type, str.toInt());
    }

    return MaksiElementPtr();
}

MaksiElementPtr UIntTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    uint32_t val = 0;
    if (!from || from->elementToUInt(&val)) {
        return MaksiElement::newUInt(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        return MaksiElement::newUInt(0, type, str.toUInt());
    }

    return MaksiElementPtr();
}

MaksiElementPtr FloatTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    float val = 0.0;
    if (!from || from->elementToFloat(&val)) {
        return MaksiElement::newFloat(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        return MaksiElement::newUInt(0, type, str.toFloat());
    }

    return MaksiElementPtr();
}

MaksiElementPtr DoubleTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    double val = 0.0;
    if (!from || from->elementToDouble(&val)) {
        return MaksiElement::newDouble(0, type, val);
    }

    QString str;
    if (from->elementToString(&str)) {
        return MaksiElement::newDouble(0, type, str.toDouble());
    }

    return MaksiElementPtr();
}

MaksiElementPtr ListTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    if (!from) {
        return MaksiElementPtr(new MaksiElement_List(0, type));
    }
    if (!m_itemType.isValid()) {
        return MaksiElementPtr();
    }

    if (from->isElementList() && from->isElementSimple()) {
        int count = from->elementCount();
        MaksiElementPtr ls(new MaksiElement_List(0, type));
        for (int i = 0; i < count; ++i) {
            MaksiElementPtr ss = from->element(i);
            MaksiElementPtr el = m_itemType.createNew(ss.data());
            if (el) {
                ls->elementAppend(el);
            } else {
                return MaksiElementPtr();
            }
        }
        return ls;
    }

    QString data;
    if (from->elementToString(&data)) {
        QList<QString> ds;
        int pos = -1;
        for (int i = 0; i < data.size(); i++) {
            if (data.at(i).isSpace()) {
                if (pos >= 0) {
                    ds << data.mid(pos, i-pos);
                    pos = -1;
                }
            } else {
                if (pos < 0) {
                    pos = i;
                }
            }
        }
        if (pos >= 0) {
            ds << data.mid(pos);
        }

        MaksiElementPtr ls(new MaksiElement_List(0, type));
        for (int i = 0; i < ds.size(); i++) {
            MaksiElementPtr ss = MaksiElement::newString(ds.at(i));
            MaksiElementPtr el = m_itemType.createNew(ss.data());
            if (el) {
                ls->elementAppend(el);
            } else {
                return MaksiElementPtr();
            }
        }
    }
    return MaksiElementPtr();
}

MaksiElementPtr UnionTypeFactory::createNew(Type /*type*/, const MaksiElement *from) const
{
    if (!from) {
        return MaksiElementPtr();
    }
    for (int i = 0; i < m_memberTypes.size(); i++) {
        MaksiElementPtr el = m_memberTypes.at(i).createNew(from);
        if (el) {
            return el;
        }
    }
    return MaksiElementPtr();
}

bool StructTypeFactory::isStruct() const
{
    return true;
}

Type StructTypeFactory::contentType() const
{
    return (m_baseType.isValid() ? m_baseType.contentType() : Type());
}

MaksiElementPtr StructTypeFactory::createNew(Type type, const MaksiElement *from) const
{
    if (!from) {
        return MaksiElementPtr(new MaksiElement_Complex(0, type));
    }

    name_t xmlName;
    if (!from->elementToQName(&xmlName)) {
        xmlName = from->elementName();
    }
    return MaksiElementPtr(new MaksiElement_Complex(xmlName, type));
}


class TypePrivate : public QSharedData
{
public:
    TypePrivate();
    explicit TypePrivate(TypeFactory *factory);
    ~TypePrivate();

    int is_atomic : 1;
    int is_simple : 1;
    int is_struct : 1;
    int is_empty : 1;
    int is_mixed : 1;
    Type::ws_facet_t ws_facet;

    bool isValid() const { return !m_factory.isNull(); }
    bool isName() const { return !m_factory.isNull() && m_factory->isName(); }

    void addAttribute(TypeAttribute attr) { m_attributes.append(attr); }
    QList<TypeAttribute> attributes() const { return m_attributes; }

    void setWsFacet(Type::ws_facet_t val) { ws_facet = val; }
    void addEnum(MaksiElementPtr val) { m_enums.append(val); }
    QList<MaksiElementPtr> enums() const { return m_enums; }

    Type contentType() const { return (m_factory.isNull() ? Type() : m_factory->contentType()); }

    QSharedPointer<TypeParticle> particle() const { return m_particle; }
    void setParticle(QSharedPointer<TypeParticle> p) { m_particle = p; }

    QSharedPointer<TypeFactory> &factory() { return m_factory; }
    void setFactory(QSharedPointer<TypeFactory> factory);

private:
    QList<TypeAttribute> m_attributes;
    QList<MaksiElementPtr> m_enums;
    QSharedPointer<TypeParticle> m_particle;
    QSharedPointer<TypeFactory> m_factory;

    friend class Type;
};

TypePrivate::TypePrivate()
{
    is_atomic = 0;
    is_simple = 0;
    is_struct = 0;
    is_empty = 0;
    is_mixed = 0;
    ws_facet = Type::WS_PRESERVE;
}

TypePrivate::TypePrivate(TypeFactory *factory) :
    m_factory(factory)
{
    is_atomic = 0;
    is_simple = 0;
    is_struct = 0;
    is_empty = 0;
    is_mixed = 0;
    ws_facet = Type::WS_PRESERVE;

    if (factory) {
        if (factory->isAtomic())
            is_atomic = 1;
        if (factory->isSimple())
            is_simple = 1;
        if (factory->isStruct())
            is_struct = 1;
    }
}

TypePrivate::~TypePrivate()
{
}

void TypePrivate::setFactory(QSharedPointer<TypeFactory> factory)
{
    m_factory = factory;
    if (m_factory) {
        is_atomic = (m_factory->isAtomic() ? 1 : 0);
        is_simple = (m_factory->isSimple() ? 1 : 0);
        is_struct = (m_factory->isStruct() ? 1 : 0);
    }
}

Type::Type() : d(0)
{
}

Type::Type(TypePrivate *x) : d(x)
{
    if (d) d->ref.ref();
}

Type::~Type()
{
    if (d && !d->ref.deref())
        delete d;
}

Type::Type(const Type &x) : d(x.d)
{
    if (d) d->ref.ref();
}

Type &Type::operator=(const Type &x)
{
    if (x.d != d) {
        if (x.d)
            x.d->ref.ref();
        TypePrivate *old = d;
        d = x.d;
        if (old && !old->ref.deref())
            delete old;
    }
    return *this;
}

void Type::clear()
{
    if (d) {
        if (!d->ref.deref())
            delete d;
        d = 0;
    }
}

Type Type::clone()
{
    Type type;
    if (d) {
        type.d = new TypePrivate(*d);
        type.d->ref.ref();
    }
    return type;
}

bool Type::isValid() const
{
    return d && d->isValid();
}

bool Type::isAtomic() const
{
    return d && d->is_atomic;
}

bool Type::isSimple() const
{
    return d && d->is_simple;
}

bool Type::isStruct() const
{
    return d && d->is_struct;
}

bool Type::isEmpty() const
{
    return d && d->is_empty;
}

bool Type::isMixed() const
{
    return d && d->is_mixed;
}

bool Type::isName() const
{
    return d && d->isName();
}

QSharedPointer<TypeFactory> Type::factory() const
{
    return d ? d->m_factory : QSharedPointer<TypeFactory>();
}

void Type::setFactory(QSharedPointer<TypeFactory> factory)
{
    if (!d) {
        d = new TypePrivate;
        d->ref.ref();
    }
    d->setFactory(factory);
}

void Type::setWsFacet(ws_facet_t val)
{
    if (!d) {
        d = new TypePrivate;
        d->ref.ref();
    }
    d->setWsFacet(val);
}

void Type::addEnum(MaksiElementPtr val)
{
    if (!d) {
        d = new TypePrivate;
        d->ref.ref();
    }
    d->addEnum(val);
}

void Type::addAttribute(TypeAttribute attr)
{
    if (!d) {
        d = new TypePrivate;
        d->ref.ref();
    }
    d->addAttribute(attr);
}

QList<TypeAttribute> Type::attributes() const
{
    if (d) {
        return d->attributes();
    }
    return QList<TypeAttribute>();
}

Type Type::contentType() const
{
    if (d) {
        return (d->is_simple ? *this : d->contentType());
    }
    return Type();
}

QSharedPointer<TypeParticle> Type::particle() const
{
    return d ? d->particle() : QSharedPointer<TypeParticle>();
}

void Type::setParticle(QSharedPointer<TypeParticle> p)
{
    if (!d) {
        d = new TypePrivate;
        d->ref.ref();
    }
    d->setParticle(p);
}

Type Type::anyType()
{
    static Type any(new TypePrivate(new TypeFactory));
    return any;
}

Type Type::stdType(name_t name)
{
    static QHash<name_t, Type> std_types;

    if (std_types.isEmpty()) {
        static const name_t xs_string = xml::qname("string", xml::xschema_uri);
        static const name_t xs_normString = xml::qname("normalizedString", xml::xschema_uri);
        static const name_t xs_token = xml::qname("token", xml::xschema_uri);
        static const name_t xs_nmtoken = xml::qname("NMTOKEN", xml::xschema_uri);
        static const name_t xs_name = xml::qname("Name", xml::xschema_uri);
        static const name_t xs_ncname = xml::qname("NCName", xml::xschema_uri);
        static const name_t xs_id = xml::qname("ID", xml::xschema_uri);
        static const name_t xs_idref = xml::qname("IDREF", xml::xschema_uri);
        static const name_t xs_qname = xml::qname("QName", xml::xschema_uri);

        static const name_t xs_hex = xml::qname("hexBinary", xml::xschema_uri);
        static const name_t xs_base64 = xml::qname("base64Binary", xml::xschema_uri);
        static const name_t xs_anyURI = xml::qname("anyURI", xml::xschema_uri);
        static const name_t xs_datetime = xml::qname("DateTime", maksi::xml::xschema_uri);
        static const name_t xs_date = xml::qname("Date", xml::xschema_uri);
        static const name_t xs_time = xml::qname("Time", xml::xschema_uri);

        static const name_t xs_bool = xml::qname("boolean", xml::xschema_uri);
        static const name_t xs_double = xml::qname("double", xml::xschema_uri);
        static const name_t xs_float = xml::qname("float", xml::xschema_uri);
        static const name_t xs_decimal = xml::qname("decimal", xml::xschema_uri);
        static const name_t xs_long = xml::qname("long", xml::xschema_uri);
        static const name_t xs_ulong = xml::qname("unsignedLong", xml::xschema_uri);
        static const name_t xs_int = xml::qname("int", xml::xschema_uri);
        static const name_t xs_uint = xml::qname("unsignedInt", xml::xschema_uri);
        static const name_t xs_short = xml::qname("short", xml::xschema_uri);
        static const name_t xs_ushort = xml::qname("unsignedShort", xml::xschema_uri);
        static const name_t xs_byte = xml::qname("byte", xml::xschema_uri);
        static const name_t xs_ubyte = xml::qname("unsignedByte", xml::xschema_uri);

        static const name_t xs_any = xml::qname("anyType", xml::xschema_uri);
        static const name_t xs_anySimple = xml::qname("anySimpleType", xml::xschema_uri);
        static const name_t xs_anyAtomic = xml::qname("anyAtomicType", xml::xschema_uri);

        std_types.insert(xs_string, Type(new TypePrivate(new StringTypeFactory)));
        std_types.insert(xs_normString, Type(new TypePrivate(new NormStringTypeFactory)));
        std_types.insert(xs_token, Type(new TypePrivate(new TokenTypeFactory)));
        std_types.insert(xs_nmtoken, Type(new TypePrivate(new NMTokenTypeFactory)));
        std_types.insert(xs_name, Type(new TypePrivate(new NameTypeFactory)));
        std_types.insert(xs_ncname, Type(new TypePrivate(new NCNameTypeFactory)));
        std_types.insert(xs_id, Type(new TypePrivate(new NCNameTypeFactory)));
        std_types.insert(xs_idref, Type(new TypePrivate(new NCNameTypeFactory)));
        std_types.insert(xs_qname, Type(new TypePrivate(new QNameTypeFactory)));
        std_types.insert(xs_hex, Type(new TypePrivate(new HexTypeFactory)));
        std_types.insert(xs_base64, Type(new TypePrivate(new Base64TypeFactory)));
        std_types.insert(xs_anyURI, Type(new TypePrivate(new URITypeFactory)));
        std_types.insert(xs_datetime, Type(new TypePrivate(new DateTimeTypeFactory)));
        std_types.insert(xs_date, Type(new TypePrivate(new DateTypeFactory)));
        std_types.insert(xs_time, Type(new TypePrivate(new TimeTypeFactory)));

        std_types.insert(xs_bool, Type(new TypePrivate(new BoolTypeFactory)));
        std_types.insert(xs_double, Type(new TypePrivate(new DoubleTypeFactory)));
        std_types.insert(xs_float, Type(new TypePrivate(new FloatTypeFactory)));
        std_types.insert(xs_decimal, Type(new TypePrivate(new NumberTypeFactory)));
        std_types.insert(xs_long, Type(new TypePrivate(new LongTypeFactory)));
        std_types.insert(xs_ulong, Type(new TypePrivate(new ULongTypeFactory)));
        std_types.insert(xs_int, Type(new TypePrivate(new IntTypeFactory)));
        std_types.insert(xs_short, Type(new TypePrivate(new IntTypeFactory)));
        std_types.insert(xs_byte, Type(new TypePrivate(new IntTypeFactory)));
        std_types.insert(xs_uint, Type(new TypePrivate(new UIntTypeFactory)));
        std_types.insert(xs_ushort, Type(new TypePrivate(new UIntTypeFactory)));
        std_types.insert(xs_ubyte, Type(new TypePrivate(new UIntTypeFactory)));

        std_types.insert(xs_any, anyType());
        std_types.insert(xs_anySimple, Type(new TypePrivate(new SimpleTypeFactory)));
        std_types.insert(xs_anyAtomic, Type(new TypePrivate(new AtomicTypeFactory)));
    }

    Type tp(std_types.value(name));
    if (std_types.contains(name)) {
    } else {
        qDebug("not found std type %s %s valid", qPrintable(name->toString()), tp.isValid() ? "" : "not");
    }
    return tp;
}

bool Type::simpleType(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type *declare)
{
    static name_t xs_restriction = maksi::xml::qname("restriction", maksi::xml::xschema_uri);
    static name_t xs_list = maksi::xml::qname("list", maksi::xml::xschema_uri);
    static name_t xs_union = maksi::xml::qname("union", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() == xs_restriction) {
                return restrictionType(maksi, node.data(), targetName, true, declare);
            } else if (node->elementName() == xs_list) {
                return listType(maksi, node.data(), targetName, declare);
            } else if (node->elementName() == xs_union) {
                return unionType(maksi, node.data(), targetName, declare);
            } else {
                if (node->elementName() != xs_annotation)
                    maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
            }
        }
    }

    maksi->sendMessage(MESSAGE_ERROR, conf, QString("either restriction or list or union must be specified"));
    return false;
}

bool Type::restrictionType(Maksi *maksi, const MaksiElement *conf, name_t targetName, bool simple, Type *declareType)
{
    static name_t xs_simpleType = maksi::xml::qname("simpleType", maksi::xml::xschema_uri);
    static name_t xs_base = maksi::xml::qname("base", maksi::xml::xschema_uri);
    static name_t xs_value = maksi::xml::qname("value", maksi::xml::xschema_uri);
    static name_t xs_enum = maksi::xml::qname("enumeration", maksi::xml::xschema_uri);
    static name_t xs_ws = maksi::xml::qname("whiteSpace", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    Type baseType;
    if (!conf->hasAttribute(xs_base)) {
        for (int i = 0; i < conf->elementCount(); i++) {
            MaksiElementPtr node = conf->element(i);
            if (node->elementName() == xs_simpleType) {
                if (!simpleType(maksi, node.data(), targetName, &baseType)) {
                    return false;
                }
                break;
            }
        }
        if (!baseType.isValid()) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("base type of restriction is required"));
            return false;
        }
    } else {
        QString baseNameS(conf->attribute(xs_base)->elementText());
        name_t baseName = maksi::xml::qname(baseNameS, conf);
        baseType = maksi->type(baseName);
        if (!baseType.isValid()) {
            baseName = maksi::xml::qname(baseNameS, 0, targetName);
            baseType = maksi->type(baseName);
            if (!baseType.isValid()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined type '%3'").arg(baseNameS));
                return false;
            }
        }
        if (simple) {
            if (!baseType.isSimple()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("base type of restriction must be simple"));
                return false;
            }
        } else {
            if (!baseType.contentType().isSimple()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("content type of restriction must be simple"));
                return false;
            }
        }
    }

    *declareType = baseType.clone();
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() == xs_enum) {
                if (node->hasAttribute(xs_value)) {
                    MaksiElementPtr data = node->attribute(xs_value);
                    MaksiElementPtr val = baseType.createNew(data.data());
                    if (!val) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("invalid value for enumeration: %1").arg(data->elementText()));
                        return false;
                    }
                    declareType->addEnum(val);
                } else {
                    maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("enumeration must have value"));
                    return false;
                }
            } else if (node->elementName() == xs_ws) {
                if (node->hasAttribute(xs_value)) {
                    QString data = node->attribute(xs_value)->elementText();
                    if (data == "preserve") {
                        declareType->setWsFacet(Type::WS_PRESERVE);
                    } else if (data == "replace") {
                        declareType->setWsFacet(Type::WS_REPLACE);
                    } else if (data == "collapse") {
                        declareType->setWsFacet(Type::WS_COLLAPSE);
                    } else {
                        maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported value for whiteSpace: '%1'").arg(data));
                    }
                }
            } else if (node->elementName() != xs_simpleType && node->elementName() != xs_annotation) {
                maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element: '%1'").arg(node->elementName()->toString()));
            }
        }
    }
    return true;
}

bool Type::listType(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type *declareType)
{
    static name_t xs_simpleType = maksi::xml::qname("simpleType", maksi::xml::xschema_uri);
    static name_t xs_itemType = maksi::xml::qname("itemType", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    Type itemType;
    if (!conf->hasAttribute(xs_itemType)) {
        for (int i = 0; i < conf->elementCount(); i++) {
            MaksiElementPtr node = conf->element(i);
            if (node->elementName() == xs_simpleType) {
                if (!simpleType(maksi, node.data(), targetName, &itemType)) {
                    return false;
                }
                break;
            }
        }
        if (!itemType.isValid()) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("itemType of list is required"));
            return false;
        }
    } else {
        QString typeNameS(conf->attribute(xs_itemType)->elementText());
        name_t typeName = maksi::xml::qname(typeNameS, conf);
        itemType = maksi->type(typeName);
        if (!itemType.isValid()) {
            typeName = maksi::xml::qname(typeNameS, 0, targetName);
            itemType = maksi->type(typeName);
            if (!itemType.isValid()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined type '%1'").arg(typeNameS));
                return false;
            }
        }
        if (!itemType.isSimple()) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("itemType of list must be simple"));
            return false;
        }
    }

    *declareType = Type(new TypePrivate(new ListTypeFactory(itemType)));
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() != xs_simpleType && node->elementName() != xs_annotation)
                maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
        }
    }
    return true;
}

bool Type::unionType(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type *declareType)
{
    static name_t xs_simpleType = maksi::xml::qname("simpleType", maksi::xml::xschema_uri);
    static name_t xs_memberTypes = maksi::xml::qname("memberTypes", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    QList<Type> memberTypes;
    if (conf->hasAttribute(xs_memberTypes)) {
        QString text(conf->attribute(xs_memberTypes)->elementText().simplified());
        QList<QString> names(text.split(' '));
        for (int i = 0; i < names.size(); ++i) {
            QString typeNameS(names.at(i));
            name_t typeName = maksi::xml::qname(typeNameS, conf);
            Type type = maksi->type(typeName);
            if (!type.isValid()) {
                typeName = maksi::xml::qname(typeNameS, 0, targetName);
                type = maksi->type(typeName);
                if (!type.isValid()) {
                    maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined type '%1'").arg(typeNameS));
                    return false;
                }
            }
            if (!type.isSimple()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("union member type must be simple"));
                return false;
            }
            memberTypes.append(type);
        }
    }

    for (int i = 0; i < conf->elementCount(); ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->elementName() == xs_simpleType) {
            Type type;
            if (!simpleType(maksi, node.data(), targetName, &type)) {
                return false;
            }
            if (type.isValid())
                memberTypes.append(type);
        }
    }

    *declareType = Type(new TypePrivate(new UnionTypeFactory(memberTypes)));
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() != xs_simpleType && node->elementName() != xs_annotation)
                maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
        }
    }
    return true;
}

bool Type::structType(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type baseType, Type *declareType)
{
    static name_t xs_attribute = maksi::xml::qname("attribute", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    *declareType = Type(new TypePrivate(new StructTypeFactory(baseType)));

    QList<name_t> names;
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (node->elementName() == xs_attribute) {
            TypeAttribute attr = attribute(maksi, node.data(), targetName, false);
            if (attr.isValid()) {
                declareType->addAttribute(attr);
                names.append(attr.name());
            }
        }
    }

    QList<TypeAttribute> baseAttr = baseType.attributes();
    for (int i = 0; i < baseAttr.size(); i++) {
        name_t baseAttrName = baseAttr.at(i).name();
        for (int n = 0; n < names.size(); n++) {
            if (names.at(n) == baseAttrName)
                goto next;
        }
        declareType->addAttribute(baseAttr.at(i));
    next:;
    }
    return true;
}

bool Type::complexType(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type *declareType, bool define)
{
    static name_t xs_simpleContent = maksi::xml::qname("simpleContent", maksi::xml::xschema_uri);
    static name_t xs_complexContent = maksi::xml::qname("complexContent", maksi::xml::xschema_uri);
    static name_t xs_mixed = maksi::xml::qname("mixed", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (node->elementName() == xs_simpleContent) {
            return simpleContent(maksi, node.data(), targetName, declareType, define);
        }
        if (node->elementName() == xs_complexContent) {
            return complexContent(maksi, node.data(), targetName, declareType, define);
        }
    }

    bool mixed = false;
    MaksiElementPtr node = conf->attribute(xs_mixed);
    if (node) {
        node->elementToBool(&mixed);
    }
    return complexRestriction(maksi, conf, targetName, anyType(), mixed, declareType, define);
}

bool Type::simpleContent(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type *declareType, bool define)
{
    static name_t xs_restriction = maksi::xml::qname("restriction", maksi::xml::xschema_uri);
    static name_t xs_extension = maksi::xml::qname("extension", maksi::xml::xschema_uri);
    static name_t xs_base = maksi::xml::qname("base", maksi::xml::xschema_uri);
    static name_t xs_anySimpleType = maksi::xml::qname("anySimpleType", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    if (define && declareType->isValid()) {
        return true;
    }

    Type baseType = stdType(xs_anySimpleType);
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() == xs_restriction) {
                if (!restrictionType(maksi, node.data(), targetName, false, &baseType))
                    return false;
                return structType(maksi, node.data(), targetName, baseType, declareType);
            } else if (node->elementName() == xs_extension) {
                if (!node->hasAttribute(xs_base)) {
                    maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("base type of extension is required"));
                    return false;
                }

                QString typeNameS(node->attribute(xs_base)->elementText());
                name_t typeName = maksi::xml::qname(typeNameS, node.data());
                baseType = maksi->type(typeName);
                if (!baseType.isValid()) {
                    typeName = maksi::xml::qname(typeNameS, 0, targetName);
                    baseType = maksi->type(typeName);
                    if (!baseType.isValid()) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("undefined type '%1'").arg(typeNameS));
                        return false;
                    }
                }
                if (!baseType.contentType().isSimple()) {
                    maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("content type of simple extension must be simple"));
                    return false;
                }
                return structType(maksi, node.data(), targetName, baseType, declareType);
            } else {
                if (node->elementName() != xs_annotation)
                    maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
            }
        }
    }

    maksi->sendMessage(MESSAGE_ERROR, conf, QString("either restriction or extension must be specified"));
    return false;
}

bool Type::complexContent(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type *declareType, bool define)
{
    static name_t xs_restriction = maksi::xml::qname("restriction", maksi::xml::xschema_uri);
    static name_t xs_extension = maksi::xml::qname("extension", maksi::xml::xschema_uri);
    static name_t xs_base = maksi::xml::qname("base", maksi::xml::xschema_uri);
    static name_t xs_mixed = maksi::xml::qname("mixed", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    bool mixed = false;
    if (conf->hasAttribute(xs_mixed)) {
        MaksiElementPtr node = conf->attribute(xs_mixed);
        node->elementToBool(&mixed);
    }

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() == xs_restriction) {
                if (!node->hasAttribute(xs_base)) {
                    maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("base type of restriction is required"));
                    return false;
                }

                QString typeNameS(node->attribute(xs_base)->elementText());
                name_t typeName = maksi::xml::qname(typeNameS, node.data());
                Type base = maksi->type(typeName);
                if (!base.isValid()) {
                    typeName = maksi::xml::qname(typeNameS, 0, targetName);
                    base = maksi->type(typeName);
                    if (!base.isValid()) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("undefined type '%1'").arg(typeNameS));
                        return false;
                    }
                }
                return complexRestriction(maksi, node.data(), targetName, base, mixed, declareType, define);
            } else if (node->elementName() == xs_extension) {
                if (!node->hasAttribute(xs_base)) {
                    maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("base type of extension is required"));
                    return false;
                }

                QString typeNameS(node->attribute(xs_base)->elementText());
                name_t typeName = maksi::xml::qname(typeNameS, node.data());
                Type base = maksi->type(typeName);
                if (!base.isValid()) {
                    typeName = maksi::xml::qname(typeNameS, 0, targetName);
                    base = maksi->type(typeName);
                    if (!base.isValid()) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("undefined type '%1'").arg(typeNameS));
                        return false;
                    }
                }
                return complexExtension(maksi, node.data(), targetName, base, mixed, declareType, define);
            } else if (node->elementName() != xs_annotation)
                maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
        }
    }

    maksi->sendMessage(MESSAGE_ERROR, conf, QString("either extension or restriction must be specified"));
    return false;
}

bool Type::complexRestriction(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type baseType, bool mixed, Type *declareType, bool define)
{
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    if (!declareType->isValid()) {
        if (baseType.isSimple()) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("base type of restriction must be complex"));
            return false;
        }
        if (!structType(maksi, conf, targetName, baseType, declareType)) {
            return false;
        }
    }
    if (!define) {
        return true;
    }

    QSharedPointer<TypeParticle> part;
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            part = modelGroup(maksi, node.data(), targetName);
            if (!part.isNull()) {
                if (!setMinMaxOccurs(maksi, node.data(), part.data())) {
                    return false;
                }
                break;
            }
        }
    }

    if (part.isNull() || part->isEmpty()) {
        //qDebug("part is null");
        if (mixed) {
            declareType->d->is_empty = 0;
            declareType->d->is_mixed = 1;
            declareType->setParticle(QSharedPointer<TypeParticle>(new TypeParticle()));
        } else {
            declareType->d->is_empty = 1;
            declareType->d->is_mixed = 0;
        }
    } else {
        //qDebug("part is not null");
        declareType->d->is_empty = 0;
        declareType->d->is_mixed = (mixed ? 1 : 0);
        declareType->setParticle(part);
    }
    return true;
}

bool Type::complexExtension(Maksi *maksi, const MaksiElement *conf, name_t targetName, Type baseType, bool mixed, Type *declareType, bool define)
{
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    if (!declareType->isValid()) {
        if (!structType(maksi, conf, targetName, baseType, declareType)) {
            return false;
        }
    }
    if (!define) {
        return true;
    }

    QSharedPointer<TypeParticle> part;
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        part = modelGroup(maksi, node.data(), targetName);
        if (!node->isElementSimple()) {
            if (!part.isNull()) {
                if (!setMinMaxOccurs(maksi, node.data(), part.data())) {
                    return false;
                }
                break;
            }
        }
    }

    if (baseType.isEmpty() || baseType.isSimple() || baseType.contentType().isSimple()) {
        if (part.isNull() || part->isEmpty()) {
            if (mixed) {
                declareType->d->is_empty = 0;
                declareType->d->is_mixed = 1;
                declareType->setParticle(QSharedPointer<TypeParticle>(new TypeParticle()));
            } else{
                declareType->d->is_empty = (baseType.isEmpty() ? 1 : 0);
                declareType->d->is_mixed = 0;
            }
        } else {
            declareType->d->is_empty = 0;
            declareType->d->is_mixed = (mixed ? 1 : 0);
            declareType->setParticle(part);
        }
    } else {
        declareType->d->is_empty = 0;
        if ((part.isNull() || part->isEmpty()) && !mixed) {
            declareType->d->is_mixed = (baseType.isMixed() ? 1 : 0);
            declareType->setParticle(baseType.particle());
        } else {
            declareType->d->is_mixed = (mixed ? 1 : 0);
            QSharedPointer<TypeParticle> baseP(baseType.particle());
            if (baseP.isNull()) {
                declareType->setParticle(part);
            } else if (baseP->compositor() == TypeGroup::COMPOSITOR_ALL) {
                if (part.isNull() || part->isEmpty()) {
                    declareType->setParticle(baseP);
                } else if (part->compositor() == TypeGroup::COMPOSITOR_ALL) {
                    TypeGroup seq;
                    seq.setCompositor(TypeGroup::COMPOSITOR_ALL);
                    seq.appendChildParticles(baseP);
                    seq.appendChildParticles(part);

                    QSharedPointer<TypeParticle> p(new TypeParticle_All(seq));
                    p->m_minOccurs = part->m_minOccurs;
                    declareType->setParticle(p);
                } else {
                    goto common;
                }
            } else {
            common:
                TypeGroup seq;
                seq.setCompositor(TypeGroup::COMPOSITOR_SEQUENCE);
                seq.appendParticle(baseP);
                seq.appendParticle(part);
                declareType->setParticle(QSharedPointer<TypeParticle>(new TypeParticle_Sequence(seq)));
            }
        }
    }
    return true;
}


TypeElement Type::element(Maksi *maksi, const MaksiElement *conf, name_t targetName)
{
    static name_t xs_name = maksi::xml::qname("name", maksi::xml::xschema_uri);
    static name_t xs_type = maksi::xml::qname("type", maksi::xml::xschema_uri);
    static name_t xs_simpleType = maksi::xml::qname("simpleType", maksi::xml::xschema_uri);
    static name_t xs_complexType = maksi::xml::qname("complexType", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    if (!conf->hasAttribute(xs_name)) {
        maksi->sendMessage(MESSAGE_ERROR, conf, QString("name of element is required"));
        return TypeElement();
    }

    name_t elemName = maksi::xml::qname(conf->attribute(xs_name)->elementText(), conf, targetName);
    if (!elemName) {
        maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid of missing attribute 'name'"));
        return TypeElement();
    }

    Type type;
    if (conf->hasAttribute(xs_type)) {
        QString typeNameS(conf->attribute(xs_type)->elementText());
        name_t typeName = maksi::xml::qname(typeNameS, conf);
        type = maksi->type(typeName);
        if (!type.isValid()) {
            typeName = maksi::xml::qname(typeNameS, 0, targetName);
            type = maksi->type(typeName);
            if (!type.isValid()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined type '%1'").arg(typeNameS));
                return TypeElement();
            }
        }
    } else {
        for (int i = 0; i < conf->elementCount(); i++) {
            MaksiElementPtr node = conf->element(i);
            if (!node->isElementSimple()) {
                if (node->elementName() == xs_simpleType) {
                    if (!simpleType(maksi, node.data(), targetName, &type)) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("invalid type for element '%1'").arg(elemName->toString()));
                        return TypeElement();
                    }
                } else if (node->elementName() == xs_complexType) {
                    if (!complexType(maksi, node.data(), targetName, &type, true)) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("invalid type for element '%1'").arg(elemName->toString()));
                        return TypeElement();
                    }
                } else {
                    if (node->elementName() != xs_annotation)
                        maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
                }
            }
        }
    }

    return TypeElement(elemName, type);
}

TypeGroup Type::group(Maksi *maksi, const MaksiElement *conf, name_t targetName)
{
    static name_t xs_all = maksi::xml::qname("all", maksi::xml::xschema_uri);
    static name_t xs_choice = maksi::xml::qname("choice", maksi::xml::xschema_uri);
    static name_t xs_sequence = maksi::xml::qname("sequence", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    TypeGroup gr;
    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            if (node->elementName() == xs_all) {
                return allGroup(maksi, node.data(), targetName);
            } else if (node->elementName() == xs_choice) {
                return choiceGroup(maksi, node.data(), targetName);
            } else if (node->elementName() == xs_sequence) {
                return sequenceGroup(maksi, node.data(), targetName);
            } else if (node->elementName() != xs_annotation) {
                maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
            }
        }
    }

    maksi->sendMessage(MESSAGE_ERROR, conf, QString("group must have 'all' or 'choice' or 'sequence' compositor"));
    return TypeGroup();
}


QSharedPointer<TypeParticle> Type::modelGroup(Maksi *maksi, const MaksiElement *conf, name_t targetName)
{
    static name_t xs_group = maksi::xml::qname("group", maksi::xml::xschema_uri);
    static name_t xs_all = maksi::xml::qname("all", maksi::xml::xschema_uri);
    static name_t xs_choice = maksi::xml::qname("choice", maksi::xml::xschema_uri);
    static name_t xs_sequence = maksi::xml::qname("sequence", maksi::xml::xschema_uri);
    static name_t xs_ref = maksi::xml::qname("ref", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    if (conf->elementName() == xs_group) {
        TypeGroup gr;
        if (conf->hasAttribute(xs_ref)) {
            QString refS(conf->attribute(xs_ref)->elementText());
            name_t ref = maksi::xml::qname(refS, conf);
            gr = maksi->group(ref);
            if (!gr.isValid()) {
                ref = maksi::xml::qname(refS, 0, targetName);
                gr = maksi->group(ref);
                if (!gr.isValid()) {
                    maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined group '%1'").arg(refS));
                    return QSharedPointer<TypeParticle>();
                }
            }
        } else {
            gr = group(maksi, conf, targetName);
        }

        switch (gr.compositor()) {
        case TypeGroup::COMPOSITOR_ALL:
            return QSharedPointer<TypeParticle>(new TypeParticle_All(gr));
        case TypeGroup::COMPOSITOR_CHOICE:
            return QSharedPointer<TypeParticle>(new TypeParticle_Choice(gr));
        case TypeGroup::COMPOSITOR_SEQUENCE:
            return QSharedPointer<TypeParticle>(new TypeParticle_Sequence(gr));
        default:;
        }
    } else if (conf->elementName() == xs_all) {
        TypeGroup gr = allGroup(maksi, conf, targetName);
        if (gr.isValid())
            return QSharedPointer<TypeParticle>(new TypeParticle_All(gr));
    } else if (conf->elementName() == xs_choice) {
        TypeGroup gr = choiceGroup(maksi, conf, targetName);
        if (gr.isValid())
            return QSharedPointer<TypeParticle>(new TypeParticle_Choice(gr));
    } else if (conf->elementName() == xs_sequence) {
        TypeGroup gr = sequenceGroup(maksi, conf, targetName);
        if (gr.isValid())
            return QSharedPointer<TypeParticle>(new TypeParticle_Sequence(gr));
    }
    return QSharedPointer<TypeParticle>();
}

TypeGroup Type::allGroup(Maksi *maksi, const MaksiElement *conf, name_t targetName)
{
    static name_t xs_element = maksi::xml::qname("element", maksi::xml::xschema_uri);
    static name_t xs_any = maksi::xml::qname("any", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_maxOccurs = maksi::xml::qname("maxOccurs", maksi::xml::xschema_uri);
    static name_t xs_ref = maksi::xml::qname("ref", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    if (conf->hasAttribute(xs_maxOccurs)) {
        int ival = 1;
        if (!conf->attribute(xs_maxOccurs)->elementToInt(&ival) || ival != 1) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("maxOccurs should be 1 in this context"));
            return TypeGroup();
        }
    }

    TypeGroup seq;
    seq.setCompositor(TypeGroup::COMPOSITOR_ALL);

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            QSharedPointer<TypeParticle> particle;
            if (node->elementName() == xs_element) {
                TypeElement elem;
                if (node->hasAttribute(xs_ref)) {
                    QString refS(node->attribute(xs_ref)->elementText());
                    name_t ref = maksi::xml::qname(refS, node.data());
                    elem = maksi->element(ref);
                    if (!elem.isValid()) {
                        ref = maksi::xml::qname(refS, 0, targetName);
                        elem = maksi->element(ref);
                        if (!elem.isValid()) {
                            maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("undefined element %1").arg(refS));
                            return TypeGroup();
                        }
                    }
                } else {
                    elem = element(maksi, node.data(), targetName);
                    if (!elem.isValid()) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("invalid element"));
                        return TypeGroup();
                    }
                }

                particle = QSharedPointer<TypeParticle>(new TypeParticle_Elem(elem));
            } else if (node->elementName() == xs_any) {
                maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("any in all compositor is not implemented"));
                return TypeGroup();
            } else if (node->elementName() == xs_annotation) {
                maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
            }
            if (particle) {
                if (!setMinMaxOccurs(maksi, node.data(), particle.data())) {
                    return TypeGroup();
                }
                seq.appendParticle(particle);
            }
        }
    }

    return seq;
}

TypeGroup Type::choiceGroup(Maksi *maksi, const MaksiElement *conf, name_t targetName)
{
    static name_t xs_element = maksi::xml::qname("element", maksi::xml::xschema_uri);
    static name_t xs_ref = maksi::xml::qname("ref", maksi::xml::xschema_uri);
    static name_t xs_any = maksi::xml::qname("any", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    TypeGroup seq;
    seq.setCompositor(TypeGroup::COMPOSITOR_CHOICE);

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            QSharedPointer<TypeParticle> particle;

            if (node->elementName() == xs_element) {
                TypeElement elem;
                if (node->hasAttribute(xs_ref)) {
                    QString refS(node->attribute(xs_ref)->elementText());
                    name_t ref = maksi::xml::qname(refS, node.data());
                    elem = maksi->element(ref);
                    if (!elem.isValid()) {
                        ref = maksi::xml::qname(refS, 0, targetName);
                        elem = maksi->element(ref);
                        if (!elem.isValid()) {
                            maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("undefined element %1").arg(refS));
                            return TypeGroup();
                        }
                    }
                } else {
                    elem = element(maksi, node.data(), targetName);
                    if (!elem.isValid()) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("invalid element"));
                        return TypeGroup();
                    }
                }

                particle = QSharedPointer<TypeParticle>(new TypeParticle_Elem(elem));
            } else if (node->elementName() == xs_any) {
                maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("any in choice compositor is not implemented"));
                return TypeGroup();
            } else {
                particle = modelGroup(maksi, node.data(), targetName);
                if (particle.isNull()) {
                    if (node->elementName() != xs_annotation)
                        maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
                }
            }
            if (particle) {
                if (!setMinMaxOccurs(maksi, node.data(), particle.data())) {
                    return TypeGroup();
                }
                seq.appendParticle(particle);
            }
        }
    }

    return seq;
}

TypeGroup Type::sequenceGroup(Maksi *maksi, const MaksiElement *conf, name_t targetName)
{
    static name_t xs_element = maksi::xml::qname("element", maksi::xml::xschema_uri);
    static name_t xs_ref = maksi::xml::qname("ref", maksi::xml::xschema_uri);
    static name_t xs_any = maksi::xml::qname("any", maksi::xml::xschema_uri);
    static name_t xs_annotation = maksi::xml::qname("annotation", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    TypeGroup seq;
    seq.setCompositor(TypeGroup::COMPOSITOR_SEQUENCE);

    for (int i = 0; i < conf->elementCount(); i++) {
        MaksiElementPtr node = conf->element(i);
        if (!node->isElementSimple()) {
            QSharedPointer<TypeParticle> particle;

            if (node->elementName() == xs_element) {
                TypeElement elem;
                if (node->hasAttribute(xs_ref)) {
                    QString refS(node->attribute(xs_ref)->elementText());
                    name_t ref = maksi::xml::qname(refS, node.data());
                    elem = maksi->element(ref);
                    if (!elem.isValid()) {
                        ref = maksi::xml::qname(refS, 0, targetName);
                        elem = maksi->element(ref);
                        if (!elem.isValid()) {
                            maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("undefined element %1").arg(refS));
                            return TypeGroup();
                        }
                    }
                } else {
                    elem = element(maksi, node.data(), targetName);
                    if (!elem.isValid()) {
                        maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("invalid element"));
                        return TypeGroup();
                    }
                }

                particle = QSharedPointer<TypeParticle>(new TypeParticle_Elem(elem));
            } else if (node->elementName() == xs_any) {
                maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("any in sequence compositor is not implemented"));
                return TypeGroup();
            } else {
                particle = modelGroup(maksi, node.data(), targetName);
                if (particle.isNull()) {
                    if (node->elementName() != xs_annotation)
                        maksi->sendMessage(MESSAGE_WARN, node.data(), QString("unsupported schema element '%1'").arg(node->elementName()->toString()));
                }
            }

            if (!particle.isNull()) {
                if (!setMinMaxOccurs(maksi, node.data(), particle.data())) {
                    return TypeGroup();
                }
                if (particle->m_maxOccurs > 0) {
                    seq.appendParticle(particle);
                }
            }
        }
    }

    return seq;
}

bool Type::setMinMaxOccurs(Maksi *maksi, const MaksiElement *conf, TypeParticle *p)
{
    static name_t xs_minOccurs = maksi::xml::qname("minOccurs", maksi::xml::xschema_uri);
    static name_t xs_maxOccurs = maksi::xml::qname("maxOccurs", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_minOccurs)) {
        QString val = conf->attribute(xs_minOccurs)->elementText();
        bool ok;
        p->m_minOccurs = val.toInt(&ok);
        if (!ok) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid value for minOccurs: '%1'").arg(val));
            return false;
        }
    } else {
        p->m_minOccurs = 1;
    }

    if (conf->hasAttribute(xs_maxOccurs)) {
        QString val = conf->attribute(xs_maxOccurs)->elementText();
        if (val == "unbounded") {
            p->m_maxOccurs = INT_MAX;
        } else {
            bool ok;
            p->m_maxOccurs = val.toInt(&ok);
            if (!ok) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid value for maxOccurs: '%1'").arg(val));
                return false;
            }
        }
    } else {
        p->m_maxOccurs = 1;
    }

    if (p->m_minOccurs > p->m_maxOccurs) {
        maksi->sendMessage(MESSAGE_ERROR, conf, QString("minOccurs must not be greater than maxOccurs"));
        return false;
    }

    return true;
}


TypeAttribute Type::attribute(Maksi *maksi, const MaksiElement *conf, name_t targetName, bool global)
{
    static name_t xs_simpleType = maksi::xml::qname("simpleType", maksi::xml::xschema_uri);
    static name_t xs_ref = maksi::xml::qname("ref", maksi::xml::xschema_uri);
    static name_t xs_name = maksi::xml::qname("name", maksi::xml::xschema_uri);
    static name_t xs_type = maksi::xml::qname("type", maksi::xml::xschema_uri);
    static name_t xs_fixed = maksi::xml::qname("fixed", maksi::xml::xschema_uri);
    static name_t xs_default = maksi::xml::qname("default", maksi::xml::xschema_uri);
    static name_t xs_use = maksi::xml::qname("use", maksi::xml::xschema_uri);
    static name_t xs_targetNamespace = maksi::xml::qname("targetNamespace", maksi::xml::xschema_uri);

    if (conf->hasAttribute(xs_targetNamespace)) {
        targetName = maksi::xml::qname("name", conf->attribute(xs_targetNamespace)->elementText());
    }

    Type type;
    TypeAttribute attr;
    if (conf->hasAttribute(xs_ref)) {
        QString refS(conf->attribute(xs_ref)->elementText());
        name_t ref = maksi::xml::qname(refS, conf);
        attr = maksi->attribute(ref);
        if (!attr.isValid()) {
            ref = maksi::xml::qname(refS, 0, targetName);
            attr = maksi->attribute(ref);
            if (!attr.isValid()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined attribute '%1'").arg(refS));
                return attr;
            }
        }
        type = attr.type();
    } else {
        if (!conf->hasAttribute(xs_name)) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("missing attribute 'name'"));
            return attr;
        }

        QString nameS = conf->attribute(xs_name)->elementText();
        name_t name = maksi::xml::qname(nameS, conf, targetName);
        if (!name) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid attribute 'name' (%1)").arg(nameS));
            return attr;
        }

        if (conf->hasAttribute(xs_type)) {
            QString typeNameS(conf->attribute(xs_type)->elementText());
            name_t typeName = maksi::xml::qname(typeNameS, conf);
            type = maksi->type(typeName);
            if (!type.isValid()) {
                typeName = maksi::xml::qname(typeNameS, 0, targetName);
                type = maksi->type(typeName);
                if (!type.isValid()) {
                    maksi->sendMessage(MESSAGE_ERROR, conf, QString("undefined type '%1'").arg(typeNameS));
                    return attr;
                }
            }
            if (!type.isSimple()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("attribute type must be simple"));
                return attr;
            }
        } else {
            for (int i = 0; i < conf->elementCount(); i++) {
                MaksiElementPtr node = conf->element(i);
                if (node->elementName() == xs_simpleType) {
                    if (!simpleType(maksi, node.data(), targetName, &type))
                        return TypeAttribute();
                    break;
                }
            }
            if (!type.isValid()) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid or missing attribute 'type'"));
                return attr;
            }
        }
        if (!type.isValid()) {
            return attr;
        }

        attr = TypeAttribute(name, type);
        if (conf->hasAttribute(xs_fixed)) {
            MaksiElementPtr atr = conf->attribute(xs_fixed);
            MaksiElementPtr val = type.createNew(atr.data());
            if (val) {
                attr.setFixed(val);
            } else {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid fixed value for attribute '%1'").arg(name->toString()));
                return TypeAttribute();
            }
        } else if (conf->hasAttribute(xs_default)) {
            MaksiElementPtr atr = conf->attribute(xs_default);
            MaksiElementPtr val = type.createNew(atr.data());
            if (val) {
                attr.setDefault(val);
            } else {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid default value for attribute '%1'").arg(name->toString()));
                return TypeAttribute();
            }
        }
    }

    if (conf->hasAttribute(xs_use)) {
        QString use = conf->attribute(xs_use)->elementText();
        if (global) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("global attribute definition must not have 'use' property"));
            return TypeAttribute();
        }

        if (attr.use() == USE_DEFAULT) {
            if (use != "optional") {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("attribute with default value must have use='optional'"));
                return TypeAttribute();
            }
        } else if (attr.use() == USE_FIXED) {
            if (use == "prohibited") {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("attribute with fixed value must not have use='prohibited'"));
                return TypeAttribute();
            } else if (use == "required") {
                attr.setUse(USE_FIXED_REQUIRED);
            }
        } else {
            if (use == "optional") {
                attr.setUse(USE_OPTIONAL);
            } else if (use == "prohibited") {
                attr.setUse(USE_PROHIBITED);
            } else if (use == "required") {
                attr.setUse(USE_REQUIRED);
            }
        }
    }
    return attr;
}


MaksiElementPtr Type::createNew(Maksi *maksi, const MaksiElement *conf, name_t elemName, bool hereNode) const
{
    if (!conf) {
        maksi->sendMessage(MESSAGE_ERROR, QString("cannot create element from {null}"));
        return MaksiElementPtr();
    }
    if (!isValid()) {
        maksi->sendMessage(MESSAGE_ERROR, conf, QString("cannot create element of undefined or invalid type"));
        return MaksiElementPtr();
    }

    const MaksiElement *node = 0;
    MaksiElementPtr tmp;

    if (isSimple()) {
        if (hereNode) {
            node = conf;
        } else {
            tmp = conf->elementContent();
            node = tmp.data();
        }

        if (node->isElementSimple()) {
            MaksiElementPtr val = createNew(node);
            if (!val) {
                maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid simple value for '%1': %2").arg(conf->elementName()->toString()).arg(node->elementText()));
            }
            return val;
        }

        maksi->sendMessage(MESSAGE_ERROR, conf, QString("content of '%1' should be simple").arg(conf->elementName()->toString()));
        return MaksiElementPtr();
    }


    if (hereNode) {
        node = conf;
    } else {
        int count = conf->elementCount();
        for (int i = 0; i < count; i++) {
            MaksiElementPtr elem = conf->element(i);
            if (elem->elementName() == elemName) {
                node = elem.data();
                break;
            }
        }
        if (!node) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("element '%1' must present here").arg(elemName->toString()));
            return MaksiElementPtr();
        }
    }

    if (node->elementName() == elemName) {
        MaksiElementPtr elem = createNew(node);
        if (!elem) {
            maksi->sendMessage(MESSAGE_ERROR, conf, QString("invalid value for '%1': %2").arg(elemName->toString()).arg(node->elementText()));
            return MaksiElementPtr();
        }

        QList<TypeAttribute> as(attributes());
        for (int i = 0; i < as.size(); i++) {
            TypeAttribute attr = as.at(i);
            if (attr.use() == USE_PROHIBITED) {
                if (node->hasAttribute(attr.name())) {
                    maksi->sendMessage(MESSAGE_ERROR, conf, QString("attribute '%1' is prohibited").arg(attr.name()->toString()));
                    return MaksiElementPtr();
                }
                continue;
            }
            if (attr.use() == USE_OPTIONAL) {
                if (!node->hasAttribute(attr.name()))
                    continue;
            }
            if (attr.use() == USE_DEFAULT || attr.use() == USE_FIXED) {
                if (!node->hasAttribute(attr.name())) {
                    elem->setAttribute(attr.name(), attr.value());
                    continue;
                }
            }
            if (attr.use() == USE_REQUIRED || attr.use() == USE_FIXED_REQUIRED) {
                if (!node->hasAttribute(attr.name())) {
                    maksi->sendMessage(MESSAGE_ERROR, node, QString("have not required attribute '%1'").arg(attr.name()->toString()));
                    return MaksiElementPtr();
                }
            }

            if (node->hasAttribute(attr.name())) {
                MaksiElementPtr va = node->attribute(attr.name());
                MaksiElementPtr val = attr.type().createNew(va.data());
                if (!val) {
                    maksi->sendMessage(MESSAGE_ERROR, node, QString("invalid value for attribute '%1'").arg(attr.name()->toString()));
                    return MaksiElementPtr();
                }
                if (attr.use() == USE_FIXED || attr.use() == USE_FIXED_REQUIRED) {
                    if (!val->isElementEqual(attr.value())) {
                        maksi->sendMessage(MESSAGE_ERROR, node, QString("value for attribute '%1' is fixed").arg(attr.name()->toString()));
                        return MaksiElementPtr();
                    }
                }
                elem->setAttribute(attr.name(), val);
            }
        }

        if (isEmpty()) {
            if (node->elementCount() > 0) {
                maksi->sendMessage(MESSAGE_ERROR, node, QString("content of element '%1' should be empty: '%2'").arg(elemName->toString()).arg(node->elementXmlText()));
                return MaksiElementPtr();
            }
        } else {
            QSharedPointer<TypeParticle> p = particle();
            if (!p.isNull()) {
                int begin = 0, end = node->elementCount();
                QSharedPointer<TypePartition> part = p->partition(node, begin, end, isMixed());
                if (part.isNull() || begin < part->m_begin || part->m_end < end) {
                    maksi->sendMessage(MESSAGE_ERROR, node, QString("cannot find valid sequence of children for element '%1'").arg(elemName->toString()));
                    return MaksiElementPtr();
                } else {
                    if (!createElements(maksi, node, part, elem.data())) {
                        maksi->sendMessage(MESSAGE_ERROR, node, QString("cannot insert child elements in element '%1'").arg(elemName->toString()));
                        return MaksiElementPtr();
                    }
                }
            } else {
                if (node->isElementSimple()) {
                    Type contType = contentType();
                    MaksiElementPtr val = contType.createNew(node);
                    if (!val) {
                        maksi->sendMessage(MESSAGE_ERROR, node, QString("invalid value for element '%1'").arg(elemName->toString()));
                        return MaksiElementPtr();
                    }
                    elem->elementAppend(val);
                } else {
                    maksi->sendMessage(MESSAGE_ERROR, node, QString("content of element '%1' should be simple").arg(elemName->toString()));
                    return MaksiElementPtr();
                }
            }
        }

        return elem;
    }

    maksi->sendMessage(MESSAGE_ERROR, conf, QString("element '%1' must present here").arg(elemName->toString()));
    return MaksiElementPtr();
}

bool Type::createElements(Maksi *maksi, const MaksiElement *conf, QSharedPointer<TypePartition> part, MaksiElement *elem) const
{
    for (int i = part->m_begin; i < part->m_end; ++i) {
        MaksiElementPtr node = conf->element(i);
        if (node->isElementSimple()) {
            if (isMixed()) {
                elem->elementAppend(node->elementClone());
            } else {
                QString txt;
                if (!node->elementToString(&txt) || !txt.trimmed().isEmpty()) {
                    maksi->sendMessage(MESSAGE_ERROR, conf, QString("cannot insert child '%1' in element '%2'").arg(node->elementText()).arg(conf->elementName()->toString()));
                    return false;
                }
            }
        } else {
            name_t name = part->m_particle->name();
            if (!name) {
                for (int n = 0; n < part->m_subsequences.size(); ++n) {
                    QSharedPointer<TypePartition> sp(part->m_subsequences.at(n));
                    //qDebug("i=%d beg=%d end=%d", i, sp->m_begin, sp->m_end);
                    for (/**/; i < sp->m_begin; ++i) {
                        MaksiElementPtr val = conf->element(i);
                        if (val->isElementSimple()) {
                            if (isMixed()) {
                                elem->elementAppend(val->elementClone());
                            } else {
                                QString txt;
                                if (!val->elementToString(&txt) || !txt.trimmed().isEmpty()) {
                                    maksi->sendMessage(MESSAGE_ERROR, conf, QString("cannot insert child '%1' in element '%2'").arg(val->elementText()).arg(conf->elementName()->toString()));
                                    return false;
                                }
                            }
                        } else {
                            //qDebug("fail i=%d", i);
                            maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("cannot insert child '%1' in element '%2'").arg(val->elementName()->toString()).arg(conf->elementName()->toString()));
                            return false;
                        }
                    }

                    if (!createElements(maksi, conf, sp, elem))
                        return false;
                    i = sp->m_end;
                }
            } else if (node->elementName() == name) {
                Type type = part->m_particle->type();
                if (type.isValid()) {
                    MaksiElementPtr val = type.createNew(maksi, node.data(), name, true);
                    if (val) {
                        //qDebug("appendElem: %s", qPrintable(name->toString()));
                        val->setElementName(name);
                        elem->elementAppend(val);
                    }
                } else {
                    maksi->sendMessage(MESSAGE_WARN, node.data(), QString("skip child %1 of unknown type in element '%2'").arg(node->elementName()->toString()).arg(conf->elementName()->toString()));
                }
            } else {
                maksi->sendMessage(MESSAGE_ERROR, node.data(), QString("cannot insert child '%1' in element '%2'").arg(node->elementName()->toString()).arg(conf->elementName()->toString()));
                return false;
            }
        }
    }

    return true;
}

MaksiElementPtr Type::createNew(const MaksiElement *from) const
{
    MaksiElementPtr tmp;

    if (d && !d->m_factory.isNull()) {
        if (isSimple()) {
            QString text;
            bool changed = false;
            if (from->elementToString(&text)) {
                if (d->ws_facet == Type::WS_REPLACE) {
                    for (int i = 0; i < text.size(); ++i) {
                        if (text.at(i).isSpace() && text.at(i) != ' ') {
                            text[i] = ' ';
                            changed = true;
                        }
                    }
                } else if (d->ws_facet == Type::WS_COLLAPSE) {
                    text = text.simplified();
                    changed = true;
                }
            }
            if (changed) {
                tmp = MaksiElement::newString(text);
                from = tmp.data();
            }
        }

        //qDebug("text: %s", qPrintable(from->elementText()));
        MaksiElementPtr val = d->m_factory->createNew(*this, from);
        if (val) {
            //qDebug("val: %s", qPrintable(val->elementText()));
            QList<MaksiElementPtr> enums = d->enums();
            if (enums.isEmpty()) {
                return val;
            }
            for (int i = 0; i < enums.size(); ++i) {
                if (enums.at(i)->isElementEqual(val)) {
                    return val;
                }
            }
        }
    }

    return MaksiElementPtr();
}



#if 0
    enum type_t {
        T_Integer,
        T_NonPositiveInteger,
        T_NegativeInteger,
        T_NonNegativeInteger,
        T_PositiveInteger,

        T_Duration,
        T_GYearMonth,
        T_GYear,
        T_GMonthDay,
        T_GDay,
        T_GMonth,

        T_NOTATION,
        T_Language,
        T_ENTITY,
    };
#endif

};

/* End of code */
