/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    scriptjob.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jan 27 10:33:47 2012
 *
 * @brief   script job
 *
 */

#ifndef MAKSI_SCRIPTJOB_H
#define MAKSI_SCRIPTJOB_H

#include "job.h"
#include "script.h"

class QNetworkReply;

//! maksi namespace
namespace maksi
{

class MAKSI_EXPORT ScriptJob : public Job
{
    Q_OBJECT
    Q_DISABLE_COPY(ScriptJob);
public:
    ScriptJob(Maksi *maksi, ScriptContext *ctx);
    virtual ~ScriptJob();

protected:
    virtual bool run();

protected slots:
    void subres(bool res, QVariant val);
    void argres(bool res, QVariant val);

private:
    Maksi *m_maksi;
    ScriptContext *m_ctx;
    QNetworkReply *m_reply;
    int m_pos;
    bool m_res;
    QVariant m_val;
    QList<QVariant> m_args;
};

};


#endif

/* End of file */
