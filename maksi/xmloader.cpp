/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2012-2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    xmloader.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Sun Nov 11 17:51:39 2012
 *
 * @brief   xmk loader
 *
 */

#include "xmloader.h"
#include "maksi.h"

#include <QXmlStreamReader>
#include <QStack>


namespace maksi
{

XmlLoader::XmlLoader(Maksi *maksi, const QUrl &url, int line) :
    m_maksi(maksi),
    m_url(url),
    m_finished(false),
    m_startLine(line)
{
}

XmlLoader::~XmlLoader()
{
}

int XmlLoader::startElement(int /*state*/, XmlNode * /*node*/)
{
    return XML_ELEMENT;
}

bool XmlLoader::endElement(int /*state*/, XmlNode * /*node*/)
{
    return true;
}

void XmlLoader::sendMessage(message_t type, int lineNumber, const QString &msg)
{
    if (m_url.isValid()) {
        m_maksi->sendMessage(type, QString("%1:%2: %3").arg(m_url.toString()).arg(lineNumber+m_startLine).arg(msg));
    } else {
        m_maksi->sendMessage(type, msg);
    }
}

bool XmlLoader::load(QXmlStreamReader &reader)
{
//    m_maksi->sendMessage(MESSAGE_DEBUG_CORE, tr("XmlLoader::loaded(): %1").arg(m_url.toString()));

    QString nodeName;
    QString chars;
    QStack<XmlNode *> stack;
    XmlNode *node = 0, *parent = 0;

    while (!reader.atEnd()) {
        QXmlStreamReader::TokenType token = reader.readNext();

        switch (token) {
        case QXmlStreamReader::NoToken:
            break;

        case QXmlStreamReader::StartDocument:
            break;

        case QXmlStreamReader::EndDocument:
            m_finished = true;
            goto cleanup;

        case QXmlStreamReader::Invalid:
            sendMessage(MESSAGE_ERROR, reader.lineNumber(), reader.errorString());
            goto cleanup;

        case QXmlStreamReader::Characters:
            chars += reader.text().toString();
            break;

        case QXmlStreamReader::Comment:
            break;

        case QXmlStreamReader::StartElement:
            if (!chars.isEmpty()) {
                if (node) {
                    MaksiElementPtr elem(new XmlString(chars, node->m_xml->elementSource()->namespaces(), m_url, reader.lineNumber()));
                    node->m_xml->elementAppend(elem);
                }
                chars.clear();
            }

            nodeName = reader.name().toString();
            if (!nodeName.isEmpty()) {
                QString nodeNS(reader.namespaceUri().toString());
                QString nodePrefix(reader.prefix().toString());

                int nodeState;
                QMap<QString, QString> ns;
                if (node) {
                    ns = node->m_xml->elementSource()->namespaces();
                    nodeState = node->m_state;
                } else {
                    nodeState = XML_DOCUMENT;
                }

                ns.insert("", nodeNS);

                QXmlStreamNamespaceDeclarations decl(reader.namespaceDeclarations());
                for (int i = 0; i < decl.size(); ++i) {
                    QString p(decl.at(i).prefix().toString());
                    QString n(decl.at(i).namespaceUri().toString());
                    //qDebug("add prefix='%s' ns='%s'", qPrintable(p), qPrintable(n));
                    ns.insert(p, n);
                }

                parent = node;
                stack.push(parent);

                name_t xmlName = xml::qname(nodeName, nodeNS, nodePrefix);
                node = new XmlNode(xmlName, ns, m_url, reader.lineNumber());

                QXmlStreamAttributes as(reader.attributes());
                for (int i = 0; i < as.size(); i++) {
                    const QXmlStreamAttribute &attr = as.at(i);
                    QString attrNS(attr.namespaceUri().toString());
                    QString attrName(attr.name().toString());
                    QString attrPrefix(attr.prefix().toString());
                    QString attrValue(attr.value().toString());
                    if (attrNS.isEmpty()) {
                        attrNS = nodeNS;
                        attrPrefix = nodePrefix;
                    }

                    name_t xname = xml::qname(attrName, attrNS, attrPrefix);
                    node->m_xml->setAttribute(xname, MaksiElementPtr(new XmlString(attrValue, ns, m_url, reader.lineNumber())));
                }

                //qDebug("start %s", qPrintable(nodeName));
                node->m_state = startElement(nodeState, node);
                if (XML_ERROR == node->m_state) {
                    // supposed that error message sent already
                    goto cleanup;
                }
            } else {
                sendMessage(MESSAGE_ERROR, reader.lineNumber(), QString::fromLatin1("unnamed xml element"));
                goto cleanup;
            }
            break;

        case QXmlStreamReader::EndElement:
            if (!chars.isEmpty()) {
                MaksiElementPtr elem(new XmlString(chars, node->m_xml->elementSource()->namespaces(), m_url, reader.lineNumber()));
                node->m_xml->elementAppend(elem);
                chars.clear();
            }

            //qDebug("end %s", qPrintable(node->m_xml->elementName()->toString()));
            if (!endElement(node->m_state, node)) {
                // supposed that error message sent already
                goto cleanup;
            }
            parent = stack.pop();

            if (XML_ELEMENT == node->m_state) {
                if (parent) {
                    //qDebug("append %s into %s", qPrintable(node->m_xml->elementName()->toString()), qPrintable(parent->m_xml->elementName()->toString()));
                    parent->m_xml->elementAppend(node->m_xml);
                } else {
                    emit loaded(node->m_xml.data());
                }
            }

            delete node;
            node = parent;
            break;

        case QXmlStreamReader::DTD:
        case QXmlStreamReader::EntityReference:
        case QXmlStreamReader::ProcessingInstruction:
        default:
            //warn(tr("%1:%2: xml element '%3' skipped").arg(m_url.toString()).arg(reader->lineNumber()).arg(reader->name().toString()));
            break;
        } /* switch() */
    } /* while() */

cleanup:
    delete node;
    while (!stack.isEmpty())
        delete stack.pop();

    return m_finished;
}

bool XmlLoader::loaded(QByteArray data)
{
    QXmlStreamReader reader(data);
    return load(reader);
}

bool XmlLoader::loaded(QString data)
{
    QXmlStreamReader reader(data);
    return load(reader);
}


};

 /* End of code */
