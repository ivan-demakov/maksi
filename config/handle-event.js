//

function keyPressEvent(view, key, mods, text, x, y) {
    debug("keyPressEvent: x="+x+" y="+y+" key="+key+" text="+text);
    return false;
}

function keyReleaseEvent(view, key, mods, x, y) {
    debug("keyReleaseEvent: x="+x+" y="+y+" key="+key);
    return false;
}

function keySequenceEvent(view, seq, mods, x, y) {
    print("keySeqEvent: view="+view+" seq="+seq);
    return false;
}


function mouseScrollMode(view, button, mods, x, y) {
    var items = view.graphicsAt(true, x, y);
    if (items.length > 0) {
        if (view.setMouseDragMode(true, x, y, button)) {
            return true;
        }
    }
    return view.setMouseScrollMode(true, x, y, button);
}

function mouseZoomInMode(view, button, mods, x, y) {
    return view.setMouseZoomInMode(true, x, y, button);
}

function mouseZoomOutMode(view, button, mods, x, y) {
    return view.setMouseZoomOutMode(true, x, y, button);
}

function mouseSelectInMode(view, button, mods, x, y) {
    return view.setMouseSelectInMode(true, x, y, button);
}

function mouseSelectOutMode(view, button, mods, x, y) {
    return view.setMouseSelectOutMode(true, x, y, button);
}

function mouseEditMode(view, button, mods, x, y) {
    if (!view.hasSelected()) {
        var items = view.graphicsAt(false, x, y);
        if (items.length > 0) {
            debug("mouseEditMode: x="+x+" y="+y+" button="+button);
            return true;
        }
    }
    return false;
}


function mousePressEvent(view, button, mods, x, y) {
    debug("mousePressEvent: x="+x+" y="+y+" button="+button);
    return false;
}

function mouseReleaseEvent(view, button, mods, x, y) {
    debug("mouseReleaseEvent: x="+x+" y="+y+" button="+button);
    return false;
}

function mouseMoveEvent(view, buttons, mods, x, y) {
    //debug("mouseMoveEvent: x="+x+" y="+y);
    return false;
}

function mouseDoubleClickEvent(view, button, mods, x, y) {
    debug("mouseDoubleClickEvent: x="+x+" y="+y+" button="+button);
    return false;
}

function mouseWheelEvent(view, delta, orient, mods, x, y) {
    debug("mouseWheelEvent: x="+x+" y="+y+" delta="+delta+" orient="+orient);
    return false;
}

function contextMenuEvent(view, reason, x, y) {
    debug("contextMenuEvent: x="+x+" y="+y+" reason="+reason);
    return false;
}
