include(../maksi.pri)

TEMPLATE = lib
CONFIG += plugin

QT -= gui

unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksi_data)
  CONFIG += debug
}
win32 {
  TARGET = maksi_data
  LIBS += -lmaksi
  CONFIG += release
}

INCLUDEPATH += ..

HEADERS = maksi_data.h
SOURCES = maksi_data.cpp

OTHER_FILES  += maksi-data.json
