/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2012, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_data.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jun 10 01:01:03 2011
 *
 * @brief   data engine
 *
 */

#ifndef MAKSI_DATA_H
#define MAKSI_DATA_H

#include <QObject>
#include <QHash>

#include <maksi/engine.h>
#include <maksi/script.h>
#include <maksi/elem.h>


#if defined(_MSC_VER)
#  pragma comment(lib, "maksi")
#endif

namespace maksi
{

class Data_EnginePlugin: public QObject, EnginePlugin
{
    Q_OBJECT;
    Q_INTERFACES(maksi::EnginePlugin);
    Q_PLUGIN_METADATA(IID MaksiEnginePlugin_IID FILE "maksi-data.json");

public:
    ~Data_EnginePlugin();

    virtual QString description() const;
    virtual Engine *newEngine(Maksi *maksi, const QString &name) const;
};


class Data_Engine : public Engine
{
    Q_OBJECT
    Q_DISABLE_COPY(Data_Engine);

public:
    Data_Engine(Maksi *maksi, const QString &name);
    ~Data_Engine();

    virtual Script *newScript(const QString &scriptName) override;
    virtual bool initEngine(const MaksiElement *conf) override;
    virtual bool evalCode(const QString &code, const QUrl &url, int line, QVariant *val) override;

protected slots:
    bool loaded(const MaksiElement *conf);

private:
    void evalElem(const MaksiElement *elem);

    QHash<QString, QVariant> m_elems;

    friend class Data_Context;
};


class Data_Script : public Script
{
public:
    Data_Script(Maksi *maksi, const QString &name, Data_Engine *engine);
    ~Data_Script();

    virtual Context *newContext();
    virtual bool addArg(const QString &name, const QVariant &val, const MaksiElement *conf) override;
    virtual void addCode(const QString &code, const MaksiElement *conf) override;

private:
    QHash<QString, QVariant> m_args;
    Data_Engine *m_engine;

    friend class Data_Context;
};


class Data_Context : public Context
{
public:
    Data_Context(Data_Engine *engine, Data_Script *script);
    ~Data_Context();

    virtual QVariant arg(const QString &name);
    virtual void setArg(const QString &name, const QVariant &val);
    virtual QVariant eval();

private:
    Data_Script *m_script;
    Data_Engine *m_engine;
};


};

#endif

/* End of file */
