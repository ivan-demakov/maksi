/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011-2017, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_data.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Fri Jun 10 01:02:59 2011
 *
 * @brief   data engine
 *
 */

#include "maksi_data.h"

#include <maksi/elem.h>
#include <maksi/maksi.h>
#include <maksi/xml.h>
#include <maksi/xmloader.h>

#include <QtPlugin>
#include <QFileInfo>
#include <QUrl>
#include <QTextStream>


namespace maksi
{

Data_EnginePlugin::~Data_EnginePlugin()
{
}

QString Data_EnginePlugin::description() const
{
    return "DATA";
}

Engine *Data_EnginePlugin::newEngine(Maksi *maksi, const QString &name) const
{
    return new Data_Engine(maksi, name);
}


Data_Engine::Data_Engine(Maksi *maksi, const QString &name) :
    Engine(maksi, name)
{
}

Data_Engine::~Data_Engine()
{
}

bool Data_Engine::evalCode(const QString &, const QUrl &, int, QVariant *)
{
    return false;
}

bool Data_Engine::initEngine(const MaksiElement *conf)
{
    static name_t maksi_load = xml::qname("load");
    static name_t maksi_href = xml::qname("location");
    static name_t maksi_eval = xml::qname("eval");

    //qDebug("MaksiEngine::initEngine");
    for (int i = 0; i < conf->elementCount(); i++) {
	MaksiElementPtr node = conf->element(i);
	if (node->elementName() == maksi_load) {
	    if (node->hasAttribute(maksi_href)) {
		QString href = node->attribute(maksi_href)->elementText();
		QUrl url(href, QUrl::StrictMode);
		QByteArray data;
		if (maksi()->loadPackageData(url, &data)) {
		    XmlLoader xl(maksi(), url, 0);
		    connect(&xl, SIGNAL(loaded(const MaksiElement *)), this, SLOT(loaded(const MaksiElement *)));
		    xl.loaded(data);
		} else {
		    maksi()->sendMessage(MESSAGE_WARN, node.data(), tr("cannot load code '%1'").arg(href));
		}
	    }
	} else if (node->elementName() == maksi_eval) {
	    int count = node->elementCount();
	    for (int i = 0; i < count; i++) {
		MaksiElementPtr e = node->element(i);
		evalElem(e.data());
	    }
	}
    }

    return true;
}

bool Data_Engine::loaded(const MaksiElement *conf)
{
    static name_t maksi_maksi = xml::qname("maksi");

    maksi()->sendMessage(MESSAGE_TRACE, tr("Data_Engine::loaded: %1").arg(conf->elementSourceUrl().toString()));

    if (conf->elementName() == maksi_maksi) {
	int count = conf->elementCount();
	for (int i = 0; i < count; ++i) {
	    MaksiElementPtr e = conf->element(i);
	    evalElem(e.data());
	}
    } else {
	maksi()->sendMessage(MESSAGE_WARN, conf, tr("cannot load unsupported type of xml document: %1").arg(conf->elementName()->toString()));
    }

    return true;
}

void Data_Engine::evalElem(const MaksiElement *elem)
{
    static name_t xs_element = xml::qname("element", maksi::xml::xschema_uri);
    static name_t xs_name = xml::qname("name", maksi::xml::xschema_uri);
    static name_t xs_type = xml::qname("type", maksi::xml::xschema_uri);
    static name_t maksi_name = xml::qname("name");

    name_t name = elem->elementName();
    if (name == xs_element) {
	if (elem->hasAttribute(xs_name)) {
	    QString name = elem->attribute(xs_name)->elementText();
	    if (elem->hasAttribute(xs_type)) {
		name_t typeName = maksi::xml::qname(elem->attribute(xs_type)->elementText(), elem);
		//qDebug("%s: %s", qPrintable(elem->attribute(maksi_type)->elementText()), qPrintable(typeName->toString()));
		Type type = maksi()->type(typeName);
		if (type.isValid()) {
		    MaksiElementPtr val = type.createNew(maksi(), elem, typeName, false);
		    if (val) {
			m_elems.insert(name, val->elementToVariant());
		    }
		} else {
		    maksi()->sendMessage(MESSAGE_ERROR, elem, tr("undefined type %1").arg(typeName->toString()));
		}
	    } else {
		m_elems.insert(name, QVariant());
	    }
	} else {
	    maksi()->sendMessage(MESSAGE_ERROR, elem, tr("element do not have required attribute 'name'"));
	}
    } else if (name) {
	Type type = maksi()->type(name);
	if (type.isValid()) {
	    QString valname;
	    if (elem->hasAttribute(xs_name)) {
		valname = elem->attribute(xs_name)->elementText();
	    } else if (elem->hasAttribute(maksi_name)) {
		valname = elem->attribute(maksi_name)->elementText();
	    } else if (elem->hasAttribute("name")) {
		valname = elem->attribute("name")->elementText();
	    }

	    if (!valname.isEmpty()) {
		MaksiElementPtr val = type.createNew(maksi(), elem, name, true);
		if (val) {
		    m_elems.insert(valname, val->elementToVariant());
		}
	    } else {
		maksi()->sendMessage(MESSAGE_ERROR, elem, tr("element do not have required attribute 'name'"));
	    }
	} else {
	    maksi()->sendMessage(MESSAGE_ERROR, elem, tr("undefined type %1").arg(name->toString()));
	}
    }
}

Script *Data_Engine::newScript(const QString &scriptName)
{
    return new Data_Script(maksi(), scriptName, this);
}


Data_Script::Data_Script(Maksi *maksi, const QString &name, Data_Engine *engine) :
    Script(maksi, name),
    m_engine(engine)
{
}

Data_Script::~Data_Script()
{
}

bool Data_Script::addArg(const QString &name, const QVariant &val, const MaksiElement *)
{
    m_args.insert(name, val);
    return true;
}

void Data_Script::addCode(const QString &, const MaksiElement *)
{
}

Context *Data_Script::newContext()
{
    return new Data_Context(m_engine, this);
}


Data_Context::Data_Context(Data_Engine *engine, Data_Script *script) :
    m_script(script),
    m_engine(engine)
{
}

Data_Context::~Data_Context()
{
}

QVariant Data_Context::arg(const QString &name)
{
    if (m_script->m_args.contains(name))
	return m_script->m_args.value(name);

    return m_engine->m_elems.value(name);
}

void Data_Context::setArg(const QString &name, const QVariant &val)
{
    m_script->m_args.insert(name, val);
}

QVariant Data_Context::eval()
{
    return QVariant();
}

};


/* End of code */
