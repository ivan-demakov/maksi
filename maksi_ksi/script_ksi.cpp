/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, 2014, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    script_ksi.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Jun 23 09:08:48 2010
 *
 * @brief   ksi script
 *
 */

#include "script_ksi.h"
#include <maksi/conf.h>

#include <QByteArray>


namespace maksi
{

static ksi_prim shape_prim = 0;
static ksi_prim signal_prim = 0;
static ksi_prim call_prim = 0;

static ksi_obj shape_proc(Script *script, ScriptObject *so, ksi_obj id, ksi_obj val, ksi_obj x, int argc, ksi_obj *argv);


static void errlog_proc(ksi_context_t _ksi, int pri, const wchar_t *msg)
{
    Ksi_Script_Context *ksi = (Ksi_Script_Context *)_ksi;

    if (ksi) {
        if (pri <= ERRLOG_ERROR) {
            ksi->m_script->error(QString("ksi: %1").arg(QString::fromWCharArray(msg)));
        } else if (pri <= ERRLOG_WARNING) {
            ksi->m_script->warn(QString("ksi: %1").arg(QString::fromWCharArray(msg)));
        } else if (pri <= ERRLOG_INFO) {
            ksi->m_script->info(QString("ksi: %1").arg(QString::fromWCharArray(msg)));
        } else {
            ksi->m_script->debug(QString("ksi: %1").arg(QString::fromWCharArray(msg)));
        }
    }
}

static ksi_obj error_proc(ksi_context_t _ksi, ksi_obj /*tag*/, ksi_obj exn)
{
    Ksi_Script_Context *ksi = (Ksi_Script_Context *)_ksi;

    if (ksi) {
        const wchar_t *msg;

        if (KSI_EXN_VAL(exn) != ksi_void) {
            if (KSI_EXN_SRC(exn) != ksi_void) {
                msg = ksi_aprintf("%ls\nerror-value: %ls\nerror-source: %ls",
                                  KSI_STR_PTR(KSI_EXN_MSG(exn)),
                                  ksi_obj2str(KSI_EXN_VAL(exn)),
                                  ksi_obj2name(KSI_EXN_SRC(exn)));
            } else {
                msg = ksi_aprintf("%ls\nerror-value: %ls",
                                  KSI_STR_PTR(KSI_EXN_MSG(exn)),
                                  ksi_obj2str(KSI_EXN_VAL(exn)));
            }
        } else {
            if (KSI_EXN_SRC(exn) != ksi_void) {
                msg = ksi_aprintf("%ls\nerror-source: %ls",
                                  KSI_STR_PTR(KSI_EXN_MSG(exn)),
                                  ksi_obj2name(KSI_EXN_SRC(exn)));
            } else {
                msg = KSI_STR_PTR(KSI_EXN_MSG(exn));
            }
        }

        ksi->m_script->error(QString("ksi: %1").arg(QString::fromWCharArray(msg)));
    }

    return 0;
}

static ksi_obj debug_proc(Ksi_Script_Context *ksi, ksi_obj msg)
{
    if (ksi) {
        if (KSI_STR_P(msg))
            ksi->m_script->debug(QString("ksi: %1").arg(QString::fromWCharArray(KSI_STR_PTR(msg))));
        else
            ksi->m_script->debug(QString("ksi: %1").arg(QString::fromWCharArray(ksi_obj2str(msg))));
    }
    return ksi_void;
}

static ksi_obj warn_proc(Ksi_Script_Context *ksi, ksi_obj msg)
{
    if (ksi) {
        if (KSI_STR_P(msg))
            ksi->m_script->warn(QString("ksi: %1").arg(QString::fromWCharArray(KSI_STR_PTR(msg))));
        else
            ksi->m_script->warn(QString("ksi: %1").arg(QString::fromWCharArray(ksi_obj2str(msg))));
    }
    return ksi_void;
}

static ksi_obj info_proc(Ksi_Script_Context *ksi, ksi_obj msg)
{
    if (ksi) {
        if (KSI_STR_P(msg))
            ksi->m_script->info(QString("ksi: %1").arg(QString::fromWCharArray(KSI_STR_PTR(msg))));
        else
            ksi->m_script->info(QString("ksi: %1").arg(QString::fromWCharArray(ksi_obj2str(msg))));
    }
    return ksi_void;
}


static ksi_obj var2obj(Script *script, const QVariant &val)
{
    switch (val.type()) {
    case QVariant::Invalid:
        return ksi_void;

    case QVariant::Bool:
        return val.toBool() ? ksi_true : ksi_false;

    case QVariant::Double:
        return ksi_double2num(val.toDouble());

    case QVariant::Int:
        return ksi_long2num(val.toInt());

    case QVariant::LongLong:
        return ksi_longlong2num(val.toLongLong());

    case QVariant::UInt:
        return ksi_ulong2num(val.toUInt());

    case QVariant::ULongLong:
        return ksi_ulonglong2num(val.toULongLong());

    case QVariant::String:
    {
        QString str = val.toString();
        QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
        int len = str.toWCharArray(buf.data());
        return ksi_str2string(buf.data(), len);
    }

    case QVariant::StringList:
    {
        QStringList ls(val.toStringList());
        ksi_obj xs = ksi_alloc_vector(ls.size(), KSI_TAG_VECTOR);
        for (int i = 0; i < ls.size(); i++) {
            QString str = ls.at(i);
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            KSI_VEC_REF(xs, i) = ksi_str2string(buf.data(), len);
        }
        return xs;
    }

    case QVariant::List:
    {
        QVariantList ls(val.toList());
        ksi_obj xs = ksi_alloc_vector(ls.size(), KSI_TAG_VECTOR);
        for (int i = 0; i < ls.size(); i++) {
            KSI_VEC_REF(xs, i) = var2obj(script, ls.at(i));
        }
        return xs;
    }

    case QVariant::Map:
    {
        QVariantMap map(val.toMap());
        ksi_obj xs = ksi_nil;
        QVariantMap::const_iterator i = map.constBegin();
        while (i != map.constEnd()) {
            QString str = i.key();
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            ksi_obj key = ksi_str2sym(buf.data(), len);
            ksi_obj val = var2obj(script, i.value());
            xs = ksi_acons(key, val, xs);
            ++i;
        }
        return xs;
    }

    default:
        if (val.canConvert<ScriptObject*>()) {
            ScriptObject *so = val.value<ScriptObject*>();
            ksi_obj argv[2];
            argv[0] = reinterpret_cast<ksi_obj>(script);
            argv[1] = reinterpret_cast<ksi_obj>(so);
            return ksi_close_proc((ksi_obj) shape_prim, 2, argv);
        }
        break;
    }

    if (script) {
        QString msg = QString("cannot convert value to ksi_obj: %1").arg(val.toString());
        script->warn(msg);
    }
    return ksi_void;
}

static bool is_alist(ksi_obj xs)
{
    while (KSI_PAIR_P(xs)) {
        ksi_obj x = KSI_CAR(xs);
        if (!KSI_PAIR_P(x))
            return false;

        x = KSI_CAR(x);
        if (!KSI_SYM_P(x) && !KSI_KEY_P(x))
            return false;

        xs = KSI_CDR(xs);
    }
    return true;
}

static QVariant obj2var(const Script *script, ksi_obj val)
{
    if (!val)
        return QVariant();

    if (val == ksi_void)
        return QVariant();

    if (val == ksi_false)
        return QVariant(false);

    if (val == ksi_true)
        return QVariant(true);

    if (ksi_long_p(val))
        return QVariant((int) ksi_num2long(val, "maksi"));

    if (ksi_ulong_p(val))
        return QVariant((uint) ksi_num2ulong(val, "maksi"));

    if (ksi_longlong_p(val))
        return QVariant((qlonglong) ksi_num2longlong(val, "maksi"));

    if (ksi_ulonglong_p(val))
        return QVariant((qulonglong) ksi_num2ulonglong(val, "maksi"));

    if (KSI_REAL_P(val))
        return QVariant(ksi_real_part(val));

    if (KSI_STR_P(val))
        return QVariant(QString::fromWCharArray(KSI_STR_PTR(val)));

    if (KSI_SYM_P(val))
        return QVariant(QString::fromWCharArray(KSI_SYM_PTR(val)));

    if (KSI_KEY_P(val))
        return QVariant(QString::fromWCharArray(KSI_KEY_PTR(val)));

    if (KSI_VEC_P(val)) {
        QVariantList ls;
        for (int i = 0; i < KSI_VEC_LEN(val); i++) {
            ls << obj2var(script, KSI_VEC_REF(val, i));
        }
        return QVariant(ls);
    }

    if (KSI_LIST_P(val)) {
        if (is_alist(val)) {
            QVariantMap ls;
            while (KSI_PAIR_P(val)) {
                ksi_obj k = KSI_CAR(KSI_CAR(val));
                QString key;
                if (KSI_SYM_P(k)) {
                    key = QString::fromWCharArray(KSI_SYM_PTR(val));
                } else if (KSI_KEY_P(k)) {
                    key = QString::fromWCharArray(KSI_KEY_PTR(val));
                } else {
                    key = QString::fromWCharArray(ksi_obj2str(k));
                    if (script) {
                        QString msg = QString("internal error: cannot convert ksi_obj to string: %1").arg(key);
                        script->warn(msg);
                    }
                }

                ls.insert(key, obj2var(script, KSI_CDR(KSI_CAR(val))));
                val = KSI_CDR(val);
            }
            return QVariant(ls);
        } else {
            QVariantList ls;
            while (KSI_PAIR_P(val)) {
                ls << obj2var(script, KSI_CAR(val));
                val = KSI_CDR(val);
            }
            return QVariant(ls);
        }
    }

    QString str = QString::fromWCharArray(ksi_obj2str(val));
    if (script) {
        QString msg = QString("cannot convert ksi_obj to value: %1").arg(str);
        script->warn(msg);
    }
    return QVariant();
}


struct Eval_Data
{
    Eval_Data() : prog(0), data(0), file(0), compiled(0), line(0) {}

    ksi_env env;
    const QString *prog;
    const QByteArray *data;
    const QString *file;
    ksi_obj *compiled;
    int line;
};

static ksi_obj eval_proc(ksi_context_t /*ksi*/, void *_data)
{
    Eval_Data *data = (Eval_Data *)_data;

    if (data->compiled && *data->compiled) {
        return ksi_eval(*data->compiled, data->env);
    }

    if (data->data) {
        return ksi_eval_bytes(data->data->constData(), data->data->size(), data->env, (data->file ? data->file->toLocal8Bit().constData() : 0), data->line, data->compiled);
    }

    QScopedArrayPointer<wchar_t> buf(new wchar_t[data->prog->length() + 1]);
    int len = data->prog->toWCharArray(buf.data());
    buf[len] = L'\0';
    ksi_obj val = ksi_eval_string(buf.data(), data->env, (data->file ? data->file->toLocal8Bit().constData() : 0), data->line, data->compiled);

    return val;
}


class CallContext_Ksi : public CallContext
{
public:
    CallContext_Ksi(Script_Ksi *script, ksi_env env) : m_script(script), m_env(env) {}
    ~CallContext_Ksi();

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    virtual QVariant eval(ScriptParam &code);

private:
    Script_Ksi *m_script;
    ksi_env m_env;
};

CallContext_Ksi::~CallContext_Ksi()
{
}

QVariant CallContext_Ksi::eval(ScriptParam &code)
{
    ksi_obj compiled_code = (ksi_obj) code.m_data;
    if (compiled_code) {
        Eval_Data data;
        data.env = m_env;
        data.compiled = &compiled_code;

        ksi_obj val = ksi_call_in_context(m_script->ksi(), &data, eval_proc, error_proc);
        return obj2var(m_script, val);
    }

    Eval_Data data;
    data.env = m_env;
    data.prog = &code.m_val;
    data.file = &code.m_file;
    data.line = code.m_line;
    data.compiled = &compiled_code;

    ksi_obj val = ksi_call_in_context(m_script->ksi(), &data, eval_proc, error_proc);
    if (compiled_code) {
        code.m_data = compiled_code;
        m_script->addObj(compiled_code); // protection from GC
    }
    return obj2var(m_script, val);
}


static ksi_obj get_arg(const wchar_t *keyname, ksi_obj val)
{
    ksi_obj key = ksi_str02key(keyname);
    while (KSI_PAIR_P(val)) {
        if (key == KSI_CAR(val)) {
            val = KSI_CDR(val);
            return KSI_PAIR_P(val) ? KSI_CAR(val) : ksi_void;
        }
        val = KSI_CDR(val);
        if (!KSI_PAIR_P(val))
            break;
        val = KSI_CDR(val);
    }
    return ksi_void;
}

static void insert_child(ScriptObject *shape, Script *script, int idx, ksi_obj val)
{
    KSI_CHECK(val, KSI_LIST_P(val), "maksi::shape.insert: invalid property list");

    ksi_obj tag = get_arg(L"tag", val);
    KSI_CHECK(tag, KSI_SYM_P(tag), "maksi::shape.insert: invalid tag");

    ksi_obj id = get_arg(L"id", val);
    KSI_CHECK(id, id == ksi_void || KSI_SYM_P(id), "maksi::shape.insert: invalid id");

    QString id_str;
    if (KSI_SYM_P(id))
        id_str = QString::fromWCharArray(KSI_SYM_PTR(id));

    ScriptObject *so = shape->insertChild(idx, id_str, QString::fromWCharArray(KSI_SYM_PTR(tag)));
    if (so) {
        while (KSI_PAIR_P(val)) {
            ksi_obj k = KSI_CAR(val);
            if (!KSI_KEY_P(k))
                break;

            val = KSI_CDR(val);
            if (!KSI_PAIR_P(val))
                break;

            ksi_obj v = KSI_CAR(val);
            val = KSI_CDR(val);

            so->setProperty(QString::fromWCharArray(KSI_SYM_PTR(k)), obj2var(script, v));
        }
    }
}

static ksi_obj shape_proc(Script *script, ScriptObject *so, ksi_obj id, ksi_obj val, ksi_obj val1, int argc, ksi_obj *argv)
{
    if (KSI_KEY_P(id)) {
        QString name(QString::fromWCharArray(KSI_KEY_PTR(id)));
        if (name == "id") {
            if (so->script_id().isEmpty())
                return ksi_void;
            QString str = so->script_id();
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            return ksi_str2sym(buf.data(), len);
        } else if (name == "tag") {
            QString str = so->script_tag();
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            return ksi_str2sym(buf.data(), len);
        } else if (name == "length") {
            return ksi_long2num(so->childCount());
        } else if (name == "valid") {
            return so->isValid() ? ksi_true : ksi_false;
        } else {
            for (int i = 0; i < so->propertyCount(); i++) {
                if (name == so->propertyId(i)) {
                    if (so->propertyLength(name) >= 0) {
                        long pos = ksi_num2long(val, "maksi::shape");
                        if (val1)
                            so->setProperty(name, pos, obj2var(script, val1));
                        return var2obj(script, so->property(name, pos));
                    } else {
                        if (val)
                            so->setProperty(name, obj2var(script, val));
                        return var2obj(script, so->property(name));
                    }
                }
            }

            if (name == "properties") {
                ksi_obj ls = ksi_nil;
                for (int i = 0; i < so->propertyCount(); i++) {
                    QString str = so->propertyId(i);
                    QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
                    int len = str.toWCharArray(buf.data());
                    ksi_obj x = ksi_str2key(buf.data(), len);
                    ls = ksi_cons(x, ls);
                }
                return ls;
            } else if (name == "has-property") {
                if (KSI_KEY_P(val)) {
                    QString name(QString::fromWCharArray(KSI_KEY_PTR(val)));
                    for (int i = 0; i < so->propertyCount(); i++) {
                        if (name == so->propertyId(i)) {
                            return ksi_true;
                        }
                    }
                }
                return ksi_false;
            } else if (name == "can-set-value") {
                if (KSI_KEY_P(val)) {
                    QString name(QString::fromWCharArray(KSI_KEY_PTR(val)));
                    for (int i = 0; i < so->propertyCount(); i++) {
                        if (name == so->propertyId(i)) {
                            if (so->isReadOnly(name))
                                return ksi_false;
                            return ksi_true;
                        }
                    }
                }
                return ksi_false;
            } else if (name == "delete") {
                if (KSI_KEY_P(val)) {
                    QString name(QString::fromWCharArray(KSI_KEY_PTR(val)));
                    for (int i = 0; i < so->propertyCount(); i++) {
                        if (name == so->propertyId(i)) {
                            if (so->propertyLength(name) >= 0) {
                                long pos = ksi_num2long(val1, "maksi::shape");
                                so->removeProperty(name, pos);
                            }
                            return ksi_void;
                        }
                    }
                } else if (KSI_SYM_P(val)) {
                    QString name(QString::fromWCharArray(KSI_SYM_PTR(val)));
                    for (int i = 0; i < so->childCount(); i++) {
                        if (name == so->child(i)->script_id()) {
                            so->removeChild(i);
                            return ksi_void;
                        }
                    }
                } else if (ksi_long_p(val)) {
                    long idx = ksi_num2long(val, "maksi::shape");
                    KSI_CHECK(val, 0 <= idx && idx < so->childCount(), "maksi::shape: child index out of range");
                    so->removeChild(idx);
                    return ksi_void;
                }
            } else if (name == "insert") {
                if (KSI_KEY_P(val)) {
                    QString name(QString::fromWCharArray(KSI_KEY_PTR(val)));
                    if (so->propertyLength(name) >= 0) {
                        long pos = ksi_num2long(val1, "maksi::shape");
                        for (int i = 0; i < argc; i++) {
                            so->insertProperty(name, pos, obj2var(script, argv[i]));
                            pos++;
                        }
                    }
                    return ksi_void;
                } else if (ksi_long_p(val)) {
                    long idx = ksi_num2long(val, "maksi::shape");
                    KSI_CHECK(val, 0 <= idx && idx <= so->childCount(), "maksi::shape: child index out of range");
                    insert_child(so, script, idx, val1);
                    return ksi_void;
                }
            }
        }
        return ksi_void;
    }

    if (KSI_SYM_P(id)) {
        QString name(QString::fromWCharArray(KSI_SYM_PTR(id)));

        int count = so->childCount();
        for (int i = 0; i < count; i++) {
            if (name == so->child(i)->script_id()) {
                ksi_obj argv[1];
                argv[0] = reinterpret_cast<ksi_obj>(script);
                argv[1] = reinterpret_cast<ksi_obj>(so->child(i));
                return ksi_close_proc((ksi_obj) shape_prim, 2, argv);
            }
        }

        count = so->methodCount();
        for (int i = 0; i < count; i++) {
            if (name == so->methodId(i)) {
                QVariantList args;
                if (val) {
                    args.append(obj2var(script, val));
                    if (val1) {
                        args.append(obj2var(script, val1));
                        for (int i = 0; i < argc; i++) {
                            args.append(obj2var(script, argv[i]));
                        }
                    }
                }
                QVariant res = so->callMethod(name, args);
                if (res.isValid())
                    return var2obj(script, res);
                break;
            }
        }
        return ksi_void;
    }

    if (ksi_long_p(id)) {
        long idx = ksi_num2long(id, "maksi::shape");
        KSI_CHECK(id, 0 <= idx && idx < so->childCount(), "maksi::shape: child index out of range");

        // return child object
        ksi_obj argv[1];
        argv[0] = reinterpret_cast<ksi_obj>(script);
        argv[1] = reinterpret_cast<ksi_obj>(so->child(idx));
        return ksi_close_proc((ksi_obj) shape_prim, 2, argv);
    }

    return ksi_void;
}

static ksi_obj signal_proc(Script_Ksi *script, ksi_obj signal, int argc, ksi_obj *argv)
{
    QString name = QString::fromWCharArray(ksi_obj2name(signal));

    QVariantList args;
    for (int i = 0; i < argc; i++) {
        args.append(obj2var(script, argv[i]));
    }

    QVariant retval = script->callCallback(name, args);
    return var2obj(script, retval);
}

struct Call_Data
{
    ksi_env env;
    ksi_obj *syms;
    Call_Data *next;
};

static ksi_obj call_proc(Script_Ksi *script, ksi_obj proc_id, int argc, ksi_obj *argv)
{
    QString name = QString::fromWCharArray(ksi_obj2name(proc_id));

    ScriptProc *proc = script->proc(name);
    if (!proc) {
        ksi_exn_error(0, proc_id, "cannot find proc %ls", ksi_obj2str(proc_id));
    }

    Call_Data *data = (Call_Data *) proc->m_data;
    if (!data) {
        data = (Call_Data *) ksi_malloc(sizeof *data);
        data->env = ksi_new_env(proc->argsCount(), script->env());
        data->syms = (ksi_obj *) ksi_malloc(sizeof(ksi_obj) * proc->argsCount());
        for (int i = 0; i < proc->argsCount(); i++) {
            QString str = proc->arg(i);
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            ksi_obj sym = ksi_str2sym(buf.data(), len);
            data->syms[i] = sym;
        }

        // protect data from garbage collector
        data->next = script->m_callData;
        script->m_callData = data;
        proc->m_data = data;
    }

    for (int i = 0; i < proc->argsCount(); i++) {
        if (i < argc) {
            ksi_define_local(data->syms[i], argv[i], data->env);
        } else {
            ksi_define_local(data->syms[i], ksi_void, data->env);
        }
    }

    CallContext_Ksi ctx(script, data->env);
    QVariant val = script->callScriptProc(proc, ctx);

    return var2obj(script, val);
}


Ksi_Script_Context::Ksi_Script_Context(Script_Ksi *script)
{
    ksi_init_context(this, 0);
    m_script = script;
}

Ksi_Script_Context::~Ksi_Script_Context()
{
}

Script_Ksi::Script_Ksi(const QString &id, Worker *worker) :
    Script(id, worker),
    m_callData(0),
    m_lst(ksi_nil)
{
    m_ksi = new Ksi_Script_Context(this);
    m_ksi->errlog_proc = errlog_proc;
    m_ksi->errlog_priority = ERRLOG_DEBUG;

    m_env = ksi_top_level_env();

    ksi_obj argv[1];
    argv[0] = reinterpret_cast<ksi_obj>(m_ksi);

    ksi_prim prim = ksi_new_prim(L"debug", (ksi_proc_t) debug_proc, KSI_CALL_ARG2, 2);
    ksi_define(ksi_str02sym(L"debug"), ksi_close_proc((ksi_obj) prim, 1, argv), m_env);

    prim = ksi_new_prim(L"info", (ksi_proc_t) info_proc, KSI_CALL_ARG2, 2);
    ksi_define(ksi_str02sym(L"info"), ksi_close_proc((ksi_obj) prim, 1, argv), m_env);

    prim = ksi_new_prim(L"warn", (ksi_proc_t) warn_proc, KSI_CALL_ARG2, 2);
    ksi_define(ksi_str02sym(L"warn"), ksi_close_proc((ksi_obj) prim, 1, argv), m_env);

    if (!shape_prim)
        shape_prim = ksi_new_prim(L"shape", (ksi_proc_t) shape_proc, KSI_CALL_REST5, 3);
    if (!signal_prim)
        signal_prim = ksi_new_prim(L"signal", (ksi_proc_t) signal_proc, KSI_CALL_REST2, 2);
    if (!call_prim)
        call_prim = ksi_new_prim(L"proc", (ksi_proc_t) call_proc, KSI_CALL_REST2, 2);
}

Script_Ksi::~Script_Ksi()
{
    delete m_ksi;
}

ksi_obj Script_Ksi::addObj(ksi_obj x)
{
    m_lst = ksi_cons(x, m_lst);
    return x;
}

QVariant Script_Ksi::evalCode(const Conf &code)
{
    QString prog = code.text();
    QString file = code.source();

    Eval_Data data;
    data.env = m_env;
    data.prog = &prog;
    data.file = &file;
    data.line = code.line();

    ksi_obj val = ksi_call_in_context(m_ksi, &data, eval_proc, error_proc);
    return obj2var(this, val);
}

QVariant Script_Ksi::evalData(const QByteArray &code, const QString &file)
{
    Eval_Data data;
    data.env = m_env;
    data.data = &code;
    data.file = &file;
    data.line = 1;

    ksi_obj val = ksi_call_in_context(m_ksi, &data, eval_proc, error_proc);
    return obj2var(this, val);
}

QVariant Script_Ksi::getVar(const QString &name) const
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[name.length()]);
    int len = name.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    if (KSI_TRUE_P(ksi_bound_p(sym, m_env))) {
        ksi_obj val = ksi_var_ref(m_env, sym);
        return obj2var(this, val);
    }

    return QVariant();
}

result_t Script_Ksi::setVar(const QString &name, const QVariant &val, bool /*def*/)
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[name.length()]);
    int len = name.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    ksi_obj x = var2obj(this, val);
    if (!x) {
        warn(QString("Script_Ksi::setVar(): cannot assign invalid value to variable %1").arg(name));
        return RESULT_INVALID_SCRIPT;
    }

    if (KSI_FALSE_P(ksi_bound_p(sym, m_env))) {
        ksi_define(sym, x, m_env);
        return RESULT_OK;
    }
    if (KSI_TRUE_P(ksi_var_p(m_env, sym))) {
        ksi_var_set(m_env, sym, x);
        return RESULT_OK;
    }

    warn(QString("Script_Ksi::setVar(): cannot assign imported variable %1").arg(name));
    return RESULT_INVALID_SCRIPT;
}

result_t Script_Ksi::defCallback(const QString &id, const QString &file, int line)
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[id.length()]);
    int len = id.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    if (KSI_TRUE_P(ksi_bound_p(sym, m_env))) {
        warn(QString("%1:%2: cannot define callback %1 (variable with such name already exists)").arg(file).arg(line).arg(id));
        return RESULT_INVALID_SCRIPT;
    }

    ksi_obj argv[2];
    argv[0] = reinterpret_cast<ksi_obj>(this);
    argv[1] = sym;
    ksi_obj val = ksi_close_proc((ksi_obj) signal_prim, 2, argv);
    ksi_define(sym, val, m_env);

    return RESULT_OK;
}

result_t Script_Ksi::defProc(const QString &proc_id, const QStringList &/*args*/, const QString &file, int line)
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[proc_id.length()]);
    int len = proc_id.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    if (KSI_TRUE_P(ksi_bound_p(sym, m_env))) {
        warn(QString("%1:%2: cannot define proc %1 (variable with such name already exists)").arg(file).arg(line).arg(proc_id));
        return RESULT_INVALID_SCRIPT;
    }

    ksi_obj argv[2];
    argv[0] = reinterpret_cast<ksi_obj>(this);
    argv[1] = sym;
    ksi_obj val = ksi_close_proc((ksi_obj) call_prim, 2, argv);
    ksi_define(sym, val, m_env);

    return RESULT_OK;
}


QVariant Script_Ksi::evalCode(ScriptObject *so, const QString &code, const QString &file, int line)
{
    Script *script = this;
    ksi_obj argv[2];
    argv[0] = reinterpret_cast<ksi_obj>(script);
    argv[1] = reinterpret_cast<ksi_obj>(so);
    ksi_obj val = ksi_close_proc((ksi_obj) shape_prim, 2, argv);

    ksi_env env = ksi_new_env(1, m_env);
    ksi_define_local(ksi_str02sym(L"this"), val, env);

    Eval_Data data;
    data.env = env;
    data.prog = &code;
    data.file = &file;
    data.line = line;

    val = ksi_call_in_context(m_ksi, &data, eval_proc, error_proc);
    return obj2var(script, val);
}

QVariant Script_Ksi::callCode(ScriptObject *so, const QString &code, const QStringList &args, const QVariantList &params, const QString &file, int line)
{
    Script *script = this;
    ksi_obj argv[2];
    argv[0] = reinterpret_cast<ksi_obj>(script);
    argv[1] = reinterpret_cast<ksi_obj>(so);
    ksi_obj val = ksi_close_proc((ksi_obj) shape_prim, 2, argv);

    ksi_env env = ksi_new_env(args.size(), m_env);
    ksi_define_local(ksi_str02sym(L"this"), val, env);
    for (int i = 0; i < args.size(); i++) {
        QString str = args.at(i);
        QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
        int len = str.toWCharArray(buf.data());
        ksi_obj sym = ksi_str2sym(buf.data(), len);

        if (i < params.size()) {
            ksi_define_local(sym, var2obj(this, params.at(i)), env);
        } else {
            ksi_define_local(sym, ksi_void, env);
        }
    }

    Eval_Data data;
    data.env = env;
    data.prog = &code;
    data.file = &file;
    data.line = line;

    val = ksi_call_in_context(m_ksi, &data, eval_proc, error_proc);
    return obj2var(script, val);
}

bool Script_Ksi::evalIf(ScriptObject *so, const QString &code)
{
    Script *script = this;
    ksi_obj argv[2];
    argv[0] = reinterpret_cast<ksi_obj>(script);
    argv[1] = reinterpret_cast<ksi_obj>(so);
    ksi_obj val = ksi_close_proc((ksi_obj) shape_prim, 2, argv);

    ksi_env env = ksi_new_env(1, m_env);
    ksi_define_local(ksi_str02sym(L"this"), val, env);

    Eval_Data data;
    data.env = env;
    data.prog = &code;

    val = ksi_call_in_context(m_ksi, &data, eval_proc, 0);
    if (!val || val == ksi_void || val == ksi_false)
        return false;
    return true;
}


struct Apply_Data
{
    ksi_obj fun;
    int size;
    ksi_obj *args;
};

static ksi_obj apply_proc(ksi_context_t /*ksi*/, void *_data)
{
    Apply_Data *data = (Apply_Data *)_data;

    return ksi_apply_proc(data->fun, data->size, data->args);
}

QVariant Script_Ksi::callProc(const QString &proc_id, const QVariantList &args)
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[proc_id.length()]);
    int len = proc_id.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    if (KSI_TRUE_P(ksi_bound_p(sym, m_env))) {
        Apply_Data data;
        data.fun = ksi_var_ref(m_env, sym);
        data.size = args.size();
        data.args = (ksi_obj *) ksi_malloc(args.size() * sizeof(data.args[0]));
        for (int i = 0; i < args.size(); i++) {
            data.args[i] = var2obj(this, args.at(i));
        }

        ksi_obj val = ksi_call_in_context(m_ksi, &data, apply_proc, error_proc);
        return obj2var(this, val);
    }

    error(QString("cannot find procedure %1").arg(proc_id));
    return QVariant();
}

};

 /* End of code */
