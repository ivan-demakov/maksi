include(../maksi.pri)

TEMPLATE = lib
CONFIG += plugin
QT -= gui
QT += network

unix {
  VERSION = $${MAKSI_VERSION}
  TARGET = $$qtLibraryTarget(maksi_ksi)
  CONFIG += debug
  INCLUDEPATH += $$system(ksi-config --includedir)
  LIBS += $$system(ksi-config --link)
}
win32 {
  TARGET = maksi_ksi
  LIBS += -lmaksi
  CONFIG += release
  INCLUDEPATH += U:/include/ksi
  LIBS += U:/lib/ksi-391.lib
}

INCLUDEPATH += ..

HEADERS = maksi_ksi.h
SOURCES = maksi_ksi.cpp

OTHER_FILES  += maksi-ksi.json
