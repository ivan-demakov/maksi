/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2010, 2011, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    script_ksi.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Wed Jun 23 09:07:21 2010
 *
 * @brief   ksi script
 *
 */

#ifndef SCRIPT_KSI_H
#define SCRIPT_KSI_H

#include <maksi/script.h>

#include <ksi/ksi.h>


namespace maksi
{

class Script_Ksi;
class Call_Data;


struct Ksi_Script_Context : public Ksi_Context
{
    Ksi_Script_Context(Script_Ksi *script);
    ~Ksi_Script_Context();

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    Script_Ksi *m_script;
};

class Script_Ksi : public Script
{
public:
    Script_Ksi(const QString &id, Worker *worker);
    virtual ~Script_Ksi();

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    virtual QVariant evalData(const QByteArray &data, const QString &file);
    virtual QVariant evalCode(const Conf &code);

    virtual QVariant getVar(const QString &id) const;
    virtual result_t setVar(const QString &id, const QVariant &val, bool def);

    virtual result_t defCallback(const QString &id, const QString &file, int line);

    virtual result_t defProc(const QString &proc_id, const QStringList &args, const QString &file, int line);
    virtual QVariant callProc(const QString &proc_id, const QVariantList &args);

    virtual QVariant evalCode(ScriptObject *so, const QString &code, const QString &file, int line);
    virtual QVariant callCode(ScriptObject *so, const QString &code, const QStringList &args, const QVariantList &params, const QString &file, int line);
    virtual bool evalIf(ScriptObject *so, const QString &code);

    ksi_env env() const { return m_env; }
    Ksi_Script_Context * ksi() const { return m_ksi; }

    ksi_obj addObj(ksi_obj x);
    Call_Data *m_callData;

private:
    ksi_env m_env;
    Ksi_Script_Context *m_ksi;
    ksi_obj m_lst;

    friend class ShapeContext_Ksi;
};

};

#endif

/* End of file */
