/* -*- tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

/**
 * @file    maksi_js.cpp
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:50 2011
 *
 * @brief   javascript engine
 *
 */

#include "maksi_ksi.h"
#include "maksi/xml.h"
#include "maksi/package.h"

#include <QtPlugin>
#include <QVariant>
#include <QLinkedList>
#include <QVector>

#if defined(_MSC_VER)
#  pragma comment(lib, "maksi")
#  pragma comment(lib, "ksi-" KSI_VERSION ".lib")
#endif

namespace maksi
{

static QString sym_ns(QString::fromLatin1("http://maksi.su/maksi/ksi/symbol"));
static QString key_ns(QString::fromLatin1("http://maksi.su/maksi/ksi/keyword"));


Ksi_EnginePlugin::~Ksi_EnginePlugin()
{
}

QString Ksi_EnginePlugin::description() const
{
    return "Ksi";
}

Engine *Ksi_EnginePlugin::newEngine(Maksi *maksi, const QString &name) const
{
    return new Ksi_MaksiEngine(maksi, name);
}


static struct Ksi_ETag tc_element =
{
    L"MaksiElement",
    ksi_default_tag_equal,
    Ksi_MaksiElement::print,
    Ksi_MaksiElement::apply
};

Ksi_MaksiElement::Ksi_MaksiElement()
{
    this->o.itag = KSI_TAG_EXTENDED;
    this->etag = &tc_element;

    ksi_register_finalizer(this, finalizer, 0);
}

Ksi_MaksiElement::Ksi_MaksiElement(const MaksiElementPtr &val) :
    m_val(val)
{
    this->o.itag = KSI_TAG_EXTENDED;
    this->etag = &tc_element;

    ksi_register_finalizer(this, finalizer, 0);
}

Ksi_MaksiElement::~Ksi_MaksiElement()
{
    ksi_unregister_finalizer(this);
}

ksi_obj Ksi_MaksiElement::apply(struct Ksi_EObj *_obj, int argc, ksi_obj *argv)
{
    Ksi_MaksiElement *obj = (Ksi_MaksiElement *)_obj;

    if (argc == 0) {
        QString str = obj->m_val->elementXmlText();

        QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
        int len = str.toWCharArray(buf.data());
        return ksi_str2string(buf.data(), len);
    }

    if (ksi_int_p(argv[0])) {
        KSI_WNA(argc == 1, (ksi_obj) obj, L"MaksiElement");

        int id = ksi_num2int(argv[0], "MaksiElement");
        KSI_CHECK(argv[0], 0 <= id && id < obj->m_val->elementCount(), "MaksiElement: child element index out of range");

        MaksiElementPtr res = obj->m_val->element(id);
        return Ksi_MaksiContext::fromElement(res);
    }

    if (KSI_KEY_P(argv[0])) {
        QString name = QString::fromWCharArray(KSI_KEY_PTR(argv[0]), KSI_KEY_LEN(argv[0]));

        if (name == "length") {
            KSI_WNA(argc == 1, (ksi_obj) obj, L"MaksiElement");
            return ksi_int2num(obj->m_val->elementCount());
        }
        if (obj->m_val->hasAttribute(name)) {
            if (argc == 1) {
                MaksiElementPtr res = obj->m_val->attribute(name);
                return Ksi_MaksiContext::fromElement(res);
            } else {
                KSI_WNA(argc == 2, (ksi_obj) obj, L"MaksiElement");
                MaksiElementPtr res = Ksi_MaksiContext::toElement(argv[1]);
                obj->m_val->setAttribute(name, res);
                return ksi_void;
            }
        }
    }

    if (KSI_SYM_P(argv[0])) {
        QString name = QString::fromWCharArray(KSI_SYM_PTR(argv[0]), KSI_SYM_LEN(argv[0]));

        if (name == "append") {
            KSI_WNA(argc == 2, (ksi_obj) obj, L"MaksiElement");
            return (obj->m_val->elementAppend(Ksi_MaksiContext::toElement(argv[1])) ? ksi_true : ksi_false);
        }

        QList<QString> ms = obj->m_val->elementMethods();
        for (int i = 0; i < ms.size(); i++) {
            if (name == ms.at(i)) {
                QList<QVariant> args;
                for (int n = 1; n < argc; n++) {
                    args.append(Ksi_MaksiEngine::toVariant(argv[n]));
                }

                QVariant res = obj->m_val->callMethod(name, args);
                return Ksi_MaksiEngine::fromVariant(res);
            }
        }
    }

    return ksi_void;
}

const wchar_t *Ksi_MaksiElement::print(struct Ksi_EObj *_obj, int)
{
    Ksi_MaksiElement *obj = (Ksi_MaksiElement *)_obj;
    QString str = obj->m_val->elementName()->toString();

    QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length() + 1]);
    int len = str.toWCharArray(buf.data());
    buf[len] = L'\0';
    return ksi_aprintf("#<MaksiElement %ls>", buf.data());
}

void Ksi_MaksiElement::finalizer(void *_obj, void *)
{
    Ksi_MaksiElement *obj = (Ksi_MaksiElement *)_obj;

    obj->m_val.clear();
}

bool Ksi_MaksiElement::is(ksi_obj x)
{
    return KSI_EXT_IS(x, &tc_element);
}


static struct Ksi_ETag tc_type =
{
    L"MaksiType",
    ksi_default_tag_equal,
    ksi_default_tag_print,
    Ksi_MaksiType::apply
};

Ksi_MaksiType::Ksi_MaksiType()
{
    this->o.itag = KSI_TAG_EXTENDED;
    this->etag = &tc_type;

    ksi_register_finalizer(this, finalizer, 0);
}

Ksi_MaksiType::Ksi_MaksiType(Type type) :
    m_type(type)
{
    this->o.itag = KSI_TAG_EXTENDED;
    this->etag = &tc_type;

    ksi_register_finalizer(this, finalizer, 0);
}

Ksi_MaksiType::~Ksi_MaksiType()
{
}

void Ksi_MaksiType::finalizer(void *_obj, void *)
{
    Ksi_MaksiType *type = (Ksi_MaksiType *)_obj;

    type->m_type.clear();
}

ksi_obj Ksi_MaksiType::apply(struct Ksi_EObj *_obj, int argc, ksi_obj *argv)
{
    Ksi_MaksiType *type = (Ksi_MaksiType *)_obj;

    KSI_WNA(argc == 1 || argc == 2, (ksi_obj) type, L"MaksiType");

    QString name;
    if (KSI_STR_P(argv[0])) {
        name = QString::fromWCharArray(KSI_STR_PTR(argv[0]), KSI_STR_LEN(argv[0]));
    } else if (KSI_SYM_P(argv[0])) {
        name = QString::fromWCharArray(KSI_SYM_PTR(argv[0]), KSI_SYM_LEN(argv[0]));
    } else {
        ksi_exn_error(L"assertion", argv[0], "MaksiType: invalid name (must be string or symbol) in arg1");
    }

    name_t xname;
    if (argc <= 1) {
        xname = maksi::xml::qname(name);
    } else if (argc <= 2) {
        QString sname;
        if (KSI_STR_P(argv[1])) {
            sname = QString::fromWCharArray(KSI_STR_PTR(argv[1]), KSI_STR_LEN(argv[1]));
        } else {
            ksi_exn_error(L"assertion", argv[1], "maksiType: invalid namespace (must be string) in arg2");
        }

        xname = maksi::xml::qname(name, sname);
    }

    if (type->m_type.isValid()) {
        MaksiElementPtr val = type->m_type.createNew(0);
        val->setElementName(xname);

        return Ksi_MaksiContext::fromElement(val);
    }
    return ksi_void;
}


Ksi_MaksiEngine::Ksi_MaksiEngine(Maksi *maksi, const QString &name) :
    maksi::Engine(maksi, name)
{
    m_ksi = new Maksi_Ksi_Context(this);
    ksi_init_context(m_ksi, 0);
    m_ksi->loader_proc = loader;
    m_ksi->loader_data = this;
    m_ksi->errlog_proc = errlog;

    m_internal = &m_ksiInternal;
    m_ksiInternal.m_env = ksi_top_level_env();
    m_ksiInternal.m_ctx = m_ksi;

    ksi_obj av[2];
    av[0] = reinterpret_cast<ksi_obj>(this);

    ksi_prim printPrim = ksi_new_prim(L"print", (ksi_proc_t) print, KSI_CALL_REST2, 2);

    av[1] = reinterpret_cast<ksi_obj>((qintptr) MESSAGE_DEBUG);
    ksi_define(ksi_str02sym(L"debug"), ksi_close_proc((ksi_obj) printPrim, 2, av), env());

    av[1] = reinterpret_cast<ksi_obj>((qintptr) MESSAGE_INFO);
    ksi_define(ksi_str02sym(L"info"), ksi_close_proc((ksi_obj) printPrim, 2, av), env());

    av[1] = reinterpret_cast<ksi_obj>((qintptr) MESSAGE_WARN);
    ksi_define(ksi_str02sym(L"warn"), ksi_close_proc((ksi_obj) printPrim, 2, av), env());

    ksi_prim typePrim = ksi_new_prim(L"MaksiType", (ksi_proc_t) maksiType, KSI_CALL_ARG3, 2);
    ksi_define(ksi_str02sym(L"MaksiType"), ksi_close_proc((ksi_obj) typePrim, 1, av), env());
}

Ksi_MaksiEngine::~Ksi_MaksiEngine()
{
    delete m_ksi;
}

void Ksi_MaksiEngine::sendMessage(message_t type, QString msg)
{
    maksi()->sendMessage(type, msg);
}

ksi_obj Ksi_MaksiEngine::maksiType(Ksi_MaksiEngine *ksi, ksi_obj _name, ksi_obj _sname)
{
    QString name;
    if (KSI_STR_P(_name)) {
        name = QString::fromWCharArray(KSI_STR_PTR(_name), KSI_STR_LEN(_name));
    } else if (KSI_SYM_P(_name)) {
        name = QString::fromWCharArray(KSI_SYM_PTR(_name), KSI_SYM_LEN(_name));
    } else {
        ksi_exn_error(L"assertion", _name, "MaksiType: invalid name (must be string or symbol) in arg1");
    }

    name_t xname;
    if (!_sname) {
        xname = xml::qname(name);
    } else {
        QString sname;
        if (KSI_STR_P(_sname)) {
            sname = QString::fromWCharArray(KSI_STR_PTR(_sname), KSI_STR_LEN(_sname));
        } else {
            ksi_exn_error(L"assertion", _sname, "MaksiType: invalid namespace (must be string) in arg2");
        }

        xname = xml::qname(name, sname);
    }

    Type type = ksi->m_ksi->m_engine->maksi()->type(xname);
    if (type.isValid()) {
        Ksi_MaksiType *x = new Ksi_MaksiType(type);
        return (ksi_obj) x;
    }

    return ksi_void;
}

ksi_obj Ksi_MaksiEngine::print(Ksi_MaksiEngine *ksi, ksi_obj _type, int argc, ksi_obj *argv)
{
    if (ksi) {
        message_t type = (message_t) (reinterpret_cast<qintptr>(_type));

        QString msg;
        for (int i = 0; i < argc; ++i) {
            const wchar_t *str;
            if (KSI_STR_P(argv[i]))
                str = KSI_STR_PTR(argv[i]);
            else
                str = ksi_obj2str(argv[i]);

            msg += QString::fromWCharArray(str);
        }

        ksi->sendMessage(type, msg);
    }
    return ksi_void;
}

ksi_byte_port Ksi_MaksiEngine::loader(const char *filename, void *data_)
{
    Ksi_MaksiEngine *engine = (Ksi_MaksiEngine *) data_;

    QUrl url(QString::fromLocal8Bit(filename), QUrl::StrictMode);
    QByteArray data;
    if (engine->maksi()->loadPackageData(url, &data)) {
        ksi_obj bvec = ksi_str2bytevector(data.data(), data.size());
        return ksi_new_bytevector_input_port(bvec);
    } else {
        engine->maksi()->sendMessage(MESSAGE_ERROR, QString("ksi: cannot load url '%1'").arg(url.toString()));
    }

    return 0;
}

void Ksi_MaksiEngine::errlog(ksi_context_t ksi_, int pri, const wchar_t *msg)
{
    Maksi_Ksi_Context *ksi = (Maksi_Ksi_Context *) ksi_;

    ksi->m_engine->sendMessage((message_t) pri, QString("ksi: %1").arg(QString::fromWCharArray(msg)));
}


struct Ksi_Eval
{
//    Ksi_Eval() : env(0), code(0), url(0), line(0) {}

    ksi_env env;
    const QString *code;
    const char *file;
    int line;

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }
};

ksi_obj Ksi_MaksiEngine::eval(ksi_context_t /*ksi*/, void *_data)
{
    Ksi_Eval *data = (Ksi_Eval *)_data;

    QScopedArrayPointer<wchar_t> buf(new wchar_t[data->code->length() + 1]);
    int len = data->code->toWCharArray(buf.data());
    buf[len] = L'\0';
    ksi_obj val = ksi_eval_string(buf.data(), data->env, data->file, data->line, 0);

    return val;
}

ksi_obj Ksi_MaksiEngine::error(ksi_context_t ksi_, ksi_obj /*tag*/, ksi_obj exn)
{
    Maksi_Ksi_Context *ksi = (Maksi_Ksi_Context *) ksi_;

    if (ksi) {
        const wchar_t *msg;

        if (KSI_EXN_VAL(exn) != ksi_void) {
            if (KSI_EXN_SRC(exn) != ksi_void) {
                msg = ksi_aprintf("%ls\nerror-value: %ls\nerror-source: %ls",
                                  KSI_STR_PTR(KSI_EXN_MSG(exn)),
                                  ksi_obj2str(KSI_EXN_VAL(exn)),
                                  ksi_obj2name(KSI_EXN_SRC(exn)));
            } else {
                msg = ksi_aprintf("%ls\nerror-value: %ls",
                                  KSI_STR_PTR(KSI_EXN_MSG(exn)),
                                  ksi_obj2str(KSI_EXN_VAL(exn)));
            }
        } else {
            if (KSI_EXN_SRC(exn) != ksi_void) {
                msg = ksi_aprintf("%ls\nerror-source: %ls",
                                  KSI_STR_PTR(KSI_EXN_MSG(exn)),
                                  ksi_obj2name(KSI_EXN_SRC(exn)));
            } else {
                msg = KSI_STR_PTR(KSI_EXN_MSG(exn));
            }
        }

        ksi->m_engine->sendMessage(MESSAGE_ERROR, QString("ksi: %1").arg(QString::fromWCharArray(msg)));
    }

    return 0;
}

bool Ksi_MaksiEngine::evalCode(const QString &code, const QUrl &url, int line, QVariant *rval)
{
    QByteArray file = url.toString().toLocal8Bit();

    Ksi_Eval data;
    data.env = env();
    data.code = &code;
    data.file = file.constData();
    data.line = line;

    ksi_obj val = ksi_call_in_context(m_ksi, &data, eval, error);
    if (val) {
        if (rval) {
            *rval = toVariant(val);
        }
        return true;
    }
    return false;
}

Script *Ksi_MaksiEngine::newScript(const QString &scriptName)
{
    return new Ksi_MaksiScript(maksi(), scriptName, this);
}

QVariant Ksi_MaksiEngine::toVariant(ksi_obj val)
{
    if (Ksi_MaksiElement::is(val)) {
        Ksi_MaksiElement *x = (Ksi_MaksiElement *) val;
        return QVariant::fromValue(x->m_val);
    }

    if (val == ksi_void) {
        return QVariant();
    } else if (val == ksi_false) {
        return false;
    } else if (val == ksi_true) {
        return true;
    } else if (KSI_SYM_P(val)) {
        return QVariant::fromValue(xml::qname(QString::fromWCharArray(KSI_SYM_PTR(val), KSI_SYM_LEN(val)), sym_ns));
    } else if (KSI_KEY_P(val)) {
        return QVariant::fromValue(xml::qname(QString::fromWCharArray(KSI_KEY_PTR(val), KSI_KEY_LEN(val)), key_ns));
    } else if (KSI_BIGNUM_P(val)) {
        if (ksi_long_p(val)) {
            long v = ksi_num2long(val, "Ksi_MaksiContext::toElement");
            if (INT_MIN <= v && v <= INT_MAX)
                return (int) v;
            return (qlonglong) v;
        } else if (ksi_ulong_p(val)) {
            ulong v = ksi_num2ulong(val, "Ksi_MaksiContext::toElement");
            if (v <= UINT_MAX)
                return (uint) v;
            return (qulonglong) v;
        } else if (ksi_longlong_p(val)) {
            return ksi_num2longlong(val, "Ksi_MaksiContext::toElement");
        } else if (ksi_ulonglong_p(val)) {
            return ksi_num2ulonglong(val, "Ksi_MaksiContext::toElement");
        }
        return ksi_real_part(val);
    } else if (KSI_FLONUM_P(val)) {
        if (ksi_real_p(val)) {
            return ksi_real_part(val);
        }
    } else if (KSI_CHAR_P(val)) {
        wchar_t ch = KSI_CHAR_CODE(val);
        return QChar(ch);
    } else if (KSI_STR_P(val)) {
        return QString::fromWCharArray(KSI_STR_PTR(val), KSI_STR_LEN(val));
    } else if (KSI_BVEC_P(val)) {
        QByteArray ba(KSI_BVEC_PTR(val), KSI_BVEC_LEN(val));
        return QVariant::fromValue(ba);
    } else if (KSI_LIST_P(val)) {
        QLinkedList<QVariant> ls;
        while (val != ksi_nil) {
            ls.append(toVariant(KSI_CAR(val)));
            val = KSI_CDR(val);
        }
        return QVariant::fromValue(ls);
    } else if (KSI_VEC_P(val)) {
        QVector<QVariant> ls(KSI_VEC_LEN(val));
        for (unsigned i = 0; i < KSI_VEC_LEN(val); ++i) {
            ls[i] = toVariant(KSI_VEC_REF(val, i));
        }
        return QVariant::fromValue(ls);
    }

    QString str = QString::fromWCharArray(ksi_obj2str(val));
    //ctx->m_maksi->sendMessage(MAKSI_MESSAGE_WARN, QString("cannot convert ksi_obj to MaksiElement: %1").arg(str));
    return QVariant(str);
}

ksi_obj Ksi_MaksiEngine::fromVariant(QVariant val)
{
    if (!val.isValid())
        return ksi_void;

    switch (static_cast<QMetaType::Type>(val.type())) {
    case QMetaType::Bool:
        return (val.toBool() ? ksi_true : ksi_false);

    case QMetaType::Int:
    case QMetaType::Short:
    case QMetaType::SChar:
        return ksi_long2num(val.toInt());

    case QMetaType::UInt:
    case QMetaType::UShort:
    case QMetaType::UChar:
        return ksi_ulong2num(val.toUInt());

    case QMetaType::Long:
    case QMetaType::LongLong:
        return ksi_longlong2num(val.toLongLong());

    case QMetaType::ULong:
    case QMetaType::ULongLong:
        return ksi_ulonglong2num(val.toULongLong());

    case QMetaType::Float:
    case QMetaType::Double:
        return ksi_double2num(val.toReal());

    case QMetaType::QChar:
        return ksi_int2char(val.toChar().unicode());

    case QMetaType::QByteArray:
    {
        QByteArray ba = val.toByteArray();
        return ksi_str2bytevector(ba.data(), ba.length());
    }

    case QMetaType::QString:
    {
        QString str = val.toString();
        QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
        int len = str.toWCharArray(buf.data());
        return ksi_str2string(buf.data(), len);
    }

    case QMetaType::QVariantList:
    {
        QVariantList ls = val.value<QVariantList>();
        ksi_obj x = ksi_alloc_vector(ls.size(), KSI_TAG_VECTOR);
        for (int i = 0; i < ls.size(); ++i) {
            KSI_VEC_REF(x, i) = fromVariant(ls.at(i));
        }
        return x;
    }

    default:
        break;
    }

    if (val.canConvert<name_t>()) {
        name_t name = val.value<name_t>();
        const QString &ns = name->namespaceUri();
        const QString &str = name->name();
        if (ns == sym_ns) {
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            return ksi_str2sym(buf.data(), len);
        } else if (ns == sym_ns) {
            QScopedArrayPointer<wchar_t> buf(new wchar_t[str.length()]);
            int len = str.toWCharArray(buf.data());
            return ksi_str2key(buf.data(), len);
        }
    } else if (val.canConvert<MaksiElementPtr>()) {
        MaksiElementPtr e = val.value<MaksiElementPtr>();
        Ksi_MaksiElement *x = new Ksi_MaksiElement(e);
        return (ksi_obj) x;
    } else if (val.canConvert<QLinkedList<QVariant> >()) {
        QLinkedList<QVariant> ls = val.value<QLinkedList<QVariant> >();
        ksi_obj x = ksi_nil, *px = &x;
        for (QLinkedList<QVariant>::const_iterator i = ls.begin(); i != ls.end(); ++i) {
            *px = ksi_cons(fromVariant(*i), *px);
            px = &KSI_CDR(*px);
        }
        return x;
    } else if (val.canConvert<QVector<QVariant> >()) {
        QVector<QVariant> ls = val.value<QVector<QVariant> >();
        ksi_obj x = ksi_alloc_vector(ls.size(), KSI_TAG_VECTOR);
        for (int i = 0; i < ls.size(); ++i) {
            KSI_VEC_REF(x, i) = fromVariant(ls.at(i));
        }
        return x;
    }

    Ksi_MaksiElement *obj = new Ksi_MaksiElement(MaksiElement::newVariant(val));
    return (ksi_obj) obj;
}


Ksi_MaksiScript::Ksi_MaksiScript(Maksi *maksi, const QString &name, Ksi_MaksiEngine *engine) :
    Script(maksi, name),
    m_engine(engine)
{
}

Ksi_MaksiScript::~Ksi_MaksiScript()
{
}

bool Ksi_MaksiScript::addArg(const QString &name, const QVariant &val, const MaksiElement *)
{
    m_args.append(Ksi_MaksiArg(name, val));
    return true;
}

void Ksi_MaksiScript::addCode(const QString &code, const MaksiElement *conf)
{
    Ksi_MaksiCode ks;
    ks.m_code = code;
    ks.m_file = conf->elementSourceUrl().toString().toLocal8Bit();
    ks.m_line = conf->elementSourceLine();

    m_code.append(ks);
}

maksi::Context *Ksi_MaksiScript::newContext()
{
    return new Ksi_MaksiContext(m_engine, m_args, m_code, m_engine->maksi());
}


Ksi_MaksiContext::Ksi_MaksiContext(Ksi_MaksiEngine *engine, QList<Ksi_MaksiArg> args, QList<Ksi_MaksiCode> code, Maksi *maksi) :
    m_compiled(0),
    m_env(0),
    m_ksi(engine->m_ksi),
    m_code(code),
    m_maksi(maksi)
{
    m_env = ksi_new_env(args.size(), engine->env());

    for (int i = 0; i < args.size(); ++i) {
        const Ksi_MaksiArg &arg(args.at(i));
        QScopedArrayPointer<wchar_t> buf(new wchar_t[arg.m_name.length()]);
        int len = arg.m_name.toWCharArray(buf.data());
        ksi_obj sym = ksi_str2sym(buf.data(), len);

        ksi_define_local(sym, Ksi_MaksiEngine::fromVariant(arg.m_val), m_env);
    }

    ksi_obj av[2];
    av[0] = reinterpret_cast<ksi_obj>(this);

    ksi_prim scriptContextPrim = ksi_new_prim(L"ScriptContext", (ksi_proc_t) scriptContext, KSI_CALL_ARG2, 2);
    ksi_define_local(ksi_str02sym(L"ScriptContext"), ksi_close_proc((ksi_obj) scriptContextPrim, 1, av), m_env);
}

Ksi_MaksiContext::~Ksi_MaksiContext()
{
    for (auto i = m_sxs.begin(); i != m_sxs.end(); ++i) {
        delete i.value();
    }
}

QVariant Ksi_MaksiContext::arg(const QString &name)
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[name.length()]);
    int len = name.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    if (KSI_TRUE_P(ksi_bound_p(sym, m_env))) {
        ksi_obj val = ksi_var_ref(m_env, sym);
        return Ksi_MaksiEngine::toVariant(val);
    }

    return QVariant();
}

void Ksi_MaksiContext::setArg(const QString &name, const QVariant &val)
{
    QScopedArrayPointer<wchar_t> buf(new wchar_t[name.length()]);
    int len = name.toWCharArray(buf.data());
    ksi_obj sym = ksi_str2sym(buf.data(), len);

    ksi_obj x = Ksi_MaksiEngine::fromVariant(val);
    if (KSI_FALSE_P(ksi_bound_p(sym, m_env))) {
        ksi_define(sym, x, m_env);
    } else if (KSI_TRUE_P(ksi_var_p(m_env, sym))) {
        ksi_var_set(m_env, sym, x);
    } else {
        m_maksi->sendMessage(MESSAGE_WARN, QString("Script_Ksi::setVar(): cannot assign imported variable: %1").arg(name));
    }
}

QVariant Ksi_MaksiContext::eval()
{
    ksi_obj val = ksi_call_in_context(m_ksi, this, evalProc, Ksi_MaksiEngine::error);
    return Ksi_MaksiEngine::toVariant(val);
}

ksi_obj Ksi_MaksiContext::evalProc(ksi_context_t /*ksi*/, void *_data)
{
    Ksi_MaksiContext *data = (Ksi_MaksiContext *)_data;

    if (!data->m_compiled) {
        data->m_compiled = ksi_nil;

        ksi_obj *ptr = &data->m_compiled;
        for (int i = 0; i < data->m_code.size(); ++i) {
            const Ksi_MaksiCode &code(data->m_code.at(i));

            QScopedArrayPointer<wchar_t> buf(new wchar_t[code.m_code.length() + 1]);
            int len = code.m_code.toWCharArray(buf.data());
            buf[len] = L'\0';
            ksi_obj compiled = ksi_comp_string(buf.data(), data->m_env, code.m_file, code.m_line);

            *ptr = ksi_cons(compiled, ksi_nil);
            ptr = &KSI_CDR(*ptr);
        }
    }

    ksi_obj res = ksi_void;
    ksi_obj code = data->m_compiled;
    while (code != ksi_nil) {
        ksi_obj xc = KSI_CAR(code);
        code = KSI_CDR(code);

//        QString s1 = QString::fromWCharArray(ksi_obj2str(xc));
//        qDebug("Script_Ksi::eval(): %s", qPrintable(s1));

        res = ksi_eval_code(xc, 0);

//        QString s2 = QString::fromWCharArray(ksi_obj2str(res));
//        qDebug("Script_Ksi::eval(): res=%s", qPrintable(s2));
    }

    return res;
}

ksi_obj Ksi_MaksiContext::fromElement(const MaksiElementPtr &val)
{
    if (!val)
        return ksi_void;

    if (!val->isElementList() && val->isElementSimple()) {
        QVariant vv = val->elementToVariant();
        return Ksi_MaksiEngine::fromVariant(vv);
    }

    Ksi_MaksiElement *obj = new Ksi_MaksiElement(val);
    return (ksi_obj) obj;
}

MaksiElementPtr Ksi_MaksiContext::toElement(ksi_obj val)
{
    if (Ksi_MaksiElement::is(val)) {
        Ksi_MaksiElement *x = (Ksi_MaksiElement *) val;
        return x->m_val;
    }

    if (val == ksi_void) {
        return MaksiElementPtr();
    } else if (val == ksi_false) {
        return MaksiElement::newBool(false);
    } else if (val == ksi_true) {
        return MaksiElement::newBool(true);
    } else if (KSI_SYM_P(val)) {
        return MaksiElement::newQName(maksi::xml::qname(QString::fromWCharArray(KSI_SYM_PTR(val), KSI_SYM_LEN(val)), sym_ns));
    } else if (KSI_KEY_P(val)) {
        return MaksiElement::newQName(maksi::xml::qname(QString::fromWCharArray(KSI_KEY_PTR(val), KSI_KEY_LEN(val)), key_ns));
    } else if (KSI_BIGNUM_P(val)) {
        if (ksi_long_p(val)) {
            long v = ksi_num2long(val, "Ksi_MaksiContext::toElement");
            if (INT_MIN <= v && v <= INT_MAX)
                return MaksiElement::newInt(v);
            return MaksiElement::newLong(v);
        } else if (ksi_ulong_p(val)) {
            return MaksiElement::newULong(ksi_num2ulong(val, "Ksi_MaksiContext::toElement"));
        } else if (ksi_longlong_p(val)) {
            return MaksiElement::newLong(ksi_num2longlong(val, "Ksi_MaksiContext::toElement"));
        } else if (ksi_ulonglong_p(val)) {
            return MaksiElement::newULong(ksi_num2ulonglong(val, "Ksi_MaksiContext::toElement"));
        }
        return MaksiElement::newDouble(ksi_real_part(val));
    } else if (KSI_FLONUM_P(val)) {
        if (ksi_real_p(val)) {
            return MaksiElement::newDouble(ksi_real_part(val));
        }
    } else if (KSI_CHAR_P(val)) {
        wchar_t ch = KSI_CHAR_CODE(val);
        return MaksiElement::newString(QString::fromWCharArray(&ch, 1));
    } else if (KSI_STR_P(val)) {
        return MaksiElement::newString(QString::fromWCharArray(KSI_STR_PTR(val), KSI_STR_LEN(val)));
    } else if (KSI_BVEC_P(val)) {
        QByteArray ba(KSI_BVEC_PTR(val), KSI_BVEC_LEN(val));
        return MaksiElement::newByteArray(ba);
    } else if (KSI_LIST_P(val)) {
        QLinkedList<QVariant> ls;
        while (val != ksi_nil) {
            ls.append(Ksi_MaksiEngine::toVariant(KSI_CAR(val)));
            val = KSI_CDR(val);
        }
        return MaksiElement::newVariant(QVariant::fromValue(ls));
    } else if (KSI_VEC_P(val)) {
        QVector<QVariant> ls(KSI_VEC_LEN(val));
        for (unsigned int i = 0; i < KSI_VEC_LEN(val); ++i) {
            ls[i] = Ksi_MaksiEngine::toVariant(KSI_VEC_REF(val, i));
        }
        return MaksiElement::newVariant(QVariant::fromValue(ls));
    }

    //QString str = QString::fromWCharArray(ksi_obj2str(val));
    //ctx->m_maksi->sendMessage(MAKSI_MESSAGE_WARN, QString("cannot convert ksi_obj to MaksiElement: %1").arg(str));
    return MaksiElementPtr();
}

ksi_obj Ksi_MaksiContext::scriptContext(Ksi_MaksiContext *ctx, ksi_obj _name)
{
    QString name = QString::fromWCharArray(KSI_STR_PTR(_name), KSI_STR_LEN(_name));

    maksi::Context *sx = ctx->m_sxs.value(name);
    if (!sx) {
	sx = maksi::scriptContext(ctx->m_maksi, name);
        if (sx) {
            ctx->m_sxs.insert(name, sx);
            //qDebug("insert context class %s", qPrintable(name));
        }
    }

    if (sx) {
        ksi_obj av[1];
        av[0] = reinterpret_cast<ksi_obj>(sx);

        static ksi_prim contextPrim = ksi_new_prim(L"ScriptContextObject", (ksi_proc_t) scriptContextProc, KSI_CALL_ARG3, 1);
        ksi_obj val = ksi_close_proc((ksi_obj) contextPrim, 1, av);
        return val;
    }

    return ksi_void;
}

ksi_obj Ksi_MaksiContext::scriptContextProc(maksi::Context *ctx, ksi_obj _name, ksi_obj _val)
{
    if (ctx) {
        if (!_name) {
            QVariant val(ctx->eval());
            //qDebug("val %s", val ? qPrintable(val->elementText()) : "[null]");
            return Ksi_MaksiEngine::fromVariant(val);
        } else {
            QString name;
            if (KSI_STR_P(_name)) {
                name = QString::fromWCharArray(KSI_STR_PTR(_name), KSI_STR_LEN(_name));
            } else if (KSI_SYM_P(_name)) {
                name = QString::fromWCharArray(KSI_SYM_PTR(_name), KSI_SYM_LEN(_name));
            } else if (KSI_KEY_P(_name)) {
                name = QString::fromWCharArray(KSI_KEY_PTR(_name), KSI_KEY_LEN(_name));
            } else {
                //thisCtx->m_maksi->sendMessage(MAKSI_MESSAGE_ERROR, QString("invalid arg name: %1").arg(val->elementText()));
                ksi_exn_error(0, _name, "ScriptContext.arg: invalid arg name");
                return ksi_err;
            }

            if (_val) {
                ctx->setArg(name, Ksi_MaksiEngine::toVariant(_val));
            } else {
                QVariant val(ctx->arg(name));
                //qDebug("val %s", val ? qPrintable(val->elementText()) : "[null]");
                return Ksi_MaksiEngine::fromVariant(val);
            }
        }
    }
    return ksi_void;
}

};

 /* End of code */
