/* -*- mode:C++; tab-width:8 -*- */
/*
 *
 * Copyright (C) 2011, 2014, 2015, ivan demakov.
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code; see the file COPYING.LESSER.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 *
 */

/**
 * @file    maksi_ksi.h
 * @author  ivan demakov <ksion@users.sourceforge.net>
 * @date    Thu Feb  3 12:19:43 2011
 *
 * @brief   ksi engine
 *
 */

#ifndef MAKSI_KSI_H
#define MAKSI_KSI_H

#include <QObject>

#include <maksi/maksi.h>
#include <maksi/engine.h>
#include <maksi/script.h>
#include <maksi/elem.h>

#include <ksi/ksi.h>


namespace maksi
{

class Ksi_MaksiEngine;
class Ksi_MaksiContext;


class Ksi_EnginePlugin: public QObject, EnginePlugin
{
    Q_OBJECT;
    Q_INTERFACES(maksi::EnginePlugin);
    Q_PLUGIN_METADATA(IID MaksiEnginePlugin_IID FILE "maksi-ksi.json");

public:
    ~Ksi_EnginePlugin();

    virtual QString description() const override;
    virtual Engine *newEngine(Maksi *maksi, const QString &name) const override;
};


struct Ksi_MaksiArg
{
    Ksi_MaksiArg(const QString &name, const QVariant &val) : m_name(name), m_val(val) {}

    QString m_name;
    QVariant m_val;
};

struct Ksi_MaksiCode
{
    QString m_code;
    QByteArray m_file;
    int m_line;
};

struct Ksi_MaksiElement : public Ksi_EObj
{
    Ksi_MaksiElement();
    Ksi_MaksiElement(const MaksiElementPtr &val);
    ~Ksi_MaksiElement();

    void *operator new(size_t sz) { return ksi_malloc_data(sz); }
    void operator delete(void *p) { ksi_free(p); }

    static ksi_obj apply(struct Ksi_EObj *obj, int argc, ksi_obj *argv);
    static const wchar_t *print(struct Ksi_EObj *obj, int slashify);
    static void finalizer(void *obj, void *data);
    static bool is(ksi_obj x);

    MaksiElementPtr m_val;
};

struct Ksi_MaksiType : public Ksi_EObj
{
    Ksi_MaksiType();
    Ksi_MaksiType(Type type);
    ~Ksi_MaksiType();

    void *operator new(size_t sz) { return ksi_malloc_data(sz); }
    void operator delete(void *p) { ksi_free(p); }

    static ksi_obj apply(struct Ksi_EObj *x, int argc, ksi_obj *argv);
    static void finalizer(void *obj, void *data);

    Type m_type;
};


struct Maksi_Ksi_Context : public Ksi_Context
{
    Maksi_Ksi_Context(Ksi_MaksiEngine *engine) : m_engine(engine) {}

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    Ksi_MaksiEngine *m_engine;
};


class Ksi_MaksiEngineInternal : public EngineInternal
{
public:
    Ksi_MaksiEngineInternal() : m_env(0), m_ctx(0) {}
    ~Ksi_MaksiEngineInternal() { m_env = 0; m_ctx = 0; }

    ksi_env env() const { return m_env; }
    //Ksi_Context *ctx const { return m_ctx; }

private:
    ksi_env m_env;
    Ksi_Context *m_ctx;

    friend class Ksi_MaksiEngine;
};


class Ksi_MaksiEngine : public Engine
{
    Q_OBJECT

public:
    Ksi_MaksiEngine(Maksi *maksi, const QString &name);
    ~Ksi_MaksiEngine();

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    ksi_env env() const { return m_ksiInternal.env(); }

    virtual bool evalCode(const QString &code, const QUrl &url, int line, QVariant *val) override;
    virtual Script *newScript(const QString &scriptName) override;

    void sendMessage(message_t type, QString msg);

    static QVariant toVariant(ksi_obj val);
    static ksi_obj fromVariant(QVariant val);

private:
    static ksi_obj maksiType(Ksi_MaksiEngine *ksi, ksi_obj name, ksi_obj sname);
    static ksi_obj print(Ksi_MaksiEngine *ksi, ksi_obj type, int argc, ksi_obj *argv);
    static ksi_obj eval(ksi_context_t ksi, void *_data);
    static ksi_obj error(ksi_context_t ksi, ksi_obj tag, ksi_obj exn);
    static ksi_byte_port loader(const char *filename, void *data);
    static void errlog(ksi_context_t ctx, int pri, const wchar_t *msg);

    Maksi_Ksi_Context *m_ksi;
    Ksi_MaksiEngineInternal m_ksiInternal;

    friend class Ksi_MaksiScript;
    friend class Ksi_MaksiContext;
};


class Ksi_MaksiScript : public Script
{
public:
    Ksi_MaksiScript(Maksi *maksi, const QString &name, Ksi_MaksiEngine *engine);
    ~Ksi_MaksiScript();

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    virtual bool addArg(const QString &name, const QVariant &val, const MaksiElement *conf) override;
    virtual void addCode(const QString &code, const MaksiElement *conf) override;
    virtual Context *newContext() override;

private:
    Ksi_MaksiEngine *m_engine;
    QList<Ksi_MaksiArg> m_args;
    QList<Ksi_MaksiCode> m_code;
};


class Ksi_MaksiContext : public Context
{
public:
    Ksi_MaksiContext(Ksi_MaksiEngine *engine, QList<Ksi_MaksiArg> args, QList<Ksi_MaksiCode> code, Maksi *maksi);
    ~Ksi_MaksiContext();

    void *operator new(size_t sz) { return ksi_malloc_eternal(sz); }
    void operator delete(void *p) { ksi_free(p); }

    virtual QVariant arg(const QString &name) override;
    virtual void setArg(const QString &name, const QVariant &val) override;
    virtual QVariant eval() override;

    static ksi_obj fromElement(const MaksiElementPtr &val);
    static MaksiElementPtr toElement(ksi_obj val);

private:
    static ksi_obj evalProc(ksi_context_t ksi, void *_data);
    static ksi_obj scriptContext(Ksi_MaksiContext *ctx, ksi_obj name);
    static ksi_obj scriptContextProc(maksi::Context *ctx, ksi_obj name, ksi_obj val);

    ksi_obj m_compiled;
    ksi_env m_env;
    Maksi_Ksi_Context *m_ksi;

    QList<Ksi_MaksiCode> m_code;
    QMap<QString, maksi::Context*> m_sxs;
    Maksi *m_maksi;
};

};

#endif

/* End of file */
